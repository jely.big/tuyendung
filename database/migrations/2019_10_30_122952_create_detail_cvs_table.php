<?php
/**
 * Migration genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Dwij\Laraadmin\Models\Module;

class CreateDetailCvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Module::generate("Detail_cvs", 'detail_cvs', 'fullname', 'fa-cube', [
            ["bangcap1", "Bằng cấp 1", "TextField", false, "", 0, 256, false],
            ["bangcap2", "Bằng cấp 2", "TextField", false, "", 0, 256, false],
            ["tentruong1", "Tên trường 1", "TextField", false, "", 0, 256, false],
            ["tentruong2", "Tên trường 2", "TextField", false, "", 0, 256, false],
            ["khoanganh1", "Khoa/ngành 1", "TextField", false, "", 0, 256, false],
            ["khoanganh2", "Khoa/ngành 2", "TextField", false, "", 0, 256, false],
            ["tgdt1", "thời gian đào tạo 1", "TextField", false, "", 0, 256, false],
            ["tgdt2", "thời gian đào tạo 2", "TextField", false, "", 0, 256, false],
            ["khoahoc1", "Khóa học 1", "TextField", false, "", 0, 256, false],
            ["khoahoc2", "Khóa học 2", "TextField", false, "", 0, 256, false],
            [" dvtc1", "Đơn vị tổ chức 1", "TextField", false, "", 0, 256, false],
            ["dvtc2", "Đơn vị tổ chức 2", "TextField", false, "", 0, 256, false],
            ["cc1", "Chứng chỉ 1", "TextField", false, "", 0, 256, false],
            ["cc2", "Chứng chỉ 2", "TextField", false, "", 0, 256, false],
            ["tgkh1", "Thời gian khóa học 1", "TextField", false, "", 0, 256, false],
            ["tgkh2", "Thời gian khóa học 2", "TextField", false, "", 0, 256, false],
            ["word", "Kỹ năng Word", "TextField", false, "", 0, 256, true],
            ["excel", "Kỹ năng Excel", "TextField", false, "", 0, 256, true],
            ["powerpoint", "Kỹ năng Powerpoint", "TextField", false, "", 0, 256, true],
            ["listening", "Kỹ năng nghe", "TextField", false, "", 0, 256, true],
            ["speaking", "Kỹ năng nói ", "TextField", false, "", 0, 256, true],
            ["writing", "Kỹ năng viết", "TextField", false, "", 0, 256, true],
            ["skill_soft", "Kỹ năng mềm", "Textarea", false, "", 0, 0, false],
            ["from_date1", "Thời gian từ 1", "TextField", false, "", 0, 256, false],
            ["from_date2", "Thời gian từ 2", "TextField", false, "", 0, 256, false],
            ["from_date3", "Thời gian từ 3", "TextField", false, "", 0, 256, false],
            ["to_date1", "Thời gian đến 1", "TextField", false, "", 0, 256, false],
            ["to_date2", "Thời gian đến 2", "TextField", false, "", 0, 256, false],
            ["to_date3", "Thời gian đến 3", "TextField", false, "", 0, 256, false],
            ["company_1", "Công ty 1", "TextField", false, "", 0, 256, false],
            ["company_2", "Công ty 2", "TextField", false, "", 0, 256, false],
            ["company_3", "Công ty 3", "TextField", false, "", 0, 256, false],
            ["title_1", "Chức danh 1", "TextField", false, "", 0, 256, false],
            ["title_2", "Chức danh 2", "TextField", false, "", 0, 256, false],
            ["title_3", "Chức danh 3", "TextField", false, "", 0, 256, false],
            ["working_1", "Công việc 1", "TextField", false, "", 0, 256, false],
            ["working_2", "Công việc 2", "TextField", false, "", 0, 256, false],
            ["working_3", "Công việc 3", "TextField", false, "", 0, 256, false],
            ["salary_1", "Thu nhập 1", "TextField", false, "", 0, 256, false],
            ["salary_2", "Thu nhập 2", "TextField", false, "", 0, 256, false],
            ["salary_3", "Thu nhập 3", "TextField", false, "", 0, 256, false],
            ["fullname_1", "Họ và tên 1", "TextField", false, "", 0, 256, false],
            ["fullname_2", "Họ và tên 2", "TextField", false, "", 0, 256, false],
            ["fullname_3", "Họ và tên 3", "TextField", false, "", 0, 256, false],
            ["relation_1", "Quan hệ 1", "TextField", false, "", 0, 256, false],
            ["relation_2", "Quan hệ 2", "TextField", false, "", 0, 256, false],
            ["relation_3", "Quan hệ 3", "TextField", false, "", 0, 256, false],
            ["relation_title_1", "Quan hệ chức danh 1", "TextField", false, "", 0, 256, false],
            ["relation_title_2", "Quan hệ chức danh 2", "TextField", false, "", 0, 256, false],
            ["relation_title_3", "Quan hệ chức danh 3", "TextField", false, "", 0, 256, false],
            ["relation_company_1", "Quan hệ công ty 1", "TextField", false, "", 0, 256, false],
            ["relation_company_2", "Quan hệ công ty 2", "TextField", false, "", 0, 256, false],
            ["relation_company_3", "Quan hệ công ty 3", "TextField", false, "", 0, 256, false],
            ["relation_phone_1", "Quan hệ điện thoại 1", "Mobile", false, "", 0, 20, false],
            ["relation_phone_2", "Quan hệ điện thoại 2", "Mobile", false, "", 0, 20, false],
            ["relation_phone_3", "Quan hệ điện thoại 3", "Mobile", false, "", 0, 20, false],
            ["new_city", "Tỉnh/TP", "TextField", false, "", 0, 256, true],
            ["ditricst", "Quận/Huyện", "TextField", false, "", 0, 256, true],
            ["content", "Trắc nghiệm", "Textarea", false, "", 0, 0, false],
            ["candidate_position", "Vị trí ứng tuyển", "TextField", false, "", 0, 256, true],
            ["date_receive_job", "Ngày nhận việc", "Date", false, "", 0, 0, true],
            ["image", "Hình ảnh", "Ckfinder", false, "", 0, 0, true],
            ["fullname", "Họ tên", "TextField", false, "", 0, 256, true],
            ["birthday", "Ngày sinh", "Date", false, "", 0, 0, true],
            ["address_birthday", "Nơi sinh", "Textarea", false, "", 0, 0, true],
            ["marital_status", "Tình trạng hôn nhân", "TextField", false, "", 0, 256, true],
            ["number_children", "Số con", "Integer", false, "", 0, 11, true],
            ["mobile", "Điện thoại", "Mobile", false, "", 0, 20, true],
            ["email", "Email", "Email", false, "", 0, 256, true],
            ["address", "Địa chỉ tạm trú", "Textarea", false, "", 0, 0, true],
        ]);
		
		/*
		Row Format:
		["field_name_db", "Label", "UI Type", "Unique", "Default_Value", "min_length", "max_length", "Required", "Pop_values"]
        Module::generate("Module_Name", "Table_Name", "view_column_name" "Fields_Array");
        
		Module::generate("Books", 'books', 'name', [
            ["address",     "Address",      "Address",  false, "",          0,  1000,   true],
            ["restricted",  "Restricted",   "Checkbox", false, false,       0,  0,      false],
            ["price",       "Price",        "Currency", false, 0.0,         0,  0,      true],
            ["date_release", "Date of Release", "Date", false, "date('Y-m-d')", 0, 0,   false],
            ["time_started", "Start Time",  "Datetime", false, "date('Y-m-d H:i:s')", 0, 0, false],
            ["weight",      "Weight",       "Decimal",  false, 0.0,         0,  20,     true],
            ["publisher",   "Publisher",    "Dropdown", false, "Marvel",    0,  0,      false, ["Bloomsbury","Marvel","Universal"]],
            ["publisher",   "Publisher",    "Dropdown", false, 3,           0,  0,      false, "@publishers"],
            ["email",       "Email",        "Email",    false, "",          0,  0,      false],
            ["file",        "File",         "File",     false, "",          0,  1,      false],
            ["files",       "Files",        "Files",    false, "",          0,  10,     false],
            ["weight",      "Weight",       "Float",    false, 0.0,         0,  20.00,  true],
            ["biography",   "Biography",    "HTML",     false, "<p>This is description</p>", 0, 0, true],
            ["profile_image", "Profile Image", "Image", false, "img_path.jpg", 0, 250,  false],
            ["pages",       "Pages",        "Integer",  false, 0,           0,  5000,   false],
            ["mobile",      "Mobile",       "Mobile",   false, "+91  8888888888", 0, 20,false],
            ["media_type",  "Media Type",   "Multiselect", false, ["Audiobook"], 0, 0,  false, ["Print","Audiobook","E-book"]],
            ["media_type",  "Media Type",   "Multiselect", false, [2,3],    0,  0,      false, "@media_types"],
            ["name",        "Name",         "Name",     false, "John Doe",  5,  250,    true],
            ["password",    "Password",     "Password", false, "",          6,  250,    true],
            ["status",      "Status",       "Radio",    false, "Published", 0,  0,      false, ["Draft","Published","Unpublished"]],
            ["author",      "Author",       "String",   false, "JRR Tolkien", 0, 250,   true],
            ["genre",       "Genre",        "Taginput", false, ["Fantacy","Adventure"], 0, 0, false],
            ["description", "Description",  "Textarea", false, "",          0,  1000,   false],
            ["short_intro", "Introduction", "TextField",false, "",          5,  250,    true],
            ["website",     "Website",      "URL",      false, "http://dwij.in", 0, 0,  false],
        ]);
		*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('detail_cvs')) {
            Schema::drop('detail_cvs');
        }
    }
}
