-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2019 at 04:42 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tuyendung`
--

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

DROP TABLE IF EXISTS `backups`;
CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `seo_title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `slug`, `parent`, `description`, `status`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, NULL, '2019-10-25 06:50:35', '2019-10-25 06:50:35', 'Anzedo - Nơi làm việc lý tưởng', 'thong-tin/anzedo-noi-lam-viec-ly-tuong', 0, '', 1, '', '', ''),
(2, NULL, '2019-10-25 06:51:01', '2019-10-25 06:51:01', 'Đào tạo và phát triển', 'thong-tin/dao-tao-va-phat-trien', 0, '', 1, '', '', ''),
(3, NULL, '2019-10-25 06:51:10', '2019-10-25 06:51:10', 'Phúc lợi', 'thong-tin/phuc-loi', 0, '', 1, '', '', ''),
(4, NULL, '2019-10-25 06:51:20', '2019-10-25 06:51:20', 'Cách thức ứng tuyển', 'thong-tin/cach-thuc-ung-tuyen', 0, '', 1, '', '', ''),
(5, NULL, '2019-10-25 06:51:28', '2019-10-25 06:51:28', 'Liên hệ	', 'thong-tin/lien-he	', 0, '', 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fullname` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `id_blog` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 'Mod', '[\"Mod\"]', '#2703F1', NULL, '2019-04-07 06:44:40', '2019-04-07 06:44:40'),
(3, 'User', '[\"User\"]', '#00FF00', NULL, '2019-04-07 06:45:18', '2019-04-07 06:45:18'),
(4, 'Client', '[\"Client\"]', '#FFFF00', NULL, '2019-04-07 06:45:32', '2019-04-07 06:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `detail_cvs`
--

DROP TABLE IF EXISTS `detail_cvs`;
CREATE TABLE `detail_cvs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `candidate_position` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Vị trí ứng tuyển	',
  `date_receive_job` date DEFAULT '1970-01-01' COMMENT 'Ngày nhận việc	',
  `fullname` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Họ tên',
  `birthday` date DEFAULT '1970-01-01' COMMENT 'Ngày sinh	',
  `address_birthday` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nơi sinh	',
  `marital_status` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Tình trạng hôn nhân	',
  `number_children` int(10) UNSIGNED DEFAULT 0 COMMENT 'Số con',
  `mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Điện thoại',
  `email` varchar(256) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Email',
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Địa chỉ tạm trú',
  `image` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Hình ảnh	',
  `bangcap1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bangcap2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tentruong1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tentruong2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `khoanganh1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `khoanganh2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgdt1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgdt2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `khoahoc1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `khoahoc2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dvtc1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dvtc2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgkh1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgkh2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `word` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `excel` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `powerpoint` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `listening` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `speaking` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `writing` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `skill_soft` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_date1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_date2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_date3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_date1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_date2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_date3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `working_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `working_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `working_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_title_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_title_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_title_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_company_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_company_2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_company_3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_phone_1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_phone_2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_phone_3` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_city` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `ditricst` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diachis`
--

DROP TABLE IF EXISTS `diachis`;
CREATE TABLE `diachis` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `slug_url` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diachis`
--

INSERT INTO `diachis` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `parent`, `slug_url`) VALUES
(1, NULL, '2019-10-25 07:38:10', '2019-10-25 07:38:10', 'Hà Nội', 1, 'ha-noi'),
(2, NULL, '2019-10-25 07:38:16', '2019-10-25 07:40:51', 'Đà Nẵng', 2, 'da-nang'),
(3, NULL, '2019-10-25 07:38:26', '2019-10-25 07:40:55', 'Hồ Chí Minh', 3, 'ho-chi-minh');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Male',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL DEFAULT '1990-01-01',
  `date_hire` date NOT NULL,
  `date_left` date NOT NULL DEFAULT '1990-01-01',
  `salary_cur` decimal(15,3) NOT NULL DEFAULT 0.000,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `mobile`, `mobile2`, `email`, `dept`, `city`, `address`, `about`, `date_birth`, `date_hire`, `date_left`, `salary_cur`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Super Admin', 'Male', '8888888888', '', 'admin@localhost.com', 1, 'Pune', 'Karve nagar, Pune 411030', 'About user / biography', '2018-07-20', '2018-07-20', '2018-07-20', '0.000', NULL, '2018-07-19 18:51:21', '2018-07-19 18:51:21');

-- --------------------------------------------------------

--
-- Table structure for table `evaluates`
--

DROP TABLE IF EXISTS `evaluates`;
CREATE TABLE `evaluates` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `html` varchar(9999) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `khuvucs`
--

DROP TABLE IF EXISTS `khuvucs`;
CREATE TABLE `khuvucs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khuvucs`
--

INSERT INTO `khuvucs` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`) VALUES
(1, NULL, '2019-10-25 07:39:39', '2019-10-25 07:39:39', 'Miền Bắc'),
(2, NULL, '2019-10-25 07:39:44', '2019-10-25 07:39:44', 'Miền Trung'),
(3, NULL, '2019-10-25 07:39:48', '2019-10-25 07:39:48', 'Miền Nam');

-- --------------------------------------------------------

--
-- Table structure for table `la_configs`
--

DROP TABLE IF EXISTS `la_configs`;
CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_configs`
--

INSERT INTO `la_configs` (`id`, `key`, `section`, `value`, `created_at`, `updated_at`) VALUES
(1, 'sitename', '', 'TuyenDung-Anzedo', '2018-07-19 18:51:06', '2019-10-25 07:09:11'),
(2, 'sitename_part1', '', 'TuyenDung-Anzedo', '2018-07-19 18:51:06', '2019-10-25 07:09:11'),
(3, 'sitename_part2', '', '1.0', '2018-07-19 18:51:06', '2019-10-25 07:09:11'),
(4, 'sitename_short', '', 'AD', '2018-07-19 18:51:06', '2019-10-25 07:09:11'),
(5, 'site_description', '', 'Copy and complete by Jely Small', '2018-07-19 18:51:06', '2019-10-25 07:09:11'),
(6, 'sidebar_search', '', '0', '2018-07-19 18:51:06', '2019-10-25 07:09:12'),
(7, 'show_messages', '', '0', '2018-07-19 18:51:06', '2019-10-25 07:09:12'),
(8, 'show_notifications', '', '0', '2018-07-19 18:51:06', '2019-10-25 07:09:12'),
(9, 'show_tasks', '', '0', '2018-07-19 18:51:06', '2019-10-25 07:09:12'),
(10, 'show_rightsidebar', '', '0', '2018-07-19 18:51:06', '2019-10-25 07:09:12'),
(11, 'skin', '', 'skin-purple', '2018-07-19 18:51:06', '2019-10-25 07:09:11'),
(12, 'layout', '', 'layout-top-nav', '2018-07-19 18:51:06', '2019-10-25 07:09:11'),
(13, 'default_email', '', 'jely.big@gmail.com', '2018-07-19 18:51:06', '2019-10-25 07:09:11');

-- --------------------------------------------------------

--
-- Table structure for table `la_menus`
--

DROP TABLE IF EXISTS `la_menus`;
CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(1, 'Team', '#', 'fa-group', 'custom', 71, 3, '2018-07-19 18:51:06', '2019-02-19 00:41:55'),
(2, 'Users', 'users', 'fa-group', 'module', 1, 1, '2018-07-19 18:51:06', '2018-07-22 03:54:58'),
(6, 'Roles', 'roles', 'fa-user-plus', 'module', 1, 2, '2018-07-19 18:51:06', '2018-07-22 03:54:58'),
(8, 'Permissions', 'permissions', 'fa-magic', 'module', 1, 3, '2018-07-19 18:51:06', '2018-07-22 03:54:58'),
(22, 'Đa phương tiện', 'media-management', 'fa-file-image-o', 'custom', 32, 4, '2018-09-02 00:39:19', '2019-11-01 07:29:11'),
(27, 'Settings', 'settings', 'fa fa-cog', 'module', 71, 2, '2018-09-15 02:02:07', '2019-02-19 00:41:53'),
(29, 'Menugroups', 'menugroups', 'fa fa-film', 'module', 32, 2, '2018-09-24 01:03:10', '2019-11-01 07:29:11'),
(31, 'Menulists', 'menulists', 'fa fa-cube', 'module', 32, 3, '2018-09-24 01:38:57', '2019-11-01 07:29:11'),
(32, 'Menu', '#', 'fa-align-justify', 'custom', 0, 3, '2018-09-25 20:23:16', '2019-11-01 07:13:21'),
(33, 'Widget', '#', 'fa-cube', 'custom', 71, 1, '2018-09-28 18:35:55', '2019-02-19 00:41:51'),
(40, 'Ảnh Slider', 'sliderimages', 'fa-cube', 'custom', 33, 1, '2018-11-21 19:00:29', '2018-11-21 19:00:59'),
(41, 'Sliders', 'sliders', 'fa-cube', 'custom', 33, 2, '2018-11-21 19:00:49', '2018-11-21 19:01:34'),
(43, 'Danh mục sản phẩm', 'productcategories', 'fa-align-justify', 'custom', 54, 1, '2018-11-26 21:03:02', '2019-02-27 02:38:17'),
(44, 'Tin liên hệ', 'contacts', 'fa-commenting', 'custom', 53, 1, '2018-11-26 21:03:33', '2019-01-01 01:23:50'),
(45, 'Sản phẩm', 'products', 'fa-cube', 'custom', 54, 3, '2018-11-26 23:53:24', '2019-02-27 02:38:18'),
(46, 'Đơn hàng', 'orders', 'fa-cube', 'custom', 54, 5, '2018-11-28 21:23:11', '2019-02-27 02:38:20'),
(51, 'Cầu hình', 'settings', 'fa-cog', 'custom', 27, 1, '2018-12-05 21:27:55', '2018-12-05 21:28:08'),
(56, 'Mail nhận tin tức', 'mail_posts', 'fa-paper-plane', 'custom', 53, 2, '2019-01-01 01:28:57', '2019-01-01 01:29:02'),
(70, 'Thương hiệu', 'product_labels', 'fa-firefox', 'custom', 54, 2, '2019-02-17 18:59:26', '2019-02-27 02:38:18'),
(76, 'Quản lý', '#', 'fa-cube', 'custom', 0, 2, '2019-02-19 00:43:44', '2019-11-01 07:13:21'),
(77, 'Tài khoản', 'users', 'fa-cube', 'custom', 0, 4, '2019-02-19 00:44:39', '2019-11-01 07:13:21'),
(79, 'Cài đặt', 'settings', 'fa-bank', 'custom', 0, 5, '2019-02-26 20:00:03', '2019-11-01 07:13:21'),
(82, 'Key Slide', 'sliders', 'fa-cube', 'custom', 32, 5, '2019-02-27 02:23:07', '2019-11-01 07:29:11'),
(83, 'Ảnh slide', 'sliderimages', 'fa-cube', 'custom', 32, 1, '2019-02-27 02:23:22', '2019-11-01 07:29:11'),
(84, 'Mã giảm giá', 'coupons', 'fa-cube', 'custom', 54, 4, '2019-02-27 02:38:06', '2019-02-27 02:38:20'),
(94, 'Việc làm theo địa chỉ', 'diachis', 'fa-cube', 'custom', 76, 3, '2019-10-25 07:35:27', '2019-10-25 07:37:04'),
(95, 'Việc làm theo ngành nghề', 'nganhnghes', 'fa-cube', 'custom', 76, 2, '2019-10-25 07:35:49', '2019-10-25 07:37:04'),
(96, 'Bài viết theo menu', 'posts', 'fa-cube', 'custom', 76, 1, '2019-10-25 07:36:57', '2019-10-25 07:37:04'),
(102, 'Tuyển dụng', '#', 'fa-cube', 'custom', 0, 1, '2019-10-28 05:14:08', '2019-11-01 07:13:21'),
(103, 'Tuyển dụng', 'tuyendungs', 'fa-cube', 'custom', 102, 2, '2019-10-28 05:14:30', '2019-10-31 09:01:38'),
(104, 'Câu hỏi mục V', 'tracnghiems', 'fa-cube', 'custom', 102, 3, '2019-10-28 05:15:06', '2019-10-31 09:01:38'),
(106, 'CV', 'detail_cvs', 'fa-cube', 'custom', 102, 1, '2019-10-31 09:01:34', '2019-10-31 09:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `mail_posts`
--

DROP TABLE IF EXISTS `mail_posts`;
CREATE TABLE `mail_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menugroups`
--

DROP TABLE IF EXISTS `menugroups`;
CREATE TABLE `menugroups` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `key` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menugroups`
--

INSERT INTO `menugroups` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `key`, `status`, `description`) VALUES
(1, NULL, '2018-09-25 20:22:11', '2018-11-19 19:41:42', 'Main Menu Vietnamese', 'main_menu', 1, 'Mo ta '),
(4, NULL, '2018-10-04 04:08:31', '2018-10-04 04:08:31', 'Footer Link Vietnamese', 'footer_link_vi', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `menulists`
--

DROP TABLE IF EXISTS `menulists`;
CREATE TABLE `menulists` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `menugroup` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `icon` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menulists`
--

INSERT INTO `menulists` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `url`, `menugroup`, `parent`, `icon`, `status`, `sort`) VALUES
(1, NULL, '2019-10-25 06:32:52', '2019-10-25 06:32:52', 'Trang chủ', '/', 1, 0, '', 1, 1),
(2, NULL, '2019-10-25 06:46:00', '2019-10-25 06:46:00', 'Về chúng tôi', '', 1, 0, '', 1, 2),
(3, NULL, '2019-10-25 06:47:15', '2019-10-25 06:47:15', 'Cách thức ứng tuyển', 'thong-tin/cach-thuc-ung-tuyen', 1, 0, '', 1, 3),
(4, NULL, '2019-10-25 06:47:25', '2019-10-25 06:47:25', 'Liên hệ', 'thong-tin/lien-he', 1, 0, '', 1, 4),
(5, NULL, '2019-10-25 06:47:45', '2019-10-25 06:47:45', 'Anzedo - Nơi làm việc lý tưởng', 'thong-tin/anzedo-noi-lam-viec-ly-tuong', 1, 2, '', 1, 1),
(6, NULL, '2019-10-25 06:47:57', '2019-10-25 06:47:57', 'Đào tạo và phát triển', 'thong-tin/dao-tao-va-phat-trien', 1, 2, '', 1, 2),
(7, NULL, '2019-10-25 06:48:08', '2019-10-25 06:48:08', 'Phúc lợi', 'thong-tin/phuc-loi', 1, 2, '', 1, 3),
(8, NULL, '2019-10-25 06:48:32', '2019-10-25 06:48:32', 'Hệ thống cửa hàng', 'http://anzedo.net/cua-hang', 1, 2, '', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2018-07-19 18:50:54', '2019-02-18 18:34:39'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2018-07-19 18:50:55', '2018-07-19 18:51:06'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2018-07-19 18:50:56', '2018-07-19 18:51:06'),
(4, 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', 1, '2018-07-19 18:50:56', '2018-07-19 18:51:06'),
(5, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2018-07-19 18:50:57', '2018-07-19 18:51:06'),
(6, 'Organizations', 'Organizations', 'organizations', 'name', 'Organization', 'OrganizationsController', 'fa-university', 1, '2018-07-19 18:50:59', '2018-07-19 18:51:06'),
(7, 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', 1, '2018-07-19 18:51:00', '2018-07-19 18:51:06'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2018-07-19 18:51:01', '2018-07-19 18:51:06'),
(24, 'Languages', 'Languages', 'languages', 'name', 'Language', 'LanguagesController', 'fa-language', 1, '2018-09-02 01:11:04', '2018-09-02 01:14:24'),
(26, 'Categories', 'Categories', 'categories', 'name', 'Category', 'CategoriesController', 'fa-bars', 1, '2018-09-06 14:41:20', '2018-09-06 14:45:06'),
(27, 'Posts', 'Posts', 'posts', 'title', 'Post', 'PostsController', 'fa-file-powerpoint-o', 1, '2018-09-08 20:39:16', '2018-09-08 22:40:09'),
(28, 'Settings', 'Settings', 'settings', 'key', 'Setting', 'SettingsController', 'fa-cog', 1, '2018-09-15 02:01:13', '2018-09-15 02:02:07'),
(34, 'Menugroups', 'Menugroups', 'menugroups', 'name', 'Menugroup', 'MenugroupsController', 'fa-film', 1, '2018-09-24 00:58:19', '2018-09-24 01:03:10'),
(38, 'Menulists', 'Menulists', 'menulists', 'name', 'Menulist', 'MenulistsController', 'fa-cube', 1, '2018-09-24 01:38:40', '2018-09-24 01:38:57'),
(39, 'Pages', 'Pages', 'pages', 'title', 'Page', 'PagesController', 'fa-cube', 1, '2018-10-04 02:28:07', '2018-10-04 02:37:46'),
(42, 'Contacts', 'Contacts', 'contacts', 'name', 'Contact', 'ContactsController', 'fa-comment-o', 1, '2018-11-19 18:23:59', '2018-11-19 18:26:38'),
(43, 'Sliders', 'Sliders', 'sliders', 'key', 'Slider', 'SlidersController', 'fa-cube', 1, '2018-11-21 18:53:02', '2018-11-21 18:55:24'),
(44, 'SliderImages', 'SliderImages', 'sliderimages', 'key', 'SliderImage', 'SliderImagesController', 'fa-cube', 1, '2018-11-21 18:57:50', '2018-11-21 18:59:49'),
(56, 'Mail_posts', 'Mail_posts', 'mail_posts', 'email', 'Mail_post', 'Mail_postsController', 'fa-send-o', 1, '2019-01-01 01:22:48', '2019-01-01 01:23:40'),
(61, 'Partners', 'Partners', 'partners', 'title', 'Partner', 'PartnersController', 'fa-adn', 1, '2019-02-14 19:00:33', '2019-02-14 19:03:04'),
(65, 'Product_labels', 'Product_labels', 'product_labels', 'name', 'Product_label', 'Product_labelsController', 'fa-circle-o', 1, '2019-02-17 18:55:11', '2019-02-17 18:57:04'),
(67, 'Comments', 'Comments', 'comments', 'fullname', 'Comment', 'CommentsController', 'fa-certificate', 1, '2019-03-26 06:37:56', '2019-03-26 06:41:22'),
(68, 'Re_Comments', 'Re_Comments', 're_comments', 'cmt_id', 'Re_Comment', 'Re_CommentsController', 'fa-bug', 1, '2019-03-30 07:44:44', '2019-03-30 07:47:20'),
(69, 'Diachis', 'Diachis', 'diachis', 'name', 'Diachi', 'DiachisController', 'fa-cube', 1, '2019-10-25 07:33:37', '2019-10-25 07:33:59'),
(70, 'Nganhnghes', 'Nganhnghes', 'nganhnghes', 'name', 'Nganhnghe', 'NganhnghesController', 'fa-cube', 1, '2019-10-25 07:34:16', '2019-10-25 07:34:45'),
(71, 'Khuvucs', 'Khuvucs', 'khuvucs', 'name', 'Khuvuc', 'KhuvucsController', 'fa-cube', 1, '2019-10-25 07:39:18', '2019-10-25 07:39:30'),
(72, 'Tuyendungs', 'Tuyendungs', 'tuyendungs', 'name', 'Tuyendung', 'TuyendungsController', 'fa-cube', 1, '2019-10-25 07:41:56', '2019-10-25 07:49:28'),
(73, 'Tracnghiems', 'Tracnghiems', 'tracnghiems', 'question', 'Tracnghiem', 'TracnghiemsController', 'fa-cube', 1, '2019-10-26 18:14:38', '2019-10-26 18:33:21'),
(74, 'Tracnghiem_Options', 'Tracnghiem_Options', 'tracnghiem_options', 'id_tracnghiem', 'Tracnghiem_Option', 'Tracnghiem_OptionsController', 'fa-cube', 1, '2019-10-26 18:15:06', '2019-10-26 18:48:46'),
(76, 'Tracnghiem_Types', 'Tracnghiem_Types', 'tracnghiem_types', 'type_field', 'Tracnghiem_Type', 'Tracnghiem_TypesController', 'fa-cube', 1, '2019-10-26 18:30:33', '2019-10-26 18:30:53'),
(77, 'Detail_CVs', 'Detail_CVs', 'detail_cvs', 'fullname', 'Detail_CV', 'Detail_CVsController', 'fa-cube', 1, '2019-10-30 04:37:59', '2019-10-30 05:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

DROP TABLE IF EXISTS `module_fields`;
CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT 0,
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(5, 'type', 'User Type', 1, 7, 0, 'Employee', 0, 0, 0, '[\"Employee\",\"Client\"]', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(6, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(7, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(8, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(9, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(10, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(11, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(12, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(13, 'name', 'Name', 3, 16, 1, '', 1, 250, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(14, 'tags', 'Tags', 3, 20, 0, '[]', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(15, 'color', 'Color', 3, 19, 0, '', 0, 50, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(16, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(17, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(18, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(19, 'mobile', 'Mobile', 4, 14, 0, '', 10, 20, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(20, 'mobile2', 'Alternative Mobile', 4, 14, 0, '', 10, 20, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(21, 'email', 'Email', 4, 8, 1, '', 5, 250, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(22, 'dept', 'Department', 4, 7, 0, '0', 0, 0, 1, '@departments', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(23, 'city', 'City', 4, 19, 0, '', 0, 50, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(24, 'address', 'Address', 4, 1, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(27, 'date_hire', 'Hiring Date', 4, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(28, 'date_left', 'Resignation Date', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(29, 'salary_cur', 'Current Salary', 4, 6, 0, '0.0', 0, 2, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(30, 'name', 'Name', 5, 16, 1, '', 1, 250, 1, '', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(31, 'display_name', 'Display Name', 5, 19, 0, '', 0, 250, 1, '', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(32, 'description', 'Description', 5, 21, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(33, 'parent', 'Parent Role', 5, 7, 0, '1', 0, 0, 0, '@roles', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(34, 'dept', 'Department', 5, 7, 0, '1', 0, 0, 0, '@departments', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(35, 'name', 'Name', 6, 16, 1, '', 5, 250, 1, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(36, 'email', 'Email', 6, 8, 1, '', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(37, 'phone', 'Phone', 6, 14, 0, '', 0, 20, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(38, 'website', 'Website', 6, 23, 0, 'http://', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(39, 'assigned_to', 'Assigned to', 6, 7, 0, '0', 0, 0, 0, '@employees', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(40, 'connect_since', 'Connected Since', 6, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(41, 'address', 'Address', 6, 1, 0, '', 0, 1000, 1, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(42, 'city', 'City', 6, 19, 0, '', 0, 250, 1, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(43, 'description', 'Description', 6, 21, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(44, 'profile_image', 'Profile Image', 6, 12, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(45, 'profile', 'Company Profile', 6, 9, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(46, 'name', 'Name', 7, 16, 1, '', 0, 250, 1, '', 0, '2018-07-19 18:51:00', '2018-07-19 18:51:00'),
(47, 'file_name', 'File Name', 7, 19, 1, '', 0, 250, 1, '', 0, '2018-07-19 18:51:00', '2018-07-19 18:51:00'),
(48, 'backup_size', 'File Size', 7, 19, 0, '0', 0, 10, 1, '', 0, '2018-07-19 18:51:00', '2018-07-19 18:51:00'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, '2018-07-19 18:51:01', '2018-07-19 18:51:01'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, '2018-07-19 18:51:01', '2018-07-19 18:51:01'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:51:01', '2018-07-19 18:51:01'),
(108, 'name', 'Name', 26, 22, 0, '', 0, 256, 1, '', 0, '2018-09-06 14:42:11', '2018-11-26 20:53:30'),
(109, 'slug', 'Slug', 26, 22, 1, '', 0, 256, 1, '', 0, '2018-09-06 14:42:26', '2018-09-09 18:32:28'),
(110, 'parent', 'Parent name', 26, 7, 0, '0', 0, 0, 0, '@categories', 0, '2018-09-06 14:43:10', '2019-03-18 05:53:52'),
(111, 'description', 'Description', 26, 21, 0, '', 0, 0, 0, '', 0, '2018-09-06 14:43:31', '2018-09-06 14:43:31'),
(112, 'status', 'Status', 26, 2, 0, '1', 0, 0, 0, '', 0, '2018-09-06 14:43:47', '2018-09-06 14:43:47'),
(113, 'seo_title', 'SEO Title', 26, 22, 0, '', 0, 256, 0, '', 0, '2018-09-06 14:44:05', '2018-09-06 14:44:05'),
(114, 'seo_description', 'SEO Description', 26, 21, 0, '', 0, 0, 0, '', 0, '2018-09-06 14:44:24', '2018-09-06 14:44:24'),
(115, 'seo_keywords', 'SEO Keywords', 26, 21, 0, '', 0, 0, 0, '', 0, '2018-09-06 14:44:54', '2018-09-06 14:44:54'),
(116, 'lang', 'Language', 27, 7, 0, '', 0, 0, 1, '@languages', 0, '2018-09-08 22:33:13', '2018-09-08 22:33:13'),
(117, 'title', 'Title', 27, 22, 0, '', 0, 256, 1, '', 0, '2018-09-08 22:35:12', '2018-09-08 22:35:12'),
(118, 'slug', 'Slug', 27, 22, 1, '', 0, 256, 1, '', 0, '2018-09-08 22:35:25', '2018-09-08 22:35:25'),
(119, 'content', 'Content', 27, 26, 0, '', 0, 0, 0, '', 0, '2018-09-08 22:35:41', '2018-09-08 22:35:41'),
(120, 'categories', 'Categories', 27, 15, 0, '', 0, 0, 1, '@categories', 0, '2018-09-08 22:36:14', '2019-10-25 06:52:41'),
(121, 'thumbnail', 'Thumbnail', 27, 25, 0, '', 0, 0, 0, '', 0, '2018-09-08 22:36:39', '2018-09-08 22:36:39'),
(122, 'author', 'Author', 27, 7, 0, '', 0, 0, 0, '@users', 0, '2018-09-08 22:37:12', '2018-09-08 22:37:12'),
(123, 'seo_title', 'Seo Title', 27, 22, 0, '', 0, 256, 0, '', 0, '2018-09-08 22:38:25', '2018-09-08 22:38:25'),
(124, 'seo_description', 'Seo Description', 27, 21, 0, '', 0, 256, 0, '', 0, '2018-09-08 22:38:38', '2018-09-08 22:39:17'),
(125, 'seo_keywords', 'Seo Keywords', 27, 21, 0, '', 0, 0, 0, '', 0, '2018-09-08 22:39:09', '2018-09-08 22:39:09'),
(126, 'tags', 'Tags', 27, 20, 0, '', 0, 256, 0, 'null', 0, '2018-09-08 22:39:59', '2018-09-08 22:39:59'),
(127, 'status', 'Status', 27, 2, 0, '1', 0, 0, 1, '', 0, '2018-09-08 22:46:22', '2018-09-08 22:46:22'),
(128, 'key', 'Key', 28, 22, 1, '', 0, 256, 1, '', 0, '2018-09-15 02:01:45', '2018-09-15 02:01:45'),
(129, 'value', 'Value', 28, 22, 0, '', 0, 256, 1, '', 0, '2018-09-15 02:02:04', '2018-09-15 02:02:04'),
(149, 'name', 'Name', 34, 22, 0, '', 0, 256, 1, '', 0, '2018-09-24 00:58:57', '2018-09-24 00:58:57'),
(150, 'key', 'Menu key', 34, 22, 1, '', 0, 256, 1, '', 0, '2018-09-24 00:59:22', '2018-09-24 00:59:22'),
(152, 'status', 'Status', 34, 2, 0, '', 0, 0, 1, '', 0, '2018-09-24 01:02:33', '2018-09-24 01:02:33'),
(153, 'description', 'Descriptions', 34, 21, 0, '', 0, 0, 0, '', 0, '2018-09-24 01:03:04', '2018-09-24 01:03:04'),
(160, 'name', 'Name', 38, 22, 0, '', 0, 256, 1, '', 0, '2018-09-24 01:38:53', '2018-09-24 01:38:53'),
(161, 'url', 'Url', 38, 22, 0, '', 0, 256, 0, '', 0, '2018-09-24 01:39:27', '2018-09-24 01:39:27'),
(162, 'menugroup', 'Menu Group', 38, 7, 0, '', 0, 0, 1, '@menugroups', 0, '2018-09-24 01:39:42', '2018-09-24 01:39:42'),
(163, 'parent', 'Parent', 38, 7, 0, '', 0, 0, 0, '@menulists', 0, '2018-09-24 01:40:02', '2018-09-24 01:40:02'),
(164, 'icon', 'Icon', 38, 22, 0, '', 0, 256, 0, '', 0, '2018-09-24 01:40:15', '2018-09-24 01:40:15'),
(165, 'status', 'Status', 38, 2, 0, '1', 0, 0, 1, '', 0, '2018-09-24 01:40:38', '2018-09-24 01:40:38'),
(166, 'sort', 'Sort', 38, 13, 0, '0', 0, 11, 1, '', 0, '2018-09-24 01:41:07', '2018-09-24 01:41:07'),
(167, 'title', 'Title', 39, 22, 0, '', 0, 256, 1, '', 0, '2018-10-04 02:28:32', '2018-10-04 02:28:32'),
(168, 'slug', 'Slug Url', 39, 22, 0, '', 0, 256, 1, '', 0, '2018-10-04 02:28:49', '2018-10-04 02:28:49'),
(170, 'contetn', 'Content', 39, 26, 0, '', 0, 0, 0, '', 0, '2018-10-04 02:29:26', '2018-10-04 02:29:26'),
(171, 'seo_title', 'Seo Title', 39, 22, 0, '', 0, 256, 0, '', 0, '2018-10-04 02:37:05', '2018-10-04 02:37:05'),
(172, 'seo_description', 'Seo Description', 39, 21, 0, '', 0, 0, 0, '', 0, '2018-10-04 02:37:24', '2018-10-04 02:37:24'),
(173, 'seo_keywords', 'Seo Keywords', 39, 21, 0, '', 0, 0, 0, '', 0, '2018-10-04 02:37:39', '2018-10-04 02:37:39'),
(176, 'name', 'Họ và tên', 42, 22, 0, '', 0, 256, 1, '', 0, '2018-11-19 18:24:29', '2018-11-19 18:24:29'),
(177, 'email', 'Email', 42, 8, 0, '', 0, 256, 1, '', 0, '2018-11-19 18:25:23', '2018-11-19 18:25:23'),
(178, 'phone', 'Số điện thoại', 42, 22, 0, '', 0, 256, 0, '', 0, '2018-11-19 18:25:43', '2018-11-19 18:25:43'),
(179, 'content', 'Nội dung', 42, 22, 0, '', 0, 256, 0, '', 0, '2018-11-19 18:26:10', '2018-11-19 18:26:10'),
(180, 'status', 'Trạng thái', 42, 2, 0, '0', 0, 0, 0, '', 0, '2018-11-19 18:26:34', '2018-11-19 18:26:34'),
(181, 'key', 'key', 43, 22, 1, '', 0, 256, 1, '', 0, '2018-11-21 18:53:50', '2018-11-21 18:53:50'),
(185, 'status', 'Trạng thái', 43, 2, 0, '1', 0, 0, 0, '', 0, '2018-11-21 18:57:33', '2018-11-21 18:57:33'),
(186, 'key', 'Key', 44, 7, 0, '', 0, 0, 1, '@sliders', 0, '2018-11-21 18:58:07', '2018-11-21 18:58:07'),
(187, 'image', 'Hình ảnh', 44, 25, 0, '', 0, 0, 1, '', 0, '2018-11-21 18:58:27', '2019-03-01 22:14:32'),
(188, 'title', 'Tiêu đề', 44, 22, 0, '', 0, 256, 1, '', 0, '2018-11-21 18:58:44', '2018-11-21 18:58:44'),
(189, 'content', 'Mô tả', 44, 21, 0, '', 0, 0, 0, '', 0, '2018-11-21 18:59:18', '2018-11-21 19:58:05'),
(190, 'html', 'Html Content', 44, 21, 0, '', 0, 0, 0, '', 0, '2018-11-21 18:59:36', '2018-11-21 19:57:51'),
(252, 'email', 'Email', 56, 22, 0, '', 0, 256, 1, '', 0, '2019-01-01 01:22:58', '2019-01-01 01:22:58'),
(271, 'title', 'Tiêu đề', 61, 22, 0, '', 0, 256, 0, '', 0, '2019-02-14 19:00:44', '2019-02-14 19:05:43'),
(272, 'image', 'Hình ảnh', 61, 25, 0, '', 0, 0, 1, '', 0, '2019-02-14 19:01:50', '2019-02-14 19:06:06'),
(286, 'name', 'Tên nhãn', 65, 22, 0, '', 0, 256, 0, '', 0, '2019-02-17 18:55:23', '2019-02-17 18:56:23'),
(287, 'key', 'Key nhãn', 65, 22, 0, '', 0, 256, 0, '', 0, '2019-02-17 18:56:39', '2019-02-17 18:56:39'),
(289, 'content', 'Tự sự về bản thân', 1, 21, 0, '', 0, 0, 0, '', 0, '2019-03-21 08:26:03', '2019-03-21 08:26:03'),
(290, 'image', 'Hình ảnh', 1, 25, 0, '', 0, 0, 0, '', 0, '2019-03-21 08:26:19', '2019-03-21 08:26:19'),
(291, 'facebook', 'Facebook', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:27:05', '2019-03-21 08:27:05'),
(292, 'twitter', 'Twitter', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:29:37', '2019-03-21 08:29:37'),
(293, 'google_plus', 'Google_plus', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:29:49', '2019-03-21 08:29:49'),
(294, 'instagram', 'Instagram', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:29:58', '2019-03-21 08:29:58'),
(295, 'status', 'Trạng thái', 1, 13, 0, '1', 0, 11, 0, '', 0, '2019-03-21 08:30:46', '2019-03-21 08:30:46'),
(299, 'fullname', 'Họ và tên', 67, 22, 0, '', 0, 256, 0, '', 3, '2019-03-26 06:38:13', '2019-03-26 06:38:13'),
(300, 'email', 'Email', 67, 8, 0, '', 0, 256, 0, '', 4, '2019-03-26 06:38:38', '2019-03-26 06:38:38'),
(301, 'phone', 'Số điện thoại', 67, 22, 0, '', 0, 256, 0, '', 5, '2019-03-26 06:38:53', '2019-03-26 06:38:53'),
(302, 'content', 'Nội dung bình luận', 67, 21, 0, '', 0, 256, 0, '', 6, '2019-03-26 06:39:10', '2019-03-30 07:47:40'),
(303, 'id_blog', 'Mã bài viết', 67, 22, 0, '', 0, 256, 0, '', 1, '2019-03-26 06:40:37', '2019-03-27 07:32:44'),
(304, 'id_user', 'Mã người dùng', 67, 22, 0, '', 0, 256, 0, '', 2, '2019-03-26 06:40:55', '2019-03-27 07:32:56'),
(305, 'cmt_id', 'Mã comment', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:45:04', '2019-03-30 07:45:32'),
(306, 'blog_id', 'Mã bài viết', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:45:25', '2019-03-30 07:45:25'),
(307, 'full_name', 'Họ và tên', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:45:59', '2019-03-30 07:45:59'),
(308, 'phone_number', 'Số điện thoại', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:46:17', '2019-03-30 07:46:17'),
(309, 'content_re', 'Nội dung', 68, 21, 0, '', 0, 0, 0, '', 0, '2019-03-30 07:46:36', '2019-03-30 07:46:36'),
(310, 'user_id', 'Mã người dùng', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:47:13', '2019-03-30 07:47:13'),
(311, 'status', 'Trạng thái', 67, 2, 0, '1', 0, 0, 0, '', 7, '2019-03-30 07:48:40', '2019-03-30 08:02:43'),
(312, 'status', 'Trạng thái', 68, 2, 0, '1', 0, 0, 0, '', 0, '2019-03-30 07:50:36', '2019-03-30 08:02:28'),
(313, 'email_re', 'Email', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 09:46:53', '2019-03-30 09:46:53'),
(314, 'phone', 'Số điện thoại', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-31 07:31:55', '2019-03-31 07:31:55'),
(315, 'name', 'Tên', 69, 22, 0, '', 0, 256, 1, '', 1, '2019-10-25 07:33:50', '2019-10-25 07:33:50'),
(316, 'name', 'Tên', 70, 22, 0, '', 0, 256, 1, '', 0, '2019-10-25 07:34:40', '2019-10-25 07:34:40'),
(317, 'name', 'Tên', 71, 22, 0, '', 0, 256, 0, '', 0, '2019-10-25 07:39:27', '2019-10-25 07:39:27'),
(318, 'parent', 'Miền', 69, 7, 0, '', 0, 0, 0, '@khuvucs', 3, '2019-10-25 07:40:21', '2019-10-25 07:40:21'),
(319, 'name', 'Tên BP (Chức danh)', 72, 22, 0, '', 0, 256, 1, '', 0, '2019-10-25 07:42:26', '2019-10-25 07:42:42'),
(320, 'slug', 'Slug', 72, 22, 0, '', 0, 256, 1, '', 0, '2019-10-25 07:42:52', '2019-10-25 07:42:52'),
(322, 'address', 'Nơi làm việc', 72, 15, 0, '', 0, 256, 0, '@diachis', 0, '2019-10-25 07:43:49', '2019-11-04 07:18:46'),
(323, 'rank', 'Cấp bậc', 72, 22, 0, '', 0, 256, 0, '', 0, '2019-10-25 07:44:36', '2019-10-25 07:44:36'),
(324, 'salary', 'Mức lương', 72, 22, 0, '', 0, 256, 0, '', 0, '2019-10-25 07:45:08', '2019-10-25 07:53:17'),
(325, 'career', 'Ngành nghề', 72, 15, 0, '', 0, 256, 0, '@nganhnghes', 0, '2019-10-25 07:46:45', '2019-11-04 06:06:25'),
(326, 'department', 'Phòng ban', 72, 22, 0, '', 0, 256, 0, '', 0, '2019-10-25 07:47:08', '2019-10-25 07:47:08'),
(327, 'test_day', 'Thời gian thử việc', 72, 13, 0, '', 0, 11, 0, '', 0, '2019-10-25 07:47:44', '2019-10-25 07:47:44'),
(328, 'form', 'Hình thức làm việc	', 72, 22, 0, '', 0, 256, 0, '', 0, '2019-10-25 07:48:05', '2019-10-25 07:48:05'),
(329, 'degree', 'Yêu cầu bằng cấp', 72, 22, 0, '', 0, 256, 0, '', 0, '2019-10-25 07:48:21', '2019-10-25 07:48:21'),
(330, 'deadline', 'Hạn nộp hồ sơ', 72, 4, 0, '', 0, 0, 0, '', 0, '2019-10-25 07:48:59', '2019-10-25 07:48:59'),
(331, 'content', 'Mô tả công việc', 72, 26, 0, '', 0, 0, 0, '', 0, '2019-10-25 07:49:15', '2019-10-25 07:49:15'),
(332, 'slug_url', 'Slug-name', 69, 22, 0, '', 0, 256, 0, '', 2, '2019-10-26 02:49:17', '2019-10-26 02:49:17'),
(333, 'slug_nganh_name', 'Slug-Name', 70, 22, 0, '', 0, 256, 0, '', 0, '2019-10-26 02:51:49', '2019-10-26 02:53:45'),
(334, 'question', 'Câu hỏi', 73, 21, 0, '', 0, 0, 1, '', 0, '2019-10-26 18:23:06', '2019-10-26 18:23:06'),
(335, 'type_field', 'Thể loại', 76, 22, 0, '', 0, 256, 0, '', 0, '2019-10-26 18:30:49', '2019-10-26 18:30:49'),
(336, 'type_tn', 'Thể loại câu hỏi', 73, 7, 0, '', 0, 0, 1, '@tracnghiem_types', 0, '2019-10-26 18:31:47', '2019-10-26 18:31:47'),
(337, 'id_tracnghiem', 'Id câu hỏi', 74, 7, 0, '', 0, 0, 1, '@tracnghiems', 0, '2019-10-26 18:47:08', '2019-10-26 18:47:14'),
(338, 'answer', 'Đáp án', 74, 22, 0, '', 0, 256, 1, '', 0, '2019-10-26 18:48:41', '2019-10-26 19:18:56'),
(339, 'candidate_position', 'Vị trí ứng tuyển', 77, 22, 0, '', 0, 256, 1, '', 1, '2019-10-30 04:40:29', '2019-10-30 04:40:29'),
(340, 'date_receive_job', 'Ngày nhận việc', 77, 4, 0, '', 0, 0, 1, '', 2, '2019-10-30 04:43:22', '2019-10-30 04:43:22'),
(341, 'fullname', 'Họ tên', 77, 22, 0, '', 0, 256, 1, '', 4, '2019-10-30 04:43:35', '2019-10-30 04:43:35'),
(342, 'birthday', 'Ngày sinh', 77, 4, 0, '', 0, 0, 1, '', 5, '2019-10-30 04:43:53', '2019-10-30 04:43:53'),
(343, 'address_birthday', 'Nơi sinh', 77, 21, 0, '', 0, 0, 1, '', 6, '2019-10-30 04:44:12', '2019-10-30 04:44:12'),
(344, 'marital_status', 'Tình trạng hôn nhân', 77, 22, 0, '', 0, 256, 1, '', 7, '2019-10-30 04:44:51', '2019-10-30 04:44:51'),
(345, 'number_children', 'Số con', 77, 13, 0, '', 0, 11, 1, '', 8, '2019-10-30 04:45:18', '2019-10-30 04:45:18'),
(346, 'mobile', 'Điện thoại', 77, 14, 0, '', 0, 20, 1, '', 9, '2019-10-30 04:45:35', '2019-10-30 04:45:35'),
(347, 'email', 'Email', 77, 8, 0, '', 0, 256, 1, '', 10, '2019-10-30 04:45:46', '2019-10-30 04:45:46'),
(348, 'address', 'Địa chỉ tạm trú', 77, 21, 0, '', 0, 0, 1, '', 11, '2019-10-30 04:45:59', '2019-10-30 04:45:59'),
(349, 'image', 'Hình ảnh', 77, 25, 0, '', 0, 0, 1, '', 3, '2019-10-30 05:07:42', '2019-10-30 05:07:42'),
(350, 'bangcap1', 'Bằng cấp 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:09:07', '2019-10-30 05:09:07'),
(351, 'bangcap2', 'Bằng cấp 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:09:22', '2019-10-30 05:09:22'),
(352, 'tentruong1', 'Tên trường 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:09:34', '2019-10-30 05:09:34'),
(353, 'tentruong2', 'Tên trường 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:09:51', '2019-10-30 05:09:51'),
(354, 'khoanganh1', 'Khoa/ngành 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:10:22', '2019-10-30 05:10:48'),
(355, 'khoanganh2', 'Khoa/ngành 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:10:36', '2019-10-30 05:10:36'),
(356, 'tgdt1', 'thời gian đào tạo 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:11:12', '2019-10-30 05:11:12'),
(357, 'tgdt2', 'thời gian đào tạo 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:11:23', '2019-10-30 05:11:23'),
(358, 'khoahoc1', 'Khóa học 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:11:41', '2019-10-30 05:11:41'),
(359, 'khoahoc2', 'Khóa học 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:11:47', '2019-10-30 05:11:47'),
(360, ' dvtc1', 'Đơn vị tổ chức 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:12:06', '2019-10-30 05:12:06'),
(361, 'dvtc2', 'Đơn vị tổ chức 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:12:14', '2019-10-30 05:12:28'),
(362, 'cc1', 'Chứng chỉ 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:12:39', '2019-10-30 05:12:39'),
(363, 'cc2', 'Chứng chỉ 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:12:46', '2019-10-30 05:12:46'),
(364, 'tgkh1', 'Thời gian khóa học 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:13:08', '2019-10-30 05:13:08'),
(365, 'tgkh2', 'Thời gian khóa học 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:13:16', '2019-10-30 05:13:16'),
(366, 'word', 'Kỹ năng Word', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:13:55', '2019-10-30 05:14:31'),
(367, 'excel', 'Kỹ năng Excel', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:14:14', '2019-10-30 05:14:14'),
(368, 'powerpoint', 'Kỹ năng Powerpoint', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:14:43', '2019-10-30 05:14:43'),
(369, 'listening', 'Kỹ năng nghe', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:14:57', '2019-10-30 05:16:08'),
(370, 'speaking', 'Kỹ năng nói ', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:15:15', '2019-10-30 05:16:04'),
(371, 'writing', 'Kỹ năng viết', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:15:49', '2019-10-30 05:15:49'),
(372, 'skill_soft', 'Kỹ năng mềm', 77, 21, 0, '', 0, 0, 0, '', 0, '2019-10-30 05:16:35', '2019-10-30 05:16:35'),
(373, 'from_date1', 'Thời gian từ 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:17:41', '2019-10-30 05:19:39'),
(374, 'from_date2', 'Thời gian từ 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:19:24', '2019-10-30 05:19:24'),
(375, 'from_date3', 'Thời gian từ 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:20:03', '2019-10-30 05:20:03'),
(376, 'to_date1', 'Thời gian đến 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:20:23', '2019-10-30 05:20:23'),
(377, 'to_date2', 'Thời gian đến 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:20:30', '2019-10-30 05:20:30'),
(378, 'to_date3', 'Thời gian đến 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:20:38', '2019-10-30 05:20:38'),
(379, 'company_1', 'Công ty 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:22:05', '2019-10-30 05:22:05'),
(380, 'company_2', 'Công ty 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:22:15', '2019-10-30 05:22:28'),
(381, 'company_3', 'Công ty 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:22:39', '2019-10-30 05:22:39'),
(382, 'title_1', 'Chức danh 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:23:23', '2019-10-30 05:23:23'),
(383, 'title_2', 'Chức danh 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:23:32', '2019-10-30 05:23:32'),
(384, 'title_3', 'Chức danh 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:23:41', '2019-10-30 05:23:41'),
(385, 'working_1', 'Công việc 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:23:57', '2019-10-30 05:23:57'),
(386, 'working_2', 'Công việc 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:24:47', '2019-10-30 05:24:47'),
(387, 'working_3', 'Công việc 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:24:54', '2019-10-30 05:24:54'),
(388, 'salary_1', 'Thu nhập 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:25:14', '2019-10-30 05:25:14'),
(389, 'salary_2', 'Thu nhập 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:25:23', '2019-10-30 05:25:23'),
(390, 'salary_3', 'Thu nhập 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:25:30', '2019-10-30 05:25:30'),
(391, 'fullname_1', 'Họ và tên 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:25:51', '2019-10-30 05:25:51'),
(392, 'fullname_2', 'Họ và tên 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:26:01', '2019-10-30 05:26:01'),
(393, 'fullname_3', 'Họ và tên 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:26:08', '2019-10-30 05:26:08'),
(394, 'relation_1', 'Quan hệ 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:26:22', '2019-10-30 05:26:22'),
(395, 'relation_2', 'Quan hệ 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:26:29', '2019-10-30 05:26:29'),
(396, 'relation_3', 'Quan hệ 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:26:37', '2019-10-30 05:26:37'),
(397, 'relation_title_1', 'Quan hệ chức danh 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:27:23', '2019-10-30 05:27:23'),
(398, 'relation_title_2', 'Quan hệ chức danh 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:27:32', '2019-10-30 05:27:32'),
(399, 'relation_title_3', 'Quan hệ chức danh 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:27:40', '2019-10-30 05:27:40'),
(400, 'relation_company_1', 'Quan hệ công ty 1', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:27:57', '2019-10-30 05:27:57'),
(401, 'relation_company_2', 'Quan hệ công ty 2', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:28:05', '2019-10-30 05:28:05'),
(402, 'relation_company_3', 'Quan hệ công ty 3', 77, 22, 0, '', 0, 256, 0, '', 0, '2019-10-30 05:28:14', '2019-10-30 05:28:14'),
(403, 'relation_phone_1', 'Quan hệ điện thoại 1', 77, 14, 0, '', 0, 20, 0, '', 0, '2019-10-30 05:28:32', '2019-10-30 05:28:32'),
(404, 'relation_phone_2', 'Quan hệ điện thoại 2', 77, 14, 0, '', 0, 20, 0, '', 0, '2019-10-30 05:28:43', '2019-10-30 05:28:43'),
(405, 'relation_phone_3', 'Quan hệ điện thoại 3', 77, 14, 0, '', 0, 20, 0, '', 0, '2019-10-30 05:28:57', '2019-10-30 05:28:57'),
(406, 'new_city', 'Tỉnh/TP', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:29:21', '2019-10-30 05:29:21'),
(407, 'ditricst', 'Quận/Huyện', 77, 22, 0, '', 0, 256, 1, '', 0, '2019-10-30 05:29:38', '2019-10-30 05:29:38'),
(408, 'content', 'Trắc nghiệm', 77, 21, 0, '', 0, 0, 0, '', 0, '2019-10-30 06:33:02', '2019-10-30 06:33:02');

-- --------------------------------------------------------

--
-- Table structure for table `module_field_types`
--

DROP TABLE IF EXISTS `module_field_types`;
CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(2, 'Checkbox', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(3, 'Currency', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(4, 'Date', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(5, 'Datetime', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(6, 'Decimal', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(7, 'Dropdown', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(8, 'Email', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(9, 'File', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(10, 'Float', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(11, 'HTML', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(12, 'Image', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(13, 'Integer', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(14, 'Mobile', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(15, 'Multiselect', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(16, 'Name', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(17, 'Password', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(18, 'Radio', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(19, 'String', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(20, 'Taginput', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(21, 'Textarea', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(22, 'TextField', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(23, 'URL', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(24, 'Files', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(25, 'Ckfinder', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(26, 'TinyMCE', '2018-07-19 18:50:53', '2018-07-19 18:50:53');

-- --------------------------------------------------------

--
-- Table structure for table `nganhnghes`
--

DROP TABLE IF EXISTS `nganhnghes`;
CREATE TABLE `nganhnghes` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug_nganh_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nganhnghes`
--

INSERT INTO `nganhnghes` (`id`, `slug_nganh_name`, `deleted_at`, `created_at`, `updated_at`, `name`) VALUES
(1, 'tuyen-dung-mien-bac', NULL, '2019-10-25 07:37:27', '2019-10-25 07:37:58', 'Tuyển dụng miền Bắc'),
(2, 'tuyen-dung-mien-trung', NULL, '2019-10-25 07:37:33', '2019-10-25 07:37:51', 'Tuyển dụng miền Trung'),
(3, 'tuyen-dung-mien-nam', NULL, '2019-10-25 07:37:45', '2019-10-25 07:37:45', 'Tuyển dụng miền Nam');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'http://',
  `assigned_to` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `connect_since` date NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contetn` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2018-07-19 18:51:06', '2018-07-19 18:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `categories` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `thumbnail` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `author` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `seo_title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[""]',
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `deleted_at`, `created_at`, `updated_at`, `title`, `slug`, `content`, `categories`, `thumbnail`, `author`, `seo_title`, `seo_description`, `seo_keywords`, `tags`, `status`) VALUES
(1, NULL, '2019-10-25 06:54:00', '2019-10-30 06:26:49', 'MÔI TRƯỜNG LÀM VIỆC', 'anzedo-noi-lam-viec-ly-tuong', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: center;\"><span style=\"color: #00ccff;\">4 L&Yacute; DO ANZEDO L&Agrave; NƠI L&Agrave;M VIỆC L&Yacute; TƯỞNG</span></p>\r\n<ol>\r\n<li>Tầm Nh&igrave;n Chiến Lược&nbsp;</li>\r\n</ol>\r\n<p style=\"padding-left: 30px;\">● L&agrave; một nh&atilde;n h&agrave;ng mới trong ngh&agrave;nh thời trang Việt, thương hiệu veston nam cao cấp ANZEDO dưới sự hậu thuẫn từ Tập đo&agrave;n CGROUP, lu&ocirc;n phấn đấu, nỗ lực kh&ocirc;ng ngừng để đem đến cho qu&yacute; kh&aacute;ch h&agrave;ng những trải nghiệm về sản phẩm tuyệt vời nhất.</p>\r\n<p style=\"padding-left: 30px;\">● Với slogan &ldquo;H&Atilde;Y L&Agrave; NHỮNG G&Igrave; BẠN MUỐN&rdquo;, ANZEDO lu&ocirc;n lắng nghe nhu cầu của kh&aacute;ch h&agrave;ng v&agrave; mong rằng kh kh&aacute;ch h&agrave;ng kho&aacute;c l&ecirc;n m&igrave;nh những bộ c&aacute;nh đến từ ANZEDO, họ sẽ cảm thấy tự tin, thoải m&aacute;i v&agrave; thấy được h&igrave;nh ảnh của ch&iacute;nh m&igrave;nh phản chiếu qua bộ trang phục đ&oacute;.</p>\r\n<p style=\"padding-left: 30px;\">● Với triết l&yacute; kinh doanh thời trang của m&igrave;nh, Anzedo đ&atilde; gặt h&aacute;i được rất nhiều th&agrave;nh c&ocirc;ng v&agrave; dần trở th&agrave;nh địa chỉ h&agrave;ng đầu cho c&aacute;c kh&aacute;ch h&agrave;ng đang cần t&igrave;m mua cho m&igrave;nh những bộ vest ưng &yacute; để lột x&aacute;c th&agrave;nh những qu&yacute; &ocirc;ng thời trang, lịch l&atilde;m v&agrave; quyến rũ.</p>\r\n<p style=\"text-align: left;\">2. Cơ Hội Ph&aacute;t Triển Nghề Nghiệp</p>\r\n<p style=\"padding-left: 30px;\">● C&ugrave;ng với rất nhiều chương tr&igrave;nh đ&agrave;o tạo chuy&ecirc;n m&ocirc;n v&agrave; kỹ năng, nh&acirc;n vi&ecirc;n tại ANZEDO lu&ocirc;n được tạo điều kiện để ph&aacute;t triển sự nghiệp cũng như thăng tiến l&ecirc;n c&aacute;c vị tr&iacute; quản l&yacute;.</p>\r\n<p style=\"padding-left: 30px;\">● ANZEDO x&acirc;y dựng một đội ngũ nh&acirc;n sự chuy&ecirc;n m&ocirc;n cao v&agrave; c&oacute; t&acirc;m với nghề, một m&ocirc;i trường l&agrave;m việc l&agrave;nh mạnh, đo&agrave;n kết, chia sẻ, c&ugrave;ng nhau ph&aacute;t triển. Hướng tới ch&iacute;nh s&aacute;ch đ&atilde;i ngộ nh&acirc;n sự xứng đ&aacute;ng, ANZEDO l&agrave; điểm đến cho những ai muốn ph&aacute;t triển bằng ch&iacute;nh thực lực.</p>\r\n<p>3. Chế Độ Cạnh Tranh</p>\r\n<p style=\"padding-left: 30px;\">● Ch&uacute;ng t&ocirc;i lu&ocirc;n x&acirc;y dựng v&agrave; &aacute;p dụng c&aacute;c ch&iacute;nh s&aacute;ch ph&uacute;c lợi mang t&iacute;nh cạnh tranh v&agrave; thu h&uacute;t so với thị trường thời trang n&oacute;i ri&ecirc;ng v&agrave; thị trường b&aacute;n lẻ n&oacute;i chung. Đời sống của nh&acirc;n vi&ecirc;n lu&ocirc;n được n&acirc;ng cao bằng c&aacute;c hoạt động sinh nhật, du lịch nghỉ m&aacute;t, teambuilding&hellip;</p>\r\n<p>4. Để Nh&acirc;n Vi&ecirc;n L&agrave; Ch&iacute;nh M&igrave;nh</p>\r\n<p style=\"padding-left: 30px;\">● Anzedo lu&ocirc;n nắm bắt được v&agrave; t&ocirc;n trọng sự kh&aacute;c biệt của nh&acirc;n vi&ecirc;n chứ kh&ocirc;ng giới hạn trong c&aacute;c mục như giới t&iacute;nh, tuổi t&aacute;c, qu&ecirc; qu&aacute;n, t&ocirc;n gi&aacute;o&hellip; gi&uacute;p bạn tạo n&ecirc;n m&ocirc;i trường l&agrave;m việc th&acirc;n thiện, thoải m&aacute;i, gi&uacute;p nh&acirc;n vi&ecirc;n ph&aacute;t huy hết được khả năng, gi&aacute; trị cốt l&otilde;i của bản th&acirc;n.</p>\r\n<p style=\"padding-left: 30px;\">● Chỉ khi l&agrave;m việc ở m&ocirc;i trường được thoải m&aacute;i l&agrave; ch&iacute;nh m&igrave;nh, mỗi c&aacute; nh&acirc;n mới c&oacute; thể tự tin cống hiến kiến thức, t&agrave;i năng v&agrave; kh&ocirc;ng ngại bị đ&aacute;nh gi&aacute; khi đưa ra c&aacute;c &yacute; tưởng mới.</p>\r\n</body>\r\n</html>', '[\"1\"]', '', 1, '', '', '', '[\"\"]', 1),
(2, NULL, '2019-10-25 06:54:24', '2019-10-30 06:27:55', 'ĐÀO TẠO VÀ PHÁT TRIỂN', 'dao-tao-va-phat-trien', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"padding-left: 30px; text-align: left;\">Nếu c&ocirc;ng ty l&agrave; một cơ thể sống th&igrave; nh&acirc;n vi&ecirc;n ch&iacute;nh l&agrave; tế b&agrave;o tuy nhỏ nhưng lại v&ocirc; c&ugrave;ng quan trọng. Đối với ANZEDO mọi nh&acirc;n vi&ecirc;n đều quan trọng v&agrave; kh&ocirc;ng thể thiếu một ai. Bởi mỗi nh&acirc;n vi&ecirc;n tại ANZEDO đều mang trong m&igrave;nh gi&aacute; trị cốt l&otilde;i để cống hiến hết m&igrave;nh v&igrave; sự ph&aacute;t triển của c&ocirc;ng ty v&agrave; x&atilde; hội.&nbsp;</p>\r\n<p>►&nbsp;Đ&Agrave;O TẠO HỘI NHẬP</p>\r\n<p style=\"padding-left: 30px;\">&ndash; Nhằm gi&uacute;p nh&acirc;n vi&ecirc;n mới hội nhập nhanh ch&oacute;ng v&agrave;o m&ocirc;i trường l&agrave;m việc v&agrave; nắm bắt c&ocirc;ng việc hiệu quả nhất, tất cả nh&acirc;n sự mới đều được hướng dẫn hội nhập.</p>\r\n<p style=\"padding-left: 30px;\">&ndash;&nbsp; Nh&acirc;n vi&ecirc;n l&agrave;m việc tại hệ thống cửa h&agrave;ng sẽ được tham gia c&aacute;c lớp đ&agrave;o tạo về kiến thức sản phẩm, kỹ năng/nghiệp vụ chuy&ecirc;n m&ocirc;n theo lộ tr&igrave;nh đ&agrave;o tạo hội nhập d&agrave;nh cho từng vị tr&iacute; c&ocirc;ng việc.</p>\r\n<p style=\"padding-left: 30px;\">&ndash; Nh&acirc;n vi&ecirc;n l&agrave;m việc tại c&aacute;c bộ phận/ph&ograve;ng ban t&ugrave;y từng vị tr&iacute; cụ thể sẽ được l&ecirc;n kế hoạch tham gia c&aacute;c lớp đ&agrave;o tạo c&aacute;c kiến thức, kỹ năng/nghiệp vụ chuy&ecirc;n m&ocirc;n của vị tr&iacute; đảm tr&aacute;ch.</p>\r\n<p style=\"padding-left: 30px;\">&ndash; Nh&acirc;n vi&ecirc;n c&oacute; kết quả c&ocirc;ng việc tốt v&agrave; thể hiện năng lực quản l&yacute; sẽ c&oacute; cơ hội thăng tiến.&nbsp;&nbsp;</p>\r\n<p>►&nbsp;Đ&Agrave;O TẠO &ndash; PH&Aacute;T TRIỂN</p>\r\n<p style=\"padding-left: 30px;\">&ndash; C&ocirc;ng ty thường xuy&ecirc;n tổ chức c&aacute;c kh&oacute;a đ&agrave;o tạo nội bộ, hội thảo v&agrave; đ&agrave;i thọ chi ph&iacute; cho CBNV tham gia c&aacute;c kh&oacute;a đ&agrave;o tạo b&ecirc;n ngo&agrave;i nhằm n&acirc;ng cao năng lực đ&aacute;p ứng tốt nhất y&ecirc;u cầu c&ocirc;ng việc.</p>\r\n<p style=\"padding-left: 30px;\">&ndash;&nbsp; Nh&acirc;n vi&ecirc;n c&oacute; kết quả c&ocirc;ng việc tốt v&agrave; thể hiện năng lực quản l&yacute; sẽ c&oacute; cơ hội thăng tiến l&ecirc;n c&aacute;c vị tr&iacute; cao hơn th&ocirc;ng qua việc đề bạt v&agrave; xem x&eacute;t của c&aacute;c cấp quản l&yacute; (nhằm đảm bảo c&oacute; cơ hội cho vị tr&iacute; mới), ho&agrave;n th&agrave;nh c&aacute;c nội dung đ&agrave;o tạo tương ứng v&agrave; được đ&aacute;nh gi&aacute; đạt qua c&aacute;c giai đoạn đ&agrave;o tạo.&nbsp;</p>\r\n<p style=\"padding-left: 30px;\">&ndash; Chương tr&igrave;nh Quản trị vi&ecirc;n tập sự: nhằm chuẩn bị đội ngũ quản l&yacute; trẻ c&oacute; đầy đủ năng lực v&agrave; phẩm chất đ&aacute;p ứng chiến lược ph&aacute;t triển của C&ocirc;ng ty, h&agrave;ng năm C&ocirc;ng ty tổ chức&nbsp; chương tr&igrave;nh Quản trị vi&ecirc;n tập sự, trong đ&oacute; tổ chức tuyển chọn những sinh vi&ecirc;n xuất sắc từ c&aacute;c Trường đại học uy t&iacute;n để đ&agrave;o tạo &ndash; ph&aacute;t triển th&agrave;nh c&aacute;c c&aacute;n bộ quản l&yacute; cho khối Kinh doanh, Văn ph&ograve;ng, X&iacute; nghiệp.&nbsp;</p>\r\n<p>►&nbsp;CƠ HỘI NGHỀ NGHIỆP/ THUY&Ecirc;N CHUYỂN</p>\r\n<p style=\"padding-left: 30px;\">&ndash; C&ocirc;ng ty c&oacute; ch&iacute;nh s&aacute;ch ưu ti&ecirc;n tuyển dụng nội bộ, nh&acirc;n vi&ecirc;n c&ocirc;ng ty được tạo điều kiện ứng tuyển v&agrave;o c&aacute;c vị tr&iacute; c&ocirc;ng việc ph&ugrave; hợp với năng lực c&aacute; nh&acirc;n.</p>\r\n<p style=\"padding-left: 30px;\">&ndash; C&aacute;c nh&acirc;n vi&ecirc;n tiềm năng, ưu t&uacute; được c&ocirc;ng ty đ&agrave;i thọ chi ph&iacute; tham dự c&aacute;c kh&oacute;a đ&agrave;o tạo b&ecirc;n trong v&agrave; b&ecirc;n ngo&agrave;i c&ocirc;ng ty nhằm c&oacute; đủ năng lực cho vị tr&iacute; mới.&nbsp;</p>\r\n<p style=\"padding-left: 30px;\">&nbsp;</p>\r\n<h4 style=\"text-align: center; padding-left: 30px;\"><span style=\"color: #3366ff;\">&ldquo;Những g&igrave; l&agrave;m từ tr&aacute;i tim sẽ chạm tới tr&aacute;i tim&rdquo;&nbsp;</span></h4>\r\n</body>\r\n</html>', '[\"2\"]', '', 1, '', '', '', '[\"\"]', 1),
(3, NULL, '2019-10-25 06:54:41', '2019-11-04 01:22:48', 'PHÚC LỢI', 'phuc-loi', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>I. Chế độ \"Bảo hiểm x&atilde; hội\"&nbsp;</p>\r\n<p><img src=\"../../../public/uploads/images/menu/phucloi/pl1.jpg\" alt=\"\" width=\"625\" height=\"469\" /><img src=\"../../../public/uploads/files/Company%20Benefits.png\" alt=\"\" width=\"750\" height=\"600\" /></p>\r\n<p style=\"padding-left: 30px;\">&bull; Một trong những chế độ đ&atilde;i ngộ v&agrave; quyền lợi quan trọng của c&aacute;c bạn khi đi l&agrave;m đ&oacute; ch&iacute;nh l&agrave; Bảo hiểm x&atilde; hội (BHXH). ANZEDO cam kết đ&oacute;ng bảo hiểm cho to&agrave;n bộ nh&acirc;n vi&ecirc;n ngay khi được tiếp nhận ch&iacute;nh thức, đi k&egrave;m l&agrave; những chế độ hấp dẫn đảm bảo quyền lợi cho nh&acirc;n vi&ecirc;n ở mức cao nhất.</p>\r\n<p style=\"padding-left: 30px;\">&bull; Người lao động được hưởng c&aacute;c chế độ ngừng việc, trợ cấp th&ocirc;i việc hoặc bồi thường theo quy định của Ph&aacute;p luật hiện h&agrave;nh.</p>\r\n<p>II. Chế độ \"Lương + Thưởng\" hấp dẫn</p>\r\n<p style=\"padding-left: 30px;\">&bull; Lương cơ bản: Mỗi nh&acirc;n vi&ecirc;n sẽ c&oacute; mức lương cơ bản kh&aacute;c nhau dựa tr&ecirc;n khối lượng c&ocirc;ng việc v&agrave; doanh số h&agrave;ng th&aacute;ng, tuy nhi&ecirc;n vẫn cực k&igrave; hấp dẫn cho d&ugrave; bản chất chỉ l&agrave; mức lương mang t&iacute;nh chất ổn định cho c&ocirc;ng việc.&nbsp;</p>\r\n<p style=\"padding-left: 30px;\">&bull; Trợ cấp hiệu suất c&ocirc;ng việc: Chi trả theo đ&aacute;nh gi&aacute; của quản l&yacute; đơn vị.</p>\r\n<p style=\"padding-left: 30px;\">&bull; Lương hiệu quả: Theo quy định của Ph&ograve;ng ban c&ocirc;ng ty.&nbsp;</p>\r\n<p style=\"padding-left: 30px;\">&bull; Khen thưởng: Nh&acirc;n vi&ecirc;n được khuyến kh&iacute;ch bằng vật chất v&agrave; tinh thần khi c&oacute; th&agrave;nh t&iacute;ch trong c&ocirc;ng t&aacute;c theo quy định của c&ocirc;ng ty&nbsp;</p>\r\n<p style=\"padding-left: 30px;\">&bull; Chế độ n&acirc;ng lương: Theo quy định của Nh&agrave; nước v&agrave; quy chế tiền lương của C&ocirc;ng ty, người lao động ho&agrave;n th&agrave;nh tốt nhiệm vụ được giao, kh&ocirc;ng vi phạm quy định hoặc kh&ocirc;ng trong thời gian xử l&yacute; kỷ luật lao động v&agrave; đủ điều kiện về thời gian theo quy chế lương th&igrave; được x&eacute;t n&acirc;ng lương.&nbsp;</p>\r\n<p>III. C&aacute;c chế độ đ&atilde;i ngộ kh&aacute;c:&nbsp;</p>\r\n<p style=\"padding-left: 30px;\">&bull; Nghỉ ph&eacute;p, thưởng c&aacute;c ng&agrave;y lễ tết.</p>\r\n<p style=\"padding-left: 30px;\">&bull; Du lịch, nghỉ m&aacute;t h&agrave;ng năm c&ugrave;ng C&ocirc;ng ty.</p>\r\n<p style=\"padding-left: 30px;\">&bull; Tham gia c&aacute;c sự kiện, event h&agrave;ng năm tại C&ocirc;ng ty.&nbsp;</p>\r\n<p><img src=\"../../../public/uploads/images/menu/phucloi/pl2.jpg\" alt=\"\" /><img src=\"../../../public/uploads/files/284ebdc50a23ec7db5328.jpg\" alt=\"\" /> <img src=\"../../../public/uploads/files/1549d17a889c6ec2378d3.jpg\" alt=\"\" /></p>\r\n</body>\r\n</html>', '[\"3\"]', '', 1, '', '', '', '[\"\"]', 1),
(4, NULL, '2019-10-25 06:55:00', '2019-10-30 06:26:16', 'CÁCH THỨC ỨNG TUYỂN', 'cach-thuc-ung-tuyen', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>►&nbsp;C&aacute;ch 1: Nộp trực tuyến</p>\r\n<p style=\"padding-left: 30px;\">&bull; Ứng vi&ecirc;n nộp CV k&egrave;m ảnh đến địa chỉ email:&nbsp;<span style=\"color: #0056b3;\"><a style=\"color: #0056b3;\" href=\"mailto:job@anzedo.net\">job@anzedo.net&nbsp;</a></span>hoặc&nbsp;<span style=\"color: #0056b3;\"><a style=\"color: #0056b3;\" href=\"mailto:tuyendung@anzedo.net\">tuyendung@anzedo.net</a></span></p>\r\n<p style=\"padding-left: 30px;\">&bull; Ti&ecirc;u đề email ghi theo mẫu HỌ T&Ecirc;N_VỊ TR&Iacute; ỨNG TUYỂN_NƠI L&Agrave;M VIỆC (V&iacute; dụ: Nguyễn Văn A_Kế to&aacute;n_HCM)&nbsp;</p>\r\n<p>►&nbsp;C&aacute;ch 2: Nộp hồ sơ trực tiếp tại:</p>\r\n<p>&bull;&nbsp;Trụ sở ch&iacute;nh</p>\r\n<p style=\"padding-left: 30px;\">+ Địa chỉ: Tầng 1, ANZEDO Tower, Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, Th&agrave;nh phố Hồ Ch&iacute; Minh.</p>\r\n<p>&bull;&nbsp;Hệ thống Showroom:</p>\r\n<p style=\"padding-left: 30px;\">&curren;&nbsp;Hồ Ch&iacute; Minh</p>\r\n<p style=\"padding-left: 60px;\">+ 656 Nguyễn Đ&igrave;nh Chiểu, Phường 3, Quận 3</p>\r\n<p style=\"padding-left: 60px;\">+ 44 Ho&agrave;ng Văn Thụ, Phường 9, Quận Ph&uacute; Nhuận</p>\r\n<p style=\"padding-left: 60px;\">+ 219E Nơ Trang Long, Phường 12, Quận B&igrave;nh Thạnh</p>\r\n<p style=\"padding-left: 60px;\">+ 247 Luỹ B&aacute;n B&iacute;ch, Phường Hiệp T&acirc;n, Quận T&acirc;n Ph&uacute;</p>\r\n<p style=\"padding-left: 30px;\">&curren;&nbsp;Đ&agrave; Nẵng</p>\r\n<p style=\"padding-left: 60px;\">+ 71 H&ugrave;ng Vương, Phường Hải Ch&acirc;u 1, Quận Hải Ch&acirc;u</p>\r\n<p style=\"padding-left: 60px;\">+ 423 Điện Bi&ecirc;n Phủ, Phường Ho&agrave; Kh&ecirc;, Quận Thanh Kh&ecirc;</p>\r\n<p style=\"padding-left: 30px;\">&curren;&nbsp;Nghệ An</p>\r\n<p style=\"padding-left: 60px;\">+ 51 Nguyễn Sỹ S&aacute;ch, Phường H&agrave; Huy Tập, Th&agrave;nh phố Vinh</p>\r\n<p style=\"padding-left: 30px;\">&curren;&nbsp;Thanh H&oacute;a</p>\r\n<p style=\"padding-left: 60px;\">+ L&ocirc; 04, Phan Chu Trinh, Phường Điện Bi&ecirc;n&nbsp;</p>\r\n<p>►&nbsp;LI&Ecirc;N HỆ: Ph&ograve;ng Nh&acirc;n sự - Thương hiệu thời trang Veston cao cấp ANZEDO</p>\r\n<p style=\"padding-left: 30px;\">- Địa chỉ: Tầng 1, Anzedo Tower, Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh.</p>\r\n<p style=\"padding-left: 30px;\">- Điện thoại: (028) 3636.8795 &ndash; Ms. Ng&acirc;n</p>\r\n<p style=\"padding-left: 30px;\">- Email:&nbsp;<span style=\"color: #0056b3;\"><a style=\"color: #0056b3;\" href=\"mailto:job@anzedo.net\">job@anzedo.net&nbsp;</a></span>hoặc&nbsp;<span style=\"color: #0056b3;\"><a style=\"color: #0056b3;\" href=\"mailto:tuyendung@anzedo.net\">tuyendung@anzedo.net&nbsp;</a></span>&nbsp;</p>\r\n</body>\r\n</html>', '[\"4\"]', '', 1, '', '', '', '[\"\"]', 1),
(5, NULL, '2019-10-25 06:55:21', '2019-10-25 06:58:50', 'LIÊN HỆ', 'lien-he', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Đang cập nhật ...</p>\r\n</body>\r\n</html>', '[\"5\"]', '', 1, '', '', '', '[\"\"]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_labels`
--

DROP TABLE IF EXISTS `product_labels`;
CREATE TABLE `product_labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_labels`
--

INSERT INTO `product_labels` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `key`) VALUES
(1, NULL, '2019-03-23 06:38:40', '2019-03-23 06:38:40', 'Bài viết mới', 'post_new');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE `promotions` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `khuyenmai` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `html` varchar(9999) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `re_comments`
--

DROP TABLE IF EXISTS `re_comments`;
CREATE TABLE `re_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cmt_id` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `blog_id` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content_re` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `email_re` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `dept` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', 1, 1, NULL, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 'MOD', 'Mod', 'Mod', 1, 2, NULL, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(3, 'CLIENT', 'Client', 'Client', 4, 4, NULL, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(4, 'USER', 'User', 'User', 2, 3, NULL, '2019-04-07 06:46:57', '2019-04-07 06:47:13');

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

DROP TABLE IF EXISTS `role_module`;
CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 1, 2, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(3, 1, 3, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(4, 1, 4, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(5, 1, 5, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(6, 1, 6, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(7, 1, 7, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(8, 1, 8, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(18, 1, 24, 1, 1, 1, 1, '2018-09-02 01:14:24', '2018-09-02 01:14:24'),
(20, 1, 26, 1, 1, 1, 1, '2018-09-06 14:45:06', '2018-09-06 14:45:06'),
(21, 1, 27, 1, 1, 1, 1, '2018-09-08 22:40:09', '2018-09-08 22:40:09'),
(22, 1, 28, 1, 1, 1, 1, '2018-09-15 02:02:07', '2018-09-15 02:02:07'),
(24, 1, 34, 1, 1, 1, 1, '2018-09-24 01:03:10', '2018-09-24 01:03:10'),
(26, 1, 38, 1, 1, 1, 1, '2018-09-24 01:38:57', '2018-09-24 01:38:57'),
(27, 1, 39, 1, 1, 1, 1, '2018-10-04 02:37:46', '2018-10-04 02:37:46'),
(28, 1, 42, 1, 1, 1, 1, '2018-11-19 18:26:38', '2018-11-19 18:26:38'),
(29, 1, 43, 1, 1, 1, 1, '2018-11-21 18:55:24', '2018-11-21 18:55:24'),
(30, 1, 44, 1, 1, 1, 1, '2018-11-21 18:59:49', '2018-11-21 18:59:49'),
(41, 1, 56, 1, 1, 1, 1, '2019-01-01 01:23:40', '2019-01-01 01:23:40'),
(45, 1, 61, 1, 1, 1, 1, '2019-02-14 19:03:04', '2019-02-14 19:03:04'),
(48, 1, 65, 1, 1, 1, 1, '2019-02-17 18:57:04', '2019-02-17 18:57:04'),
(50, 1, 67, 1, 1, 1, 1, '2019-03-26 06:41:22', '2019-03-26 06:41:22'),
(51, 1, 68, 1, 1, 1, 1, '2019-03-30 07:47:20', '2019-03-30 07:47:20'),
(52, 2, 1, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(53, 2, 2, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(54, 2, 3, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(55, 2, 4, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(56, 2, 5, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(57, 2, 6, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(58, 2, 7, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(59, 2, 8, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(60, 2, 24, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(61, 2, 26, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(62, 2, 27, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(63, 2, 28, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(64, 2, 34, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(65, 2, 38, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(66, 2, 39, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(67, 2, 42, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(68, 2, 43, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(69, 2, 44, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(70, 2, 56, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(71, 2, 61, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(72, 2, 65, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(73, 2, 67, 1, 0, 0, 0, '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(74, 2, 68, 1, 0, 0, 0, '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(75, 4, 1, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(76, 4, 2, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(77, 4, 3, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(78, 4, 4, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(79, 4, 5, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(80, 4, 6, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(81, 4, 7, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(82, 4, 8, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(83, 4, 24, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(84, 4, 26, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(85, 4, 27, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(86, 4, 28, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(87, 4, 34, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(88, 4, 38, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(89, 4, 39, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(90, 4, 42, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(91, 4, 43, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(92, 4, 44, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(93, 4, 56, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(94, 4, 61, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(95, 4, 65, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(96, 4, 67, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(97, 4, 68, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(98, 3, 1, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(99, 3, 2, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(100, 3, 3, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(101, 3, 4, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(102, 3, 5, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(103, 3, 6, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(104, 3, 7, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(105, 3, 8, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(106, 3, 24, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(107, 3, 26, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(108, 3, 27, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(109, 3, 28, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(110, 3, 34, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(111, 3, 38, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(112, 3, 39, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(113, 3, 42, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(114, 3, 43, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(115, 3, 44, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(116, 3, 56, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(117, 3, 61, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(118, 3, 65, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(119, 3, 67, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(120, 3, 68, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(121, 1, 69, 1, 1, 1, 1, '2019-10-25 07:33:59', '2019-10-25 07:33:59'),
(122, 1, 70, 1, 1, 1, 1, '2019-10-25 07:34:45', '2019-10-25 07:34:45'),
(123, 1, 71, 1, 1, 1, 1, '2019-10-25 07:39:30', '2019-10-25 07:39:30'),
(124, 1, 72, 1, 1, 1, 1, '2019-10-25 07:49:28', '2019-10-25 07:49:28'),
(125, 1, 76, 1, 1, 1, 1, '2019-10-26 18:30:53', '2019-10-26 18:30:53'),
(126, 1, 73, 1, 1, 1, 1, '2019-10-26 18:33:21', '2019-10-26 18:33:21'),
(127, 1, 74, 1, 1, 1, 1, '2019-10-26 18:48:47', '2019-10-26 18:48:47'),
(128, 1, 77, 1, 1, 1, 1, '2019-10-30 05:29:52', '2019-10-30 05:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `role_module_fields`
--

DROP TABLE IF EXISTS `role_module_fields`;
CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 1, 2, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(3, 1, 3, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(4, 1, 4, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(5, 1, 5, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(6, 1, 6, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(7, 1, 7, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(8, 1, 8, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(9, 1, 9, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(10, 1, 10, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(11, 1, 11, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(12, 1, 12, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(13, 1, 13, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(14, 1, 14, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(15, 1, 15, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(16, 1, 16, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(17, 1, 17, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(18, 1, 18, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(19, 1, 19, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(20, 1, 20, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(21, 1, 21, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(22, 1, 22, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(23, 1, 23, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(24, 1, 24, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(25, 1, 25, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(26, 1, 26, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(27, 1, 27, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(28, 1, 28, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(29, 1, 29, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(30, 1, 30, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(31, 1, 31, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(32, 1, 32, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(33, 1, 33, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(34, 1, 34, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(35, 1, 35, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(36, 1, 36, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(37, 1, 37, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(38, 1, 38, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(39, 1, 39, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(40, 1, 40, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(41, 1, 41, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(42, 1, 42, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(43, 1, 43, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(44, 1, 44, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(45, 1, 45, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(46, 1, 46, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(47, 1, 47, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(48, 1, 48, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(49, 1, 49, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(50, 1, 50, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(51, 1, 51, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(87, 1, 108, 'write', '2018-09-06 14:42:11', '2018-09-06 14:42:11'),
(88, 1, 109, 'write', '2018-09-06 14:42:26', '2018-09-06 14:42:26'),
(89, 1, 110, 'write', '2018-09-06 14:43:10', '2018-09-06 14:43:10'),
(90, 1, 111, 'write', '2018-09-06 14:43:31', '2018-09-06 14:43:31'),
(91, 1, 112, 'write', '2018-09-06 14:43:48', '2018-09-06 14:43:48'),
(92, 1, 113, 'write', '2018-09-06 14:44:05', '2018-09-06 14:44:05'),
(93, 1, 114, 'write', '2018-09-06 14:44:24', '2018-09-06 14:44:24'),
(94, 1, 115, 'write', '2018-09-06 14:44:54', '2018-09-06 14:44:54'),
(95, 1, 116, 'write', '2018-09-08 22:33:13', '2018-09-08 22:33:13'),
(96, 1, 117, 'write', '2018-09-08 22:35:12', '2018-09-08 22:35:12'),
(97, 1, 118, 'write', '2018-09-08 22:35:26', '2018-09-08 22:35:26'),
(98, 1, 119, 'write', '2018-09-08 22:35:41', '2018-09-08 22:35:41'),
(99, 1, 120, 'write', '2018-09-08 22:36:14', '2018-09-08 22:36:14'),
(100, 1, 121, 'write', '2018-09-08 22:36:40', '2018-09-08 22:36:40'),
(101, 1, 122, 'write', '2018-09-08 22:37:12', '2018-09-08 22:37:12'),
(102, 1, 123, 'write', '2018-09-08 22:38:25', '2018-09-08 22:38:25'),
(103, 1, 124, 'write', '2018-09-08 22:38:38', '2018-09-08 22:38:38'),
(104, 1, 125, 'write', '2018-09-08 22:39:09', '2018-09-08 22:39:09'),
(105, 1, 126, 'write', '2018-09-08 22:39:59', '2018-09-08 22:39:59'),
(106, 1, 127, 'write', '2018-09-08 22:46:22', '2018-09-08 22:46:22'),
(107, 1, 128, 'write', '2018-09-15 02:01:46', '2018-09-15 02:01:46'),
(108, 1, 129, 'write', '2018-09-15 02:02:04', '2018-09-15 02:02:04'),
(128, 1, 149, 'write', '2018-09-24 00:58:57', '2018-09-24 00:58:57'),
(129, 1, 150, 'write', '2018-09-24 00:59:22', '2018-09-24 00:59:22'),
(131, 1, 152, 'write', '2018-09-24 01:02:33', '2018-09-24 01:02:33'),
(132, 1, 153, 'write', '2018-09-24 01:03:04', '2018-09-24 01:03:04'),
(139, 1, 160, 'write', '2018-09-24 01:38:53', '2018-09-24 01:38:53'),
(140, 1, 161, 'write', '2018-09-24 01:39:27', '2018-09-24 01:39:27'),
(141, 1, 162, 'write', '2018-09-24 01:39:43', '2018-09-24 01:39:43'),
(142, 1, 163, 'write', '2018-09-24 01:40:02', '2018-09-24 01:40:02'),
(143, 1, 164, 'write', '2018-09-24 01:40:15', '2018-09-24 01:40:15'),
(144, 1, 165, 'write', '2018-09-24 01:40:39', '2018-09-24 01:40:39'),
(145, 1, 166, 'write', '2018-09-24 01:41:07', '2018-09-24 01:41:07'),
(146, 1, 167, 'write', '2018-10-04 02:28:32', '2018-10-04 02:28:32'),
(147, 1, 168, 'write', '2018-10-04 02:28:49', '2018-10-04 02:28:49'),
(149, 1, 170, 'write', '2018-10-04 02:29:26', '2018-10-04 02:29:26'),
(150, 1, 171, 'write', '2018-10-04 02:37:05', '2018-10-04 02:37:05'),
(151, 1, 172, 'write', '2018-10-04 02:37:24', '2018-10-04 02:37:24'),
(152, 1, 173, 'write', '2018-10-04 02:37:39', '2018-10-04 02:37:39'),
(155, 1, 176, 'write', '2018-11-19 18:24:30', '2018-11-19 18:24:30'),
(156, 1, 177, 'write', '2018-11-19 18:25:24', '2018-11-19 18:25:24'),
(157, 1, 178, 'write', '2018-11-19 18:25:43', '2018-11-19 18:25:43'),
(158, 1, 179, 'write', '2018-11-19 18:26:10', '2018-11-19 18:26:10'),
(159, 1, 180, 'write', '2018-11-19 18:26:35', '2018-11-19 18:26:35'),
(160, 1, 181, 'write', '2018-11-21 18:53:51', '2018-11-21 18:53:51'),
(164, 1, 185, 'write', '2018-11-21 18:57:34', '2018-11-21 18:57:34'),
(165, 1, 186, 'write', '2018-11-21 18:58:08', '2018-11-21 18:58:08'),
(166, 1, 187, 'write', '2018-11-21 18:58:28', '2018-11-21 18:58:28'),
(167, 1, 188, 'write', '2018-11-21 18:58:44', '2018-11-21 18:58:44'),
(168, 1, 189, 'write', '2018-11-21 18:59:19', '2018-11-21 18:59:19'),
(169, 1, 190, 'write', '2018-11-21 18:59:37', '2018-11-21 18:59:37'),
(231, 1, 252, 'write', '2019-01-01 01:22:59', '2019-01-01 01:22:59'),
(250, 1, 271, 'write', '2019-02-14 19:00:44', '2019-02-14 19:00:44'),
(251, 1, 272, 'write', '2019-02-14 19:01:50', '2019-02-14 19:01:50'),
(265, 1, 286, 'write', '2019-02-17 18:55:24', '2019-02-17 18:55:24'),
(266, 1, 287, 'write', '2019-02-17 18:56:40', '2019-02-17 18:56:40'),
(268, 1, 289, 'write', '2019-03-21 08:26:04', '2019-03-21 08:26:04'),
(269, 1, 290, 'write', '2019-03-21 08:26:20', '2019-03-21 08:26:20'),
(270, 1, 291, 'write', '2019-03-21 08:27:05', '2019-03-21 08:27:05'),
(271, 1, 292, 'write', '2019-03-21 08:29:38', '2019-03-21 08:29:38'),
(272, 1, 293, 'write', '2019-03-21 08:29:49', '2019-03-21 08:29:49'),
(273, 1, 294, 'write', '2019-03-21 08:29:58', '2019-03-21 08:29:58'),
(274, 1, 295, 'write', '2019-03-21 08:30:46', '2019-03-21 08:30:46'),
(278, 1, 299, 'write', '2019-03-26 06:38:13', '2019-03-26 06:38:13'),
(279, 1, 300, 'write', '2019-03-26 06:38:38', '2019-03-26 06:38:38'),
(280, 1, 301, 'write', '2019-03-26 06:38:53', '2019-03-26 06:38:53'),
(281, 1, 302, 'write', '2019-03-26 06:39:10', '2019-03-26 06:39:10'),
(282, 1, 303, 'write', '2019-03-26 06:40:37', '2019-03-26 06:40:37'),
(283, 1, 304, 'write', '2019-03-26 06:40:56', '2019-03-26 06:40:56'),
(284, 1, 305, 'write', '2019-03-30 07:45:05', '2019-03-30 07:45:05'),
(285, 1, 306, 'write', '2019-03-30 07:45:26', '2019-03-30 07:45:26'),
(286, 1, 307, 'write', '2019-03-30 07:45:59', '2019-03-30 07:45:59'),
(287, 1, 308, 'write', '2019-03-30 07:46:18', '2019-03-30 07:46:18'),
(288, 1, 309, 'write', '2019-03-30 07:46:36', '2019-03-30 07:46:36'),
(289, 1, 310, 'write', '2019-03-30 07:47:13', '2019-03-30 07:47:13'),
(290, 1, 311, 'write', '2019-03-30 07:48:40', '2019-03-30 07:48:40'),
(291, 1, 312, 'write', '2019-03-30 07:50:36', '2019-03-30 07:50:36'),
(292, 1, 313, 'write', '2019-03-30 09:46:53', '2019-03-30 09:46:53'),
(293, 1, 314, 'write', '2019-03-31 07:31:55', '2019-03-31 07:31:55'),
(294, 2, 1, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(295, 2, 2, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(296, 2, 3, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(297, 2, 4, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(298, 2, 5, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(299, 2, 289, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(300, 2, 290, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(301, 2, 291, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(302, 2, 292, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(303, 2, 293, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(304, 2, 294, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(305, 2, 295, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(306, 2, 314, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(307, 2, 6, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(308, 2, 7, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(309, 2, 8, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(310, 2, 9, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(311, 2, 10, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(312, 2, 11, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(313, 2, 12, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(314, 2, 13, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(315, 2, 14, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(316, 2, 15, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(317, 2, 16, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(318, 2, 17, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(319, 2, 18, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(320, 2, 19, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(321, 2, 20, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(322, 2, 21, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(323, 2, 22, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(324, 2, 23, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(325, 2, 24, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(326, 2, 25, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(327, 2, 26, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(328, 2, 27, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(329, 2, 28, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(330, 2, 29, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(331, 2, 30, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(332, 2, 31, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(333, 2, 32, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(334, 2, 33, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(335, 2, 34, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(336, 2, 35, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(337, 2, 36, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(338, 2, 37, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(339, 2, 38, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(340, 2, 39, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(341, 2, 40, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(342, 2, 41, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(343, 2, 42, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(344, 2, 43, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(345, 2, 44, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(346, 2, 45, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(347, 2, 46, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(348, 2, 47, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(349, 2, 48, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(350, 2, 49, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(351, 2, 50, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(352, 2, 51, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(353, 2, 108, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(354, 2, 109, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(355, 2, 110, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(356, 2, 111, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(357, 2, 112, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(358, 2, 113, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(359, 2, 114, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(360, 2, 115, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(361, 2, 116, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(362, 2, 117, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(363, 2, 118, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(364, 2, 119, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(365, 2, 120, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(366, 2, 121, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(367, 2, 122, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(368, 2, 123, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(369, 2, 124, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(370, 2, 125, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(371, 2, 126, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(372, 2, 127, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(373, 2, 128, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(374, 2, 129, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(375, 2, 149, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(376, 2, 150, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(377, 2, 152, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(378, 2, 153, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(379, 2, 160, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(380, 2, 161, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(381, 2, 162, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(382, 2, 163, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(383, 2, 164, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(384, 2, 165, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(385, 2, 166, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(386, 2, 167, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(387, 2, 168, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(388, 2, 170, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(389, 2, 171, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(390, 2, 172, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(391, 2, 173, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(392, 2, 176, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(393, 2, 177, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(394, 2, 178, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(395, 2, 179, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(396, 2, 180, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(397, 2, 181, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(398, 2, 185, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(399, 2, 186, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(400, 2, 187, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(401, 2, 188, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(402, 2, 189, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(403, 2, 190, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(404, 2, 252, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(405, 2, 271, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(406, 2, 272, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(407, 2, 286, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(408, 2, 287, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(409, 2, 303, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(410, 2, 304, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(411, 2, 299, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(412, 2, 300, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(413, 2, 301, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(414, 2, 302, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(415, 2, 311, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(416, 2, 305, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(417, 2, 306, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(418, 2, 307, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(419, 2, 308, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(420, 2, 309, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(421, 2, 310, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(422, 2, 312, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(423, 2, 313, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(424, 4, 1, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(425, 4, 2, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(426, 4, 3, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(427, 4, 4, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(428, 4, 5, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(429, 4, 289, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(430, 4, 290, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(431, 4, 291, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(432, 4, 292, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(433, 4, 293, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(434, 4, 294, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(435, 4, 295, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(436, 4, 314, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(437, 4, 6, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(438, 4, 7, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(439, 4, 8, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(440, 4, 9, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(441, 4, 10, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(442, 4, 11, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(443, 4, 12, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(444, 4, 13, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(445, 4, 14, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(446, 4, 15, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(447, 4, 16, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(448, 4, 17, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(449, 4, 18, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(450, 4, 19, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(451, 4, 20, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(452, 4, 21, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(453, 4, 22, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(454, 4, 23, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(455, 4, 24, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(456, 4, 25, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(457, 4, 26, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(458, 4, 27, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(459, 4, 28, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(460, 4, 29, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(461, 4, 30, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(462, 4, 31, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(463, 4, 32, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(464, 4, 33, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(465, 4, 34, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(466, 4, 35, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(467, 4, 36, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(468, 4, 37, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(469, 4, 38, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(470, 4, 39, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(471, 4, 40, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(472, 4, 41, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(473, 4, 42, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(474, 4, 43, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(475, 4, 44, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(476, 4, 45, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(477, 4, 46, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(478, 4, 47, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(479, 4, 48, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(480, 4, 49, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(481, 4, 50, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(482, 4, 51, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(483, 4, 108, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(484, 4, 109, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(485, 4, 110, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(486, 4, 111, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(487, 4, 112, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(488, 4, 113, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(489, 4, 114, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(490, 4, 115, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(491, 4, 116, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(492, 4, 117, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(493, 4, 118, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(494, 4, 119, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(495, 4, 120, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(496, 4, 121, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(497, 4, 122, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(498, 4, 123, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(499, 4, 124, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(500, 4, 125, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(501, 4, 126, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(502, 4, 127, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(503, 4, 128, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(504, 4, 129, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(505, 4, 149, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(506, 4, 150, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(507, 4, 152, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(508, 4, 153, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(509, 4, 160, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(510, 4, 161, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(511, 4, 162, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(512, 4, 163, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(513, 4, 164, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(514, 4, 165, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(515, 4, 166, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(516, 4, 167, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(517, 4, 168, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(518, 4, 170, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(519, 4, 171, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(520, 4, 172, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(521, 4, 173, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(522, 4, 176, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(523, 4, 177, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(524, 4, 178, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(525, 4, 179, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(526, 4, 180, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(527, 4, 181, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(528, 4, 185, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(529, 4, 186, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(530, 4, 187, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(531, 4, 188, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(532, 4, 189, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(533, 4, 190, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(534, 4, 252, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(535, 4, 271, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(536, 4, 272, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(537, 4, 286, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(538, 4, 287, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(539, 4, 303, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(540, 4, 304, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(541, 4, 299, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(542, 4, 300, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(543, 4, 301, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(544, 4, 302, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(545, 4, 311, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(546, 4, 305, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(547, 4, 306, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(548, 4, 307, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(549, 4, 308, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(550, 4, 309, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(551, 4, 310, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(552, 4, 312, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(553, 4, 313, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(554, 3, 1, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(555, 3, 2, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(556, 3, 3, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(557, 3, 4, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(558, 3, 5, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(559, 3, 289, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(560, 3, 290, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(561, 3, 291, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(562, 3, 292, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(563, 3, 293, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(564, 3, 294, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(565, 3, 295, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(566, 3, 314, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(567, 3, 6, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(568, 3, 7, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(569, 3, 8, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(570, 3, 9, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(571, 3, 10, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(572, 3, 11, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(573, 3, 12, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(574, 3, 13, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(575, 3, 14, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(576, 3, 15, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(577, 3, 16, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(578, 3, 17, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(579, 3, 18, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(580, 3, 19, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(581, 3, 20, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(582, 3, 21, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(583, 3, 22, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(584, 3, 23, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(585, 3, 24, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(586, 3, 25, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(587, 3, 26, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(588, 3, 27, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(589, 3, 28, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(590, 3, 29, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(591, 3, 30, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(592, 3, 31, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(593, 3, 32, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(594, 3, 33, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(595, 3, 34, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(596, 3, 35, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(597, 3, 36, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(598, 3, 37, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(599, 3, 38, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(600, 3, 39, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(601, 3, 40, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(602, 3, 41, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(603, 3, 42, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(604, 3, 43, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(605, 3, 44, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(606, 3, 45, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(607, 3, 46, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(608, 3, 47, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(609, 3, 48, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(610, 3, 49, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(611, 3, 50, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(612, 3, 51, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(613, 3, 108, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(614, 3, 109, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(615, 3, 110, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(616, 3, 111, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(617, 3, 112, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(618, 3, 113, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(619, 3, 114, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(620, 3, 115, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(621, 3, 116, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(622, 3, 117, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(623, 3, 118, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(624, 3, 119, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(625, 3, 120, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(626, 3, 121, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(627, 3, 122, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(628, 3, 123, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(629, 3, 124, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(630, 3, 125, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(631, 3, 126, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(632, 3, 127, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(633, 3, 128, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(634, 3, 129, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(635, 3, 149, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(636, 3, 150, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(637, 3, 152, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(638, 3, 153, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(639, 3, 160, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(640, 3, 161, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(641, 3, 162, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(642, 3, 163, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(643, 3, 164, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(644, 3, 165, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(645, 3, 166, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(646, 3, 167, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(647, 3, 168, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(648, 3, 170, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(649, 3, 171, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(650, 3, 172, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(651, 3, 173, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(652, 3, 176, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(653, 3, 177, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(654, 3, 178, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(655, 3, 179, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(656, 3, 180, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(657, 3, 181, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(658, 3, 185, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(659, 3, 186, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(660, 3, 187, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(661, 3, 188, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(662, 3, 189, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(663, 3, 190, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(664, 3, 252, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(665, 3, 271, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(666, 3, 272, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(667, 3, 286, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(668, 3, 287, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(669, 3, 303, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(670, 3, 304, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(671, 3, 299, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(672, 3, 300, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(673, 3, 301, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(674, 3, 302, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(675, 3, 311, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(676, 3, 305, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(677, 3, 306, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(678, 3, 307, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(679, 3, 308, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(680, 3, 309, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(681, 3, 310, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(682, 3, 312, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(683, 3, 313, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(684, 1, 315, 'write', '2019-10-25 07:33:50', '2019-10-25 07:33:50'),
(685, 1, 316, 'write', '2019-10-25 07:34:41', '2019-10-25 07:34:41'),
(686, 1, 317, 'write', '2019-10-25 07:39:27', '2019-10-25 07:39:27'),
(687, 1, 318, 'write', '2019-10-25 07:40:22', '2019-10-25 07:40:22'),
(688, 1, 319, 'write', '2019-10-25 07:42:26', '2019-10-25 07:42:26'),
(689, 1, 320, 'write', '2019-10-25 07:42:53', '2019-10-25 07:42:53'),
(691, 1, 322, 'write', '2019-10-25 07:43:51', '2019-10-25 07:43:51'),
(692, 1, 323, 'write', '2019-10-25 07:44:37', '2019-10-25 07:44:37'),
(693, 1, 324, 'write', '2019-10-25 07:45:08', '2019-10-25 07:45:08'),
(694, 1, 325, 'write', '2019-10-25 07:46:47', '2019-10-25 07:46:47'),
(695, 1, 326, 'write', '2019-10-25 07:47:09', '2019-10-25 07:47:09'),
(696, 1, 327, 'write', '2019-10-25 07:47:45', '2019-10-25 07:47:45'),
(697, 1, 328, 'write', '2019-10-25 07:48:05', '2019-10-25 07:48:05'),
(698, 1, 329, 'write', '2019-10-25 07:48:22', '2019-10-25 07:48:22'),
(699, 1, 330, 'write', '2019-10-25 07:49:00', '2019-10-25 07:49:00'),
(700, 1, 331, 'write', '2019-10-25 07:49:15', '2019-10-25 07:49:15'),
(701, 1, 332, 'write', '2019-10-26 02:49:18', '2019-10-26 02:49:18'),
(702, 1, 333, 'write', '2019-10-26 02:51:50', '2019-10-26 02:51:50'),
(703, 1, 334, 'write', '2019-10-26 18:23:07', '2019-10-26 18:23:07'),
(704, 1, 335, 'write', '2019-10-26 18:30:49', '2019-10-26 18:30:49'),
(705, 1, 336, 'write', '2019-10-26 18:31:48', '2019-10-26 18:31:48'),
(706, 1, 337, 'write', '2019-10-26 18:47:10', '2019-10-26 18:47:10'),
(707, 1, 338, 'write', '2019-10-26 18:48:41', '2019-10-26 18:48:41'),
(708, 1, 339, 'write', '2019-10-30 04:40:31', '2019-10-30 04:40:31'),
(709, 1, 340, 'write', '2019-10-30 04:43:23', '2019-10-30 04:43:23'),
(710, 1, 341, 'write', '2019-10-30 04:43:35', '2019-10-30 04:43:35'),
(711, 1, 342, 'write', '2019-10-30 04:43:53', '2019-10-30 04:43:53'),
(712, 1, 343, 'write', '2019-10-30 04:44:13', '2019-10-30 04:44:13'),
(713, 1, 344, 'write', '2019-10-30 04:44:51', '2019-10-30 04:44:51'),
(714, 1, 345, 'write', '2019-10-30 04:45:19', '2019-10-30 04:45:19'),
(715, 1, 346, 'write', '2019-10-30 04:45:35', '2019-10-30 04:45:35'),
(716, 1, 347, 'write', '2019-10-30 04:45:46', '2019-10-30 04:45:46'),
(717, 1, 348, 'write', '2019-10-30 04:45:59', '2019-10-30 04:45:59'),
(718, 1, 349, 'write', '2019-10-30 05:07:43', '2019-10-30 05:07:43'),
(719, 1, 350, 'write', '2019-10-30 05:09:08', '2019-10-30 05:09:08'),
(720, 1, 351, 'write', '2019-10-30 05:09:22', '2019-10-30 05:09:22'),
(721, 1, 352, 'write', '2019-10-30 05:09:35', '2019-10-30 05:09:35'),
(722, 1, 353, 'write', '2019-10-30 05:09:51', '2019-10-30 05:09:51'),
(723, 1, 354, 'write', '2019-10-30 05:10:22', '2019-10-30 05:10:22'),
(724, 1, 355, 'write', '2019-10-30 05:10:36', '2019-10-30 05:10:36'),
(725, 1, 356, 'write', '2019-10-30 05:11:13', '2019-10-30 05:11:13'),
(726, 1, 357, 'write', '2019-10-30 05:11:23', '2019-10-30 05:11:23'),
(727, 1, 358, 'write', '2019-10-30 05:11:41', '2019-10-30 05:11:41'),
(728, 1, 359, 'write', '2019-10-30 05:11:48', '2019-10-30 05:11:48'),
(729, 1, 360, 'write', '2019-10-30 05:12:06', '2019-10-30 05:12:06'),
(730, 1, 361, 'write', '2019-10-30 05:12:15', '2019-10-30 05:12:15'),
(731, 1, 362, 'write', '2019-10-30 05:12:39', '2019-10-30 05:12:39'),
(732, 1, 363, 'write', '2019-10-30 05:12:46', '2019-10-30 05:12:46'),
(733, 1, 364, 'write', '2019-10-30 05:13:08', '2019-10-30 05:13:08'),
(734, 1, 365, 'write', '2019-10-30 05:13:16', '2019-10-30 05:13:16'),
(735, 1, 366, 'write', '2019-10-30 05:13:55', '2019-10-30 05:13:55'),
(736, 1, 367, 'write', '2019-10-30 05:14:14', '2019-10-30 05:14:14'),
(737, 1, 368, 'write', '2019-10-30 05:14:44', '2019-10-30 05:14:44'),
(738, 1, 369, 'write', '2019-10-30 05:14:57', '2019-10-30 05:14:57'),
(739, 1, 370, 'write', '2019-10-30 05:15:15', '2019-10-30 05:15:15'),
(740, 1, 371, 'write', '2019-10-30 05:15:49', '2019-10-30 05:15:49'),
(741, 1, 372, 'write', '2019-10-30 05:16:35', '2019-10-30 05:16:35'),
(742, 1, 373, 'write', '2019-10-30 05:17:41', '2019-10-30 05:17:41'),
(743, 1, 374, 'write', '2019-10-30 05:19:24', '2019-10-30 05:19:24'),
(744, 1, 375, 'write', '2019-10-30 05:20:03', '2019-10-30 05:20:03'),
(745, 1, 376, 'write', '2019-10-30 05:20:23', '2019-10-30 05:20:23'),
(746, 1, 377, 'write', '2019-10-30 05:20:30', '2019-10-30 05:20:30'),
(747, 1, 378, 'write', '2019-10-30 05:20:39', '2019-10-30 05:20:39'),
(748, 1, 379, 'write', '2019-10-30 05:22:06', '2019-10-30 05:22:06'),
(749, 1, 380, 'write', '2019-10-30 05:22:15', '2019-10-30 05:22:15'),
(750, 1, 381, 'write', '2019-10-30 05:22:39', '2019-10-30 05:22:39'),
(751, 1, 382, 'write', '2019-10-30 05:23:23', '2019-10-30 05:23:23'),
(752, 1, 383, 'write', '2019-10-30 05:23:32', '2019-10-30 05:23:32'),
(753, 1, 384, 'write', '2019-10-30 05:23:41', '2019-10-30 05:23:41'),
(754, 1, 385, 'write', '2019-10-30 05:23:57', '2019-10-30 05:23:57'),
(755, 1, 386, 'write', '2019-10-30 05:24:48', '2019-10-30 05:24:48'),
(756, 1, 387, 'write', '2019-10-30 05:24:55', '2019-10-30 05:24:55'),
(757, 1, 388, 'write', '2019-10-30 05:25:14', '2019-10-30 05:25:14'),
(758, 1, 389, 'write', '2019-10-30 05:25:23', '2019-10-30 05:25:23'),
(759, 1, 390, 'write', '2019-10-30 05:25:30', '2019-10-30 05:25:30'),
(760, 1, 391, 'write', '2019-10-30 05:25:52', '2019-10-30 05:25:52'),
(761, 1, 392, 'write', '2019-10-30 05:26:01', '2019-10-30 05:26:01'),
(762, 1, 393, 'write', '2019-10-30 05:26:08', '2019-10-30 05:26:08'),
(763, 1, 394, 'write', '2019-10-30 05:26:22', '2019-10-30 05:26:22'),
(764, 1, 395, 'write', '2019-10-30 05:26:29', '2019-10-30 05:26:29'),
(765, 1, 396, 'write', '2019-10-30 05:26:37', '2019-10-30 05:26:37'),
(766, 1, 397, 'write', '2019-10-30 05:27:24', '2019-10-30 05:27:24'),
(767, 1, 398, 'write', '2019-10-30 05:27:32', '2019-10-30 05:27:32'),
(768, 1, 399, 'write', '2019-10-30 05:27:40', '2019-10-30 05:27:40'),
(769, 1, 400, 'write', '2019-10-30 05:27:58', '2019-10-30 05:27:58'),
(770, 1, 401, 'write', '2019-10-30 05:28:05', '2019-10-30 05:28:05'),
(771, 1, 402, 'write', '2019-10-30 05:28:15', '2019-10-30 05:28:15'),
(772, 1, 403, 'write', '2019-10-30 05:28:32', '2019-10-30 05:28:32'),
(773, 1, 404, 'write', '2019-10-30 05:28:43', '2019-10-30 05:28:43'),
(774, 1, 405, 'write', '2019-10-30 05:28:58', '2019-10-30 05:28:58'),
(775, 1, 406, 'write', '2019-10-30 05:29:22', '2019-10-30 05:29:22'),
(776, 1, 407, 'write', '2019-10-30 05:29:38', '2019-10-30 05:29:38'),
(777, 1, 408, 'write', '2019-10-30 06:33:03', '2019-10-30 06:33:03');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `key` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `deleted_at`, `created_at`, `updated_at`, `key`, `value`) VALUES
(404, NULL, NULL, NULL, '_token', 'hstHhenXoOwDFNxvu71qturoPIpGphDobjs0GUDD'),
(405, NULL, NULL, NULL, 'logo', '/public/uploads/files/Logo/anzedo.jpg'),
(406, NULL, NULL, NULL, 'logo_50', '/public/uploads/files/Logo/anzedo.png'),
(407, NULL, NULL, NULL, 'favicon', '/public/uploads/files/Logo/ANZEDO_FavIcon_16x16.png'),
(408, NULL, NULL, NULL, 'ads_1', ''),
(409, NULL, NULL, NULL, 'ads_2', ''),
(410, NULL, NULL, NULL, 'ads_3', ''),
(411, NULL, NULL, NULL, 'company_name', 'CÔNG TY TNHH ANZEDO VIỆT NAM'),
(412, NULL, NULL, NULL, 'company_address', 'Tầng 1,  ANZEDO Tower,  9B Vũ Ngọc Phan,  Phường 13, Quận Bình Thạnh, Thành Phố Hồ Chí Minh'),
(413, NULL, NULL, NULL, 'company_address1', '219E Nơ Trang Long, Phường 12, Quận Bình Thạnh, TP.Hồ Chí Minh'),
(414, NULL, NULL, NULL, 'company_address2', ''),
(415, NULL, NULL, NULL, 'company_about', ''),
(416, NULL, NULL, NULL, 'company_about_short', 'Anzedo mang trong mình khát vọng đem đến cho cánh mày râu Việt Nam những bộ vest đỉnh cao, dẫn đầu cả về chất lượng lẫn phong cách. '),
(417, NULL, NULL, NULL, 'company_content', ''),
(418, NULL, NULL, NULL, 'google_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.5554924218727!2d106.67845151428696!3d10.768700462282155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f21de3e19a3%3A0x69e3e6766ae567e6!2zNjU2IE5ndXnhu4VuIMSQw6xuaCBDaGnhu4N1'),
(419, NULL, NULL, NULL, 'company_email', 'tuyendung@anzedo.net'),
(420, NULL, NULL, NULL, 'company_phone', '02822.691.555'),
(421, NULL, NULL, NULL, 'company_hotline', ' (+84) 899.486.333'),
(422, NULL, NULL, NULL, 'company_fax', '02822.691.777'),
(423, NULL, NULL, NULL, 'facebook', 'https://www.facebook.com/anzedovn/'),
(424, NULL, NULL, NULL, 'fanpage_facebook', ''),
(425, NULL, NULL, NULL, 'instagram', 'https://www.facebook.com/anzedovn/'),
(426, NULL, NULL, NULL, 'youtube', 'https://www.facebook.com/anzedovn/'),
(427, NULL, NULL, NULL, 'googleplus', 'https://www.facebook.com/anzedovn/'),
(428, NULL, NULL, NULL, 'linkedin', 'https://www.facebook.com/anzedovn/'),
(429, NULL, NULL, NULL, 'twitter', 'https://www.facebook.com/anzedovn/'),
(430, NULL, NULL, NULL, 'pinterest', 'https://www.facebook.com/anzedovn/'),
(431, NULL, NULL, NULL, 'copyright', '© 2019 - ANZEDO - TUYỂN DỤNG'),
(432, NULL, NULL, NULL, 'web_status', 'off');

-- --------------------------------------------------------

--
-- Table structure for table `sliderimages`
--

DROP TABLE IF EXISTS `sliderimages`;
CREATE TABLE `sliderimages` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `key` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliderimages`
--

INSERT INTO `sliderimages` (`id`, `deleted_at`, `created_at`, `updated_at`, `key`, `image`, `title`, `content`, `html`) VALUES
(1, NULL, '2019-10-25 06:35:25', '2019-10-25 06:43:37', 6, '/public/uploads/images/banner/banner.jpg', 'HOME_SLIDE', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `key` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `deleted_at`, `created_at`, `updated_at`, `key`, `status`) VALUES
(6, NULL, '2019-10-25 06:33:42', '2019-10-25 06:33:42', 'HOME_SLIDE', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tracnghiems`
--

DROP TABLE IF EXISTS `tracnghiems`;
CREATE TABLE `tracnghiems` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `type_tn` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tracnghiem_options`
--

DROP TABLE IF EXISTS `tracnghiem_options`;
CREATE TABLE `tracnghiem_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `id_tracnghiem` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `answer` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tracnghiem_types`
--

DROP TABLE IF EXISTS `tracnghiem_types`;
CREATE TABLE `tracnghiem_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type_field` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tracnghiem_types`
--

INSERT INTO `tracnghiem_types` (`id`, `deleted_at`, `created_at`, `updated_at`, `type_field`) VALUES
(1, NULL, '2019-10-26 18:32:25', '2019-10-26 18:32:36', 'Checked'),
(2, NULL, '2019-10-26 18:32:41', '2019-10-26 18:32:41', 'Reply');

-- --------------------------------------------------------

--
-- Table structure for table `tuyendungs`
--

DROP TABLE IF EXISTS `tuyendungs`;
CREATE TABLE `tuyendungs` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `slug` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(256) COLLATE utf8_unicode_ci DEFAULT '[]',
  `rank` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `career` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `department` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_day` int(10) UNSIGNED DEFAULT NULL,
  `form` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `degree` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tuyendungs`
--

INSERT INTO `tuyendungs` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `slug`, `address`, `rank`, `salary`, `career`, `department`, `test_day`, `form`, `degree`, `deadline`, `content`) VALUES
(1, NULL, '2019-11-03 19:26:04', '2019-11-04 07:38:55', 'Chuyên viên Content Marketing ', 'chuyen-vien-content-marketing', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6-10 ', '[\"1\",\"2\",\"3\"]', 'Khối văn phòng', 3, 'Toàn thời gian ', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li><strong>Địa điểm l&agrave;m việc:</strong></li>\r\n</ul>\r\n<p>Trụ sở c&ocirc;ng ty: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh.</p>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>S&aacute;ng tạo nội dung phục vụ Marketing Online</li>\r\n<li>Nghi&ecirc;n cứu, t&igrave;m hiểu, b&aacute;o c&aacute;o v&agrave; đa dạng h&oacute;a c&aacute;c h&igrave;nh thức, xu hướng Content Marketing tr&ecirc;n MXH</li>\r\n<li>Đề xuất phương &aacute;n sản xuất v&agrave; triển khai content theo c&aacute;c xu hướng đ&atilde; nắm bắt được</li>\r\n<li>Viết b&agrave;i v&agrave; kiểm so&aacute;t nội dung, &yacute; tưởng video, h&igrave;nh ảnh, banner tr&ecirc;n Fanpage ... nhằm mục đ&iacute;ch quảng b&aacute;, marketing sản phẩm &amp; tiếp cận kh&aacute;ch h&agrave;ng hiệu quả hơn.</li>\r\n<li>Phối hợp với bộ phận Media v&agrave; Stylist để sản xuất content ảnh/ video</li>\r\n<li>Thực hiện c&aacute;c c&ocirc;ng việc kh&aacute;c theo y&ecirc;u cầu của cấp tr&ecirc;n.</li>\r\n</ol>\r\n<p><strong>&nbsp;Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Trung cấp trở l&ecirc;n</li>\r\n<li>Nam/nữ, tuổi từ 23 &ndash; 33</li>\r\n<li>C&oacute; kinh nghiệm Content marketing tr&ecirc;n 6 th&aacute;ng, ưu ti&ecirc;n ng&agrave;nh thời trang, l&agrave;m đẹp, spa, tmv...</li>\r\n<li>C&oacute; khả năng viết content b&aacute;n h&agrave;ng trực tiếp</li>\r\n<li>Y&ecirc;u th&iacute;ch, am hiểu về content marketting v&agrave; c&oacute; khả năng t&igrave;m kiếm, ph&acirc;n t&iacute;ch, tổng hợp th&ocirc;ng tin tr&ecirc;n MXH.</li>\r\n<li>Lu&ocirc;n chủ động, linh hoạt, s&aacute;ng tạo, c&oacute; tr&aacute;ch nhiệm trong c&ocirc;ng việc</li>\r\n<li>Nắm bắt trend tốt, c&oacute; khả năng s&aacute;ng tạo nội dung theo trend</li>\r\n<li>C&oacute; kĩ năng l&agrave;m việc nh&oacute;m.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(2, NULL, '2019-11-03 19:48:24', '2019-11-03 19:59:21', 'Chuyên viên Tuyển dụng', 'chuyen-vien-tuyen-dung', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 10 ', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 3, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Kết hợp với c&aacute;c ph&ograve;ng ban để x&acirc;y dựng ti&ecirc;u ch&iacute; tuyển dụng</li>\r\n<li>Lập kế hoạch tuyển dụng, dự tr&ugrave; chi ph&iacute; trong qu&aacute; tr&igrave;nh tuyển dụng</li>\r\n<li>Phỏng vấn ứng vi&ecirc;n theo những ti&ecirc;u ch&iacute; ph&ugrave; hợp với y&ecirc;u cầu c&ocirc;ng việc</li>\r\n<li>Tiếp nhận nh&acirc;n vi&ecirc;n mới; quản l&yacute; t&agrave;i khoản, trang tuyển dụng của C&ocirc;ng ty</li>\r\n<li>C&aacute;c c&ocirc;ng việc kh&aacute;c theo chỉ đạo từ Trưởng ph&ograve;ng.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>&nbsp;Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng, Đại học c&aacute;c chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n</ol>\r\n<ol start=\"2\" type=\"1\">\r\n<li>Nam / nữ, tuổi từ 22 - 30 tuổi.</li>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>Ưu ti&ecirc;n ứng vi&ecirc;n đ&atilde; c&oacute; kinh nghiệm tuyển dụng trong lĩnh vực b&aacute;n lẻ, thời trang</li>\r\n<li>C&oacute; khả năng đi c&ocirc;ng t&aacute;c</li>\r\n<li>Nhiệt huyết, năng động, tinh thần l&agrave;m việc nh&oacute;m tốt</li>\r\n</ol>\r\n<ol start=\"7\">\r\n<li>Kỹ năng tổ chức, quản l&yacute; c&ocirc;ng việc tốt</li>\r\n</ol>\r\n</body>\r\n</html>'),
(3, NULL, '2019-11-03 20:07:51', '2019-11-03 20:07:51', 'Nhân viên Kỹ thuật điện/IT Helpdesk', 'nhan-vien-k-thuat-dien-it-helpdesk', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '7-9', '[\"1\",\"2\",\"3\"]', 'Khối Cửa hàng', 3, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Tiếp nhận c&aacute;c th&ocirc;ng tin về hư hỏng của hệ thống điện chiếu s&aacute;ng v&agrave; c&aacute;c thiết bị khối cửa h&agrave;ng, l&ecirc;n phương &aacute;n xử l&yacute; v&agrave; tiến h&agrave;nh sửa chữa nhanh ch&oacute;ng.</li>\r\n<li>Gi&aacute;m s&aacute;t điện c&ocirc;ng tr&igrave;nh.</li>\r\n<li>Bảo h&agrave;nh c&aacute;c thiết bị đi&ecirc;n.</li>\r\n<li>C&acirc;n đấu, điện 3 pha, điện c&ocirc;ng nghiệp</li>\r\n<li>Xử l&yacute; c&aacute;c vấn đề về điện cho c&aacute;c CH trong hệ thống.</li>\r\n<li>C&oacute; thể đi c&ocirc;ng t&aacute;c tỉnh ( gi&aacute;m s&aacute;t c&ocirc;ng tr&igrave;nh từ 15 - 20 ng&agrave;y )</li>\r\n<li>Cụ thể sẽ trao đổi khi phỏng vấn</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Nam &ndash; Dưới 35 tuổi</li>\r\n<li>Tốt nghiệp Trung cấp trở l&ecirc;n, chuy&ecirc;n ng&agrave;nh Kỹ thuật điện, ưu ti&ecirc;n Trung cấp điện hoặc cao đẳng c&ocirc;ng nghiệp khoa kỹ thuật điện</li>\r\n<li>Nhanh nhẹn, hoạt b&aacute;t, trung thực, c&oacute; tinh thần cầu tiến, ham học hỏi.</li>\r\n</ol>\r\n<ol start=\"4\">\r\n<li>Chịu &aacute;p lực c&ocirc;ng việc tốt, nhiệt t&igrave;nh, cẩn thận</li>\r\n<li>C&oacute; &iacute;t nhất 02 năm kinh nghiệm l&agrave;m việc trực tiếp tại c&ocirc;ng tr&igrave;nh ( cả điện d&acirc;n dụng v&agrave; c&ocirc;ng nghiệp ) .</li>\r\n</ol>\r\n<ol start=\"6\" type=\"1\">\r\n<li>C&oacute; khả năng đi c&ocirc;ng t&aacute;c xa .</li>\r\n</ol>\r\n<ol start=\"7\">\r\n<li>C&oacute; kinh nghiệm về điện 3 pha.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(4, NULL, '2019-11-03 22:52:26', '2019-11-03 22:52:26', 'Nhân viên Hành chính - Nhân sự', 'nhan-vien-hanh-chinh-nhan-su', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6-8', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>Địa điểm l&agrave;m việc:</li>\r\n</ul>\r\n<p>Trụ sở ch&iacute;nh : Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</p>\r\n<p><strong>&nbsp;Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>L&agrave; đ&acirc;̀u m&ocirc;́i ti&ecirc;́p nh&acirc;̣n v&agrave; gửi đi c&ocirc;ng văn , giấy tờ của c&ocirc;ng ty. Có nghĩa vụ chuyển cấp có thẩm quyềt giải quy&ecirc;́t khi c&acirc;̀n thi&ecirc;́t hoặc có y&ecirc;u c&acirc;̀u.</li>\r\n<li>Lưu trữ, sắp x&ecirc;́p th&ocirc;ng tin c&ocirc;ng văn, giấy tờ, hợp đồng, t&agrave;i liệu li&ecirc;n quan.</li>\r\n<li>Ti&ecirc;́p nh&acirc;̣n đơn xin đ&ecirc;́n muộn , về sớm, đơn xin nghỉ &ocirc;́m, nghỉ ph&eacute;p, đơn xét tăng lương, &hellip; các loại gi&acirc;́y tờ c&ocirc;ng văn khác của Nh&acirc;n vi&ecirc;n trong c&ocirc;ng ty.</li>\r\n<li>Theo d&otilde;i v&agrave; quản lý lịch l&agrave;m việc của Nh&acirc;n sự trong doanh nghi&ecirc;̣p. Theo d&otilde;i ch&ecirc;́ độ bảo hiểm y t&ecirc;́, bảo hiểm x&atilde; hội cho nh&acirc;n vi&ecirc;n c&ocirc;ng ty. Ti&ecirc;́p nh&acirc;̣n các nhu c&acirc;̀u v&ecirc;̀ đi&ecirc;̀u chỉnh phúc lợi , c&aacute;c ch&ecirc;́ độ đ&atilde;i ngộ v&agrave; ch&iacute;nh s&aacute;ch thưởng phạt kh&aacute;c để đảm bảo quyền lợi của nh&acirc;n vi&ecirc;n trong c&ocirc;ng ty.</li>\r\n<li>Phụ tr&aacute;ch Photocopy , c&aacute;c vấn đề in ấn trong doanh nghiệp&nbsp; (gi&acirc;́y, mực, gọi kỹ thu&acirc;̣t sửa chữa m&aacute;y in khi máy hỏng, &hellip;), văn phòng ph&acirc;̉m (bảng, m&aacute;y chi&ecirc;́u, &hellip;).</li>\r\n<li>Phụ tr&aacute;ch mua sắm c&aacute;c t&agrave;i sản , thi&ecirc;́t bi hay đ&ocirc;̀ dùng th&ocirc;ng thường trong doanh nghi&ecirc;̣p . H&ocirc;̃ trợ quản lý cơ sở v&acirc;̣t ch&acirc;́t bằng cách lưu trữ các hóa đơn , chứng từ, hợp đ&ocirc;̀ng có li&ecirc;n quan tới mua bán , thu&ecirc;, mượn cơ sở v&acirc;̣t ch&acirc;́t, m&aacute;y móc thi&ecirc;́t bị trong c&ocirc;ng ty.</li>\r\n<li>L&agrave;m h&acirc;̣u c&acirc;̀n chuẩn bị đồ đạc cho c&aacute;c sự kiện của c&ocirc;ng ty.</li>\r\n<li>T&ocirc;̉ chức sinh nh&acirc;̣t cho các thành vi&ecirc;n của c&ocirc;ng ty trong tháng.</li>\r\n</ol>\r\n<p><strong>&nbsp;Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>Ưu ti&ecirc;n ứng vi&ecirc;n đ&atilde; c&oacute; kinh nghiệm h&agrave;nh ch&iacute;nh &ndash; nh&acirc;n sự từ 1 năm trở l&ecirc;n</li>\r\n<li>Nhanh nhẹn, giao tiếp tốt</li>\r\n<li>Kỹ năng tổ chức, quản l&yacute; c&ocirc;ng việc tốt</li>\r\n<li>C&oacute; khả năng chịu &aacute;p lực trong c&ocirc;ng việc</li>\r\n<li>Sử dụng th&agrave;nh thạo vi t&iacute;nh văn ph&ograve;ng, c&aacute;c phần mềm li&ecirc;n quan đến c&ocirc;ng việc.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>'),
(5, NULL, '2019-11-03 22:58:00', '2019-11-03 22:58:00', 'Nhân viên sửa đồ kiêm bán hàng', 'nhan-vien-sua-do-kiem-ban-hang', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '5-8', '[\"1\",\"2\",\"3\"]', 'Khối Cửa hàng', 3, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc: </strong>Hệ thống showroom Anzedo tr&ecirc;n to&agrave;n quốc.</p>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Thời gian l&agrave;m việc: 8h00 - 21h30</li>\r\n<li>Sửa chữa c&aacute;c sản phẩm kh&aacute;ch h&agrave;ng đ&atilde; mua.</li>\r\n<li>Bảo h&agrave;nh c&aacute;c sản phẩm theo y&ecirc;u cầu của kh&aacute;ch h&agrave;ng v&agrave; y&ecirc;u cầu của c&ocirc;ng ty.</li>\r\n<li>Tư vấn, ngắm đồ v&agrave; tư vấn sửa đồ cho kh&aacute;ch.</li>\r\n<li>Sửa đ&uacute;ng kỹ thuật, đảm bảo về mỹ thuật, kịp thời.</li>\r\n<li>Thường xuy&ecirc;n kiểm tra showroom chăm ch&uacute;t sản phẩm, hướng dẫn kh&aacute;ch h&agrave;ng xem thử đồ đ&uacute;ng c&aacute;ch.</li>\r\n<li>Vệ sinh khu vực l&agrave;m việc h&agrave;ng ng&agrave;y. Trưng b&agrave;y sản phẩm l&ecirc;n c&aacute;c quầy kệ, gi&aacute; treo, theo đ&uacute;ng quy định v&agrave; thẩm mỹ của Showroom.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Mức Lương: 5-8 Triệu + Doanh số + Thưởng n&oacute;ng</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Nhanh nhẹn, hoạt b&aacute;t, trung thực, c&oacute; tinh thần cầu tiến, ham học hỏi.</li>\r\n<li>C&oacute; kinh nghiệm cắt may, sửa chữa c&aacute;c sản phẩm thời trang, đặc biệt l&agrave; thời trang cao cấp</li>\r\n<li>Sử dụng th&agrave;nh thạo c&aacute;c loại m&aacute;y may, c&aacute;c m&aacute;y chuy&ecirc;n d&ugrave;ng kh&aacute;c.</li>\r\n</ol>\r\n<ol start=\"4\">\r\n<li>Chịu &aacute;p lực c&ocirc;ng việc tốt.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(6, NULL, '2019-11-03 23:04:54', '2019-11-04 00:02:47', 'Cửa hàng trưởng', 'cua-hang-truong', '[\"1\",\"2\",\"3\"]', 'Quản lý', '8-12', '[\"1\",\"2\",\"3\"]', 'Khối Của hàng', 4, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc: </strong>Hệ thống showroom Anzedo tr&ecirc;n to&agrave;n quốc.</p>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Thời gian l&agrave;m việc: 10h00 - 21h30</li>\r\n<li>Quản l&yacute; nh&acirc;n sự trong showroom</li>\r\n<li>Ho&agrave;n th&agrave;nh chỉ ti&ecirc;u về doanh số được giao.</li>\r\n<li>Kiểm tra, gi&aacute;m s&aacute;t th&aacute;i độ v&agrave; tinh thần l&agrave;m việc của từng nh&acirc;n vi&ecirc;n của m&igrave;nh quản l&yacute;.</li>\r\n<li>Đ&agrave;o tạo v&agrave; huấn luyện nh&acirc;n vi&ecirc;n thuộc quyền quản l&yacute; của m&igrave;nh.</li>\r\n<li>Trực tiếp huấn luyện NVBH thử việc.</li>\r\n<li>Trực cửa h&agrave;ng theo lịch đ&atilde; ph&acirc;n c&ocirc;ng.</li>\r\n<li>Đảm bảo chất lượng dịch vụ kh&aacute;ch h&agrave;ng.</li>\r\n<li>Đảm bảo duy tr&igrave; h&igrave;nh ảnh, trưng b&agrave;y h&agrave;ng h&oacute;a, sản phẩm tại showroom.</li>\r\n<li>Chi tiết c&ocirc;ng việc trao đổi khi phỏng vấn.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Mức Lương: 7 - 10 Triệu + Doanh số + Thưởng n&oacute;ng</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>&nbsp;Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Tốt nghiệp Trung cấp trở l&ecirc;n</li>\r\n<li>Hiểu biết về ng&agrave;nh h&agrave;ng thời trang cao cấp</li>\r\n<li>Ưu ti&ecirc;n những ứng vi&ecirc;n đ&atilde; l&agrave;m quản l&yacute; trong ng&agrave;nh thời trang, b&aacute;n lẻ, dịch vụ l&agrave;m đẹp, ng&agrave;ng h&agrave;ng ti&ecirc;u d&ugrave;ng</li>\r\n<li>Khả năng giao tiếp, tư vấn b&aacute;n h&agrave;ng v&agrave; thuyết phục tốt.</li>\r\n<li>C&oacute; kỹ năng lập kế hoạch trong c&ocirc;ng việc</li>\r\n<li>C&oacute; kỹ năng quản l&yacute; v&agrave; đ&agrave;o tạo nh&acirc;n sự tốt</li>\r\n<li>C&oacute; kinh nghiệm quản l&yacute; sản phẩm h&agrave;ng h&oacute;a số lượng lớn</li>\r\n<li>Nhanh nhẹn, hoạt b&aacute;t, trung thực, c&oacute; tinh thần cầu tiến, ham học hỏi.</li>\r\n</ol>\r\n<ol start=\"9\">\r\n<li>Chịu được &aacute;p lực c&ocirc;ng việc.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(7, NULL, '2019-11-03 23:17:48', '2019-11-03 23:17:48', 'Kế toán bán hàng', 'ke-toan-ban-hang', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6-8', '[\"1\",\"2\",\"3\"]', 'Khối Cửa hàng', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong> Hệ thống showroom Anzedo tr&ecirc;n to&agrave;n quốc</p>\r\n<ul>\r\n<li>Miền Bắc: H&agrave; Nội</li>\r\n<li>Miền Trung: Đ&agrave; Nẵng</li>\r\n<li>Miền Nam: Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>L&agrave;m c&aacute;c nghiệp vụ xuất nhập h&agrave;ng h&oacute;a của Showroom (phần mềm v&agrave; thực tế)</li>\r\n<li>Giao tiếp với kh&aacute;ch h&agrave;ng; tư vấn, giới thiệu h&agrave;ng b&aacute;n cho kh&aacute;ch h&agrave;ng; chăm s&oacute;c kh&aacute;ch h&agrave;ng theo ti&ecirc;u chuẩn quy định</li>\r\n<li>Thực hiện c&ocirc;ng t&aacute;c thanh to&aacute;n cho kh&aacute;ch h&agrave;ng theo quy tr&igrave;nh</li>\r\n<li>Ghi nhận, cập nhật v&agrave; quản l&yacute; th&ocirc;ng tin kh&aacute;ch h&agrave;ng; sử dụng th&ocirc;ng tin kh&aacute;ch h&agrave;ng để l&agrave;m c&aacute;c loại thẻ ưu đ&atilde;i (thẻ VIP) cho kh&aacute;ch nếu c&oacute;</li>\r\n<li>Theo d&otilde;i, t&iacute;nh chiết khấu cho kh&aacute;ch h&agrave;ng.</li>\r\n<li>Kiểm k&ecirc; h&agrave;ng h&oacute;a trong cửa h&agrave;ng định kỳ số lượng, t&igrave;nh trạng sản phẩm...</li>\r\n<li>Lập b&aacute;o c&aacute;o cho cấp tr&ecirc;n</li>\r\n<li>Cập nhật gi&aacute; cả, sản phẩm mới.</li>\r\n<li>Chi tiết c&ocirc;ng việc sẽ trao đổi khi phỏng vấn</li>\r\n</ol>\r\n<p><strong>&nbsp;Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n c&aacute;c chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>Ưu ti&ecirc;n ứng vi&ecirc;n c&oacute; kinh nghiệm l&agrave;m việc Thu ng&acirc;n, Kế to&aacute;n b&aacute;n h&agrave;ng</li>\r\n<li>Th&agrave;nh thạo Tin học văn ph&ograve;ng.</li>\r\n<li>Trung thực, nhanh nhẹn, cẩn thận, c&oacute; tr&aacute;ch nhiệm</li>\r\n<li>C&oacute; khả năng xử l&yacute; t&igrave;nh huống v&agrave; giao tiếp tốt.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(8, NULL, '2019-11-03 23:27:17', '2019-11-03 23:27:17', 'Kế toán Văn phòng', 'ke-toan-van-phong', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6-10', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở C&ocirc;ng ty : Số 9B Vũ Ngọc Phan, phường 13, quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>&nbsp;Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Sắp xếp chứng từ sổ s&aacute;ch.</li>\r\n<li>Quản l&yacute; v&agrave; kiểm so&aacute;t to&agrave;n bộ c&aacute;c chứng từ, h&oacute;a đơn li&ecirc;n quan đến hoạt động của cửa h&agrave;ng (quỹ, h&agrave;ng h&oacute;a...)</li>\r\n<li>Li&ecirc;n hệ kh&aacute;ch h&agrave;ng để nhận h&oacute;a đơn, chứng từ; Nhập liệu h&oacute;a đơn, chứng từ v&agrave;o phần mềm Excel, phần mềm kế to&aacute;n</li>\r\n<li>Cập nhật thường xuy&ecirc;n, li&ecirc;n tục v&agrave; c&oacute; hệ thống v&agrave; ch&iacute;nh x&aacute;c c&aacute;c nghiệp vụ kinh tế ph&aacute;t sinh.</li>\r\n<li>Tổ chức, tham gia phối hợp c&ocirc;ng t&aacute;c thống k&ecirc;, kiểm k&ecirc; t&agrave;i sản định kỳ tại c&aacute;c đơn vị cơ sở.</li>\r\n<li>Hướng dẫn xử l&yacute; v&agrave; hạch to&aacute;n c&aacute;c nghiệp vụ kế to&aacute;n.</li>\r\n<li>Thống k&ecirc; v&agrave; tổng hợp số liệu kế to&aacute;n khi c&oacute; y&ecirc;u cầu.</li>\r\n<li>Cung cấp số liệu Cho Ban Gi&aacute;m Đốc hoặc c&aacute;c đơn vị chức năng khi c&oacute; y&ecirc;u cầu.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h00-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p>&nbsp;<strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng, Đại học c&aacute;c chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>C&oacute; &iacute;t nhất 1 năm kinh nghiệm ở vị tr&iacute; tương đương</li>\r\n<li>Th&agrave;nh thạo Tin học văn ph&ograve;ng.</li>\r\n<li>Trung thực, nhanh nhẹn, cẩn thận, c&oacute; tr&aacute;ch nhiệm</li>\r\n<li>C&oacute; khả năng xử l&yacute; t&igrave;nh huống v&agrave; giao tiếp tốt.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(9, NULL, '2019-11-03 23:52:07', '2019-11-03 23:59:25', 'Kế toán thuế', 'ke-toan-thue', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6-10', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul>\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, phường 13, quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Hạch to&aacute;n, kiểm tra chứng từ kế to&aacute;n v&agrave; c&aacute;c nghiệp vụ ph&aacute;t sinh</li>\r\n<li>Ho&agrave;n thiện hồ sơ kế to&aacute;n theo đ&uacute;ng luật kế to&aacute;n</li>\r\n<li>H&agrave;ng th&aacute;ng l&agrave;m b&aacute;o c&aacute;o thuế GTGT, thuế TNCN, thuấ thu nhập doanh nghiệp v&agrave; b&aacute;o c&aacute;o sử dụng h&oacute;a đơn</li>\r\n<li>L&agrave;m việc với cơ quan thuế, kiểm to&aacute;n, ng&acirc;n h&agrave;ng</li>\r\n<li>Cuối năm l&agrave;m b&aacute;o c&aacute;o t&agrave;i ch&iacute;nh, thuế, quyết to&aacute;n thuế TNCN</li>\r\n<li>Thống k&ecirc; v&agrave; tổng hợp số liệu kế to&aacute;n khi c&oacute; y&ecirc;u cầu</li>\r\n<li>Thực hiện c&aacute;c c&ocirc;ng việc kh&aacute;c li&ecirc;n quan hoặc theo chỉ đạo của cấp tr&ecirc;n.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>&nbsp;Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng, Đại học c&aacute;c chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>Th&agrave;nh thạo nghiệp vụ kế to&aacute;n, am hiểu c&aacute;c quy tr&igrave;nh về thuế, t&agrave;i ch&iacute;nh v&agrave; c&aacute;c ch&iacute;nh s&aacute;ch kh&aacute;c hiện h&agrave;nh</li>\r\n<li>C&oacute; &iacute;t nhất 1 năm kinh nghiệm ở vị tr&iacute; Kế to&aacute;n thuế</li>\r\n<li>Th&agrave;nh thạo Tin học văn ph&ograve;ng.</li>\r\n<li>Trung thực, nhanh nhẹn, cẩn thận, c&oacute; tr&aacute;ch nhiệm</li>\r\n<li>C&oacute; khả năng xử l&yacute; t&igrave;nh huống v&agrave; giao tiếp tốt.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(10, NULL, '2019-11-04 00:11:29', '2019-11-04 00:11:29', 'Cửa hàng phó', 'cua-hang-pho', '[\"1\",\"2\",\"3\"]', 'Quản lý', '6-10', '[\"1\",\"2\",\"3\"]', 'Khối Cửa hàng', 4, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm: Hệ thống showroom Anzedo tr&ecirc;n to&agrave;n quốc</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Miền Bắc: H&agrave; Nội</li>\r\n<li>Miền Trung: Đ&agrave; Nẵng</li>\r\n<li>Miền Nam: Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Thời gian l&agrave;m việc: 10h00 &ndash; 21h30</li>\r\n<li>Quản l&yacute; nh&acirc;n sự trong showroom</li>\r\n<li>Ho&agrave;n th&agrave;nh chỉ ti&ecirc;u về doanh số được giao.</li>\r\n<li>Đảm bảo chất lượng dịch vụ kh&aacute;ch h&agrave;ng.</li>\r\n<li>Gi&aacute;m s&aacute;t t&igrave;nh h&igrave;nh l&agrave;m việc v&agrave; tinh thần th&aacute;i độ của nh&acirc;n vi&ecirc;n tại cửa h&agrave;ng.</li>\r\n<li>Đảm bảo duy tr&igrave; h&igrave;nh ảnh, trưng b&agrave;y h&agrave;ng h&oacute;a, sản phẩm tại showroom.</li>\r\n<li>Chi tiết c&ocirc;ng việc trao đổi khi phỏng vấn.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Mức Lương: 6 - 10 Triệu + Doanh số + Thưởng n&oacute;ng</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Tốt nghiệp Trung cấp trở l&ecirc;n</li>\r\n<li>Hiểu biết về ng&agrave;nh h&agrave;ng thời trang cao cấp</li>\r\n<li>Ưu ti&ecirc;n những ứng vi&ecirc;n đ&atilde; l&agrave;m quản l&yacute; trong ng&agrave;nh thời trang, b&aacute;n lẻ, dịch vụ l&agrave;m đẹp, ng&agrave;ng h&agrave;ng ti&ecirc;u d&ugrave;ng</li>\r\n<li>Khả năng giao tiếp, tư vấn b&aacute;n h&agrave;ng v&agrave; thuyết phục tốt.</li>\r\n<li>C&oacute; kỹ năng lập kế hoạch trong c&ocirc;ng việc</li>\r\n<li>C&oacute; kỹ năng quản l&yacute; v&agrave; đ&agrave;o tạo nh&acirc;n sự tốt</li>\r\n<li>C&oacute; kinh nghiệm quản l&yacute; sản phẩm h&agrave;ng h&oacute;a số lượng lớn</li>\r\n<li>Nhanh nhẹn, hoạt b&aacute;t, trung thực, c&oacute; tinh thần cầu tiến, ham học hỏi.</li>\r\n</ol>\r\n<ol start=\"9\">\r\n<li>Chịu &aacute;p lực c&ocirc;ng việc tốt.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(11, NULL, '2019-11-04 00:18:35', '2019-11-04 00:18:35', 'Quản lý bộ phận Sale online', 'quan-l-bo-phan-sale-online', '[\"1\",\"2\",\"3\"]', 'Quản lý', '8-10', '[\"1\",\"2\",\"3\"]', 'Phòng Kinh doanh', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa Điểm L&agrave;m Việc</strong></p>\r\n<ul>\r\n<li>Trụ sở C&ocirc;ng ty: Số 9B Vũ Ngọc Phan, phường 13, quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Quản l&yacute; nh&acirc;n sự: trực tiếp tuyển dụng / đ&agrave;o tạo c&aacute;c vị tr&iacute; tại bộ phận như: Sale Online, website content, COD, quản l&yacute; kho,...</li>\r\n<li>Chịu tr&aacute;ch nhiệm về h&agrave;ng h&oacute;a &amp; h&igrave;nh ảnh h&agrave;ng h&oacute;a tại BP Online</li>\r\n<li>Nắm được số lượng / mẫu m&atilde; h&agrave;ng h&oacute;a c&oacute; trong kho BP Online</li>\r\n<li>Thực hiện những động t&aacute;c điều chuyển h&agrave;ng h&oacute;a cần thiết để đảm bảo số lượng / chất lượng h&agrave;ng h&oacute;a ph&ugrave; hợp với khả năng b&aacute;n h&agrave;ng của bộ phận</li>\r\n<li>Chịu tr&aacute;ch nhiệm về website: www.anzedo.net</li>\r\n<li>Chịu tr&aacute;ch nhiệm về Doanh số tại BP Online</li>\r\n<li>X&acirc;y dựng phương &aacute;n t&igrave;m kiếm kh&aacute;ch h&agrave;ng Online</li>\r\n<li>X&acirc;y dựng - tối ưu quy tr&igrave;nh xử l&yacute; đơn h&agrave;ng, quy tr&igrave;nh chăm s&oacute;c kh&aacute;ch h&agrave;ng</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h-17h)</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa.&nbsp;</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>C&oacute; kinh nghiệm từ 1 - 2 năm Quản l&yacute; k&ecirc;nh b&aacute;n h&agrave;ng Online của c&aacute;c thương hiệu b&aacute;n lẻ</li>\r\n<li>C&oacute; kĩ năng quản l&yacute; nh&acirc;n sự</li>\r\n<li>C&oacute; khả năng lập - triển khai kế hoạch Doanh số bộ phận</li>\r\n<li>Th&agrave;nh thạo Tin học văn ph&ograve;ng</li>\r\n<li>Nhanh nhẹn, s&aacute;ng tạo, giao tiếp tốt.</li>\r\n<li>Trung thực, chịu kh&oacute;, c&oacute; tinh thần tr&aacute;ch nhiệm</li>\r\n</ol>\r\n</body>\r\n</html>'),
(12, NULL, '2019-11-04 00:23:31', '2019-11-04 00:23:31', 'Nhân viên PR- Marketing', 'nhan-vien-pr-marketing', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6-10', '[\"1\",\"2\",\"3\"]', 'Phòng Marketing', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa Điểm L&agrave;m Việc</strong></p>\r\n<ul>\r\n<li>Trụ sở C&ocirc;ng ty: Số 9B Vũ Ngọc Phan, phường 13, quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>&nbsp;Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>T&igrave;m kiếm v&agrave; ph&aacute;t triển hệ thống kh&aacute;ch h&agrave;ng tại địa phương</li>\r\n<li>Nghi&ecirc;n cứu thị trường, ph&acirc;n t&iacute;ch đối thủ cạnh tranh</li>\r\n<li>X&acirc;y dựng chiến lược PR/ Marketing, ph&aacute;t triển thương hiệu&hellip; ngắn hạn v&agrave; d&agrave;i hạn</li>\r\n<li>Gi&aacute;m s&aacute;t, quản l&yacute; h&igrave;nh ảnh cửa h&agrave;ng theo c&aacute;c quy định từ ph&ograve;ng Marketing</li>\r\n<li>Tổ chức thực hiện về mặt h&igrave;nh ảnh theo c&aacute;c chiến dịch sales hay triển khai window v&agrave; event h&agrave;ng th&aacute;ng tại địa phương</li>\r\n<li>Thực hiện viết v&agrave; gửi b&agrave;i PR cho địa phương, gửi b&agrave;i về cho ph&ograve;ng marketing tại Hồ Ch&iacute; Minh</li>\r\n<li>Đề xuất c&aacute;c kế hoạch tiếp thị v&agrave; truyền th&ocirc;ng ph&ugrave; hợp tại địa phương</li>\r\n<li>Hỗ trợ c&aacute;c c&ocirc;ng việc kh&aacute;c tại Showroom</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>Th&agrave;nh thạo Tin học văn ph&ograve;ng</li>\r\n<li>Nhanh nhẹn, s&aacute;ng tạo, giao tiếp tốt.</li>\r\n<li>Trung thực, chịu kh&oacute;</li>\r\n<li>Đ&atilde; c&oacute; kinh nghiệm l&agrave;m việc &iacute;t nhất 06 th&aacute;ng ở vị tr&iacute; tương đương.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(13, NULL, '2019-11-04 00:28:24', '2019-11-04 00:28:24', 'Tài xế lái xe', 'tai-xe-lai-xe', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '7-10', '[\"1\",\"2\",\"3\"]', '', 3, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc: </strong>H&agrave; Nội, Đ&agrave; Nẵng, Hồ Ch&iacute; Minh</p>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Thời gian l&agrave;m việc: 8h00-19h00</li>\r\n<li>Bảo quản v&agrave; giữ g&igrave;n xe tốt. Đảm bảo xe lu&ocirc;n sạch sẽ v&agrave; trong t&igrave;nh trạng sẵn s&agrave;ng phục vụ.</li>\r\n<li>Đảm bảo an to&agrave;n cho người v&agrave; xe, đảm bảo thời gian v&agrave; địa điểm đưa đ&oacute;n đ&uacute;ng theo y&ecirc;u cầu.</li>\r\n<li>Giữ g&igrave;n b&iacute; mật nội dung c&ocirc;ng việc c&aacute;n bộ hoặc nh&acirc;n vi&ecirc;n trao đổi tr&ecirc;n xe, kh&ocirc;ng b&agrave;n t&aacute;n, tung tin, n&oacute;i xấu xuy&ecirc;n tạc sự thật l&agrave;m giảm uy t&iacute;n kh&aacute;ch v&agrave; C&ocirc;ng ty.</li>\r\n<li>Trong l&uacute;c l&agrave;m nhiệm vụ phải t&ocirc;n trọng luật lệ giao th&ocirc;ng v&agrave; c&aacute;c quy định kh&aacute;c của nh&agrave; nước.&nbsp;&nbsp;</li>\r\n<li>Thực hiện c&aacute;c c&ocirc;ng việc kh&aacute;c li&ecirc;n quan khi c&oacute; y&ecirc;u cầu.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương 7-10 triệu/th&aacute;ng + thưởng</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Nam giới tuổi từ 22-35</li>\r\n<li>Ngoại h&igrave;nh dễ nh&igrave;n, sức khỏe tốt</li>\r\n<li>C&oacute; hộ khẩu tại H&agrave; Nội/TP.HCM/Đ&agrave; Nẵng</li>\r\n<li>Trung thực, thật th&agrave;, chăm chỉ</li>\r\n<li>C&oacute; bằng l&aacute;i xe B2 v&agrave; kinh nghiệm l&agrave;m việc tr&ecirc;n 2 năm</li>\r\n</ol>\r\n<ol start=\"6\">\r\n<li>Th&ocirc;ng thạo đường H&agrave; Nội, Hồ Ch&iacute; Minh, Đ&agrave; Nẵng</li>\r\n</ol>\r\n</body>\r\n</html>'),
(14, NULL, '2019-11-04 00:35:49', '2019-11-04 00:44:46', 'Nhân viên phụ trách trưng bày (Visual Merchandiser)', 'nhan-vien-phu-trach-trung-bay-visual-merchandiser', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Khối Cửa hàng', 3, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:&nbsp;</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li><strong>Trụ sở ch&iacute;nh:&nbsp;</strong>Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>L&ecirc;n &yacute; tưởng v&agrave; thiết kế trưng b&agrave;y tại cửa h&agrave;ng theo concept: bộ sựu tập, theo m&ugrave;a hoặc c&aacute;c sự kiện t&ugrave;y từng thời điểm. Phối hợp với Photographer v&agrave; nh&acirc;n vi&ecirc;n truyền th&ocirc;ng để đưa c&aacute;c h&igrave;nh ảnh tr&ecirc;n l&ecirc;n c&aacute;c phương tiện th&ocirc;ng tin đại ch&uacute;ng v&agrave; website, facebook của c&ocirc;ng ty.</li>\r\n<li>Phối hợp c&ugrave;ng c&aacute;c nh&acirc;n vi&ecirc;n Marketing để đưa ra c&aacute;c &yacute; tưởng b&agrave;y biện, sắp xếp, c&aacute;c icon v&agrave; poster quảng c&aacute;o trong cửa h&agrave;ng những dịp đặc biệt</li>\r\n<li>Duy tr&igrave; v&agrave; hướng dẫn nh&acirc;n vi&ecirc;n tại cửa h&agrave;ng trưng b&agrave;y theo quy định</li>\r\n<li>Đi c&ocirc;ng t&aacute;c hỗ trợ trưng b&agrave;y khai trương c&aacute;c cửa h&agrave;ng tỉnh khi cần</li>\r\n<li>C&aacute;c c&ocirc;ng việc kh&aacute;c ph&aacute;t sinh theo y&ecirc;u cầu của Quản l&yacute;</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng, Đại học c&aacute;c chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>Nam/Nữ, tuổi từ 23-30</li>\r\n<li>Tối thiểu 01 năm kinh nghiệm ở vị tr&iacute; tương đương tại lĩnh vực thời trang</li>\r\n<li>C&oacute; mắt thẩm mỹ tốt, từng cộng t&aacute;c l&agrave;m stylist cho c&aacute;c thương hiệu thời trang hoặc b&aacute;o, tạp ch&iacute;.</li>\r\n<li>Tốt nghiệp Trung cấp trở l&ecirc;n chuy&ecirc;n ng&agrave;nh Thiết kế, Thời trang, Mỹ thuật, Kinh tế,...</li>\r\n<li>Kỹ năng trưng b&agrave;y tốt</li>\r\n<li>C&oacute; khiếu thẩm mỹ v&agrave; s&aacute;ng tạo</li>\r\n<li>Nhanh nhẹn, chủ động trong c&ocirc;ng việc</li>\r\n<li>Nhạy b&eacute;n với xu hướng thời trang, am hiểu c&aacute;ch thức trưng b&agrave;y v&agrave; trang tr&iacute; cửa h&agrave;ng</li>\r\n</ol>\r\n</body>\r\n</html>'),
(15, NULL, '2019-11-04 00:42:27', '2019-11-04 00:44:08', 'Trưởng phòng Nhân sự', 'truong-phong-nhan-su', '[\"1\",\"2\",\"3\"]', 'Quản lý', '7 - 8', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Cao đẳng - Đại học', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li><strong>Địa điểm l&agrave;m việc:</strong></li>\r\n</ul>\r\n<p>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</p>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Tham mưu cho Ban Gi&aacute;m Đốc về tổ chức bộ m&aacute;y nh&acirc;n sự v&agrave; x&acirc;y dựng chiến lược ph&aacute;t triển nguồn nh&acirc;n lực to&agrave;n C&ocirc;ng ty;</li>\r\n<li>Lập kế hoạch đ&agrave;o tạo, tuyển dụng định kỳ h&agrave;ng năm, qu&yacute;, th&aacute;ng nhằm phục vụ tốt cho qu&aacute; tr&igrave;nh sản xuất kinh doanh v&agrave; chiến lược của c&ocirc;ng ty;</li>\r\n<li>X&acirc;y dựng v&agrave; quản l&yacute; ng&acirc;n s&aacute;ch đ&agrave;o tạo, tuyển dụng h&agrave;ng năm của C&ocirc;ng ty;</li>\r\n<li>X&acirc;y dựng hệ thống c&aacute;c ch&iacute;nh s&aacute;ch, chế độ, quy chế, quy tr&igrave;nh cho c&ocirc;ng ty v&agrave; gi&aacute;m s&aacute;t việc chấp h&agrave;nh c&aacute;c nội quy đ&oacute;;</li>\r\n<li>Cải tiến, x&acirc;y dựng ch&iacute;nh s&aacute;ch nh&acirc;n sự, nội quy, quy chế trong thẩm quyền v&agrave; đảm bảo tu&acirc;n thủ c&aacute;c quy định của Ph&aacute;p luật;</li>\r\n<li>Tổ chức thực hiện c&aacute;c nghiệp vụ C&amp;B: Lương, thưởng, ph&uacute;c lợi, BHXH, quan hệ lao động,&hellip;;</li>\r\n<li>C&ocirc;ng t&aacute;c tổ chức v&agrave; hậu cần cho c&aacute;c hoạt động sự kiện của c&ocirc;ng ty (hội nghị, hội thảo, vui chơi, giải tr&iacute;, tham quan, du lịch cho c&aacute;n bộ nh&acirc;n vi&ecirc;n C&ocirc;ng ty, c&aacute;c c&ocirc;ng t&aacute;c hiểu hỉ, lễ tết, thăm hỏi của C&ocirc;ng đo&agrave;n&hellip;);</li>\r\n<li>C&aacute;c c&ocirc;ng việc kh&aacute;c theo sự chỉ đạo của Ban Gi&aacute;m đốc. Chi tiết sẽ được trao đổi trong buổi phỏng vấn.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Nam-Nữ từ 30 - 45 tuổi.</li>\r\n<li>Tốt nghiệp đại học c&aacute;c ng&agrave;nh li&ecirc;n quan quản trị nh&acirc;n lực, luật;</li>\r\n<li>C&oacute; &iacute;t nhất 3 năm ở vị tr&iacute; tương đương;</li>\r\n<li>C&oacute; khả năng nh&igrave;n nhận con người tốt, c&oacute; kỹ năng phỏng vấn tốt;</li>\r\n<li>Trung thực, nhiệt t&igrave;nh, c&oacute; tinh thần tr&aacute;ch nhiệm cao trong c&ocirc;ng việc;</li>\r\n<li>Nắm vững bộ luật lao động, luật thuế TNCN, luật BHYT, BHXH, c&oacute; kiến thức v&agrave; kỹ năng quản trị nh&acirc;n lực;</li>\r\n<li>C&oacute; khả năng định hướng về cơ cấu, quy m&ocirc; ph&aacute;t triển, h&igrave;nh thức đ&agrave;o tạo, x&acirc;y dựng đội ngũ nh&acirc;n sự C&ocirc;ng ty;</li>\r\n<li>Nắm bắt được t&acirc;m l&yacute; người lao động nhằm xử l&yacute; kịp thời c&aacute;c t&igrave;nh huống nh&acirc;n sự ph&aacute;t sinh.</li>\r\n</ol>\r\n<ol start=\"9\">\r\n<li>C&oacute; khả năng đi c&ocirc;ng t&aacute;c ngắn ng&agrave;y.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(16, NULL, '2019-11-04 00:49:38', '2019-11-04 00:49:38', 'Trợ lý Giám đốc kinh doanh', 'tro-l-giam-doc-kinh-doanh', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '7 - 9', '[\"1\",\"2\",\"3\"]', 'Phòng Kinh doanh', 4, 'Toàn thời gian', 'Cao đẳng - Đại học trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở c&ocirc;ng ty: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Tham mưu, hỗ trợ Gi&aacute;m đốc Kinh doanh trong việc hoạch định v&agrave; triển khai c&aacute;c chiến lược kinh doanh nhằm ph&aacute;t triển doanh số, thị trường.</li>\r\n<li>Chủ tr&igrave;, sắp xếp, triển khai v&agrave; gi&aacute;m s&aacute;t, đ&ocirc;n đốc c&aacute;c ch&iacute;nh s&aacute;ch/ kế hoạch/ marketing ph&aacute;t triển kinh doanh từ Gi&aacute;m đốc Kinh doanh đến c&aacute;c bộ phận;</li>\r\n<li>Phối hợp c&ugrave;ng Bộ phận Kinh doanh triển khai c&aacute;c chương tr&igrave;nh b&aacute;n h&agrave;ng, kiểm so&aacute;t hiệu quả việc thực hiện c&aacute;c chương tr&igrave;nh theo định hướng của Gi&aacute;m đốc kinh doanh;</li>\r\n<li>Tham vấn, hỗ trợ c&aacute;c Trưởng ph&ograve;ng B&aacute;n lẻ trong việc lập mục ti&ecirc;u, kế hoạch hoạt động, thiết lập cuộc hẹn với đối t&aacute;c, ch&iacute;nh s&aacute;ch theo chức năng, định hướng của c&ocirc;ng ty;</li>\r\n<li>Tham gia đ&agrave;m ph&aacute;n với c&aacute;c b&ecirc;n đối t&aacute;c, soạn thảo hợp đồng dịch vụ, hợp đồng thương mại, văn bản, quyết định...</li>\r\n<li>Tham gia lập kế hoạch, chương tr&igrave;nh, tổ chức hội nghị, hội thảo, họp giao ban, &hellip; v&agrave; hỗ trợ trong c&aacute;c cuộc họp cho Gi&aacute;m đốc Kinh doanh;</li>\r\n<li>Thực hiện c&aacute;c c&ocirc;ng việc kh&aacute;c khi c&oacute; y&ecirc;u cầu.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/Nữ, tuổi từ 24 trở l&ecirc;n, tốt nghiệp Đại học trở l&ecirc;n c&aacute;c chuy&ecirc;n ng&agrave;nh Quản trị kinh doanh, Thương mại, Luật, Kinh tế, Kinh doanh...</li>\r\n<li>Tiếng Anh th&agrave;nh thạo, dịch v&agrave; viết tốt</li>\r\n<li>&Iacute;t nhất 2 năm kinh nghiệm trong lĩnh vực kinh doanh</li>\r\n<li>Khả năng giao tiếp tốt</li>\r\n<li>C&oacute; kỹ năng tổng hợp v&agrave; ph&acirc;n t&iacute;ch số liệu, lập b&aacute;o c&aacute;o thống k&ecirc; kế to&aacute;n</li>\r\n<li>C&oacute; khả năng thuyết phục, giao tiếp tốt v&agrave; c&oacute; tr&aacute;ch nhiệm cao trong c&ocirc;ng việc</li>\r\n<li>Nhanh nhẹn, chủ động trong c&ocirc;ng việc.</li>\r\n</ol>\r\n</body>\r\n</html>');
INSERT INTO `tuyendungs` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `slug`, `address`, `rank`, `salary`, `career`, `department`, `test_day`, `form`, `degree`, `deadline`, `content`) VALUES
(17, NULL, '2019-11-04 00:57:49', '2019-11-04 00:57:49', 'Trade Marketing Executive', 'trade-marketing-executive', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '8-10', '[\"1\",\"2\",\"3\"]', 'Marketing', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở c&ocirc;ng ty: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Lập kế hoạch v&agrave; triển khai c&aacute;c chương tr&igrave;nh hỗ trợ thương mại d&agrave;i hạn, ngắn hạn (Chương tr&igrave;nh Trade Promotion; Roadshow; Event; Activation&hellip;) nhằm hỗ trợ c&aacute;c Showroom b&aacute;n h&agrave;ng tại từng thời điểm</li>\r\n<li>Triển khai chương tr&igrave;nh đến đội ngũ b&aacute;n h&agrave;ng</li>\r\n<li>Thực hiện hoạt động nghi&ecirc;n cứu thị trường:</li>\r\n<li>Nghi&ecirc;n cứu đối thủ: Sản phẩm - Thị trường - Truyền th&ocirc;ng</li>\r\n<li>Nghi&ecirc;n cứu thị trường: Tiềm năng - Đ&aacute;nh gi&aacute; - Định hướng ph&aacute;t triển sản phẩm</li>\r\n<li>Tham gia nghi&ecirc;n cứu thị trường tr&ecirc;n k&ecirc;nh ph&acirc;n phối:</li>\r\n<li>Lập kế hoạch về ng&acirc;n s&aacute;ch v&agrave; kiểm so&aacute;t ng&acirc;n s&aacute;ch</li>\r\n<li>B&aacute;o c&aacute;o c&aacute;c chương tr&igrave;nh triển khai theo tuần/th&aacute;ng/qu&yacute;.kinh doanh nhằm ph&aacute;t triển doanh số, thị trường.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>&nbsp;Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/nữ, tuổi từ 23 - 33</li>\r\n<li>Tốt nghiệp cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh Marketing, truyền th&ocirc;ng, b&aacute;o ch&iacute;, quản trị kinh doanh&hellip;</li>\r\n<li>C&oacute; &iacute;t nhất 01 năm kinh nghiệm ở vị tr&iacute; tương đương tại ng&agrave;nh thời trang, b&aacute;n lẻ&hellip;</li>\r\n<li>Nắm vững c&aacute;c quy định của ph&aacute;p luật li&ecirc;n quan đến hoạt động Marketing, PR, quảng c&aacute;o v&agrave; truyền th&ocirc;ng.</li>\r\n<li>Tin học th&agrave;nh thạo</li>\r\n<li>C&oacute; sức khỏe tốt, c&oacute; thể đi c&ocirc;ng t&aacute;c tỉnh.</li>\r\n<li>Nhanh nhẹn, giao tiếp tốt</li>\r\n<li>Kỹ năng đ&agrave;m ph&aacute;n, thuyết phục v&agrave; giải quyết vấn đề</li>\r\n</ol>\r\n</body>\r\n</html>'),
(18, NULL, '2019-11-04 01:00:55', '2019-11-04 01:00:55', 'Nhân viên Quay - dựng video', 'nhan-vien-quay-dung-video', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6-7', '[\"1\",\"2\",\"3\"]', 'Phòng Sáng tạo', 4, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở c&ocirc;ng ty: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Quay dựng video</li>\r\n<li>Phụ tr&aacute;ch quay v&agrave; dựng ho&agrave;n chỉnh c&aacute;c video quảng c&aacute;o, giới thiệu sản phẩm thời trang, sự kiện nội bộ v&agrave; truyền th&ocirc;ng của c&ocirc;ng ty</li>\r\n<li>Phối hợp với nh&oacute;m nội dung để dựng v&agrave; xử l&yacute; hậu kỳ video theo kịch bản sẵn c&oacute; v&agrave; đảm bảo deadline.</li>\r\n<li>Đề xuất c&aacute;c &yacute; tưởng, cập nhật c&aacute;c kĩ thuật quay, dựng mới để hiệu quả c&ocirc;ng việc tốt hơn</li>\r\n<li>Thực hiện c&aacute;c c&ocirc;ng việc kh&aacute;c theo y&ecirc;u cầu của cấp tr&ecirc;n</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/nữ, tuổi từ 23 - 33 tốt nghiệp Trung cấp trở l&ecirc;n chuy&ecirc;n ng&agrave;nh li&ecirc;n quan.</li>\r\n<li>C&oacute; kinh nghiệm quay dựng video từ 1-2 năm.</li>\r\n<li>Y&ecirc;u th&iacute;ch v&agrave; am hiểu về video marketing</li>\r\n<li>C&oacute; &oacute;c s&aacute;ng tạo, thẩm mỹ cao, tư duy h&igrave;nh ảnh tốt.</li>\r\n<li>Chịu được &aacute;p lực c&ocirc;ng việc, l&agrave;m việc theo Deadline</li>\r\n<li>Sử dụng th&agrave;nh thạo c&aacute;c phần mềm dựng phim: Premiere, AE. Ưu ti&ecirc;n đ&atilde; từng l&agrave;m việc trong ng&agrave;nh thời trang.</li>\r\n<li>C&oacute; kĩ năng l&agrave;m việc nh&oacute;m.</li>\r\n<li>S&aacute;ng tạo, chủ động trong c&ocirc;ng việc.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(19, NULL, '2019-11-04 01:06:05', '2019-11-04 01:06:05', 'Nhân viên quản lý kho online', 'nhan-vien-quan-l-kho-online', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở c&ocirc;ng ty: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Quản l&yacute; xuất - nhập - tồn h&agrave;ng h&oacute;a tr&ecirc;n thực tế v&agrave; phần mềm</li>\r\n<li>Sắp xếp h&agrave;ng h&oacute;a gọn g&agrave;ng, dễ t&igrave;m kiếm</li>\r\n<li>Đảm bảo điều kiện lưu trữ h&agrave;ng h&oacute;a ph&ugrave; hợp</li>\r\n<li>L&agrave;m việc với đối t&aacute;c chuyển ph&aacute;t nhanh để vận chuyển h&agrave;ng h&oacute;a</li>\r\n<li>B&aacute;o c&aacute;o lại kịp thời cho Quản l&yacute; c&aacute;c vấn đề ph&aacute;t sinh li&ecirc;n quan đến h&agrave;ng h&oacute;a trong Kho</li>\r\n<li>C&ocirc;ng việc kh&aacute;c do Quản l&yacute; giao</li>\r\n<li>T&igrave;m hiểu, ph&acirc;n t&iacute;ch thị trường, c&aacute;c đối thủ cạnh tranh; chỉ đạo thực hiện c&aacute;c chiến dịch mở rộng thị trường kh&aacute;ch h&agrave;ng.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/nữ, tuổi từ 23 &ndash; 33</li>\r\n<li>Tr&igrave;nh độ học vấn: Tốt nghiệp Cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh Quản trị kinh doanh, thương mại, khối ng&agrave;nh kinh tế</li>\r\n<li>Kinh nghiệm: Ưu ti&ecirc;n c&aacute;c bạn c&oacute; kinh nghiệm Quản l&yacute; kho trong lĩnh vực thời trang, b&aacute;n lẻ,&hellip;</li>\r\n<li>C&oacute; kỹ năng tổng hợp v&agrave; ph&acirc;n t&iacute;ch số liệu, lập b&aacute;o c&aacute;o thống k&ecirc;; kỹ năng m&aacute;y t&iacute;nh tốt</li>\r\n<li>C&oacute; kỹ năng quản l&yacute; sản phẩm h&agrave;ng h&oacute;a.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(20, NULL, '2019-11-04 01:10:24', '2019-11-04 01:10:24', 'Photographer', 'photographer', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Phòng Sáng tạo', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở c&ocirc;ng ty: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>C&oacute; kĩ năng chụp h&igrave;nh tốt. Ưu ti&ecirc;n đ&atilde; từng l&agrave;m việc trong ng&agrave;nh thời trang.</li>\r\n<li>Sử dụng th&agrave;nh thạo c&aacute;c thiết bị chụp h&igrave;nh studio v&agrave; c&aacute;c phương &aacute;n chụp h&igrave;nh ngo&agrave;i trời.</li>\r\n<li>C&oacute; tố chất nhiếp ảnh thời trang (gu thẩm mỹ, bố cục, nắm bắt thần th&aacute;i v&agrave; tương t&aacute;c với đối tượng chụp h&igrave;nh.</li>\r\n<li>Th&agrave;nh thạo kĩ năng chỉnh sửa ảnh hậu k&igrave; (Adobe Photoshop). Đặc biệt c&oacute; kĩ năng về in ấn/ chế bản l&agrave; một lợi thế.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/nữ, tuổi từ 23 &ndash; 33</li>\r\n<li>C&oacute; &iacute;t nhất 01 năm kinh nghiệm ở vị tr&iacute; tương đương tại ng&agrave;nh thời trang, b&aacute;n lẻ&hellip;</li>\r\n<li>C&oacute; kĩ năng chụp h&igrave;nh tốt. Ưu ti&ecirc;n đ&atilde; từng l&agrave;m việc trong ng&agrave;nh thời trang.</li>\r\n<li>Sử dụng th&agrave;nh thạo c&aacute;c thiết bị chụp h&igrave;nh studio v&agrave; c&aacute;c phương &aacute;n chụp h&igrave;nh ngo&agrave;i trời.</li>\r\n<li>C&oacute; tố chất nhiếp ảnh thời trang (gu thẩm mỹ, bố cục, nắm bắt thần th&aacute;i v&agrave; tương t&aacute;c với đối tượng chụp h&igrave;nh.</li>\r\n<li>Th&agrave;nh thạo kĩ năng chỉnh sửa ảnh hậu k&igrave; (Adobe Photoshop). Đặc biệt c&oacute; kĩ năng về in ấn/ chế bản l&agrave; một lợi thế.</li>\r\n<li>C&oacute; kĩ năng l&agrave;m việc nh&oacute;m, s&aacute;ng tạo, chủ động trong c&ocirc;ng việc.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(21, NULL, '2019-11-04 01:13:21', '2019-11-04 01:13:21', 'Nhân viên Xuất nhập khẩu', 'nhan-vien-xuat-nhap-khau', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '8-10', '[\"1\",\"2\",\"3\"]', 'Chuỗi cung ứng', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở c&ocirc;ng ty: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Thực hiện c&aacute;c hoạt động giao dịch, đ&agrave;m ph&aacute;n, k&yacute; kết hợp đồng với kh&aacute;ch h&agrave;ng v&agrave; nh&agrave; cung cấp.</li>\r\n<li>Ho&agrave;n tất c&aacute;c thủ tục v&agrave; chứng từ xuất nhập khẩu h&agrave;ng h&oacute;a.</li>\r\n<li>Tiếp nhận, kiểm tra, đối chiếu, hồ sơ h&agrave;ng h&oacute;a nhập khẩu, xuất khẩu với số lượng thực tế tại cửa khẩu trong qu&aacute; tr&igrave;nh l&agrave;m hồ sơ th&ocirc;ng quan h&agrave;ng h&oacute;a.</li>\r\n<li>Quản l&yacute;, theo d&otilde;i c&aacute;c đơn h&agrave;ng, hợp đồng.</li>\r\n<li>Phối hợp với c&aacute;c bộ phận c&oacute; li&ecirc;n quan để đảm bảo đ&uacute;ng tiến độ giao h&agrave;ng cũng như nhận h&agrave;ng.</li>\r\n<li>Thực hiện việc t&igrave;m kiếm kh&aacute;ch h&agrave;ng mới, mở rộng thị trường xuất khẩu theo chiến lược c&ocirc;ng ty đ&atilde; đề ra.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n</li>\r\n<li>Giao tiếp th&agrave;nh thạo tiếng Anh</li>\r\n<li>Ưu ti&ecirc;n ứng vi&ecirc;n đ&atilde; c&oacute; kinh nghiệm trong lĩnh vực Xuất nhập khẩu, l&agrave;m việc với c&aacute;c nh&agrave; cung cấp, Hải quan.</li>\r\n<li>Cẩn thận, c&oacute; tr&aacute;ch nhiệm trong c&ocirc;ng việc</li>\r\n<li>C&oacute; khả năng đi c&ocirc;ng t&aacute;c (2-3 ng&agrave;y/th&aacute;ng)</li>\r\n<li>C&oacute; th&aacute;i độ trung thực, chăm chỉ, chịu được &aacute;p lực.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(22, NULL, '2019-11-04 01:18:20', '2019-11-04 01:18:20', 'Nhân viên Truyền thông', 'nhan-vien-truyen-thong', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Marketing', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>&nbsp;&nbsp;&nbsp;&nbsp; Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Sản xuất kiểm duyệt nội dung b&aacute;o ch&iacute;.</li>\r\n<li>Booking, điều phối tin b&agrave;i b&aacute;o ch&iacute;.</li>\r\n<li>X&acirc;y dựng nội dung truyền th&ocirc;ng tr&ecirc;n Facebook phục vụ mục đ&iacute;ch thương hiệu</li>\r\n<li>X&acirc;y dựng v&agrave; ph&acirc;n phối c&aacute;c nội dung truyền th&ocirc;ng nội bộ</li>\r\n<li>Hỗ trợ ph&ograve;ng nh&acirc;n sự tổ chức c&aacute;c sự kiện nội bộ.</li>\r\n<li>C&aacute;c c&ocirc;ng việc kh&aacute;c theo sự chỉ đạo của trưởng ph&ograve;ng.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n</li>\r\n<li>Nhanh nhẹn.</li>\r\n<li>C&oacute; kĩ năng giao tiếp rất tốt.</li>\r\n<li>C&oacute; kinh nghiệm l&agrave;m việc với người nổi tiếng hoặc giới giải tr&iacute;.</li>\r\n<li>Ưu ti&ecirc;n đ&atilde; từng l&agrave;m việc tại c&aacute;c cơ quan b&aacute;o ch&iacute;, tạp ch&iacute;, xuất bản, truyền h&igrave;nh.</li>\r\n<li>Y&ecirc;u th&iacute;ch v&agrave; am hiểu thời trang l&agrave; một lợi thế.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(23, NULL, '2019-11-04 01:24:45', '2019-11-04 01:24:45', 'Nhân viên Thiết kế thời trang', 'nhan-vien-thiet-ke-thoi-trang', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '8-10', '[\"1\",\"2\",\"3\"]', 'Phòng Sáng tạo', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Chọn tranh &amp; vẽ thiết kế đ&uacute;ng y&ecirc;u cầu kh&aacute;ch h&agrave;ng mục ti&ecirc;u, thương hiệu &amp; đ&uacute;ng y&ecirc;u cầu của ban l&atilde;nh đạo c&ocirc;ng ty, chọn nguy&ecirc;n liệu, m&agrave;u sắc v&agrave; c&aacute;c phụ kiện trang tr&iacute; cho mẫu thiết kế đ&oacute;.</li>\r\n<li>Gửi mẫu đ&atilde; vẽ ho&agrave;n chỉnh cho ban l&atilde;nh đạo duyệt .</li>\r\n<li>Ho&agrave;n thiện, trang tr&iacute; sản phẩm sau khi ph&ograve;ng mẫu may ho&agrave;n chỉnh.</li>\r\n<li>Nắm r&otilde; c&aacute;c nguy&ecirc;n liệu c&oacute; sẵn trong kho của c&ocirc;ng ty. Chọn thiết kế ph&ugrave; hợp với nguy&ecirc;n liệu c&oacute; sẵn.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n</li>\r\n<li>Nam/Nữ, tuổi từ 23-30</li>\r\n<li>Khả năng vẽ tay v&agrave; vẽ m&aacute;y tốt</li>\r\n<li>C&oacute; kinh nghiệm l&agrave; một lợi thế lớn</li>\r\n<li>Đam m&ecirc; với c&ocirc;ng việc thiết kế thời trang</li>\r\n<li>C&oacute; kiến thức cập nhật c&aacute;c thương hiệu, c&aacute;c xu hướng v&agrave; phong c&aacute;ch thời trɑng mới nhất.</li>\r\n<li>Năng động, nhiệt huyết, kh&ocirc;ng ngại học hỏi.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(24, NULL, '2019-11-04 01:27:46', '2019-11-04 01:27:46', 'Digital Marketing', 'digital-marketing', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Marketing', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul>\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Ph&aacute;t triển c&aacute;c chương tr&igrave;nh PR, quảng c&aacute;o tr&ecirc;n c&aacute;c phương tiện truyền th&ocirc;ng online.</li>\r\n<li>Lập kế hoạch SEO, SEM, Google Adwords tối ưu h&oacute;a thứ hạng của website tr&ecirc;n c&aacute;c trang t&igrave;m kiếm. Thống k&ecirc; v&agrave; ph&acirc;n t&iacute;ch từ kh&oacute;a định kỳ.</li>\r\n<li>Định vị v&agrave; ph&aacute;t triển thương hiệu v&agrave; sản phẩm tr&ecirc;n c&aacute;c website, forum, facebook, wordpress... v&agrave; c&aacute;c mạng x&atilde; hội kh&aacute;c,&hellip;</li>\r\n<li>Triển khai quảng b&aacute; qua Email Marketing v&agrave; Mobile Marketing.</li>\r\n<li>Thu thập, ph&acirc;n t&iacute;ch th&ocirc;ng tin thị trường, sản phẩm v&agrave; c&aacute;c chương tr&igrave;nh Digital Marketing, Social Media của đối thủ cạnh tranh.</li>\r\n<li>Đề xuất v&agrave; thực hiện c&aacute;c chương tr&igrave;nh khuyến mại, truyền th&ocirc;ng, quảng b&aacute; v&agrave; x&acirc;y dựng thương hiệu cho c&ocirc;ng ty v&agrave; c&aacute;c sản phẩm của c&ocirc;ng ty.</li>\r\n<li>C&aacute;c c&ocirc;ng việc kh&aacute;c theo chỉ định của cấp tr&ecirc;n.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng - Đại học chuy&ecirc;n ng&agrave;nh Marketing</li>\r\n<li>C&oacute; &iacute;t nhất 2 năm kinh nghiệm về Digital Marketing</li>\r\n<li>Th&agrave;nh thạo c&ocirc;ng cụ đăng b&agrave;i tr&ecirc;n web, c&oacute; hiểu biết về HTML</li>\r\n<li>Tiếng Anh th&agrave;nh thạo</li>\r\n<li>Khả năng học hỏi nhanh, nhạy b&eacute;n với kiến thức mới v&agrave; c&oacute; khả năng ph&acirc;n t&iacute;ch</li>\r\n<li>C&oacute; kĩ năng l&agrave;m việc nh&oacute;m.</li>\r\n<li>S&aacute;ng tạo, chủ động trong c&ocirc;ng việc.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(25, NULL, '2019-11-04 01:31:52', '2019-11-04 01:31:52', 'Facebook Adwords', 'facebook-adwords', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Phòng Truyền thông', 4, 'Toàn thời gian', 'Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Tối ưu quảng c&aacute;o Facebook</li>\r\n<li>Lập ng&acirc;n s&aacute;ch, kiểm so&aacute;t, đ&aacute;nh gi&aacute; v&agrave; th&ocirc;ng b&aacute;o kết quả mỗi ng&agrave;y về nội dung b&agrave;i chạy quảng c&aacute;o cho Lead</li>\r\n<li>Tiến h&agrave;nh chạy v&agrave; tối ưu chi ph&iacute; để quảng c&aacute;o tr&ecirc;n Facebook đạt hiệu quả.</li>\r\n<li>T&igrave;m kiếm, nghi&ecirc;n cứu c&aacute;c b&agrave;i quảng c&aacute;o hiệu quả để đề xuất nội dung x&acirc;y dựng b&agrave;i quảng c&aacute;o cho Content triển khai</li>\r\n<li>Chủ động thực hiện y&ecirc;u c&acirc;u v&agrave; nhiệm vụ li&ecirc;n quan đến c&ocirc;ng việc m&agrave; Trưởng nh&oacute;m đề ra.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Y&ecirc;u th&iacute;ch, am hiểu về content marketting v&agrave; c&oacute; khả năng t&igrave;m kiếm, ph&acirc;n t&iacute;ch, tổng hợp th&ocirc;ng tin tr&ecirc;n MXH.</li>\r\n<li>Kinh nghiệm thực chiến tr&ecirc;n 6 th&aacute;ng; th&agrave;nh thạo c&aacute;c h&igrave;nh thức quảng c&aacute;o Facebook</li>\r\n<li>C&oacute; khả năng viết, l&agrave;m nội dung quảng c&aacute;o b&aacute;n h&agrave;ng</li>\r\n<li>Biết quảng c&aacute;o tr&ecirc;n c&aacute;c k&ecirc;nh Social kh&aacute;c (Google,GDN...) l&agrave; 1 lợi thế.</li>\r\n<li>Lu&ocirc;n chủ động, linh hoạt, s&aacute;ng tạo, c&oacute; tr&aacute;ch nhiệm trong c&ocirc;ng việc</li>\r\n<li>Nắm bắt trend tốt, c&oacute; khả năng s&aacute;ng tạo nội dung theo trend</li>\r\n<li>C&oacute; kĩ năng l&agrave;m việc nh&oacute;m.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(26, NULL, '2019-11-04 01:34:56', '2019-11-04 01:34:56', 'Junior PHP Developer (MySQL)', 'junior-php-developer-mysql', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '7-9', '[\"1\",\"2\",\"3\"]', 'Bộ phận IT', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>X&acirc;y dựng giao diện tối ưu tr&ecirc;n c&aacute;c thiết bị mobile , tablet , PC</li>\r\n<li>L&agrave;m việc với bộ phận ph&aacute;t triễn backend, đội ngũ chăm s&oacute;c kh&aacute;ch h&agrave;ng tối ưu thao t&aacute;c người d&ugrave;ng</li>\r\n<li>Cập nhật cải tiến tr&atilde;i nghiệm người d&ugrave;ng để sản phẩm ng&agrave;y c&agrave;ng ho&agrave;n thiện chất lượng hơn</li>\r\n<li>Lập tr&igrave;nh ứng dụng Web sử dụng PHP</li>\r\n<li>Phối hợp c&ugrave;ng đồng đội x&acirc;y dựng, ph&aacute;t triển API kết nối sản phẩm, giải ph&aacute;p dịch vụ của c&ocirc;ng ty</li>\r\n<li>Biết cắt HTML</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>C&oacute; kinh nghiệm code PHP từ 1 năm trở l&ecirc;n</li>\r\n<li>Biết code MySQL</li>\r\n<li>Tốt nghiệp Cao đăng - Đại học chuy&ecirc;n ng&agrave;nh Khoa học m&aacute;y t&iacute;nh hoặc chuy&ecirc;n ng&agrave;nh c&oacute; li&ecirc;n quan.</li>\r\n<li>C&oacute; kinh nghiệm thiết kế web l&agrave; một lợi thế.</li>\r\n<li>C&oacute; tư duy tốt, khả năng học hỏi tốt, chịu được &aacute;p lực.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(27, NULL, '2019-11-04 01:45:31', '2019-11-04 01:45:31', 'Nhân viên Bảo hành sản phẩm', 'nhan-vien-bao-hanh-san-pham', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Bảo hành - Sửa chữa', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul>\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>&nbsp;Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Nhận th&ocirc;ng tin, nhận b&agrave;n giao v&agrave; l&ecirc;n danh s&aacute;ch c&aacute;c sản phẩm lỗi, sản phẩm cần sửa, bảo h&agrave;nh</li>\r\n<li>Quản l&yacute; h&agrave;ng lỗi, h&agrave;ng bảo h&agrave;nh</li>\r\n<li>Trực tiếp sửa gi&agrave;y, t&uacute;i, quần &aacute;o chất liệu len, b&ograve; (denim), da (nếu lỗi nhỏ tự sửa được)</li>\r\n<li>L&agrave;m việc với c&aacute;c Nh&agrave; cung cấp để hỗ trợ sửa, bảo h&agrave;nh c&aacute;c sản phẩm lỗi phức tạp</li>\r\n<li>C&aacute;c c&ocirc;ng việc kh&aacute;c theo ph&acirc;n c&ocirc;ng của quản l&yacute; trực tiếp.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng - Đại học trở l&ecirc;n</li>\r\n<li>Nắm được sơ bộ kĩ thuật may quần &aacute;o, t&uacute;i x&aacute;ch, gi&agrave;y d&eacute;p v&agrave; c&oacute; khả năng khắc phục</li>\r\n<li>Sử dụng tốt Word, Excel</li>\r\n<li>Ưu ti&ecirc;n c&oacute; kinh nghiệm về KCS kiểm h&agrave;ng.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(28, NULL, '2019-11-04 01:47:49', '2019-11-04 01:47:49', 'Nhân viên Chăm sóc khách hàng', 'nhan-vien-cham-soc-khach-hang', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul>\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Giải đ&aacute;p c&aacute;c thắc mắc, khiếu nại của kh&aacute;ch h&agrave;ng khi sử dụng dịch vụ</li>\r\n<li>X&acirc;y dựng chiến lược v&agrave; kế hoạch chăm s&oacute;c KH cho thương hiệu</li>\r\n<li>Gi&aacute;m s&aacute;t chất lượng dịch vụ chăm s&oacute;c kh&aacute;ch h&agrave;ng tại khu vực</li>\r\n<li>Phối hợp với c&aacute;c bộ phận li&ecirc;n quan để triển khai c&aacute;c sự kiện Chăm s&oacute;c kh&aacute;ch h&agrave;ng theo kế hoạch: khai trương cửa h&agrave;ng, kỷ niệm sinh nhật cửa h&agrave;ng, VIP&rsquo;s day,vv..</li>\r\n<li>C&aacute;c c&ocirc;ng việc kh&aacute;c do quản l&yacute; giao.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Trình đ&ocirc;̣: T&ocirc;́t nghi&ecirc;̣p Trung c&acirc;́p trở l&ecirc;n</li>\r\n<li>C&oacute; khả năng chịu &aacute;p lực trong c&ocirc;ng việc</li>\r\n<li>C&oacute; khả năng giao tiếp, thương lượng tốt; Giọng n&oacute;i dễ nghe</li>\r\n<li>Sử dụng th&agrave;nh thạo vi t&iacute;nh văn ph&ograve;ng, c&aacute;c phần mềm li&ecirc;n quan đến c&ocirc;ng việc;</li>\r\n<li>Ham học hỏi, li&ecirc;n tục cập nhật c&aacute;c th&ocirc;ng tin về sản phẩm mới v&agrave; t&igrave;nh trạng thị trường quảng c&aacute;o online, kinh doanh online tại Việt Nam</li>\r\n<li>Kỹ năng giao tiếp tốt, ứng biến nhanh</li>\r\n<li>C&oacute; th&aacute;i độ trung thực, chăm chỉ, ki&ecirc;n nhẫn</li>\r\n<li>Ưu ti&ecirc;n c&aacute;c bạn c&oacute; kinh nghiệm l&agrave;m Sale hoặc CSKH trong lĩnh vực thời trang, spa, thẩm mỹ, mỹ phẩm, l&agrave;m đẹp,&hellip;</li>\r\n</ol>\r\n</body>\r\n</html>'),
(29, NULL, '2019-11-04 01:51:34', '2019-11-04 01:51:34', 'Nhân viên đối soát', 'nhan-vien-doi-soat', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Kinh doanh', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Theo d&otilde;i, kiểm tra đối so&aacute;t c&aacute;c đơn h&agrave;ng v&agrave; li&ecirc;n hệ với đối t&aacute;c để vận chuyển h&agrave;ng h&oacute;a</li>\r\n<li>Quản l&yacute; đơn h&agrave;ng: quản l&yacute; t&igrave;nh trạng đơn h&agrave;ng, l&agrave;m việc với b&ecirc;n vận chuyển về t&igrave;nh trạng đơn h&agrave;ng, giải quyết những vấn đề ph&aacute;t sinh để đảm bảo h&agrave;ng h&oacute;a đến tay kh&aacute;ch h&agrave;ng đ&uacute;ng thời gian, đảm bảo về quy c&aacute;ch</li>\r\n<li>Thực hiện chốt c&ocirc;ng nợ với đối t&aacute;c vận chuyển</li>\r\n<li>L&agrave;m việc với đối t&aacute;c vận chuyển, x&acirc;y dựng c&aacute;c chương tr&igrave;nh vận chuyển đặc biệt để th&uacute;c đẩy nhu cầu mua h&agrave;ng Online của kh&aacute;ch h&agrave;ng</li>\r\n<li>Giải quyết khiếu nại của kh&aacute;ch h&agrave;ng li&ecirc;n quan đến vận chuyển</li>\r\n<li>Hỗ trợ kho Online</li>\r\n<li>Thực hiện việc b&aacute;o c&aacute;o c&ocirc;ng việc cho cấp quản l&yacute; trực tiếp v&agrave; thực hiện c&aacute;c c&ocirc;ng việc li&ecirc;n quan.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Trình đ&ocirc;̣: T&ocirc;́t nghi&ecirc;̣p Cao đẳng trở l&ecirc;n</li>\r\n<li>C&oacute; khả năng chịu &aacute;p lực trong c&ocirc;ng việc</li>\r\n<li>C&oacute; khả năng giao tiếp, đ&agrave;m ph&aacute;n, thương lượng tốt;</li>\r\n<li>Sử dụng th&agrave;nh thạo vi t&iacute;nh văn ph&ograve;ng, c&aacute;c phần mềm li&ecirc;n quan đến c&ocirc;ng việc;</li>\r\n<li>Kỹ năng giải quyết vấn đề v&agrave; xử l&yacute; t&igrave;nh huống tốt</li>\r\n<li>Cẩn thận, ưa th&iacute;ch c&aacute;c c&ocirc;ng việc y&ecirc;u cầu chi tiết</li>\r\n<li>C&oacute; th&aacute;i độ trung thực, chăm chỉ, chịu được &aacute;p lực</li>\r\n</ol>\r\n</body>\r\n</html>'),
(30, NULL, '2019-11-04 01:53:27', '2019-11-04 01:53:27', 'Nhân viên IT Support', 'nhan-vien-it-support', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 10 ', '[\"1\",\"2\",\"3\"]', 'Bộ phận IT', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Quản trị v&agrave; vận h&agrave;nh hệ thống c&ocirc;ng nghệ th&ocirc;ng tin của c&ocirc;ng ty.</li>\r\n<li>Kiểm tra bảo dưỡng hệ thống mạng, c&agrave;i đặt phần mềm, hỗ trợ cửa h&agrave;ng.</li>\r\n<li>Quản trị network cơ bản.</li>\r\n<li>Quản trị v&agrave; vận h&agrave;nh hệ thống ứng dụng tr&ecirc;n nền Windows Server &ndash; AD, DNS, DHCP v&agrave; c&aacute;c ứng dụng kh&aacute;c.</li>\r\n<li>Xử l&yacute; y&ecirc;u cầu, khắc phực sự cố phần mềm/ phần cứng tr&ecirc;n thiết bị.</li>\r\n<li>C&agrave;i đặt hệ điều h&agrave;nh, phần mềm tr&ecirc;n Windows Server.</li>\r\n<li>Quản trị hệ thống switch Cisco&hellip;</li>\r\n<li>Quản trị hệ thống camera, mail nội bộ, điện thoại analog.</li>\r\n<li>Chi tiết c&ocirc;ng việc sẽ trao đổi cụ thể trong buổi phỏng vấn.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng - Đại học chuy&ecirc;n ng&agrave;nh Khoa học m&aacute;y t&iacute;nh hoặc chuy&ecirc;n ng&agrave;nh c&oacute; li&ecirc;n quan.</li>\r\n<li>C&oacute; khả năng học hỏi, trung thực, nhiệt t&igrave;nh</li>\r\n<li>C&oacute; khả năng chịu &aacute;p lực trong c&ocirc;ng việc</li>\r\n<li>C&oacute; thể đi c&ocirc;ng t&aacute;c.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(31, NULL, '2019-11-04 02:01:33', '2019-11-04 02:01:33', 'Nhân viên Stylist', 'nhan-vien-stylist', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '8-10', '[\"1\",\"2\",\"3\"]', 'Phòng Sáng tạo', 4, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Theo d&otilde;i cập nhật t&igrave;nh trạng h&igrave;nh ảnh sản phẩm mỗi khi h&agrave;ng mới l&ecirc;n</li>\r\n<li>Book lịch chụp h&igrave;nh với bộ phận Marketing &ndash; Studio.</li>\r\n<li>L&ecirc;n kế hoạch v&agrave; thực hiện c&aacute;c c&ocirc;ng đoạn trong việc chụp h&igrave;nh như: thời giɑn thực hiện, &yacute; tưởng cho BST, chọn người mẫu, địɑ điểm, trɑng phục,&hellip;</li>\r\n<li>Tạo d&aacute;ng cho người mẫu nổi bật trang phục</li>\r\n<li>Kiểm so&aacute;t số lượng h&agrave;ng h&oacute;a v&agrave; phụ kiện- C&aacute;c c&ocirc;ng việc kh&aacute;c do trưởng bộ phận y&ecirc;u cầu.</li>\r\n<li>Phối c&aacute;c combo thời trang.</li>\r\n<li>Đề xuất c&aacute;c giải ph&aacute;p lưu trữ, quản l&yacute; th&ocirc;ng tin sản phẩm</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/Nữ, tuổi từ 23-30</li>\r\n<li>C&oacute; mắt thẩm mỹ tốt, từng cộng t&aacute;c l&agrave;m stylist cho c&aacute;c thương hiệu thời trang hoặc b&aacute;o, tạp ch&iacute;.</li>\r\n<li>C&oacute; kiến thức cập nhật c&aacute;c thương hiệu, c&aacute;c xu hướng v&agrave; phong c&aacute;ch thời trɑng mới nhất.</li>\r\n<li>Năng động, nhiệt huyết, kh&ocirc;ng ngại học hỏi.</li>\r\n<li>Kỹ năng tổ chức, quản l&yacute; c&ocirc;ng việc tốt.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(32, NULL, '2019-11-04 02:05:47', '2019-11-04 02:33:03', 'Nhân viên Thiết kế sản xuất', 'nhan-vien-thiet-ke-san-xuat', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Phòng Sáng tjao', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Thiết kế banner, backdrop, brochure, quảng c&aacute;o của c&ocirc;ng ty;</li>\r\n<li>Thiết kế sự kiện, s&acirc;n khấu, hội nghị hội thảo;</li>\r\n<li>C&ocirc;ng việc đ&ograve;i hỏi t&iacute;nh s&aacute;ng tạo cao, c&oacute; nhiều &yacute; tưởng mới, hay v&agrave; đặc biệt;</li>\r\n<li>C&oacute; kỹ năng v&agrave; thẩm mỹ trong thiết kế website, brochures, posters, etc&hellip;</li>\r\n<li>C&oacute; khả năng sử dụng th&agrave;nh thạo c&aacute;c c&ocirc;ng cụ thiết kế (như vẽ tay v&agrave; c&aacute;c phần mềm thiết kế đồ họa) để đưa những &yacute; tưởng s&aacute;ng tạo v&agrave;o ứng dụng thực tiễn.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường c&oacute; t&iacute;nh s&aacute;ng tạo cao;</li>\r\n<li>Nội dung c&ocirc;ng việc, thời gian l&agrave;m việc v&agrave; c&aacute;c vấn đề chi tiết sẽ trao đổi trong v&ograve;ng phỏng vấn;</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n c&aacute;c chuy&ecirc;n ng&agrave;nh Thiết kế li&ecirc;n quan</li>\r\n<li>Ưu ti&ecirc;n ứng vi&ecirc;n đ&atilde; c&oacute; kinh nghiệm l&agrave;m về thiết kế in ấn</li>\r\n<li>Khả năng l&agrave;m việc độc lập v&agrave; l&agrave;m việc theo nh&oacute;m tốt</li>\r\n<li>Trung thực, tr&aacute;ch nhiệm, c&oacute; khả năng s&aacute;ng tạo</li>\r\n</ol>\r\n</body>\r\n</html>'),
(33, NULL, '2019-11-04 02:09:10', '2019-11-04 02:09:10', 'Nhân viên Sale online', 'nhan-vien-sale-online', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '7 - 8', '[\"1\",\"2\",\"3\"]', 'Phòng Kinh doanh', 4, 'Toàn thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul style=\"list-style-type: circle;\">\r\n<li>Trụ sở ch&iacute;nh: số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>&nbsp;Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Gọi điện li&ecirc;n hệ với KH, tư vấn sản phẩm thời trang dựa tr&ecirc;n nhu cầu của KH</li>\r\n<li>Chăm s&oacute;c KH, hỗ trợ giải đ&aacute;p thắc mắc của KH qua email, điện thoại, Facebook</li>\r\n<li>T&igrave;m kiếm, tiếp cận, b&aacute;n h&agrave;ng qua c&aacute;c k&ecirc;nh Facebook, Zalo, Instagram,...</li>\r\n<li>Cụ thể sẽ trao đổi trực tiếp khi phỏng vấn</li>\r\n<li>Tiếp nhận những phản hồi về sản phẩm của kh&aacute;ch h&agrave;ng. Lu&ocirc;n duy tr&igrave; v&agrave; truyền tải h&igrave;nh ảnh đẹp của sản phẩm, của c&ocirc;ng ty tới kh&aacute;ch h&agrave;ng.</li>\r\n<li>Thực hiện giao dịch với kh&aacute;ch h&agrave;ng qua điện thoại.</li>\r\n<li>Thực hiện việc b&aacute;o c&aacute;o c&ocirc;ng việc cho cấp quản l&yacute; trực tiếp v&agrave; thực hiện c&aacute;c c&ocirc;ng việc li&ecirc;n quan.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/Nữ, tuổi từ 23-30</li>\r\n<li>Trình đ&ocirc;̣: T&ocirc;́t nghi&ecirc;̣p trung c&acirc;́p trở l&ecirc;n</li>\r\n<li>C&oacute; khả năng chịu &aacute;p lực trong c&ocirc;ng việc</li>\r\n<li>C&oacute; khả năng giao tiếp, đ&agrave;m ph&aacute;n, thương lượng tốt;</li>\r\n<li>Sử dụng th&agrave;nh thạo vi t&iacute;nh văn ph&ograve;ng, c&aacute;c phần mềm li&ecirc;n quan đến c&ocirc;ng việc;</li>\r\n<li>Ham học hỏi, li&ecirc;n tục cập nhật c&aacute;c th&ocirc;ng tin về sản phẩm mới v&agrave; t&igrave;nh trạng thị trường quảng c&aacute;o online, kinh doanh online tại Việt Nam</li>\r\n<li>Ưu ti&ecirc;n c&aacute;c bạn c&oacute; kinh nghiệm l&agrave;m việc trong lĩnh vực thời trang, spa, thẩm mỹ, mỹ phẩm, l&agrave;m đẹp,&hellip;</li>\r\n<li>C&oacute; th&aacute;i độ trung thực, chăm chỉ, chịu được &aacute;p lực</li>\r\n</ol>\r\n</body>\r\n</html>');
INSERT INTO `tuyendungs` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `slug`, `address`, `rank`, `salary`, `career`, `department`, `test_day`, `form`, `degree`, `deadline`, `content`) VALUES
(34, NULL, '2019-11-04 02:14:05', '2019-11-04 02:14:05', 'Nhân viên Quản lý web', 'nhan-vien-quan-l-web', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 8', '[\"1\",\"2\",\"3\"]', 'Marketing', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul>\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Theo d&otilde;i cập nhật t&igrave;nh trạng h&igrave;nh ảnh sản phẩm mỗi khi h&agrave;ng mới l&ecirc;n</li>\r\n<li>Chuẩn bị ri&ecirc;ng danh s&aacute;ch h&agrave;ng cần chụp h&igrave;nh</li>\r\n<li>Book lịch chụp h&igrave;nh với bộ phận Marketing &ndash; Studio.</li>\r\n<li>Order banner website theo c&aacute;c nội dung ph&aacute;t sinh.</li>\r\n<li>Theo d&otilde;i, gi&aacute;m s&aacute;t để li&ecirc;n tục cập nhật đầy đủ h&igrave;nh ảnh h&agrave;ng h&oacute;a tại c&aacute;c k&ecirc;nh b&aacute;n h&agrave;ng.</li>\r\n<li>Cập nhật th&ocirc;ng tin m&ocirc; tả sản phẩm</li>\r\n<li>Cập nhật lookbook theo từng bộ sưu tập được cung cấp bởi BP. Marketing</li>\r\n<li>Cập nhật m&atilde;, gi&aacute; th&ocirc;ng tin sản phẩm theo c&aacute;c chương tr&igrave;nh (Cất - dỡ h&agrave;ng, chương tr&igrave;nh khuyến mại,..)</li>\r\n<li>Thực hiện bổ sung, thay đổi c&aacute;c nội dung tại c&aacute;c mục tại website theo y&ecirc;u cầu của cấp tr&ecirc;n</li>\r\n<li>Đề xuất c&aacute;c giải ph&aacute;p lưu trữ, quản l&yacute; th&ocirc;ng tin sản phẩm</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/Nữ, tuổi từ 23-30</li>\r\n<li>Tốt nghiệp Đại học, cao đẳng chuy&ecirc;n ng&agrave;nh Marketing, Quản trị kinh doanh, PR b&aacute;o ch&iacute;, Thương mại điện tử&hellip;</li>\r\n<li>C&oacute; kĩ năng viết v&agrave; s&aacute;ng tạo nội dung</li>\r\n<li>Nhanh nhẹn, nhiệt t&igrave;nh, c&oacute; tr&aacute;ch nhiệm trong c&ocirc;ng việc</li>\r\n<li>Sử dụng th&agrave;nh thạo tin học văn ph&ograve;ng.</li>\r\n<li>Kinh nghiệm: Tr&ecirc;n 6 th&aacute;ng ở vị tr&iacute; tương đương</li>\r\n</ol>\r\n</body>\r\n</html>'),
(35, NULL, '2019-11-04 02:18:37', '2019-11-04 02:18:37', 'Nhân viên mua hàng - Phụ trách vật tư', 'nhan-vien-mua-hang-phu-trach-vat-tu', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '7 - 8', '[\"1\",\"2\",\"3\"]', 'Phòng Kinh doanh', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul>\r\n<li>Trụ sở ch&iacute;nh: Số 9B Vũ Ngọc Phan, Phường 13, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Mua&nbsp; c&aacute;c nguy&ecirc;n phụ liệu về thời trang (đ&atilde; c&oacute; sẵn nh&agrave; cung cấp)</li>\r\n<li>Theo d&otilde;i qu&aacute; tr&igrave;nh mua h&agrave;ng từ l&uacute;c đặt h&agrave;ng đến l&uacute;c giao h&agrave;ng</li>\r\n<li>Theo d&otilde;i phản hồi của c&aacute;c bộ phận về chất lượng h&agrave;ng h&oacute;a đ&atilde; mua v&agrave; phản hồi cho nh&agrave; cung cấp</li>\r\n<li>Theo d&otilde;i, thống k&ecirc; khối lượng nguy&ecirc;n vật liệu</li>\r\n<li>Kiểm tra sự c&acirc;n đối giữa số liệu xuất nhập tồn</li>\r\n<li>Cụ thể c&ocirc;ng việc sẽ trao đổi trong buổi phỏng vấn</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Lương thưởng hấp dẫn theo thỏa thuận, tương xứng với đ&oacute;ng g&oacute;p cho C&ocirc;ng ty.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động, nhiều hoạt động văn h&oacute;a s&ocirc;i nổi</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>Ph&aacute;t huy tối đa sở trường của bản th&acirc;n.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Nam/Nữ, tuổi từ 23-30</li>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n</li>\r\n<li>Giao tiếp tốt, nhạy b&eacute;n, chịu kh&oacute; trong c&ocirc;ng việc</li>\r\n<li>Trung thực, c&oacute; kinh nghiệm l&agrave;m việc độc lập v&agrave; c&oacute; tr&aacute;ch nhiệm với c&ocirc;ng việc</li>\r\n<li>Kỹ năng Excel tốt</li>\r\n</ol>\r\n</body>\r\n</html>'),
(36, NULL, '2019-11-04 02:21:02', '2019-11-04 02:21:02', 'Nhân viên Bán hàng', 'nhan-vien-ban-hang', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '5-8', '[\"1\",\"2\",\"3\"]', 'Khối Cửa hàng', 4, 'Toàn thời gian/Bán thời gian', 'Tốt nghiệp Trung cấp trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong> Hệ thống Showroom tr&ecirc;n to&agrave;n quốc</p>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo 3 ca lu&acirc;n phi&ecirc;n: ca 1: 8h00-17h00, ca 2: 13h00-22h00. Fulltime: 8h00-22h00</li>\r\n<li>Thực hiện c&aacute;c bước trong quy tr&igrave;nh b&aacute;n h&agrave;ng t&igrave;m hiểu nhu cầu, giới thiệu v&agrave; tư vấn c&aacute;c sản phẩm cho kh&aacute;ch h&agrave;ng.</li>\r\n<li>Thường xuy&ecirc;n kiểm tra showroom chăm ch&uacute;t sản phẩm, hướng dẫn kh&aacute;ch h&agrave;ng xem thử đồ đ&uacute;ng c&aacute;ch.</li>\r\n<li>Vệ sinh khu vực l&agrave;m việc h&agrave;ng ng&agrave;y. Trưng b&agrave;y sản phẩm l&ecirc;n c&aacute;c quầy kệ, gi&aacute; treo, theo đ&uacute;ng quy định v&agrave; thẩm mỹ của Showroom.</li>\r\n<li>Lu&ocirc;n duy tr&igrave; v&agrave; truyền tải h&igrave;nh ảnh đẹp của sản phẩm, của c&ocirc;ng ty tới kh&aacute;ch h&agrave;ng.</li>\r\n<li>Thực hiện c&aacute;c y&ecirc;u cầu của Cửa H&agrave;ng Trưởng.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Tốt nghiệp THPT trở l&ecirc;n ( Ưu ti&ecirc;n c&aacute;c ứng vi&ecirc;n tốt nghiệp Trung cấp trở l&ecirc;n )</li>\r\n<li>Tuổi từ 18 - 27, ngoại h&igrave;nh kh&aacute;</li>\r\n<li>Khả năng giao tiếp, tư vấn b&aacute;n h&agrave;ng v&agrave; thuyết phục tốt.</li>\r\n<li>Nhanh nhẹn, hoạt b&aacute;t, trung thực, c&oacute; tinh thần cầu tiến, ham học hỏi.</li>\r\n<li>Chịu &aacute;p lực c&ocirc;ng việc tốt.</li>\r\n<li>Ưu ti&ecirc;n c&aacute;c ứng vi&ecirc;n đ&atilde; c&oacute; kinh nghiệm b&aacute;n h&agrave;ng</li>\r\n</ol>\r\n</body>\r\n</html>'),
(37, NULL, '2019-11-04 02:23:15', '2019-11-04 02:23:15', 'Trưởng phòng Bán lẻ/Trưởng phòng Kinh doanh', 'truong-phong-ban-le-truong-phong-kinh-doanh', '[\"1\",\"2\",\"3\"]', 'Quản lý', '7-9', '[\"1\",\"2\",\"3\"]', 'Phòng Kinh doanh', 0, '04', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\"><strong>Địa Điểm L&agrave;m Việc</strong></p>\r\n<ul>\r\n<li style=\"text-align: justify;\">Trụ sở C&ocirc;ng ty: Số 9B Vũ Ngọc Phan, phường 13, quận B&igrave;nh Thạnh, TP HCM.</li>\r\n<li style=\"text-align: justify;\">Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li style=\"text-align: justify;\">Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li style=\"text-align: justify;\">X&acirc;y dựng chỉ ti&ecirc;u doanh số theo ng&agrave;y, tuần, th&aacute;ng, qu&yacute; cho c&aacute;c showroom trong chuỗi quản l&yacute;; thực hiện theo chỉ ti&ecirc;u doanh số, triển khai, gi&aacute;m s&aacute;t t&igrave;nh h&igrave;nh thực hiện kế hoạch doanh thu. L&ecirc;n kế hoạch x&acirc;y dựng c&aacute;c chương tr&igrave;nh giảm gi&aacute; chung tr&ecirc;n to&agrave;n hệ thống v&agrave; từng showroom ri&ecirc;ng lẻ theo đặc th&ugrave; thị trường từng khu vực.</li>\r\n<li style=\"text-align: justify;\">Theo d&otilde;i t&igrave;nh h&igrave;nh hoạt động của c&aacute;c cửa h&agrave;ng trực thuộc: kiểm tra đột xuất về giờ giấc, nội quy &ndash; quy định, h&agrave;ng ho&aacute; tại cửa h&agrave;ng, c&aacute;c th&ocirc;ng tin v&agrave; b&aacute;o c&aacute;o của c&aacute;c cửa h&agrave;ng trưởng, t&aacute;c phong v&agrave; tinh thần l&agrave;m việc của đội ngũ b&aacute;n h&agrave;ng.</li>\r\n<li style=\"text-align: justify;\">Quản l&yacute; số lượng h&agrave;ng h&oacute;a tồn đọng v&agrave; b&aacute;n được theo từng ng&agrave;y của từng showroom, nắm vững số lượng b&aacute;n chậm, b&aacute;n chạy, ph&acirc;n t&iacute;ch, t&igrave;m hiểu nguy&ecirc;n nh&acirc;n; cập nhật, theo d&otilde;i th&ocirc;ng tin bộ sưu tập mới; điều phối h&agrave;ng h&oacute;a trong hệ thống quản l&yacute; theo mức b&aacute;n chậm, b&aacute;n chạy, nhu cầu thị trường.</li>\r\n<li style=\"text-align: justify;\">Chỉ đạo set up h&igrave;nh ảnh: window display, khu vực bộ sưu tập, trưng b&agrave;y h&agrave;ng h&oacute;a theo cụm, theo guideline C&ocirc;ng ty triển khai. Nắm r&otilde; nguy&ecirc;n tắc trưng b&agrave;y h&agrave;ng h&oacute;a, thường xuy&ecirc;n kiểm tra b&aacute;o c&aacute;o h&igrave;nh ảnh của showroom.</li>\r\n<li style=\"text-align: justify;\">Chỉ đạo nh&acirc;n vi&ecirc;n thực hiện chăm s&oacute;c kh&aacute;ch h&agrave;ng trước v&agrave; sau khi mua h&agrave;ng, chăm s&oacute;c kh&aacute;ch h&agrave;ng VIP.</li>\r\n<li style=\"text-align: justify;\">T&igrave;m hiểu, ph&acirc;n t&iacute;ch thị trường, c&aacute;c đối thủ cạnh tranh; chỉ đạo thực hiện c&aacute;c chiến dịch mở rộng thị trường kh&aacute;ch h&agrave;ng.</li>\r\n</ol>\r\n<p style=\"text-align: justify;\"><strong>Quyền lợi:</strong></p>\r\n<ol style=\"text-align: justify;\">\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p style=\"text-align: justify;\"><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li style=\"text-align: justify;\">Tốt nghiệp Cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh Quản trị kinh doanh, thương mại, kinh tế...</li>\r\n<li style=\"text-align: justify;\">Th&agrave;nh thạo Tin học văn ph&ograve;ng</li>\r\n<li style=\"text-align: justify;\">C&oacute; kỹ năng tổng hợp v&agrave; ph&acirc;n t&iacute;ch số liệu, lập b&aacute;o c&aacute;o thống k&ecirc;;</li>\r\n<li style=\"text-align: justify;\">C&oacute; kỹ năng quản l&yacute; sản phẩm h&agrave;ng h&oacute;a;</li>\r\n<li style=\"text-align: justify;\">Nhanh nhẹn, s&aacute;ng tạo, giao tiếp tốt.</li>\r\n<li style=\"text-align: justify;\">Trung thực, chịu kh&oacute;</li>\r\n<li style=\"text-align: justify;\">Đ&atilde; c&oacute; kinh nghiệm l&agrave;m việc &iacute;t nhất 06 th&aacute;ng ở vị tr&iacute; tương đương</li>\r\n</ol>\r\n</body>\r\n</html>'),
(38, NULL, '2019-11-04 02:25:11', '2019-11-04 07:42:13', 'Nhân viên Phát triển mặt bằng', 'nhan-vien-phat-trien-mat-bang', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '6 - 10 ', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa điểm l&agrave;m việc:</strong></p>\r\n<ul>\r\n<li>Trụ sở C&ocirc;ng ty : Số 9B Vũ Ngọc Phan, phường 13, quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>T&igrave;m kiếm, khảo s&aacute;t mặt bằng v&agrave; đ&agrave;m ph&aacute;n hợp đồng thu&ecirc; c&aacute;c mặt bằng kinh doanh ở c&aacute;c khu vực để ph&aacute;t triển chuỗi cửa h&agrave;ng, mở rộng kinh doanh khu vực HCM theo nhu cầu ph&aacute;t triển của c&ocirc;ng ty.</li>\r\n<li>T&igrave;m hiểu, ph&acirc;n t&iacute;ch, b&aacute;o c&aacute;o t&igrave;nh h&igrave;nh về thị trường mặt bằng thuộc khu vực phụ tr&aacute;ch cho cấp quản l&yacute;</li>\r\n<li>Giải quyết tất cả c&aacute;c vấn đề ph&aacute;t sinh (nếu c&oacute;) li&ecirc;n quan đến chủ nh&agrave; v&agrave; hợp đồng thu&ecirc; nh&agrave;.</li>\r\n<li>Thực hiện c&aacute;c c&ocirc;ng việc kh&aacute;c theo y&ecirc;u cầu của Quản l&yacute; trực tiếp.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 ( 8h00-11h30, 13h00-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h00-17h00).</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;.</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol start=\"1\" type=\"1\">\r\n<li>Nam, tuổi từ 22 trở l&ecirc;n</li>\r\n<li>L&agrave; người c&oacute; tr&aacute;ch nhiệm, nhiệt huyết v&agrave; tận tụy với c&ocirc;ng việc.</li>\r\n<li>Tự tin trong giao tiếp v&agrave; c&oacute; kỹ năng đ&agrave;m ph&aacute;n thương lương tốt</li>\r\n<li>Biết r&otilde; c&aacute;c tuyến đường tại khu vực HCM</li>\r\n<li>Ưu ti&ecirc;n ứng vi&ecirc;n kinh nghiệm từng l&agrave;m hoặc c&oacute; mối quan hệ với&nbsp; c&aacute;c C&ocirc;ng ty M&ocirc;i giới BĐS/ c&ocirc;ng việc chuy&ecirc;n t&igrave;m kiếm mặt bằng cho doanh nghiệp b&aacute;n lẻ.</li>\r\n</ol>\r\n</body>\r\n</html>'),
(39, NULL, '2019-11-04 02:28:31', '2019-11-04 08:37:28', 'Nhân viên mua hàng tiếng Trung', 'nhan-vien-mua-hang-tieng-trung', '[\"1\",\"2\",\"3\"]', 'Nhân viên', '8-15', '[\"1\",\"2\",\"3\"]', 'Khối Văn phòng', 4, 'Toàn thời gian', 'Tốt nghiệp Cao đẳng trở lên', '2020-12-31', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Địa Điểm L&agrave;m Việc</strong></p>\r\n<ul>\r\n<li>Trụ sở C&ocirc;ng ty: Số 9B Vũ Ngọc Phan, phường 13, quận B&igrave;nh Thạnh, TP HCM</li>\r\n<li>Văn ph&ograve;ng H&agrave; Nội</li>\r\n<li>Văn ph&ograve;ng Đ&agrave; Nẵng</li>\r\n</ul>\r\n<p><strong>Nhiệm vụ cụ thể:</strong></p>\r\n<ol>\r\n<li>Giao tiếp th&agrave;nh thạo tiếng Trung</li>\r\n<li>Dịch c&aacute;c t&agrave;i liệu văn bản li&ecirc;n quan đến sản phẩm, dịch vụ sang tiếng Trung v&agrave; ngược lại</li>\r\n<li>T&igrave;m kiếm NCC mới; thiết lập v&agrave; duy tr&igrave; mối quan hệ với NCC hiện tại, cập nhập th&ocirc;ng tin thị trường phản hồi của kh&aacute;ch h&agrave;ng, so s&aacute;nh, đ&aacute;nh gi&aacute;, nh&agrave; cung cấp đối thủ.</li>\r\n<li>Đ&agrave;m ph&aacute;n với nh&agrave; cung cấp về gi&aacute; cả, chất lượng, hợp đồng, c&aacute;ch thức thanh to&aacute;n, h&igrave;nh thức v&agrave; ng&agrave;y giao h&agrave;ng, theo d&otilde;i lộ tr&igrave;nh l&ocirc; h&agrave;ng nhập khẩu</li>\r\n<li>L&agrave;m việc với c&aacute;c bộ phận kỹ thuật, mua h&agrave;ng v&agrave; vật tư cho c&aacute;c loại vải v&agrave; phụ kiện từ c&aacute;c nh&agrave; cung cấp trong nước v&agrave; nước ngo&agrave;i.</li>\r\n</ol>\r\n<p><strong>Quyền lợi:</strong></p>\r\n<ol>\r\n<li>L&agrave;m việc theo giờ h&agrave;nh ch&iacute;nh từ Thứ 2 &ndash; Thứ 6 (8h00-11h30, 13h-17h30) v&agrave; thứ 7 đ&acirc;̀u ti&ecirc;n của tháng (8h30-11h30, 13h-17h)</li>\r\n<li>Hỗ trợ phụ cấp tiền cơm trưa, cấp thẻ giữ xe kh&ocirc;ng mất ph&iacute;</li>\r\n<li>Được cung cấp c&aacute;c trang thiết bị hiện đại cần thiết để n&acirc;ng cao hiệu quả l&agrave;m việc.</li>\r\n<li>L&agrave;m việc trong m&ocirc;i trường năng động.</li>\r\n<li>C&oacute; cơ hội được đ&agrave;o tạo n&acirc;ng cao nghiệp vụ thường xuy&ecirc;n.</li>\r\n<li>C&aacute;c chế độ ph&uacute;c lợi kh&aacute;c như : Nghỉ lễ, tết, ph&eacute;p năm&hellip;</li>\r\n<li>Ch&iacute;nh s&aacute;ch BHXH, BHYT v&agrave; c&aacute;c ph&uacute;c lợi kh&aacute;c theo đ&uacute;ng quy định của Luật lao động v&agrave; của C&ocirc;ng ty.</li>\r\n</ol>\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n<ol>\r\n<li>Tốt nghiệp Cao đẳng trở l&ecirc;n chuy&ecirc;n ng&agrave;nh li&ecirc;n quan</li>\r\n<li>Hiểu biết về Ph&aacute;p luật li&ecirc;n quan đến hoạt động kinh doanh, xuất nhập khẩu</li>\r\n<li>Cẩn thận, c&oacute; tr&aacute;ch nhiệm trong c&ocirc;ng việc</li>\r\n<li>Th&agrave;nh thạo Tin học văn ph&ograve;ng</li>\r\n<li>C&oacute; &iacute;t nhất 01 năm kinh nghiệm ở vị tr&iacute; tương đương</li>\r\n</ol>\r\n</body>\r\n</html>');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Client',
  `context_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `google_plus` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `deleted_at`, `created_at`, `updated_at`, `remember_token`, `name`, `email`, `password`, `type`, `context_id`, `content`, `image`, `facebook`, `twitter`, `google_plus`, `instagram`, `status`, `phone`) VALUES
(1, NULL, '2018-07-19 18:51:21', '2019-03-31 06:27:55', 'Zja7eUUNKhNYi6uSCJx8b9mhzrsHGxEhXzNcZ1E9BOxIH5oAUHvvDA0zT9JM', 'Admin', 'tuyendung.anzedo@gmail.com', '$2y$10$DoittzN9elPkFLrrfdnFC.gx2rRM3.d6LuYZr1St8kMF1kOXva4fS', 'Employee', 1, '', '', '', '', '', '', 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_dept_foreign` (`dept`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Indexes for table `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nganhnghes`
--
ALTER TABLE `nganhnghes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `organizations_name_unique` (`name`),
  ADD UNIQUE KEY `organizations_email_unique` (`email`),
  ADD KEY `organizations_assigned_to_foreign` (`assigned_to`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `posts_author_foreign` (`author`);

--
-- Indexes for table `product_labels`
--
ALTER TABLE `product_labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `re_comments`
--
ALTER TABLE `re_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Indexes for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliderimages`
--
ALTER TABLE `sliderimages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliderimages_key_foreign` (`key`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tracnghiems`
--
ALTER TABLE `tracnghiems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tracnghiems_type_tn_foreign` (`type_tn`);

--
-- Indexes for table `tracnghiem_options`
--
ALTER TABLE `tracnghiem_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tracnghiem_options_id_tracnghiem_foreign` (`id_tracnghiem`);

--
-- Indexes for table `tracnghiem_types`
--
ALTER TABLE `tracnghiem_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuyendungs`
--
ALTER TABLE `tuyendungs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tuyendungs_address_foreign` (`address`),
  ADD KEY `tuyendungs_career_foreign` (`career`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=409;

--
-- AUTO_INCREMENT for table `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `nganhnghes`
--
ALTER TABLE `nganhnghes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_labels`
--
ALTER TABLE `product_labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `re_comments`
--
ALTER TABLE `re_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=778;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433;

--
-- AUTO_INCREMENT for table `sliderimages`
--
ALTER TABLE `sliderimages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tracnghiems`
--
ALTER TABLE `tracnghiems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tracnghiem_options`
--
ALTER TABLE `tracnghiem_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tracnghiem_types`
--
ALTER TABLE `tracnghiem_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tuyendungs`
--
ALTER TABLE `tuyendungs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`);

--
-- Constraints for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `employees` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_author_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`);

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliderimages`
--
ALTER TABLE `sliderimages`
  ADD CONSTRAINT `sliderimages_key_foreign` FOREIGN KEY (`key`) REFERENCES `sliders` (`id`);

--
-- Constraints for table `tracnghiems`
--
ALTER TABLE `tracnghiems`
  ADD CONSTRAINT `tracnghiems_type_tn_foreign` FOREIGN KEY (`type_tn`) REFERENCES `tracnghiem_types` (`id`);

--
-- Constraints for table `tracnghiem_options`
--
ALTER TABLE `tracnghiem_options`
  ADD CONSTRAINT `tracnghiem_options_id_tracnghiem_foreign` FOREIGN KEY (`id_tracnghiem`) REFERENCES `tracnghiems` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
