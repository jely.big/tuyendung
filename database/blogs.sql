-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 23, 2019 lúc 11:41 AM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `blogs`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `backups`
--

DROP TABLE IF EXISTS `backups`;
CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `seo_title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `slug`, `parent`, `description`, `status`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, NULL, '2019-03-18 05:55:32', '2019-03-18 05:55:32', 'Lập trình', 'lap-trinh', 0, 'Lập trình', 1, '', '', ''),
(2, NULL, '2019-03-18 05:55:50', '2019-03-18 06:56:19', 'HTML', 'html', 1, 'HTML', 1, '', '', ''),
(3, NULL, '2019-03-18 05:56:02', '2019-03-18 06:56:13', 'CSS', 'css', 1, 'CSS', 1, '', '', ''),
(4, NULL, '2019-03-18 05:56:14', '2019-03-18 06:56:33', 'PHP', 'php', 1, 'PHP', 1, '', '', ''),
(5, NULL, '2019-03-18 05:56:27', '2019-03-18 06:56:25', 'Laravel', 'laravel', 1, 'Laravel', 1, '', '', ''),
(6, NULL, '2019-03-18 05:56:37', '2019-03-21 05:58:30', 'Công nghệ', 'cong-nghe', 0, 'Công nghệ', 1, '', '', ''),
(7, NULL, '2019-03-18 05:56:49', '2019-03-18 06:56:01', 'Android', 'android', 6, 'Android', 1, '', '', ''),
(8, NULL, '2019-03-18 05:56:59', '2019-03-18 06:56:07', 'IOS', 'ios', 6, 'IOS', 1, '', '', ''),
(9, NULL, '2019-03-18 05:57:06', '2019-03-18 05:57:06', 'Chuyện trò', 'chuyen-tro', 0, 'Chuyện trò', 1, '', '', ''),
(10, NULL, '2019-03-18 05:57:28', '2019-03-18 06:55:55', 'Tâm sự Coder', 'tam-su-coder', 9, 'Tâm sự Coder', 1, '', '', ''),
(11, NULL, '2019-03-18 05:57:38', '2019-03-21 07:17:28', 'Chia sẽ của bạn', 'chia-se-cua-ban', 9, 'Chia sẽ của bạn', 1, '', '', ''),
(12, NULL, '2019-03-18 05:57:44', '2019-03-21 07:17:04', 'Blogs', 'chia-se', 0, 'Blogs', 1, '', '', ''),
(13, NULL, '2019-03-18 05:57:56', '2019-03-18 06:45:33', 'Con người', 'con-nguoi', 12, 'Con người', 1, '', '', ''),
(14, NULL, '2019-03-18 05:58:08', '2019-03-18 06:46:06', 'Sức khỏe', 'suc-khoe', 12, 'Sức khỏe', 1, '', '', ''),
(15, NULL, '2019-03-18 05:58:18', '2019-03-18 06:45:59', 'Giáo dục', 'giao-duc', 12, 'Giáo dục', 1, '', '', ''),
(16, NULL, '2019-03-18 05:58:30', '2019-03-18 06:46:12', 'Xã hội', 'xa-hoi', 12, 'Xã hội', 1, '', '', ''),
(17, NULL, '2019-03-18 05:58:42', '2019-03-18 06:45:50', 'Du lịch', 'du-lich', 12, 'Du lịch', 1, '', '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fullname` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `id_blog` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `deleted_at`, `created_at`, `updated_at`, `fullname`, `email`, `phone`, `content`, `id_blog`, `id_user`, `status`) VALUES
(1, NULL, '2019-03-31 05:59:35', '2019-03-31 05:59:35', '1', '1@gmail.com', '1', '1', '10', NULL, 1),
(2, NULL, '2019-03-31 06:05:58', '2019-03-31 06:05:58', '3', '3@gmail.com', '3', '3', '10', NULL, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contacts`
--

INSERT INTO `contacts` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `email`, `phone`, `content`, `status`) VALUES
(1, NULL, '2019-03-17 03:57:53', '2019-03-17 03:57:53', 'Nguyen Phuoc Thu', 'jely.big@gmail.com', '0359399320', '123 123 123 123', 0),
(2, NULL, '2019-03-17 03:58:32', '2019-03-17 03:58:32', 'Nguyen Phuoc Thu', 'jely.big@gmail.com', '0359399320', '123 123 123 123', 0),
(3, NULL, '2019-03-17 03:59:36', '2019-03-17 03:59:36', 'Nguyen Phuoc Thu', 'jely.big@gmail.com', '0359399320', 'fdsfdsfdsfsfsdfsdfdsfdsfsdfsd', 0),
(4, NULL, '2019-03-17 04:00:09', '2019-03-17 04:00:09', 'Nguyen Phuoc Thu', 'jely.big@gmail.com', '0359399320', 'fdsfdsfsdfsdfsdfdsfsdfsdfsd', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 'Mod', '[\"Mod\"]', '#2703F1', NULL, '2019-04-07 06:44:40', '2019-04-07 06:44:40'),
(3, 'User', '[\"User\"]', '#00FF00', NULL, '2019-04-07 06:45:18', '2019-04-07 06:45:18'),
(4, 'Client', '[\"Client\"]', '#FFFF00', NULL, '2019-04-07 06:45:32', '2019-04-07 06:45:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Male',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL DEFAULT '1990-01-01',
  `date_hire` date NOT NULL,
  `date_left` date NOT NULL DEFAULT '1990-01-01',
  `salary_cur` decimal(15,3) NOT NULL DEFAULT '0.000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `mobile`, `mobile2`, `email`, `dept`, `city`, `address`, `about`, `date_birth`, `date_hire`, `date_left`, `salary_cur`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Super Admin', 'Male', '8888888888', '', 'admin@localhost.com', 1, 'Pune', 'Karve nagar, Pune 411030', 'About user / biography', '2018-07-20', '2018-07-20', '2018-07-20', '0.000', NULL, '2018-07-19 18:51:21', '2018-07-19 18:51:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `evaluates`
--

DROP TABLE IF EXISTS `evaluates`;
CREATE TABLE `evaluates` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `html` varchar(9999) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `la_configs`
--

DROP TABLE IF EXISTS `la_configs`;
CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `la_configs`
--

INSERT INTO `la_configs` (`id`, `key`, `section`, `value`, `created_at`, `updated_at`) VALUES
(1, 'sitename', '', 'Pixels', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(2, 'sitename_part1', '', 'Pixels', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(3, 'sitename_part2', '', '1.0', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(4, 'sitename_short', '', 'JS', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(5, 'site_description', '', 'Copy and complete by Jely Small', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(6, 'sidebar_search', '', '0', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(7, 'show_messages', '', '0', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(8, 'show_notifications', '', '0', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(9, 'show_tasks', '', '0', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(10, 'show_rightsidebar', '', '0', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(11, 'skin', '', 'skin-green', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(12, 'layout', '', 'layout-top-nav', '2018-07-19 18:51:06', '2019-03-31 06:17:54'),
(13, 'default_email', '', 'jely.big@gmail.com', '2018-07-19 18:51:06', '2019-03-31 06:17:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `la_menus`
--

DROP TABLE IF EXISTS `la_menus`;
CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(1, 'Team', '#', 'fa-group', 'custom', 71, 3, '2018-07-19 18:51:06', '2019-02-19 00:41:55'),
(2, 'Users', 'users', 'fa-group', 'module', 1, 1, '2018-07-19 18:51:06', '2018-07-22 03:54:58'),
(6, 'Roles', 'roles', 'fa-user-plus', 'module', 1, 2, '2018-07-19 18:51:06', '2018-07-22 03:54:58'),
(8, 'Permissions', 'permissions', 'fa-magic', 'module', 1, 3, '2018-07-19 18:51:06', '2018-07-22 03:54:58'),
(22, 'Đa phương tiện', 'media-management', 'fa-file-image-o', 'custom', 32, 3, '2018-09-02 00:39:19', '2019-02-19 00:41:59'),
(25, 'Categories', 'categories', 'fa fa-bars', 'module', 36, 2, '2018-09-06 14:45:06', '2018-10-21 00:32:40'),
(26, 'Posts', 'posts', 'fa fa-file-powerpoint-o', 'module', 36, 1, '2018-09-08 22:40:09', '2018-10-21 00:32:37'),
(27, 'Settings', 'settings', 'fa fa-cog', 'module', 71, 2, '2018-09-15 02:02:07', '2019-02-19 00:41:53'),
(29, 'Menugroups', 'menugroups', 'fa fa-film', 'module', 32, 1, '2018-09-24 01:03:10', '2018-09-25 20:23:33'),
(31, 'Menulists', 'menulists', 'fa fa-cube', 'module', 32, 2, '2018-09-24 01:38:57', '2018-09-25 20:23:33'),
(32, 'Menu', '#', 'fa-align-justify', 'custom', 0, 5, '2018-09-25 20:23:16', '2019-02-27 02:23:37'),
(33, 'Widget', '#', 'fa-cube', 'custom', 71, 1, '2018-09-28 18:35:55', '2019-02-19 00:41:51'),
(34, 'Pages', 'pages', 'fa fa-cube', 'module', 36, 3, '2018-10-04 02:37:46', '2018-12-05 21:28:59'),
(36, 'Bài viết', '#', 'fa-cube', 'custom', 0, 3, '2018-10-21 00:32:35', '2019-02-27 02:23:37'),
(40, 'Ảnh Slider', 'sliderimages', 'fa-cube', 'custom', 33, 1, '2018-11-21 19:00:29', '2018-11-21 19:00:59'),
(41, 'Sliders', 'sliders', 'fa-cube', 'custom', 33, 2, '2018-11-21 19:00:49', '2018-11-21 19:01:34'),
(43, 'Danh mục sản phẩm', 'productcategories', 'fa-align-justify', 'custom', 54, 1, '2018-11-26 21:03:02', '2019-02-27 02:38:17'),
(44, 'Tin liên hệ', 'contacts', 'fa-commenting', 'custom', 53, 1, '2018-11-26 21:03:33', '2019-01-01 01:23:50'),
(45, 'Sản phẩm', 'products', 'fa-cube', 'custom', 54, 3, '2018-11-26 23:53:24', '2019-02-27 02:38:18'),
(46, 'Đơn hàng', 'orders', 'fa-cube', 'custom', 54, 5, '2018-11-28 21:23:11', '2019-02-27 02:38:20'),
(51, 'Cầu hình', 'settings', 'fa-cog', 'custom', 27, 1, '2018-12-05 21:27:55', '2018-12-05 21:28:08'),
(53, 'Mail', '#', 'fa-qq', 'custom', 0, 4, '2019-01-01 01:17:17', '2019-02-27 02:23:37'),
(56, 'Mail nhận tin tức', 'mail_posts', 'fa-paper-plane', 'custom', 53, 2, '2019-01-01 01:28:57', '2019-01-01 01:29:02'),
(64, 'Đối tác', 'partners', 'fa-adn', 'custom', 36, 7, '2019-02-14 19:03:20', '2019-02-14 19:03:30'),
(70, 'Thương hiệu', 'product_labels', 'fa-firefox', 'custom', 54, 2, '2019-02-17 18:59:26', '2019-02-27 02:38:18'),
(76, 'Quản lý', '#', 'fa-cube', 'custom', 0, 1, '2019-02-19 00:43:44', '2019-02-27 02:23:37'),
(77, 'Tài khoản', 'users', 'fa-cube', 'custom', 76, 1, '2019-02-19 00:44:39', '2019-02-19 00:44:46'),
(79, 'Cài đặt', 'settings', 'fa-bank', 'custom', 0, 6, '2019-02-26 20:00:03', '2019-02-27 02:23:37'),
(82, 'Key Slide', 'sliders', 'fa-cube', 'custom', 32, 4, '2019-02-27 02:23:07', '2019-02-27 02:23:37'),
(83, 'Ảnh slide', 'sliderimages', 'fa-cube', 'custom', 32, 5, '2019-02-27 02:23:22', '2019-02-27 02:23:37'),
(84, 'Mã giảm giá', 'coupons', 'fa-cube', 'custom', 54, 4, '2019-02-27 02:38:06', '2019-02-27 02:38:20'),
(86, 'Comments', 'comments', 'fa fa-certificate', 'module', 0, 0, '2019-03-26 06:41:22', '2019-03-26 06:41:22'),
(87, 'Re_Comments', 're_comments', 'fa fa-bug', 'module', 0, 0, '2019-03-30 07:47:20', '2019-03-30 07:47:20'),
(88, 'Roles', 'roles', 'fa-user-plus', 'module', 0, 0, '2019-04-07 06:11:25', '2019-04-07 06:11:25'),
(89, 'Employees', 'employees', 'fa-group', 'module', 0, 0, '2019-04-07 06:11:27', '2019-04-07 06:11:27'),
(90, 'Departments', 'departments', 'fa-tags', 'module', 0, 0, '2019-04-07 06:11:28', '2019-04-07 06:11:28'),
(91, 'Permissions', 'permissions', 'fa-magic', 'module', 0, 0, '2019-04-07 06:11:30', '2019-04-07 06:11:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mail_posts`
--

DROP TABLE IF EXISTS `mail_posts`;
CREATE TABLE `mail_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `mail_posts`
--

INSERT INTO `mail_posts` (`id`, `deleted_at`, `created_at`, `updated_at`, `email`) VALUES
(1, NULL, '2019-03-23 05:42:49', '2019-03-23 05:42:49', 'test@gmail.com'),
(2, NULL, '2019-03-23 05:46:54', '2019-03-23 05:46:54', 'admin@localhost.com');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menugroups`
--

DROP TABLE IF EXISTS `menugroups`;
CREATE TABLE `menugroups` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `key` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menugroups`
--

INSERT INTO `menugroups` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `key`, `status`, `description`) VALUES
(1, NULL, '2018-09-25 20:22:11', '2018-11-19 19:41:42', 'Main Menu Vietnamese', 'main_menu', 1, 'Mo ta '),
(2, '2018-10-24 11:42:04', '2018-09-25 20:22:28', '2018-10-24 11:42:04', 'Main Menu English', 'main_menu_en', 1, ''),
(3, '2018-09-26 22:26:13', '2018-09-26 22:26:07', '2018-09-26 22:26:13', 'Test Menu', 'test_menu_vi', 0, ''),
(4, NULL, '2018-10-04 04:08:31', '2018-10-04 04:08:31', 'Footer Link Vietnamese', 'footer_link_vi', 1, ''),
(5, '2018-10-24 11:42:07', '2018-10-04 04:08:47', '2018-10-24 11:42:07', 'Footer Link English', 'footer_link_en', 1, ''),
(6, '2018-10-24 11:42:30', '2018-10-24 11:42:23', '2018-10-24 11:42:30', 'đasa', 'dsadsa', 1, 'dsadas');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menulists`
--

DROP TABLE IF EXISTS `menulists`;
CREATE TABLE `menulists` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `menugroup` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `icon` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menulists`
--

INSERT INTO `menulists` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `url`, `menugroup`, `parent`, `icon`, `status`, `sort`) VALUES
(1, NULL, '2019-03-14 07:30:13', '2019-03-14 07:30:13', 'Trang chủ', '/', 1, 0, '', 1, 1),
(2, NULL, '2019-03-14 07:31:57', '2019-03-21 07:24:35', 'Lập trình', '/lap-trinh.html', 1, 0, '', 1, 2),
(3, NULL, '2019-03-14 07:32:12', '2019-03-21 07:24:42', 'Công nghệ', '/cong-nghe.html', 1, 0, '', 1, 3),
(4, NULL, '2019-03-14 07:32:39', '2019-03-21 07:24:06', 'Chuyện trò', '/chuyen-tro.html', 1, 0, '', 1, 4),
(5, NULL, '2019-03-14 07:33:07', '2019-03-17 04:01:28', 'Giới thiệu', 'gioi-thieu.html', 1, 0, '', 1, 6),
(6, NULL, '2019-03-14 07:33:17', '2019-03-17 04:01:13', 'Liên hệ', 'lien-he.html', 1, 0, '', 1, 7),
(7, NULL, '2019-03-14 07:36:29', '2019-03-21 06:42:07', 'HTML', '/lap-trinh/html', 1, 2, '', 1, 1),
(8, NULL, '2019-03-14 07:35:28', '2019-03-21 06:42:20', 'Android', '/cong-nghe/android', 1, 3, '', 1, 1),
(9, NULL, '2019-03-14 07:35:36', '2019-03-21 06:42:25', 'IOS', '/cong-nghe/ios', 1, 3, '', 1, 2),
(10, NULL, '2019-03-14 07:36:39', '2019-03-21 06:41:32', 'CSS', '/lap-trinh/css', 1, 2, '', 1, 2),
(11, NULL, '2019-03-14 07:34:59', '2019-03-21 06:42:13', 'PHP', '/lap-trinh/php', 1, 2, '', 1, 3),
(12, NULL, '2019-03-14 07:36:48', '2019-03-21 06:42:17', 'Laravel', '/lap-trinh/laravel', 1, 2, '', 1, 4),
(13, NULL, '2019-03-14 08:30:31', '2019-03-21 07:25:01', 'Blogs', '/chia-se.html', 1, 0, '', 1, 5),
(14, NULL, '2019-03-14 08:37:04', '2019-03-21 06:43:08', 'Con người', '/chia-se/con-nguoi', 1, 13, '', 1, 1),
(15, NULL, '2019-03-14 08:32:47', '2019-03-21 06:43:15', 'Sức khỏe', '/chia-se/suc-khoe', 1, 13, '', 1, 2),
(16, NULL, '2019-03-14 08:31:56', '2019-03-21 06:43:20', 'Giáo dục', '/chia-se/giao-duc', 1, 13, '', 1, 3),
(17, NULL, '2019-03-14 08:31:40', '2019-03-21 06:43:26', 'Xã hội', '/chia-se/xa-hoi', 1, 13, '', 1, 4),
(18, NULL, '2019-03-14 08:32:35', '2019-03-21 06:43:36', 'Du lịch', '/chia-se/du-lich', 1, 13, '', 1, 5),
(19, NULL, '2019-03-15 04:53:35', '2019-03-21 06:42:53', 'Tâm sự Coder', '/chuyen-tro/tam-su-coder', 1, 4, '', 1, 1),
(20, NULL, '2019-03-15 04:56:13', '2019-03-21 06:42:59', 'Chia sẽ của bạn', '/chuyen-tro/chia-se-cua-ban', 1, 4, '', 1, 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2018-07-19 18:50:54', '2019-02-18 18:34:39'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2018-07-19 18:50:55', '2018-07-19 18:51:06'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2018-07-19 18:50:56', '2018-07-19 18:51:06'),
(4, 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', 1, '2018-07-19 18:50:56', '2018-07-19 18:51:06'),
(5, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2018-07-19 18:50:57', '2018-07-19 18:51:06'),
(6, 'Organizations', 'Organizations', 'organizations', 'name', 'Organization', 'OrganizationsController', 'fa-university', 1, '2018-07-19 18:50:59', '2018-07-19 18:51:06'),
(7, 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', 1, '2018-07-19 18:51:00', '2018-07-19 18:51:06'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2018-07-19 18:51:01', '2018-07-19 18:51:06'),
(24, 'Languages', 'Languages', 'languages', 'name', 'Language', 'LanguagesController', 'fa-language', 1, '2018-09-02 01:11:04', '2018-09-02 01:14:24'),
(26, 'Categories', 'Categories', 'categories', 'name', 'Category', 'CategoriesController', 'fa-bars', 1, '2018-09-06 14:41:20', '2018-09-06 14:45:06'),
(27, 'Posts', 'Posts', 'posts', 'title', 'Post', 'PostsController', 'fa-file-powerpoint-o', 1, '2018-09-08 20:39:16', '2018-09-08 22:40:09'),
(28, 'Settings', 'Settings', 'settings', 'key', 'Setting', 'SettingsController', 'fa-cog', 1, '2018-09-15 02:01:13', '2018-09-15 02:02:07'),
(34, 'Menugroups', 'Menugroups', 'menugroups', 'name', 'Menugroup', 'MenugroupsController', 'fa-film', 1, '2018-09-24 00:58:19', '2018-09-24 01:03:10'),
(38, 'Menulists', 'Menulists', 'menulists', 'name', 'Menulist', 'MenulistsController', 'fa-cube', 1, '2018-09-24 01:38:40', '2018-09-24 01:38:57'),
(39, 'Pages', 'Pages', 'pages', 'title', 'Page', 'PagesController', 'fa-cube', 1, '2018-10-04 02:28:07', '2018-10-04 02:37:46'),
(42, 'Contacts', 'Contacts', 'contacts', 'name', 'Contact', 'ContactsController', 'fa-comment-o', 1, '2018-11-19 18:23:59', '2018-11-19 18:26:38'),
(43, 'Sliders', 'Sliders', 'sliders', 'key', 'Slider', 'SlidersController', 'fa-cube', 1, '2018-11-21 18:53:02', '2018-11-21 18:55:24'),
(44, 'SliderImages', 'SliderImages', 'sliderimages', 'key', 'SliderImage', 'SliderImagesController', 'fa-cube', 1, '2018-11-21 18:57:50', '2018-11-21 18:59:49'),
(56, 'Mail_posts', 'Mail_posts', 'mail_posts', 'email', 'Mail_post', 'Mail_postsController', 'fa-send-o', 1, '2019-01-01 01:22:48', '2019-01-01 01:23:40'),
(61, 'Partners', 'Partners', 'partners', 'title', 'Partner', 'PartnersController', 'fa-adn', 1, '2019-02-14 19:00:33', '2019-02-14 19:03:04'),
(65, 'Product_labels', 'Product_labels', 'product_labels', 'name', 'Product_label', 'Product_labelsController', 'fa-circle-o', 1, '2019-02-17 18:55:11', '2019-02-17 18:57:04'),
(67, 'Comments', 'Comments', 'comments', 'fullname', 'Comment', 'CommentsController', 'fa-certificate', 1, '2019-03-26 06:37:56', '2019-03-26 06:41:22'),
(68, 'Re_Comments', 'Re_Comments', 're_comments', 'cmt_id', 'Re_Comment', 'Re_CommentsController', 'fa-bug', 1, '2019-03-30 07:44:44', '2019-03-30 07:47:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `module_fields`
--

DROP TABLE IF EXISTS `module_fields`;
CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT '0',
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(5, 'type', 'User Type', 1, 7, 0, 'Employee', 0, 0, 0, '[\"Employee\",\"Client\"]', 0, '2018-07-19 18:50:54', '2018-07-19 18:50:54'),
(6, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(7, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(8, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(9, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(10, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(11, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(12, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, '2018-07-19 18:50:55', '2018-07-19 18:50:55'),
(13, 'name', 'Name', 3, 16, 1, '', 1, 250, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(14, 'tags', 'Tags', 3, 20, 0, '[]', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(15, 'color', 'Color', 3, 19, 0, '', 0, 50, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(16, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(17, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(18, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(19, 'mobile', 'Mobile', 4, 14, 0, '', 10, 20, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(20, 'mobile2', 'Alternative Mobile', 4, 14, 0, '', 10, 20, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(21, 'email', 'Email', 4, 8, 1, '', 5, 250, 1, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(22, 'dept', 'Department', 4, 7, 0, '0', 0, 0, 1, '@departments', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(23, 'city', 'City', 4, 19, 0, '', 0, 50, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(24, 'address', 'Address', 4, 1, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(27, 'date_hire', 'Hiring Date', 4, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(28, 'date_left', 'Resignation Date', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(29, 'salary_cur', 'Current Salary', 4, 6, 0, '0.0', 0, 2, 0, '', 0, '2018-07-19 18:50:56', '2018-07-19 18:50:56'),
(30, 'name', 'Name', 5, 16, 1, '', 1, 250, 1, '', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(31, 'display_name', 'Display Name', 5, 19, 0, '', 0, 250, 1, '', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(32, 'description', 'Description', 5, 21, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(33, 'parent', 'Parent Role', 5, 7, 0, '1', 0, 0, 0, '@roles', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(34, 'dept', 'Department', 5, 7, 0, '1', 0, 0, 0, '@departments', 0, '2018-07-19 18:50:57', '2018-07-19 18:50:57'),
(35, 'name', 'Name', 6, 16, 1, '', 5, 250, 1, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(36, 'email', 'Email', 6, 8, 1, '', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(37, 'phone', 'Phone', 6, 14, 0, '', 0, 20, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(38, 'website', 'Website', 6, 23, 0, 'http://', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(39, 'assigned_to', 'Assigned to', 6, 7, 0, '0', 0, 0, 0, '@employees', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(40, 'connect_since', 'Connected Since', 6, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(41, 'address', 'Address', 6, 1, 0, '', 0, 1000, 1, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(42, 'city', 'City', 6, 19, 0, '', 0, 250, 1, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(43, 'description', 'Description', 6, 21, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(44, 'profile_image', 'Profile Image', 6, 12, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(45, 'profile', 'Company Profile', 6, 9, 0, '', 0, 250, 0, '', 0, '2018-07-19 18:50:59', '2018-07-19 18:50:59'),
(46, 'name', 'Name', 7, 16, 1, '', 0, 250, 1, '', 0, '2018-07-19 18:51:00', '2018-07-19 18:51:00'),
(47, 'file_name', 'File Name', 7, 19, 1, '', 0, 250, 1, '', 0, '2018-07-19 18:51:00', '2018-07-19 18:51:00'),
(48, 'backup_size', 'File Size', 7, 19, 0, '0', 0, 10, 1, '', 0, '2018-07-19 18:51:00', '2018-07-19 18:51:00'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, '2018-07-19 18:51:01', '2018-07-19 18:51:01'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, '2018-07-19 18:51:01', '2018-07-19 18:51:01'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, '2018-07-19 18:51:01', '2018-07-19 18:51:01'),
(108, 'name', 'Name', 26, 22, 0, '', 0, 256, 1, '', 0, '2018-09-06 14:42:11', '2018-11-26 20:53:30'),
(109, 'slug', 'Slug', 26, 22, 1, '', 0, 256, 1, '', 0, '2018-09-06 14:42:26', '2018-09-09 18:32:28'),
(110, 'parent', 'Parent name', 26, 7, 0, '0', 0, 0, 0, '@categories', 0, '2018-09-06 14:43:10', '2019-03-18 05:53:52'),
(111, 'description', 'Description', 26, 21, 0, '', 0, 0, 0, '', 0, '2018-09-06 14:43:31', '2018-09-06 14:43:31'),
(112, 'status', 'Status', 26, 2, 0, '1', 0, 0, 0, '', 0, '2018-09-06 14:43:47', '2018-09-06 14:43:47'),
(113, 'seo_title', 'SEO Title', 26, 22, 0, '', 0, 256, 0, '', 0, '2018-09-06 14:44:05', '2018-09-06 14:44:05'),
(114, 'seo_description', 'SEO Description', 26, 21, 0, '', 0, 0, 0, '', 0, '2018-09-06 14:44:24', '2018-09-06 14:44:24'),
(115, 'seo_keywords', 'SEO Keywords', 26, 21, 0, '', 0, 0, 0, '', 0, '2018-09-06 14:44:54', '2018-09-06 14:44:54'),
(116, 'lang', 'Language', 27, 7, 0, '', 0, 0, 1, '@languages', 0, '2018-09-08 22:33:13', '2018-09-08 22:33:13'),
(117, 'title', 'Title', 27, 22, 0, '', 0, 256, 1, '', 0, '2018-09-08 22:35:12', '2018-09-08 22:35:12'),
(118, 'slug', 'Slug', 27, 22, 1, '', 0, 256, 1, '', 0, '2018-09-08 22:35:25', '2018-09-08 22:35:25'),
(119, 'content', 'Content', 27, 26, 0, '', 0, 0, 0, '', 0, '2018-09-08 22:35:41', '2018-09-08 22:35:41'),
(120, 'categories', 'Categories', 27, 15, 0, '', 0, 0, 1, '@categories', 0, '2018-09-08 22:36:14', '2018-09-08 22:36:14'),
(121, 'thumbnail', 'Thumbnail', 27, 25, 0, '', 0, 0, 0, '', 0, '2018-09-08 22:36:39', '2018-09-08 22:36:39'),
(122, 'author', 'Author', 27, 7, 0, '', 0, 0, 0, '@users', 0, '2018-09-08 22:37:12', '2018-09-08 22:37:12'),
(123, 'seo_title', 'Seo Title', 27, 22, 0, '', 0, 256, 0, '', 0, '2018-09-08 22:38:25', '2018-09-08 22:38:25'),
(124, 'seo_description', 'Seo Description', 27, 21, 0, '', 0, 256, 0, '', 0, '2018-09-08 22:38:38', '2018-09-08 22:39:17'),
(125, 'seo_keywords', 'Seo Keywords', 27, 21, 0, '', 0, 0, 0, '', 0, '2018-09-08 22:39:09', '2018-09-08 22:39:09'),
(126, 'tags', 'Tags', 27, 20, 0, '', 0, 256, 0, 'null', 0, '2018-09-08 22:39:59', '2018-09-08 22:39:59'),
(127, 'status', 'Status', 27, 2, 0, '1', 0, 0, 1, '', 0, '2018-09-08 22:46:22', '2018-09-08 22:46:22'),
(128, 'key', 'Key', 28, 22, 1, '', 0, 256, 1, '', 0, '2018-09-15 02:01:45', '2018-09-15 02:01:45'),
(129, 'value', 'Value', 28, 22, 0, '', 0, 256, 1, '', 0, '2018-09-15 02:02:04', '2018-09-15 02:02:04'),
(149, 'name', 'Name', 34, 22, 0, '', 0, 256, 1, '', 0, '2018-09-24 00:58:57', '2018-09-24 00:58:57'),
(150, 'key', 'Menu key', 34, 22, 1, '', 0, 256, 1, '', 0, '2018-09-24 00:59:22', '2018-09-24 00:59:22'),
(152, 'status', 'Status', 34, 2, 0, '', 0, 0, 1, '', 0, '2018-09-24 01:02:33', '2018-09-24 01:02:33'),
(153, 'description', 'Descriptions', 34, 21, 0, '', 0, 0, 0, '', 0, '2018-09-24 01:03:04', '2018-09-24 01:03:04'),
(160, 'name', 'Name', 38, 22, 0, '', 0, 256, 1, '', 0, '2018-09-24 01:38:53', '2018-09-24 01:38:53'),
(161, 'url', 'Url', 38, 22, 0, '', 0, 256, 0, '', 0, '2018-09-24 01:39:27', '2018-09-24 01:39:27'),
(162, 'menugroup', 'Menu Group', 38, 7, 0, '', 0, 0, 1, '@menugroups', 0, '2018-09-24 01:39:42', '2018-09-24 01:39:42'),
(163, 'parent', 'Parent', 38, 7, 0, '', 0, 0, 0, '@menulists', 0, '2018-09-24 01:40:02', '2018-09-24 01:40:02'),
(164, 'icon', 'Icon', 38, 22, 0, '', 0, 256, 0, '', 0, '2018-09-24 01:40:15', '2018-09-24 01:40:15'),
(165, 'status', 'Status', 38, 2, 0, '1', 0, 0, 1, '', 0, '2018-09-24 01:40:38', '2018-09-24 01:40:38'),
(166, 'sort', 'Sort', 38, 13, 0, '0', 0, 11, 1, '', 0, '2018-09-24 01:41:07', '2018-09-24 01:41:07'),
(167, 'title', 'Title', 39, 22, 0, '', 0, 256, 1, '', 0, '2018-10-04 02:28:32', '2018-10-04 02:28:32'),
(168, 'slug', 'Slug Url', 39, 22, 0, '', 0, 256, 1, '', 0, '2018-10-04 02:28:49', '2018-10-04 02:28:49'),
(170, 'contetn', 'Content', 39, 26, 0, '', 0, 0, 0, '', 0, '2018-10-04 02:29:26', '2018-10-04 02:29:26'),
(171, 'seo_title', 'Seo Title', 39, 22, 0, '', 0, 256, 0, '', 0, '2018-10-04 02:37:05', '2018-10-04 02:37:05'),
(172, 'seo_description', 'Seo Description', 39, 21, 0, '', 0, 0, 0, '', 0, '2018-10-04 02:37:24', '2018-10-04 02:37:24'),
(173, 'seo_keywords', 'Seo Keywords', 39, 21, 0, '', 0, 0, 0, '', 0, '2018-10-04 02:37:39', '2018-10-04 02:37:39'),
(176, 'name', 'Họ và tên', 42, 22, 0, '', 0, 256, 1, '', 0, '2018-11-19 18:24:29', '2018-11-19 18:24:29'),
(177, 'email', 'Email', 42, 8, 0, '', 0, 256, 1, '', 0, '2018-11-19 18:25:23', '2018-11-19 18:25:23'),
(178, 'phone', 'Số điện thoại', 42, 22, 0, '', 0, 256, 0, '', 0, '2018-11-19 18:25:43', '2018-11-19 18:25:43'),
(179, 'content', 'Nội dung', 42, 22, 0, '', 0, 256, 0, '', 0, '2018-11-19 18:26:10', '2018-11-19 18:26:10'),
(180, 'status', 'Trạng thái', 42, 2, 0, '0', 0, 0, 0, '', 0, '2018-11-19 18:26:34', '2018-11-19 18:26:34'),
(181, 'key', 'key', 43, 22, 1, '', 0, 256, 1, '', 0, '2018-11-21 18:53:50', '2018-11-21 18:53:50'),
(185, 'status', 'Trạng thái', 43, 2, 0, '1', 0, 0, 0, '', 0, '2018-11-21 18:57:33', '2018-11-21 18:57:33'),
(186, 'key', 'Key', 44, 7, 0, '', 0, 0, 1, '@sliders', 0, '2018-11-21 18:58:07', '2018-11-21 18:58:07'),
(187, 'image', 'Hình ảnh', 44, 25, 0, '', 0, 0, 1, '', 0, '2018-11-21 18:58:27', '2019-03-01 22:14:32'),
(188, 'title', 'Tiêu đề', 44, 22, 0, '', 0, 256, 1, '', 0, '2018-11-21 18:58:44', '2018-11-21 18:58:44'),
(189, 'content', 'Mô tả', 44, 21, 0, '', 0, 0, 0, '', 0, '2018-11-21 18:59:18', '2018-11-21 19:58:05'),
(190, 'html', 'Html Content', 44, 21, 0, '', 0, 0, 0, '', 0, '2018-11-21 18:59:36', '2018-11-21 19:57:51'),
(252, 'email', 'Email', 56, 22, 0, '', 0, 256, 1, '', 0, '2019-01-01 01:22:58', '2019-01-01 01:22:58'),
(271, 'title', 'Tiêu đề', 61, 22, 0, '', 0, 256, 0, '', 0, '2019-02-14 19:00:44', '2019-02-14 19:05:43'),
(272, 'image', 'Hình ảnh', 61, 25, 0, '', 0, 0, 1, '', 0, '2019-02-14 19:01:50', '2019-02-14 19:06:06'),
(286, 'name', 'Tên nhãn', 65, 22, 0, '', 0, 256, 0, '', 0, '2019-02-17 18:55:23', '2019-02-17 18:56:23'),
(287, 'key', 'Key nhãn', 65, 22, 0, '', 0, 256, 0, '', 0, '2019-02-17 18:56:39', '2019-02-17 18:56:39'),
(289, 'content', 'Tự sự về bản thân', 1, 21, 0, '', 0, 0, 0, '', 0, '2019-03-21 08:26:03', '2019-03-21 08:26:03'),
(290, 'image', 'Hình ảnh', 1, 25, 0, '', 0, 0, 0, '', 0, '2019-03-21 08:26:19', '2019-03-21 08:26:19'),
(291, 'facebook', 'Facebook', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:27:05', '2019-03-21 08:27:05'),
(292, 'twitter', 'Twitter', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:29:37', '2019-03-21 08:29:37'),
(293, 'google_plus', 'Google_plus', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:29:49', '2019-03-21 08:29:49'),
(294, 'instagram', 'Instagram', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-21 08:29:58', '2019-03-21 08:29:58'),
(295, 'status', 'Trạng thái', 1, 13, 0, '1', 0, 11, 0, '', 0, '2019-03-21 08:30:46', '2019-03-21 08:30:46'),
(299, 'fullname', 'Họ và tên', 67, 22, 0, '', 0, 256, 0, '', 3, '2019-03-26 06:38:13', '2019-03-26 06:38:13'),
(300, 'email', 'Email', 67, 8, 0, '', 0, 256, 0, '', 4, '2019-03-26 06:38:38', '2019-03-26 06:38:38'),
(301, 'phone', 'Số điện thoại', 67, 22, 0, '', 0, 256, 0, '', 5, '2019-03-26 06:38:53', '2019-03-26 06:38:53'),
(302, 'content', 'Nội dung bình luận', 67, 21, 0, '', 0, 256, 0, '', 6, '2019-03-26 06:39:10', '2019-03-30 07:47:40'),
(303, 'id_blog', 'Mã bài viết', 67, 22, 0, '', 0, 256, 0, '', 1, '2019-03-26 06:40:37', '2019-03-27 07:32:44'),
(304, 'id_user', 'Mã người dùng', 67, 22, 0, '', 0, 256, 0, '', 2, '2019-03-26 06:40:55', '2019-03-27 07:32:56'),
(305, 'cmt_id', 'Mã comment', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:45:04', '2019-03-30 07:45:32'),
(306, 'blog_id', 'Mã bài viết', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:45:25', '2019-03-30 07:45:25'),
(307, 'full_name', 'Họ và tên', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:45:59', '2019-03-30 07:45:59'),
(308, 'phone_number', 'Số điện thoại', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:46:17', '2019-03-30 07:46:17'),
(309, 'content_re', 'Nội dung', 68, 21, 0, '', 0, 0, 0, '', 0, '2019-03-30 07:46:36', '2019-03-30 07:46:36'),
(310, 'user_id', 'Mã người dùng', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 07:47:13', '2019-03-30 07:47:13'),
(311, 'status', 'Trạng thái', 67, 2, 0, '1', 0, 0, 0, '', 7, '2019-03-30 07:48:40', '2019-03-30 08:02:43'),
(312, 'status', 'Trạng thái', 68, 2, 0, '1', 0, 0, 0, '', 0, '2019-03-30 07:50:36', '2019-03-30 08:02:28'),
(313, 'email_re', 'Email', 68, 22, 0, '', 0, 256, 0, '', 0, '2019-03-30 09:46:53', '2019-03-30 09:46:53'),
(314, 'phone', 'Số điện thoại', 1, 22, 0, '', 0, 256, 0, '', 0, '2019-03-31 07:31:55', '2019-03-31 07:31:55');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `module_field_types`
--

DROP TABLE IF EXISTS `module_field_types`;
CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(2, 'Checkbox', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(3, 'Currency', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(4, 'Date', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(5, 'Datetime', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(6, 'Decimal', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(7, 'Dropdown', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(8, 'Email', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(9, 'File', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(10, 'Float', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(11, 'HTML', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(12, 'Image', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(13, 'Integer', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(14, 'Mobile', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(15, 'Multiselect', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(16, 'Name', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(17, 'Password', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(18, 'Radio', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(19, 'String', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(20, 'Taginput', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(21, 'Textarea', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(22, 'TextField', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(23, 'URL', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(24, 'Files', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(25, 'Ckfinder', '2018-07-19 18:50:53', '2018-07-19 18:50:53'),
(26, 'TinyMCE', '2018-07-19 18:50:53', '2018-07-19 18:50:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `organizations`
--

DROP TABLE IF EXISTS `organizations`;
CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'http://',
  `assigned_to` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `connect_since` date NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contetn` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pages`
--

INSERT INTO `pages` (`id`, `deleted_at`, `created_at`, `updated_at`, `title`, `slug`, `contetn`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, NULL, '2018-11-21 02:27:29', '2019-02-13 02:41:26', 'Giới thiệu về Sạp', 'about', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<div>\r\n<p>Trang giới thiệu gi&uacute;p kh&aacute;ch h&agrave;ng hiểu r&otilde; hơn về cửa h&agrave;ng của bạn. H&atilde;y cung cấp th&ocirc;ng tin cụ thể về việc kinh doanh, về cửa h&agrave;ng, th&ocirc;ng tin li&ecirc;n hệ. Điều n&agrave;y sẽ gi&uacute;p kh&aacute;ch h&agrave;ng cảm thấy tin tưởng khi mua h&agrave;ng tr&ecirc;n website của bạn.</p>\r\n<p>Một v&agrave;i gợi &yacute; cho nội dung trang Giới thiệu:</p>\r\n<div>\r\n<ul style=\"list-style-type: none;\">\r\n<li>Bạn l&agrave; ai</li>\r\n<li>Gi&aacute; trị kinh doanh của bạn l&agrave; g&igrave;</li>\r\n<li>Địa chỉ cửa h&agrave;ng</li>\r\n<li>Bạn đ&atilde; kinh doanh trong ng&agrave;nh h&agrave;ng n&agrave;y bao l&acirc;u rồi</li>\r\n<li>Bạn kinh doanh ng&agrave;nh h&agrave;ng online được bao l&acirc;u</li>\r\n<li>Đội ngũ của bạn gồm những ai</li>\r\n<li>Th&ocirc;ng tin li&ecirc;n hệ</li>\r\n<li>Li&ecirc;n kết đến c&aacute;c trang mạng x&atilde; hội (Twitter, Facebook)</li>\r\n</ul>\r\n</div>\r\n<p>Bạn c&oacute; thể chỉnh sửa hoặc xo&aacute; b&agrave;i viết n&agrave;y tại&nbsp;<a href=\"https://beauty-farm.myharavan.com/admin/page#/detail/1000514874\">đ&acirc;y</a>&nbsp;hoặc th&ecirc;m những b&agrave;i viết mới trong phần quản l&yacute;&nbsp;<a href=\"https://beauty-farm.myharavan.com/admin/page#/new\">Trang nội dung</a>.</p>\r\n</div>\r\n', '', '', ''),
(2, NULL, '2019-02-13 02:41:53', '2019-02-13 02:41:53', 'Hướng dẫn mua hàng', 'slug', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Trang giới thiệu gi&uacute;p kh&aacute;ch h&agrave;ng hiểu r&otilde; hơn về cửa h&agrave;ng của bạn. H&atilde;y cung cấp th&ocirc;ng tin cụ thể về việc kinh doanh, về cửa h&agrave;ng, th&ocirc;ng tin li&ecirc;n hệ. Điều n&agrave;y sẽ gi&uacute;p kh&aacute;ch h&agrave;ng cảm thấy tin tưởng khi mua h&agrave;ng tr&ecirc;n website của bạn.</p>\r\n<p>Một v&agrave;i gợi &yacute; cho nội dung trang Giới thiệu:</p>\r\n<div>\r\n<ul style=\"list-style-type: none;\">\r\n<li>Bạn l&agrave; ai</li>\r\n<li>Gi&aacute; trị kinh doanh của bạn l&agrave; g&igrave;</li>\r\n<li>Địa chỉ cửa h&agrave;ng</li>\r\n<li>Bạn đ&atilde; kinh doanh trong ng&agrave;nh h&agrave;ng n&agrave;y bao l&acirc;u rồi</li>\r\n<li>Bạn kinh doanh ng&agrave;nh h&agrave;ng online được bao l&acirc;u</li>\r\n<li>Đội ngũ của bạn gồm những ai</li>\r\n<li>Th&ocirc;ng tin li&ecirc;n hệ</li>\r\n<li>Li&ecirc;n kết đến c&aacute;c trang mạng x&atilde; hội (Twitter, Facebook)</li>\r\n</ul>\r\n</div>\r\n<p>Bạn c&oacute; thể chỉnh sửa hoặc xo&aacute; b&agrave;i viết n&agrave;y tại&nbsp;<a href=\"https://beauty-farm.myharavan.com/admin/page#/detail/1000514874\">đ&acirc;y</a>&nbsp;hoặc th&ecirc;m những b&agrave;i viết mới trong phần quản l&yacute;&nbsp;<a href=\"https://beauty-farm.myharavan.com/admin/page#/new\">Trang nội dung</a>.</p>\r\n</body>\r\n</html>', '', '', ''),
(3, '2019-02-14 18:21:45', '2019-02-14 18:15:12', '2019-02-14 18:21:45', 'ĐÔI NÉT VỀ CHÚNG TÔI', 'doi-net-ve-chung-toi', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Phục vụ nhu cầu của Cuyahoga&nbsp;<br />County trong hơn 110 năm. Ch&uacute;ng t&ocirc;i l&agrave; một bệnh viện được c&ocirc;ng nhận đầy đủ bởi Ủy ban Li&ecirc;n hợp với Trung t&acirc;m Chấn thương cấp II được chứng nhận. Trung t&acirc;m Ung thư Clinic Cleveland ở bệnh viện Fairview Moll Pavilion, nằm ngay ph&iacute;a b&ecirc;n kia đường từ ch&iacute;nh nằm trực tiếp tr&ecirc;n đường phố.</p>\r\n</body>\r\n</html>', '', '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partners`
--

INSERT INTO `partners` (`id`, `deleted_at`, `created_at`, `updated_at`, `title`, `image`) VALUES
(1, NULL, '2019-02-14 19:04:46', '2019-03-25 05:58:55', 'Sống là hãy cho đi', '/uploads/images/Partner/galery-1.jpg'),
(2, NULL, '2019-02-14 19:05:28', '2019-03-25 05:59:01', 'Sống là hãy cho đi', '/uploads/images/Partner/galery-2.jpg'),
(3, NULL, '2019-02-14 19:05:54', '2019-03-25 05:59:09', 'Sống là hãy cho đi', '/uploads/images/Partner/galery-3.jpg'),
(4, NULL, '2019-02-14 19:06:13', '2019-03-25 05:59:15', 'Sống là hãy cho đi', '/uploads/images/Partner/galery-4.jpg'),
(5, NULL, '2019-02-14 19:06:23', '2019-03-25 05:59:22', 'Sống là hãy cho đi', '/uploads/images/Partner/galery-5.jpg'),
(6, NULL, '2019-02-14 19:06:33', '2019-03-25 05:59:32', 'Sống là hãy cho đi', '/uploads/images/Partner/galery-6.jpg'),
(7, '2019-03-25 05:59:39', '2019-02-14 19:06:40', '2019-03-25 05:59:39', 'Đối tác từ chúng tôi', '/uploads/images/Partner/brand_7_image7437.png'),
(8, '2019-03-25 05:59:38', '2019-02-14 19:06:46', '2019-03-25 05:59:38', 'Đối tác từ chúng tôi', '/uploads/images/Partner/brand_8_image7437.png'),
(9, '2019-03-25 05:59:36', '2019-02-14 19:06:53', '2019-03-25 05:59:36', 'Đối tác từ chúng tôi', '/uploads/images/Partner/brand_9_image7437.png'),
(10, '2019-03-25 05:59:35', '2019-02-14 19:06:59', '2019-03-25 05:59:35', 'Đối tác từ chúng tôi', '/uploads/images/Partner/brand_10_image7437.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10'),
('jely.big@gmail.com', '8e9604d82d523407998f35001e92c941b8f0b1305d1ebc91d4c2e032179f7d35', '2018-12-31 21:13:12'),
('nhatduy510312@gmail.com', 'a96840b20ac72ef38ed101bc032a606abb854ee120d6617a1479d61905068f1d', '2018-12-31 21:14:33'),
('sapdoque@gmail.com', '11c4bb76e1eac5a4bcfe61cade78e6c5479f0c1f7b1e740188934f05d08389ad', '2018-12-31 21:15:10');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2018-07-19 18:51:06', '2018-07-19 18:51:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `categories` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `thumbnail` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `author` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `seo_title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci,
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[""]',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `deleted_at`, `created_at`, `updated_at`, `title`, `slug`, `content`, `categories`, `thumbnail`, `author`, `seo_title`, `seo_description`, `seo_keywords`, `tags`, `status`) VALUES
(1, NULL, '2019-02-27 02:55:33', '2019-03-21 05:17:59', '(SOHA.VN) GỢI Ý MẶC VEST ”CHUẨN KHỎI CHỈNH” NHƯ 2 ”SOÁI CA” SHOWBIZ VIỆT', 'sohavn-goi-mac-vest-chuan-khoi-chinh-nhu-2-soai-ca-showbiz-viet', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Một lần nữa, vẻ ngo&agrave;i điển trai v&agrave; lịch l&atilde;m cuốn h&uacute;t của c&aacute;c nam thần lại được tỏa s&aacute;ng nhờ những bộ vest đẳng cấp từ BST vest của Adam Store.</p>\r\n<p>Được biết đến từ c&aacute;c chương tr&igrave;nh ph&aacute;t thanh, truyền h&igrave;nh v&agrave; cả những bộ phim điện ảnh, VJ Nam Hee v&agrave; ph&aacute;t thanh vi&ecirc;n Nguyễn Hải Dương đang dần khẳng định vị tr&iacute; của m&igrave;nh trong l&ograve;ng kh&aacute;n giả, kh&ocirc;ng chỉ bởi chất giọng truyền cảm m&agrave; c&ograve;n bởi vẻ ngo&agrave;i điển trai, lịch l&atilde;m đậm chất &ldquo;so&aacute;i ca&rdquo;.</p>\r\n<p>Đọc th&ecirc;m tại:&nbsp;<a href=\"http://soha.vn/goi-y-mac-vest-chuan-khoi-chinh-nhu-2-soai-ca-showbiz-viet-20161229203131273.htm\">Gợi &yacute; mặc vest &ldquo;chuẩn khỏi chỉnh&rdquo; như 2 &ldquo;so&aacute;i ca&rdquo; showbiz Việt</a></p>\r\n<p><img style=\"max-width: 100%;\" src=\"//file.hstatic.net/1000333436/file/adam-store-soha1r-436x500_grande.jpg\" /></p>\r\n<h2>L&agrave;m thế n&agrave;o để trở th&agrave;nh một qu&yacute; &ocirc;ng trẻ trung, lịch l&atilde;m v&agrave; cuốn h&uacute;t?</h2>\r\n<p>H&atilde;y theo d&otilde;i những cập nhật mới nhất về trang phục v&agrave; phụ kiện đẳng cấp d&agrave;nh ri&ecirc;ng cho c&aacute;c qu&yacute; &ocirc;ng từ Adam Store tr&ecirc;n website v&agrave;&nbsp;<a href=\"https://www.facebook.com/adamstore.hn\">fanpage</a>&nbsp;của ch&uacute;ng t&ocirc;i! Bạn chắc chắn sẽ t&igrave;m được cho m&igrave;nh những item đỉnh nhất để n&acirc;ng cấp h&igrave;nh ảnh bản th&acirc;n m&igrave;nh!</p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', '', '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(2, NULL, '2019-02-27 02:56:15', '2019-03-21 05:17:52', '(MARRY.VN) MẸO TIẾT KIỆM CHI PHÍ VEST CƯỚI TỪ ANZEDO STORE', 'marryvn-meo-tiet-kiem-chi-phi-vest-cuoi-tu-adam-store', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Đ&aacute;m cưới l&agrave; việc quan trọng nhất của đời người n&ecirc;n bạn c&oacute; trăm mối lo bộn bề v&agrave; trăm khoản chi ti&ecirc;u. C&oacute; những khoản bạn c&oacute; thể cắt bớt nhưng những khoản quan trọng như vest cưới th&igrave; l&agrave;m sao c&oacute; thể tối ưu chi ph&iacute; đ&acirc;y? Adam Store c&ugrave;ng Marry.vn xin &ldquo;hiến kế&rdquo; gi&uacute;p bạn 4 c&aacute;ch th&agrave;nh c&ocirc;ng để c&oacute; thể tiết kiệm chi ph&iacute; vest cưới: vừa c&oacute; vest xịn, đẹp m&agrave; gi&aacute; lại vừa tầm.</p>\r\n<h2><strong>1. Mua vest may sẵn thay v&igrave; may đo</strong></h2>\r\n<p>Vest may đo thường mất chi ph&iacute; kh&aacute; cao n&ecirc;n bạn h&atilde;y chọn lựa c&aacute;c mẫu vest may sẵn với gi&aacute; th&agrave;nh tiết kiệm hơn. Vest may sẵn ng&agrave;y nay cũng được ưu &aacute;i hơn v&igrave; ch&uacute; rể c&oacute; thể thử trực tiếp, kh&ocirc;ng mất thời gian chờ đợi l&acirc;u.</p>\r\n<p>Để c&oacute; thiết kế chuẩn nhất, chất liệu ổn nhất v&agrave; size vừa người nhất, bạn n&ecirc;n mua ở những hệ thống vest nổi tiếng. Hiện nay, hệ thống vest Adam Store l&agrave; nổi tiếng nhất với phong c&aacute;ch thiết kế hiện đại, m&agrave;u sắc trẻ trung v&agrave; chi ph&iacute; hợp l&yacute;. Adam Store cũng c&oacute; hệ thống tr&ecirc;n khắp to&agrave;n quốc n&ecirc;n rất dễ cho kh&aacute;ch h&agrave;ng mua h&agrave;ng.</p>\r\n<h2>2.Mua vest c&oacute; thể mặc trong nhiều ho&agrave;n cảnh</h2>\r\n<p>Bỏ ra tiền triệu nếu chỉ để mặc một lần l&agrave; rất l&atilde;ng ph&iacute; phải kh&ocirc;ng? Vest hiện nay rất linh hoạt, bạn c&oacute; thể chọn mua những bộ vest mặc được trong mọi ho&agrave;n cảnh kh&aacute;c nhau, như một trang phục thời trang h&agrave;ng ng&agrave;y của bạn. Vậy l&agrave; c&ugrave;ng một chi ph&iacute; nhưng bạn c&oacute; thể mặc trong thời gian rất d&agrave;i, v&agrave; tiết kiệm được một khoản đầu tư trang phục cho c&aacute;c sự kiện kh&aacute;c.</p>\r\n<p><strong>Đọc th&ecirc;m tại:</strong>&nbsp;<a href=\"http://www.marry.vn/loi-khuyen-tu-marry/5-meo-tiet-kiem-chi-phi-vest-cuoi\">Mẹo tiết kiệm chi ph&iacute; vest cưới</a></p>\r\n<p><img style=\"max-width: 100%;\" src=\"//file.hstatic.net/1000333436/file/adam-store-5-meo-nho-403x500_grande.jpg\" /></p>\r\n<p>ADAM STORE &ndash; THƯƠNG HIỆU VESTON &ndash; GI&Agrave;Y DA HANDMADE SỐ 1 TẠI VIỆT NAM</p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', '', '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(3, NULL, '2019-02-27 03:01:02', '2019-03-21 05:50:57', 'TỎA SÁNG CÙNG QUÝ ÔNG COLOR MAN', 'toa-sang-cung-qu-ong-color-man', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Thanh lịch nhưng lu&ocirc;n #sang_trọng, tưởng đơn giản nhưng phải #cuốn_h&uacute;t, h&agrave;i ho&agrave; nhưng t&ocirc;n #c&aacute;_t&iacute;nh l&agrave; những g&igrave; sao Việt y&ecirc;u cầu về trang phục khi xuất hiện trước c&ocirc;ng ch&uacute;ng. Với sự khắt khe trong quy tr&igrave;nh, từ kh&acirc;u lựa chọn chất liệu, thiết kế, đưa v&agrave;o sản xuất, suốt 6 năm qua Adam Store đ&atilde; chiếm trọn niềm tin y&ecirc;u của những ng&ocirc;i sao, ca sĩ, MC h&agrave;ng đầu showbiz.<br /><br />Đặc biệt, những ng&agrave;y cuối năm Adam Store nhận được sự quan t&acirc;m của Qu&yacute; &ocirc;ng Color Man - &ocirc;ng Đỗ Văn Bửu Điền - Chủ Tịch HĐQT C&ocirc;ng Ty Truyền Th&ocirc;ng &amp; Giải Tr&iacute; Điền Qu&acirc;n, c&ocirc;ng ty giải tr&iacute; nổi tiếng với c&aacute;c gameshow ăn kh&aacute;ch bậc nhất hiện nay: Th&aacute;ch Thức Danh H&agrave;i, S&agrave;n Đấu Ca Từ, Đấu Trường Tiếu L&acirc;m, H&aacute;t M&atilde;i Ước Mơ, Phi&ecirc;n Bản Ho&agrave;n Hảo, Biệt T&agrave;i T&iacute; Hon, Danh Ca Vọng Cổ qua lựa chọn trang phục.<br /><br />V&agrave; đ&oacute; l&agrave; l&yacute; do gi&uacute;p &ocirc;ng lu&ocirc;n c&oacute; diện mạo sang trọng, lịch l&atilde;m. H&atilde;y đến Adam để kh&aacute;m ph&aacute; chất \"#sao\" ẩn s&acirc;u trong bạn.</span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/2e3a4648_copy_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/2e3a4672_copy_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/2e3a4731_copy_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/2e3a4744_copy_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/2e3a4970_copy_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/2e3a4935_copy_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/2e3a4935_copy_077c67d0d8834332bc4b579c8279b29c_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"2\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', '', '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(4, NULL, '2019-02-27 03:01:40', '2019-03-21 06:55:42', '1 TRẤN THÀNH BẢNH BAO CÙNG VEST ADAM TRONG CÁC CHƯƠNG TRÌNH', 'tran-thanh-banh-bao-cung-vest-adam-trong-cac-chuong-trinh', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với ngoại h&igrave;nh lu&ocirc;n bảnh, nụ cười duy&ecirc;n v&agrave; t&agrave;i ứng biến t&agrave;i t&igrave;nh, MC Trấn Th&agrave;nh được biết đến l&agrave; MC h&agrave;ng đầu của Việt Nam. Ngoại ra, anh c&ograve;n được coi l&agrave; ch&agrave;ng rể quốc d&acirc;n m&agrave; chị em lu&ocirc;n ao ước.&nbsp;<br /><br />Để lu&ocirc;n bảnh bảo th&igrave; d&ugrave; xuất hiện tr&ecirc;n s&oacute;ng truyền h&igrave;nh v&agrave; c&aacute;c sự kiện lớn nhỏ, MC Trấn Th&agrave;nh lu&ocirc;n lựa chọn vest Adam v&agrave; d&ugrave; c&oacute; phối với &aacute;o sơ mi hay &aacute;o ph&ocirc;ng đi chăng nữa th&igrave; anh vẫn v&ocirc; c&ugrave;ng l&atilde;ng tử, đầy lịch l&atilde;m thu h&uacute;t sự ch&uacute; &yacute; của tất cả mọi người xung quanh.&nbsp;<br /><br />H&atilde;y c&ugrave;ng ngắm nh&igrave;n ch&agrave;ng Mc đang được c&ocirc;ng ch&uacute;ng v&ocirc; c&ugrave;ng y&ecirc;u th&iacute;ch lịch l&atilde;m trong trang phục veston Adam Store nh&eacute;❗<br /><br /></span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3728_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3730_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3729_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', '', '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(5, NULL, '2019-02-27 03:01:40', '2019-03-21 06:55:42', '2 TRẤN THÀNH BẢNH BAO CÙNG VEST ADAM TRONG CÁC CHƯƠNG TRÌNH', 'tran-thanh-banh-bao-cung-vest1-adam-trong-cac-chuong-trinh', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với ngoại h&igrave;nh lu&ocirc;n bảnh, nụ cười duy&ecirc;n v&agrave; t&agrave;i ứng biến t&agrave;i t&igrave;nh, MC Trấn Th&agrave;nh được biết đến l&agrave; MC h&agrave;ng đầu của Việt Nam. Ngoại ra, anh c&ograve;n được coi l&agrave; ch&agrave;ng rể quốc d&acirc;n m&agrave; chị em lu&ocirc;n ao ước.&nbsp;<br /><br />Để lu&ocirc;n bảnh bảo th&igrave; d&ugrave; xuất hiện tr&ecirc;n s&oacute;ng truyền h&igrave;nh v&agrave; c&aacute;c sự kiện lớn nhỏ, MC Trấn Th&agrave;nh lu&ocirc;n lựa chọn vest Adam v&agrave; d&ugrave; c&oacute; phối với &aacute;o sơ mi hay &aacute;o ph&ocirc;ng đi chăng nữa th&igrave; anh vẫn v&ocirc; c&ugrave;ng l&atilde;ng tử, đầy lịch l&atilde;m thu h&uacute;t sự ch&uacute; &yacute; của tất cả mọi người xung quanh.&nbsp;<br /><br />H&atilde;y c&ugrave;ng ngắm nh&igrave;n ch&agrave;ng Mc đang được c&ocirc;ng ch&uacute;ng v&ocirc; c&ugrave;ng y&ecirc;u th&iacute;ch lịch l&atilde;m trong trang phục veston Adam Store nh&eacute;❗<br /><br /></span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3728_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3730_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3729_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', NULL, '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(6, NULL, '2019-02-27 03:01:40', '2019-03-21 06:55:42', '3 TRẤN THÀNH BẢNH BAO CÙNG VEST ADAM TRONG CÁC CHƯƠNG TRÌNH', 'tran-thanh-ba1nh-bao-cung-vest1-adam-trong-cac-chuong-trinh', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với ngoại h&igrave;nh lu&ocirc;n bảnh, nụ cười duy&ecirc;n v&agrave; t&agrave;i ứng biến t&agrave;i t&igrave;nh, MC Trấn Th&agrave;nh được biết đến l&agrave; MC h&agrave;ng đầu của Việt Nam. Ngoại ra, anh c&ograve;n được coi l&agrave; ch&agrave;ng rể quốc d&acirc;n m&agrave; chị em lu&ocirc;n ao ước.&nbsp;<br /><br />Để lu&ocirc;n bảnh bảo th&igrave; d&ugrave; xuất hiện tr&ecirc;n s&oacute;ng truyền h&igrave;nh v&agrave; c&aacute;c sự kiện lớn nhỏ, MC Trấn Th&agrave;nh lu&ocirc;n lựa chọn vest Adam v&agrave; d&ugrave; c&oacute; phối với &aacute;o sơ mi hay &aacute;o ph&ocirc;ng đi chăng nữa th&igrave; anh vẫn v&ocirc; c&ugrave;ng l&atilde;ng tử, đầy lịch l&atilde;m thu h&uacute;t sự ch&uacute; &yacute; của tất cả mọi người xung quanh.&nbsp;<br /><br />H&atilde;y c&ugrave;ng ngắm nh&igrave;n ch&agrave;ng Mc đang được c&ocirc;ng ch&uacute;ng v&ocirc; c&ugrave;ng y&ecirc;u th&iacute;ch lịch l&atilde;m trong trang phục veston Adam Store nh&eacute;❗<br /><br /></span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3728_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3730_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3729_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', NULL, '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(7, NULL, '2019-02-27 03:01:40', '2019-03-21 06:55:42', '4 TRẤN THÀNH BẢNH BAO CÙNG VEST ADAM TRONG CÁC CHƯƠNG TRÌNH', 'tran-thanh-ba1nh-b1ao-cung-vest1-adam-trong-cac-chuong-trinh', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với ngoại h&igrave;nh lu&ocirc;n bảnh, nụ cười duy&ecirc;n v&agrave; t&agrave;i ứng biến t&agrave;i t&igrave;nh, MC Trấn Th&agrave;nh được biết đến l&agrave; MC h&agrave;ng đầu của Việt Nam. Ngoại ra, anh c&ograve;n được coi l&agrave; ch&agrave;ng rể quốc d&acirc;n m&agrave; chị em lu&ocirc;n ao ước.&nbsp;<br /><br />Để lu&ocirc;n bảnh bảo th&igrave; d&ugrave; xuất hiện tr&ecirc;n s&oacute;ng truyền h&igrave;nh v&agrave; c&aacute;c sự kiện lớn nhỏ, MC Trấn Th&agrave;nh lu&ocirc;n lựa chọn vest Adam v&agrave; d&ugrave; c&oacute; phối với &aacute;o sơ mi hay &aacute;o ph&ocirc;ng đi chăng nữa th&igrave; anh vẫn v&ocirc; c&ugrave;ng l&atilde;ng tử, đầy lịch l&atilde;m thu h&uacute;t sự ch&uacute; &yacute; của tất cả mọi người xung quanh.&nbsp;<br /><br />H&atilde;y c&ugrave;ng ngắm nh&igrave;n ch&agrave;ng Mc đang được c&ocirc;ng ch&uacute;ng v&ocirc; c&ugrave;ng y&ecirc;u th&iacute;ch lịch l&atilde;m trong trang phục veston Adam Store nh&eacute;❗<br /><br /></span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3728_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3730_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3729_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', NULL, '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(8, NULL, '2019-02-27 03:01:40', '2019-03-21 06:55:42', '5 TRẤN THÀNH BẢNH BAO CÙNG VEST ADAM TRONG CÁC CHƯƠNG TRÌNH', 'tran-thanh-ba1nh-b1ao-cung-vest1-adam-tr1ong-cac-chuong-trinh', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với ngoại h&igrave;nh lu&ocirc;n bảnh, nụ cười duy&ecirc;n v&agrave; t&agrave;i ứng biến t&agrave;i t&igrave;nh, MC Trấn Th&agrave;nh được biết đến l&agrave; MC h&agrave;ng đầu của Việt Nam. Ngoại ra, anh c&ograve;n được coi l&agrave; ch&agrave;ng rể quốc d&acirc;n m&agrave; chị em lu&ocirc;n ao ước.&nbsp;<br /><br />Để lu&ocirc;n bảnh bảo th&igrave; d&ugrave; xuất hiện tr&ecirc;n s&oacute;ng truyền h&igrave;nh v&agrave; c&aacute;c sự kiện lớn nhỏ, MC Trấn Th&agrave;nh lu&ocirc;n lựa chọn vest Adam v&agrave; d&ugrave; c&oacute; phối với &aacute;o sơ mi hay &aacute;o ph&ocirc;ng đi chăng nữa th&igrave; anh vẫn v&ocirc; c&ugrave;ng l&atilde;ng tử, đầy lịch l&atilde;m thu h&uacute;t sự ch&uacute; &yacute; của tất cả mọi người xung quanh.&nbsp;<br /><br />H&atilde;y c&ugrave;ng ngắm nh&igrave;n ch&agrave;ng Mc đang được c&ocirc;ng ch&uacute;ng v&ocirc; c&ugrave;ng y&ecirc;u th&iacute;ch lịch l&atilde;m trong trang phục veston Adam Store nh&eacute;❗<br /><br /></span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3728_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3730_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3729_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', NULL, '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(9, NULL, '2019-02-27 03:01:40', '2019-03-21 06:55:42', '6 TRẤN THÀNH BẢNH BAO CÙNG VEST ADAM TRONG CÁC CHƯƠNG TRÌNH', 'tran-thanh-ba1nh-b1ao-cung-vest1-adam-tr1ong-cac-chuon1g-trinh', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với ngoại h&igrave;nh lu&ocirc;n bảnh, nụ cười duy&ecirc;n v&agrave; t&agrave;i ứng biến t&agrave;i t&igrave;nh, MC Trấn Th&agrave;nh được biết đến l&agrave; MC h&agrave;ng đầu của Việt Nam. Ngoại ra, anh c&ograve;n được coi l&agrave; ch&agrave;ng rể quốc d&acirc;n m&agrave; chị em lu&ocirc;n ao ước.&nbsp;<br /><br />Để lu&ocirc;n bảnh bảo th&igrave; d&ugrave; xuất hiện tr&ecirc;n s&oacute;ng truyền h&igrave;nh v&agrave; c&aacute;c sự kiện lớn nhỏ, MC Trấn Th&agrave;nh lu&ocirc;n lựa chọn vest Adam v&agrave; d&ugrave; c&oacute; phối với &aacute;o sơ mi hay &aacute;o ph&ocirc;ng đi chăng nữa th&igrave; anh vẫn v&ocirc; c&ugrave;ng l&atilde;ng tử, đầy lịch l&atilde;m thu h&uacute;t sự ch&uacute; &yacute; của tất cả mọi người xung quanh.&nbsp;<br /><br />H&atilde;y c&ugrave;ng ngắm nh&igrave;n ch&agrave;ng Mc đang được c&ocirc;ng ch&uacute;ng v&ocirc; c&ugrave;ng y&ecirc;u th&iacute;ch lịch l&atilde;m trong trang phục veston Adam Store nh&eacute;❗<br /><br /></span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3728_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3730_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3729_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', NULL, '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1),
(10, NULL, '2019-02-27 03:01:40', '2019-03-21 06:55:42', '7 TRẤN THÀNH BẢNH BAO CÙNG VEST ADAM TRONG CÁC CHƯƠNG TRÌNH', 'tran-thanh-ba1nh-b1ao-cung-vest1-adam-tr1ong-cac-chuon1g-trinh1', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với ngoại h&igrave;nh lu&ocirc;n bảnh, nụ cười duy&ecirc;n v&agrave; t&agrave;i ứng biến t&agrave;i t&igrave;nh, MC Trấn Th&agrave;nh được biết đến l&agrave; MC h&agrave;ng đầu của Việt Nam. Ngoại ra, anh c&ograve;n được coi l&agrave; ch&agrave;ng rể quốc d&acirc;n m&agrave; chị em lu&ocirc;n ao ước.&nbsp;<br /><br />Để lu&ocirc;n bảnh bảo th&igrave; d&ugrave; xuất hiện tr&ecirc;n s&oacute;ng truyền h&igrave;nh v&agrave; c&aacute;c sự kiện lớn nhỏ, MC Trấn Th&agrave;nh lu&ocirc;n lựa chọn vest Adam v&agrave; d&ugrave; c&oacute; phối với &aacute;o sơ mi hay &aacute;o ph&ocirc;ng đi chăng nữa th&igrave; anh vẫn v&ocirc; c&ugrave;ng l&atilde;ng tử, đầy lịch l&atilde;m thu h&uacute;t sự ch&uacute; &yacute; của tất cả mọi người xung quanh.&nbsp;<br /><br />H&atilde;y c&ugrave;ng ngắm nh&igrave;n ch&agrave;ng Mc đang được c&ocirc;ng ch&uacute;ng v&ocirc; c&ugrave;ng y&ecirc;u th&iacute;ch lịch l&atilde;m trong trang phục veston Adam Store nh&eacute;❗<br /><br /></span></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3728_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3730_grande.jpg\" /></p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000333436/file/img_3729_grande.jpg\" /></p>\r\n</body>\r\n</html>', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '/uploads/images/kim-tu-thap_large.jpg', 1, '', '', NULL, '[\"Anzedo\",\"All\",\"Blog\",\"Tin tuc\"]', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_labels`
--

DROP TABLE IF EXISTS `product_labels`;
CREATE TABLE `product_labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_labels`
--

INSERT INTO `product_labels` (`id`, `deleted_at`, `created_at`, `updated_at`, `name`, `key`) VALUES
(1, NULL, '2019-03-23 06:38:40', '2019-03-23 06:38:40', 'Bài viết mới', 'post_new');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promotions`
--

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE `promotions` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `khuyenmai` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `html` varchar(9999) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `re_comments`
--

DROP TABLE IF EXISTS `re_comments`;
CREATE TABLE `re_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cmt_id` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `blog_id` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content_re` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `email_re` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `re_comments`
--

INSERT INTO `re_comments` (`id`, `deleted_at`, `created_at`, `updated_at`, `cmt_id`, `blog_id`, `full_name`, `phone_number`, `content_re`, `user_id`, `status`, `email_re`) VALUES
(1, NULL, '2019-03-31 06:06:11', '2019-03-31 06:06:11', '1', '', '4', '4', '4', '', 1, '4@gmail.com'),
(2, NULL, '2019-03-31 06:06:17', '2019-03-31 06:06:17', '2', '', '5', '5', '5', '', 1, '5@gmail.com');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', 1, 1, NULL, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 'MOD', 'Mod', 'Mod', 1, 2, NULL, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(3, 'CLIENT', 'Client', 'Client', 4, 4, NULL, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(4, 'USER', 'User', 'User', 2, 3, NULL, '2019-04-07 06:46:57', '2019-04-07 06:47:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_module`
--

DROP TABLE IF EXISTS `role_module`;
CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 1, 2, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(3, 1, 3, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(4, 1, 4, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(5, 1, 5, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(6, 1, 6, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(7, 1, 7, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(8, 1, 8, 1, 1, 1, 1, '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(18, 1, 24, 1, 1, 1, 1, '2018-09-02 01:14:24', '2018-09-02 01:14:24'),
(20, 1, 26, 1, 1, 1, 1, '2018-09-06 14:45:06', '2018-09-06 14:45:06'),
(21, 1, 27, 1, 1, 1, 1, '2018-09-08 22:40:09', '2018-09-08 22:40:09'),
(22, 1, 28, 1, 1, 1, 1, '2018-09-15 02:02:07', '2018-09-15 02:02:07'),
(24, 1, 34, 1, 1, 1, 1, '2018-09-24 01:03:10', '2018-09-24 01:03:10'),
(26, 1, 38, 1, 1, 1, 1, '2018-09-24 01:38:57', '2018-09-24 01:38:57'),
(27, 1, 39, 1, 1, 1, 1, '2018-10-04 02:37:46', '2018-10-04 02:37:46'),
(28, 1, 42, 1, 1, 1, 1, '2018-11-19 18:26:38', '2018-11-19 18:26:38'),
(29, 1, 43, 1, 1, 1, 1, '2018-11-21 18:55:24', '2018-11-21 18:55:24'),
(30, 1, 44, 1, 1, 1, 1, '2018-11-21 18:59:49', '2018-11-21 18:59:49'),
(41, 1, 56, 1, 1, 1, 1, '2019-01-01 01:23:40', '2019-01-01 01:23:40'),
(45, 1, 61, 1, 1, 1, 1, '2019-02-14 19:03:04', '2019-02-14 19:03:04'),
(48, 1, 65, 1, 1, 1, 1, '2019-02-17 18:57:04', '2019-02-17 18:57:04'),
(50, 1, 67, 1, 1, 1, 1, '2019-03-26 06:41:22', '2019-03-26 06:41:22'),
(51, 1, 68, 1, 1, 1, 1, '2019-03-30 07:47:20', '2019-03-30 07:47:20'),
(52, 2, 1, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(53, 2, 2, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(54, 2, 3, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(55, 2, 4, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(56, 2, 5, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(57, 2, 6, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(58, 2, 7, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(59, 2, 8, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(60, 2, 24, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(61, 2, 26, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(62, 2, 27, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(63, 2, 28, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(64, 2, 34, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(65, 2, 38, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(66, 2, 39, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(67, 2, 42, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(68, 2, 43, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(69, 2, 44, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(70, 2, 56, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(71, 2, 61, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(72, 2, 65, 1, 0, 0, 0, '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(73, 2, 67, 1, 0, 0, 0, '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(74, 2, 68, 1, 0, 0, 0, '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(75, 4, 1, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(76, 4, 2, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(77, 4, 3, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(78, 4, 4, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(79, 4, 5, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(80, 4, 6, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(81, 4, 7, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(82, 4, 8, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(83, 4, 24, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(84, 4, 26, 1, 0, 0, 0, '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(85, 4, 27, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(86, 4, 28, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(87, 4, 34, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(88, 4, 38, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(89, 4, 39, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(90, 4, 42, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(91, 4, 43, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(92, 4, 44, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(93, 4, 56, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(94, 4, 61, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(95, 4, 65, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(96, 4, 67, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(97, 4, 68, 1, 0, 0, 0, '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(98, 3, 1, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(99, 3, 2, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(100, 3, 3, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(101, 3, 4, 1, 0, 0, 0, '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(102, 3, 5, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(103, 3, 6, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(104, 3, 7, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(105, 3, 8, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(106, 3, 24, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(107, 3, 26, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(108, 3, 27, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(109, 3, 28, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(110, 3, 34, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(111, 3, 38, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(112, 3, 39, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(113, 3, 42, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(114, 3, 43, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(115, 3, 44, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(116, 3, 56, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(117, 3, 61, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(118, 3, 65, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(119, 3, 67, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(120, 3, 68, 1, 0, 0, 0, '2019-04-07 06:47:36', '2019-04-07 06:47:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_module_fields`
--

DROP TABLE IF EXISTS `role_module_fields`;
CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(2, 1, 2, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(3, 1, 3, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(4, 1, 4, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(5, 1, 5, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(6, 1, 6, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(7, 1, 7, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(8, 1, 8, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(9, 1, 9, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(10, 1, 10, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(11, 1, 11, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(12, 1, 12, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(13, 1, 13, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(14, 1, 14, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(15, 1, 15, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(16, 1, 16, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(17, 1, 17, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(18, 1, 18, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(19, 1, 19, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(20, 1, 20, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(21, 1, 21, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(22, 1, 22, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(23, 1, 23, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(24, 1, 24, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(25, 1, 25, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(26, 1, 26, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(27, 1, 27, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(28, 1, 28, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(29, 1, 29, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(30, 1, 30, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(31, 1, 31, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(32, 1, 32, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(33, 1, 33, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(34, 1, 34, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(35, 1, 35, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(36, 1, 36, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(37, 1, 37, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(38, 1, 38, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(39, 1, 39, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(40, 1, 40, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(41, 1, 41, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(42, 1, 42, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(43, 1, 43, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(44, 1, 44, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(45, 1, 45, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(46, 1, 46, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(47, 1, 47, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(48, 1, 48, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(49, 1, 49, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(50, 1, 50, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(51, 1, 51, 'write', '2018-07-19 18:51:06', '2018-07-19 18:51:06'),
(87, 1, 108, 'write', '2018-09-06 14:42:11', '2018-09-06 14:42:11'),
(88, 1, 109, 'write', '2018-09-06 14:42:26', '2018-09-06 14:42:26'),
(89, 1, 110, 'write', '2018-09-06 14:43:10', '2018-09-06 14:43:10'),
(90, 1, 111, 'write', '2018-09-06 14:43:31', '2018-09-06 14:43:31'),
(91, 1, 112, 'write', '2018-09-06 14:43:48', '2018-09-06 14:43:48'),
(92, 1, 113, 'write', '2018-09-06 14:44:05', '2018-09-06 14:44:05'),
(93, 1, 114, 'write', '2018-09-06 14:44:24', '2018-09-06 14:44:24'),
(94, 1, 115, 'write', '2018-09-06 14:44:54', '2018-09-06 14:44:54'),
(95, 1, 116, 'write', '2018-09-08 22:33:13', '2018-09-08 22:33:13'),
(96, 1, 117, 'write', '2018-09-08 22:35:12', '2018-09-08 22:35:12'),
(97, 1, 118, 'write', '2018-09-08 22:35:26', '2018-09-08 22:35:26'),
(98, 1, 119, 'write', '2018-09-08 22:35:41', '2018-09-08 22:35:41'),
(99, 1, 120, 'write', '2018-09-08 22:36:14', '2018-09-08 22:36:14'),
(100, 1, 121, 'write', '2018-09-08 22:36:40', '2018-09-08 22:36:40'),
(101, 1, 122, 'write', '2018-09-08 22:37:12', '2018-09-08 22:37:12'),
(102, 1, 123, 'write', '2018-09-08 22:38:25', '2018-09-08 22:38:25'),
(103, 1, 124, 'write', '2018-09-08 22:38:38', '2018-09-08 22:38:38'),
(104, 1, 125, 'write', '2018-09-08 22:39:09', '2018-09-08 22:39:09'),
(105, 1, 126, 'write', '2018-09-08 22:39:59', '2018-09-08 22:39:59'),
(106, 1, 127, 'write', '2018-09-08 22:46:22', '2018-09-08 22:46:22'),
(107, 1, 128, 'write', '2018-09-15 02:01:46', '2018-09-15 02:01:46'),
(108, 1, 129, 'write', '2018-09-15 02:02:04', '2018-09-15 02:02:04'),
(128, 1, 149, 'write', '2018-09-24 00:58:57', '2018-09-24 00:58:57'),
(129, 1, 150, 'write', '2018-09-24 00:59:22', '2018-09-24 00:59:22'),
(131, 1, 152, 'write', '2018-09-24 01:02:33', '2018-09-24 01:02:33'),
(132, 1, 153, 'write', '2018-09-24 01:03:04', '2018-09-24 01:03:04'),
(139, 1, 160, 'write', '2018-09-24 01:38:53', '2018-09-24 01:38:53'),
(140, 1, 161, 'write', '2018-09-24 01:39:27', '2018-09-24 01:39:27'),
(141, 1, 162, 'write', '2018-09-24 01:39:43', '2018-09-24 01:39:43'),
(142, 1, 163, 'write', '2018-09-24 01:40:02', '2018-09-24 01:40:02'),
(143, 1, 164, 'write', '2018-09-24 01:40:15', '2018-09-24 01:40:15'),
(144, 1, 165, 'write', '2018-09-24 01:40:39', '2018-09-24 01:40:39'),
(145, 1, 166, 'write', '2018-09-24 01:41:07', '2018-09-24 01:41:07'),
(146, 1, 167, 'write', '2018-10-04 02:28:32', '2018-10-04 02:28:32'),
(147, 1, 168, 'write', '2018-10-04 02:28:49', '2018-10-04 02:28:49'),
(149, 1, 170, 'write', '2018-10-04 02:29:26', '2018-10-04 02:29:26'),
(150, 1, 171, 'write', '2018-10-04 02:37:05', '2018-10-04 02:37:05'),
(151, 1, 172, 'write', '2018-10-04 02:37:24', '2018-10-04 02:37:24'),
(152, 1, 173, 'write', '2018-10-04 02:37:39', '2018-10-04 02:37:39'),
(155, 1, 176, 'write', '2018-11-19 18:24:30', '2018-11-19 18:24:30'),
(156, 1, 177, 'write', '2018-11-19 18:25:24', '2018-11-19 18:25:24'),
(157, 1, 178, 'write', '2018-11-19 18:25:43', '2018-11-19 18:25:43'),
(158, 1, 179, 'write', '2018-11-19 18:26:10', '2018-11-19 18:26:10'),
(159, 1, 180, 'write', '2018-11-19 18:26:35', '2018-11-19 18:26:35'),
(160, 1, 181, 'write', '2018-11-21 18:53:51', '2018-11-21 18:53:51'),
(164, 1, 185, 'write', '2018-11-21 18:57:34', '2018-11-21 18:57:34'),
(165, 1, 186, 'write', '2018-11-21 18:58:08', '2018-11-21 18:58:08'),
(166, 1, 187, 'write', '2018-11-21 18:58:28', '2018-11-21 18:58:28'),
(167, 1, 188, 'write', '2018-11-21 18:58:44', '2018-11-21 18:58:44'),
(168, 1, 189, 'write', '2018-11-21 18:59:19', '2018-11-21 18:59:19'),
(169, 1, 190, 'write', '2018-11-21 18:59:37', '2018-11-21 18:59:37'),
(231, 1, 252, 'write', '2019-01-01 01:22:59', '2019-01-01 01:22:59'),
(250, 1, 271, 'write', '2019-02-14 19:00:44', '2019-02-14 19:00:44'),
(251, 1, 272, 'write', '2019-02-14 19:01:50', '2019-02-14 19:01:50'),
(265, 1, 286, 'write', '2019-02-17 18:55:24', '2019-02-17 18:55:24'),
(266, 1, 287, 'write', '2019-02-17 18:56:40', '2019-02-17 18:56:40'),
(268, 1, 289, 'write', '2019-03-21 08:26:04', '2019-03-21 08:26:04'),
(269, 1, 290, 'write', '2019-03-21 08:26:20', '2019-03-21 08:26:20'),
(270, 1, 291, 'write', '2019-03-21 08:27:05', '2019-03-21 08:27:05'),
(271, 1, 292, 'write', '2019-03-21 08:29:38', '2019-03-21 08:29:38'),
(272, 1, 293, 'write', '2019-03-21 08:29:49', '2019-03-21 08:29:49'),
(273, 1, 294, 'write', '2019-03-21 08:29:58', '2019-03-21 08:29:58'),
(274, 1, 295, 'write', '2019-03-21 08:30:46', '2019-03-21 08:30:46'),
(278, 1, 299, 'write', '2019-03-26 06:38:13', '2019-03-26 06:38:13'),
(279, 1, 300, 'write', '2019-03-26 06:38:38', '2019-03-26 06:38:38'),
(280, 1, 301, 'write', '2019-03-26 06:38:53', '2019-03-26 06:38:53'),
(281, 1, 302, 'write', '2019-03-26 06:39:10', '2019-03-26 06:39:10'),
(282, 1, 303, 'write', '2019-03-26 06:40:37', '2019-03-26 06:40:37'),
(283, 1, 304, 'write', '2019-03-26 06:40:56', '2019-03-26 06:40:56'),
(284, 1, 305, 'write', '2019-03-30 07:45:05', '2019-03-30 07:45:05'),
(285, 1, 306, 'write', '2019-03-30 07:45:26', '2019-03-30 07:45:26'),
(286, 1, 307, 'write', '2019-03-30 07:45:59', '2019-03-30 07:45:59'),
(287, 1, 308, 'write', '2019-03-30 07:46:18', '2019-03-30 07:46:18'),
(288, 1, 309, 'write', '2019-03-30 07:46:36', '2019-03-30 07:46:36'),
(289, 1, 310, 'write', '2019-03-30 07:47:13', '2019-03-30 07:47:13'),
(290, 1, 311, 'write', '2019-03-30 07:48:40', '2019-03-30 07:48:40'),
(291, 1, 312, 'write', '2019-03-30 07:50:36', '2019-03-30 07:50:36'),
(292, 1, 313, 'write', '2019-03-30 09:46:53', '2019-03-30 09:46:53'),
(293, 1, 314, 'write', '2019-03-31 07:31:55', '2019-03-31 07:31:55'),
(294, 2, 1, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(295, 2, 2, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(296, 2, 3, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(297, 2, 4, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(298, 2, 5, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(299, 2, 289, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(300, 2, 290, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(301, 2, 291, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(302, 2, 292, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(303, 2, 293, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(304, 2, 294, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(305, 2, 295, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(306, 2, 314, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(307, 2, 6, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(308, 2, 7, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(309, 2, 8, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(310, 2, 9, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(311, 2, 10, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(312, 2, 11, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(313, 2, 12, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(314, 2, 13, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(315, 2, 14, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(316, 2, 15, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(317, 2, 16, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(318, 2, 17, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(319, 2, 18, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(320, 2, 19, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(321, 2, 20, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(322, 2, 21, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(323, 2, 22, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(324, 2, 23, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(325, 2, 24, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(326, 2, 25, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(327, 2, 26, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(328, 2, 27, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(329, 2, 28, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(330, 2, 29, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(331, 2, 30, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(332, 2, 31, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(333, 2, 32, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(334, 2, 33, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(335, 2, 34, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(336, 2, 35, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(337, 2, 36, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(338, 2, 37, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(339, 2, 38, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(340, 2, 39, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(341, 2, 40, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(342, 2, 41, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(343, 2, 42, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(344, 2, 43, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(345, 2, 44, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(346, 2, 45, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(347, 2, 46, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(348, 2, 47, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(349, 2, 48, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(350, 2, 49, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(351, 2, 50, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(352, 2, 51, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(353, 2, 108, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(354, 2, 109, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(355, 2, 110, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(356, 2, 111, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(357, 2, 112, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(358, 2, 113, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(359, 2, 114, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(360, 2, 115, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(361, 2, 116, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(362, 2, 117, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(363, 2, 118, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(364, 2, 119, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(365, 2, 120, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(366, 2, 121, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(367, 2, 122, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(368, 2, 123, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(369, 2, 124, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(370, 2, 125, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(371, 2, 126, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(372, 2, 127, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(373, 2, 128, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(374, 2, 129, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(375, 2, 149, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(376, 2, 150, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(377, 2, 152, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(378, 2, 153, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(379, 2, 160, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(380, 2, 161, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(381, 2, 162, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(382, 2, 163, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(383, 2, 164, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(384, 2, 165, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(385, 2, 166, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(386, 2, 167, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(387, 2, 168, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(388, 2, 170, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(389, 2, 171, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(390, 2, 172, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(391, 2, 173, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(392, 2, 176, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(393, 2, 177, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(394, 2, 178, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(395, 2, 179, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(396, 2, 180, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(397, 2, 181, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(398, 2, 185, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(399, 2, 186, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(400, 2, 187, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(401, 2, 188, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(402, 2, 189, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(403, 2, 190, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(404, 2, 252, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(405, 2, 271, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(406, 2, 272, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(407, 2, 286, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(408, 2, 287, 'readonly', '2019-04-07 06:46:08', '2019-04-07 06:46:08'),
(409, 2, 303, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(410, 2, 304, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(411, 2, 299, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(412, 2, 300, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(413, 2, 301, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(414, 2, 302, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(415, 2, 311, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(416, 2, 305, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(417, 2, 306, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(418, 2, 307, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(419, 2, 308, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(420, 2, 309, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(421, 2, 310, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(422, 2, 312, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(423, 2, 313, 'readonly', '2019-04-07 06:46:09', '2019-04-07 06:46:09'),
(424, 4, 1, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(425, 4, 2, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(426, 4, 3, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(427, 4, 4, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(428, 4, 5, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(429, 4, 289, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(430, 4, 290, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(431, 4, 291, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(432, 4, 292, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(433, 4, 293, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(434, 4, 294, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(435, 4, 295, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(436, 4, 314, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(437, 4, 6, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(438, 4, 7, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(439, 4, 8, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(440, 4, 9, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(441, 4, 10, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(442, 4, 11, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(443, 4, 12, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(444, 4, 13, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(445, 4, 14, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(446, 4, 15, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(447, 4, 16, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(448, 4, 17, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(449, 4, 18, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(450, 4, 19, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(451, 4, 20, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(452, 4, 21, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(453, 4, 22, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(454, 4, 23, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(455, 4, 24, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(456, 4, 25, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(457, 4, 26, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(458, 4, 27, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(459, 4, 28, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(460, 4, 29, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(461, 4, 30, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(462, 4, 31, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(463, 4, 32, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(464, 4, 33, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(465, 4, 34, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(466, 4, 35, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(467, 4, 36, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(468, 4, 37, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(469, 4, 38, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(470, 4, 39, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(471, 4, 40, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(472, 4, 41, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(473, 4, 42, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(474, 4, 43, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(475, 4, 44, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(476, 4, 45, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(477, 4, 46, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(478, 4, 47, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(479, 4, 48, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(480, 4, 49, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(481, 4, 50, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(482, 4, 51, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(483, 4, 108, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(484, 4, 109, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(485, 4, 110, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(486, 4, 111, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(487, 4, 112, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(488, 4, 113, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(489, 4, 114, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(490, 4, 115, 'readonly', '2019-04-07 06:46:57', '2019-04-07 06:46:57'),
(491, 4, 116, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(492, 4, 117, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(493, 4, 118, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(494, 4, 119, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(495, 4, 120, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(496, 4, 121, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(497, 4, 122, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(498, 4, 123, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(499, 4, 124, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(500, 4, 125, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(501, 4, 126, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(502, 4, 127, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(503, 4, 128, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(504, 4, 129, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(505, 4, 149, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(506, 4, 150, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(507, 4, 152, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(508, 4, 153, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(509, 4, 160, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(510, 4, 161, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(511, 4, 162, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(512, 4, 163, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(513, 4, 164, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(514, 4, 165, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(515, 4, 166, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(516, 4, 167, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(517, 4, 168, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(518, 4, 170, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(519, 4, 171, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(520, 4, 172, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(521, 4, 173, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(522, 4, 176, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(523, 4, 177, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(524, 4, 178, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(525, 4, 179, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(526, 4, 180, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(527, 4, 181, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(528, 4, 185, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(529, 4, 186, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(530, 4, 187, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(531, 4, 188, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(532, 4, 189, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(533, 4, 190, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(534, 4, 252, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(535, 4, 271, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(536, 4, 272, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(537, 4, 286, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(538, 4, 287, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(539, 4, 303, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(540, 4, 304, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(541, 4, 299, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(542, 4, 300, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(543, 4, 301, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(544, 4, 302, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(545, 4, 311, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(546, 4, 305, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(547, 4, 306, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(548, 4, 307, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(549, 4, 308, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(550, 4, 309, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(551, 4, 310, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(552, 4, 312, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(553, 4, 313, 'readonly', '2019-04-07 06:46:58', '2019-04-07 06:46:58'),
(554, 3, 1, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(555, 3, 2, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(556, 3, 3, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(557, 3, 4, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(558, 3, 5, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(559, 3, 289, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(560, 3, 290, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(561, 3, 291, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(562, 3, 292, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(563, 3, 293, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(564, 3, 294, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(565, 3, 295, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(566, 3, 314, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(567, 3, 6, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(568, 3, 7, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(569, 3, 8, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(570, 3, 9, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(571, 3, 10, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(572, 3, 11, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(573, 3, 12, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(574, 3, 13, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(575, 3, 14, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(576, 3, 15, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(577, 3, 16, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(578, 3, 17, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(579, 3, 18, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(580, 3, 19, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(581, 3, 20, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(582, 3, 21, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(583, 3, 22, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(584, 3, 23, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(585, 3, 24, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(586, 3, 25, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(587, 3, 26, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(588, 3, 27, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(589, 3, 28, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(590, 3, 29, 'readonly', '2019-04-07 06:47:35', '2019-04-07 06:47:35'),
(591, 3, 30, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(592, 3, 31, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(593, 3, 32, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(594, 3, 33, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(595, 3, 34, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(596, 3, 35, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(597, 3, 36, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(598, 3, 37, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(599, 3, 38, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(600, 3, 39, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(601, 3, 40, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(602, 3, 41, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(603, 3, 42, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(604, 3, 43, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(605, 3, 44, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(606, 3, 45, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(607, 3, 46, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(608, 3, 47, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(609, 3, 48, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(610, 3, 49, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(611, 3, 50, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(612, 3, 51, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(613, 3, 108, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(614, 3, 109, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(615, 3, 110, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(616, 3, 111, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(617, 3, 112, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(618, 3, 113, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(619, 3, 114, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(620, 3, 115, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(621, 3, 116, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(622, 3, 117, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(623, 3, 118, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(624, 3, 119, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(625, 3, 120, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(626, 3, 121, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(627, 3, 122, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(628, 3, 123, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(629, 3, 124, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(630, 3, 125, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(631, 3, 126, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(632, 3, 127, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(633, 3, 128, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(634, 3, 129, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(635, 3, 149, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(636, 3, 150, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(637, 3, 152, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(638, 3, 153, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(639, 3, 160, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(640, 3, 161, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(641, 3, 162, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(642, 3, 163, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(643, 3, 164, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(644, 3, 165, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(645, 3, 166, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(646, 3, 167, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(647, 3, 168, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(648, 3, 170, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(649, 3, 171, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(650, 3, 172, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(651, 3, 173, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(652, 3, 176, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(653, 3, 177, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(654, 3, 178, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(655, 3, 179, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(656, 3, 180, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(657, 3, 181, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(658, 3, 185, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(659, 3, 186, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(660, 3, 187, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(661, 3, 188, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(662, 3, 189, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(663, 3, 190, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(664, 3, 252, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(665, 3, 271, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(666, 3, 272, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(667, 3, 286, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(668, 3, 287, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(669, 3, 303, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(670, 3, 304, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(671, 3, 299, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(672, 3, 300, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(673, 3, 301, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(674, 3, 302, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(675, 3, 311, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(676, 3, 305, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(677, 3, 306, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(678, 3, 307, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(679, 3, 308, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(680, 3, 309, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(681, 3, 310, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(682, 3, 312, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36'),
(683, 3, 313, 'readonly', '2019-04-07 06:47:36', '2019-04-07 06:47:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `key` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `deleted_at`, `created_at`, `updated_at`, `key`, `value`) VALUES
(840, NULL, NULL, NULL, '_token', 'YV82BxC1ptI0yN6sQ354O7HZU1TaxYTxmsrWLRaN'),
(841, NULL, NULL, NULL, 'logo', '/uploads/images/Logo/logo.png'),
(842, NULL, NULL, NULL, 'logo_50', '/uploads/images/Logo/anzedo-bg.png'),
(843, NULL, NULL, NULL, 'favicon', '/uploads/images/Logo/logo.png'),
(844, NULL, NULL, NULL, 'ads_1', '/uploads/images/Ads_300/ad-3.jpg'),
(845, NULL, NULL, NULL, 'ads_2', '/uploads/images/Ads_728/ad-2.jpg'),
(846, NULL, NULL, NULL, 'ads_3', '/uploads/images/Ads_450/ad-1.jpg'),
(847, NULL, NULL, NULL, 'company_name', 'Pixels'),
(848, NULL, NULL, NULL, 'company_address', 'Việt Nam'),
(849, NULL, NULL, NULL, 'company_address1', 'CS2: 287 Trương Định, Hà Nội'),
(850, NULL, NULL, NULL, 'company_address2', ''),
(851, NULL, NULL, NULL, 'company_about', 'Pixels - \"Sống là hãy cho đi\" !'),
(852, NULL, NULL, NULL, 'company_about_short', 'Pixels - Lắng nghe và sẽ chia...'),
(853, NULL, NULL, NULL, 'company_content', '...'),
(854, NULL, NULL, NULL, 'company_email', 'pixels_team@gmail.com'),
(855, NULL, NULL, NULL, 'company_phone', '035.939.9320'),
(856, NULL, NULL, NULL, 'company_hotline', '035.939.9320'),
(857, NULL, NULL, NULL, 'company_fax', '035.939.9320'),
(858, NULL, NULL, NULL, 'facebook', 'https://www.facebook.com/Pixels_Team-308805819803311/'),
(859, NULL, NULL, NULL, 'instagram', ''),
(860, NULL, NULL, NULL, 'youtube', ''),
(861, NULL, NULL, NULL, 'googleplus', ''),
(862, NULL, NULL, NULL, 'linkedin', ''),
(863, NULL, NULL, NULL, 'twitter', ''),
(864, NULL, NULL, NULL, 'pinterest', ''),
(865, NULL, NULL, NULL, 'copyright', '© 2019 - Pixels'),
(866, NULL, NULL, NULL, 'web_status', 'on');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliderimages`
--

DROP TABLE IF EXISTS `sliderimages`;
CREATE TABLE `sliderimages` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `key` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sliderimages`
--

INSERT INTO `sliderimages` (`id`, `deleted_at`, `created_at`, `updated_at`, `key`, `image`, `title`, `content`, `html`) VALUES
(1, NULL, '2018-11-21 19:03:30', '2019-02-27 20:17:07', 1, '0', 'ANZEDO STORE', '', ''),
(2, '2018-12-21 03:29:04', '2018-11-21 19:10:50', '2018-12-21 03:29:04', 1, '0', 'Thực phẩm hữu cơ', 'TỐT CHO SỨC KHỎE - TỐT CHO CUỘC SỐNG', '<div class=\"slider-button\">\r\n                        <a class=\"btnHome btn-shopNow\" href=\"/blog\" data-animation=\"fadeInLeftBig\" data-delay=\"0.5s\">Xem ngay</a>\r\n                        <a class=\"btnHome btn-left\" href=\"/store/category/danh-muc\" data-animation=\"fadeInRightBig\" data-delay=\"0.5s\">Mua ngay</a>\r\n                    </div>'),
(3, '2018-12-21 03:29:06', '2018-11-21 19:11:10', '2018-12-21 03:29:06', 1, '0', 'Thực phẩm hữu cơ', 'TỐT CHO SỨC KHỎE - TỐT CHO CUỘC SỐNG', '<div class=\"slider-button\">\r\n                        <a class=\"btnHome btn-shopNow\" href=\"/blog\" data-animation=\"fadeInLeftBig\" data-delay=\"0.5s\">Xem ngay</a>\r\n                        <a class=\"btnHome btn-left\" href=\"/store/category/danh-muc\" data-animation=\"fadeInRightBig\" data-delay=\"0.5s\">Mua ngay</a>\r\n                    </div>'),
(4, NULL, '2018-12-11 10:27:07', '2018-12-21 03:55:43', 2, '0', 'Góc sạp', '', ''),
(5, NULL, '2018-12-21 03:54:24', '2018-12-21 03:54:24', 2, '0', 'góc sạp', '', ''),
(6, '2018-12-21 03:57:40', '2018-12-21 03:56:35', '2018-12-21 03:57:40', 2, '0', 'góc sạp', '', ''),
(7, '2019-02-27 02:24:50', '2018-12-21 04:13:42', '2019-02-27 02:24:50', 1, '0', 'Chào mừng bạn đến với Đặc Sản Thiên Nhiên', '', '<div class=\"slider-button\">\r\n                        <a class=\"btnHome btn-shopNow\" href=\"/store/category/danh-muc\" data-animation=\"fadeInLeftBig\" data-delay=\"0.5s\">Xem ngay</a>\r\n                        <a class=\"btnHome btn-left\" href=\"/store/details/dau-ngam-tieu-ot\" data-animation=\"fadeInRightBig\" data-delay=\"0.5s\">Mua ngay</a>\r\n                    </div>'),
(8, NULL, '2019-02-13 18:50:08', '2019-02-13 18:50:08', 3, '0', 'BANNER_PAGES', '', ''),
(9, NULL, '2019-02-14 03:34:19', '2019-02-14 03:34:19', 4, '0', 'PROMOTION', 'PROMOTION', ''),
(10, '2019-02-26 21:08:40', '2019-02-14 18:05:53', '2019-02-26 21:08:40', 1, '0', 'Chào mừng bạn đến với Đặc Sản Thiên Nhiên', '', ''),
(11, NULL, '2019-02-14 18:11:22', '2019-02-27 02:25:49', 5, '0', 'ANZEDO STORE', '', ''),
(12, NULL, '2019-02-14 18:11:32', '2019-02-27 02:26:46', 5, '0', 'ANZEDO STORE', '', ''),
(13, NULL, '2019-02-14 18:11:41', '2019-02-27 02:28:31', 5, '0', 'ANZEDO STORE', '', ''),
(14, NULL, '2019-02-14 18:11:50', '2019-02-27 02:29:31', 5, '0', 'ANZEDO STORE', '', ''),
(15, NULL, '2019-02-27 20:17:16', '2019-02-27 20:17:16', 1, '0', 'ANZEDO STORE', '', ''),
(16, NULL, '2019-02-27 20:17:24', '2019-02-27 20:17:24', 1, '0', 'ANZEDO STORE', '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `key` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sliders`
--

INSERT INTO `sliders` (`id`, `deleted_at`, `created_at`, `updated_at`, `key`, `status`) VALUES
(1, NULL, '2018-11-21 19:01:54', '2018-11-21 19:01:54', 'HOME_SLIDER', 1),
(2, NULL, '2018-12-11 10:26:46', '2018-12-11 10:26:46', 'GOC_SAP', 1),
(3, NULL, '2019-02-13 18:48:47', '2019-02-13 18:48:59', 'BANNER_PAGES', 1),
(4, NULL, '2019-02-14 03:33:44', '2019-02-14 03:33:44', 'PROMOTION', 1),
(5, NULL, '2019-02-14 18:10:46', '2019-02-14 18:10:46', 'BANNER_TOP', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `uploads`
--

DROP TABLE IF EXISTS `uploads`;
CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Client',
  `context_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `google_plus` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `deleted_at`, `created_at`, `updated_at`, `remember_token`, `name`, `email`, `password`, `type`, `context_id`, `content`, `image`, `facebook`, `twitter`, `google_plus`, `instagram`, `status`, `phone`) VALUES
(1, NULL, '2018-07-19 18:51:21', '2019-03-31 06:27:55', 'Zja7eUUNKhNYi6uSCJx8b9mhzrsHGxEhXzNcZ1E9BOxIH5oAUHvvDA0zT9JM', 'Admin', 'admin@localhost.com', '$2y$10$fRH8iD3rvM3ea/.5rAHUOe6qCEAPR6e0CIkwSI2OJqm7Y6Tt5jdaO', 'Employee', 1, '', '', '', '', '', '', 1, ''),
(2, NULL, '2019-04-07 05:56:14', '2019-04-07 05:56:14', 'hhBUFznR61WVqoc1TVSOCRWaZHqYfKH5sx37Odpi', 'Mod', 'jely.big@gmail.com', '$2y$10$YyTbRoqgJ.RtqQuZ1XVtDuSbxVVXp/qReJPMfmzm8NPU3lavMRrvO', 'Employee', 0, 'Hello Admin Pixel!', '0', 'https://www.facebook.com/Jely.Big', '', '', '', 1, '0359399320');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `backups_name_unique` (`name`),
  ADD UNIQUE KEY `backups_file_name_unique` (`file_name`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Chỉ mục cho bảng `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_dept_foreign` (`dept`);

--
-- Chỉ mục cho bảng `evaluates`
--
ALTER TABLE `evaluates`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `la_configs`
--
ALTER TABLE `la_configs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `la_menus`
--
ALTER TABLE `la_menus`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `mail_posts`
--
ALTER TABLE `mail_posts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menugroups`
--
ALTER TABLE `menugroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Chỉ mục cho bảng `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Chỉ mục cho bảng `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `organizations_name_unique` (`name`),
  ADD UNIQUE KEY `organizations_email_unique` (`email`),
  ADD KEY `organizations_assigned_to_foreign` (`assigned_to`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Chỉ mục cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `posts_author_foreign` (`author`);

--
-- Chỉ mục cho bảng `product_labels`
--
ALTER TABLE `product_labels`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `re_comments`
--
ALTER TABLE `re_comments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Chỉ mục cho bảng `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Chỉ mục cho bảng `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Chỉ mục cho bảng `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sliderimages`
--
ALTER TABLE `sliderimages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliderimages_key_foreign` (`key`);

--
-- Chỉ mục cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `evaluates`
--
ALTER TABLE `evaluates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `la_configs`
--
ALTER TABLE `la_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `la_menus`
--
ALTER TABLE `la_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT cho bảng `mail_posts`
--
ALTER TABLE `mail_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `menugroups`
--
ALTER TABLE `menugroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT cho bảng `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;

--
-- AUTO_INCREMENT cho bảng `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `product_labels`
--
ALTER TABLE `product_labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `re_comments`
--
ALTER TABLE `re_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT cho bảng `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=684;

--
-- AUTO_INCREMENT cho bảng `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=867;

--
-- AUTO_INCREMENT cho bảng `sliderimages`
--
ALTER TABLE `sliderimages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`);

--
-- Các ràng buộc cho bảng `module_fields`
--
ALTER TABLE `module_fields`
  ADD CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `employees` (`id`);

--
-- Các ràng buộc cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_author_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`);

--
-- Các ràng buộc cho bảng `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `sliderimages`
--
ALTER TABLE `sliderimages`
  ADD CONSTRAINT `sliderimages_key_foreign` FOREIGN KEY (`key`) REFERENCES `sliders` (`id`);

--
-- Các ràng buộc cho bảng `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
