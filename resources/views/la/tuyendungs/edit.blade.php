@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/tuyendungs') }}">Tuyendung</a> :
@endsection
@section("contentheader_description", $tuyendung->$view_col)
@section("section", "Tuyendungs")
@section("section_url", url(config('laraadmin.adminRoute') . '/tuyendungs'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Tuyendungs Edit : ".$tuyendung->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<!-- <div class="col-md-8 col-md-offset-2"> -->
			<div class="col-md-12">
				{!! Form::model($tuyendung, ['route' => [config('laraadmin.adminRoute') . '.tuyendungs.update', $tuyendung->id ], 'method'=>'PUT', 'id' => 'tuyendung-edit-form']) !!}
					<!-- @la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'slug')
					@la_input($module, 'address')
					@la_input($module, 'rank')
					@la_input($module, 'salary')
					@la_input($module, 'career')
					@la_input($module, 'department')
					@la_input($module, 'test_day')
					@la_input($module, 'form')
					@la_input($module, 'degree')
					@la_input($module, 'deadline')
					@la_input($module, 'content')
					--}} -->
					<div class=" tab-content">
						<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
							<div class="tab-content">
								<div class="panel infolist">
									<div class="row panel-body">
										<div class="col-md-8">
											@la_input($module, 'name')
											@la_input($module, 'slug')
											@la_input($module, 'rank')
											@la_input($module, 'salary')
											@la_input($module, 'content')
										</div>
										<div class="col-md-4">
											@la_input($module, 'department')
											@la_input($module, 'test_day')
											@la_input($module, 'form')
											@la_input($module, 'degree')
											@la_input($module, 'deadline')
											<div class="form-group">
												<label for="address">Nơi làm việc :</label>
												<select class="form-control select2-hidden-accessible" data-placeholder="Select multiple Nơi làm việcs" multiple="" rel="select2" name="address[]" tabindex="-1" aria-hidden="true">
													{!! $getListDiaChi !!}
													<!-- <option value="1">Hà Nội</option>
													<option value="2">Đà Nẵng</option>
													<option value="3">Hồ Chí Minh</option> -->
												</select>
											</div>
											<div class="form-group">
												<label for="career">Ngành nghề :</label>
												<select class="form-control select2-hidden-accessible" data-placeholder="Select multiple Ngành nghềs" multiple="" rel="select2" name="career[]" tabindex="-1" aria-hidden="true">
													{!! $getListNganhNghe !!}
													<!-- <option value="1">Tuyển dụng miền Bắc</option>
													<option value="2">Tuyển dụng miền Trung</option>
													<option value="3">Tuyển dụng miền Nam</option> -->
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/tuyendungs') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#tuyendung-edit-form").validate({
		
	});
});
</script>
@endpush
