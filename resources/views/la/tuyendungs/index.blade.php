@extends("la.layouts.app")

@section("contentheader_title", "Tuyendungs")
@section("contentheader_description", "Tuyendungs listing")
@section("section", "Tuyendungs")
@section("sub_section", "Listing")
@section("htmlheader_title", "Tuyendungs Listing")

@section("headerElems")
@la_access("Tuyendungs", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Tuyendung</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Tuyendungs", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Tuyendung</h4>
			</div>
			{!! Form::open(['action' => 'LA\TuyendungsController@store', 'id' => 'tuyendung-add-form']) !!}
			<!-- <div class="modal-body">
				<div class="box-body">
			                    @la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'slug')
					@la_input($module, 'address')
					@la_input($module, 'rank')
					@la_input($module, 'salary')
					@la_input($module, 'career')
					@la_input($module, 'department')
					@la_input($module, 'test_day')
					@la_input($module, 'form')
					@la_input($module, 'degree')
					@la_input($module, 'deadline')
					@la_input($module, 'content')
					--}}
				</div>
			</div> -->
			<div class="modal-body">
				<div class="box-body">
					<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
						<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Thêm mới</a></li>
					</ul>
					<div class=" tab-content">
						<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
							<div class="tab-content">
								<div class="panel infolist">
									<div class="row panel-body">
										<div class="col-md-8">
											@la_input($module, 'name')
											@la_input($module, 'slug')
											@la_input($module, 'rank')
											@la_input($module, 'salary')
											@la_input($module, 'content')
										</div>
										<div class="col-md-4">
											@la_input($module, 'department')
											@la_input($module, 'test_day')
											@la_input($module, 'form')
											@la_input($module, 'degree')
											@la_input($module, 'deadline')
											@la_input($module, 'address')
											@la_input($module, 'career')
											<!-- @php
												$catJson = ["1","2","3"];
											@endphp
											<div class="form-group">
												<label for="address">Nơi làm việc :</label>
												<select class="form-control select2-hidden-accessible" data-placeholder="Select multiple Nơi làm việcs" multiple="" rel="select2" name="address[]" tabindex="-1" aria-hidden="true">
												</select>
											</div>
											<div class="form-group">
												<label for="career">Ngành nghề :</label>
												<select class="form-control select2-hidden-accessible" data-placeholder="Select multiple Ngành nghềs" multiple="" rel="select2" name="career[]" tabindex="-1" aria-hidden="true">
													
												</select>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/tuyendung_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$('input[name="name"]').on('input', function() {
        var slug = function(str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "ãàáảạäâấầẫẩậăặắẳẵằđẻẹẽèéëêếềểễệìíỉĩịïîọõỏõòóöôốồổỗộơớờởỡợụũủùúüûưứừửữựñç·/_,:;";
            var to   = "aaaaaaaaaaaaaaaaaadeeeeeeeeeeeeiiiiiiiooooooooooooooooooouuuuuuuuuuuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }
            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes
            return str;
        };
        var slug = slug( $(this).val() );
        $('input[name="slug"]').val(slug);
    });
	$("#tuyendung-add-form").validate({
		
	});
});
</script>
@endpush
