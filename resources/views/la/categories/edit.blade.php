@extends("la.layouts.app")

@section("contentheader_title", "Categories")
@section("contentheader_description", "Categories Editing")
@section("section", "Categories")
@section("sub_section", "Editing")
@section("htmlheader_title", "Categories Editing")

@section("headerElems")

@endsection

@section("main-content")

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-success">
        <!--<div class="box-header"></div>-->
        <div class="box-body">

            <div class=" col-md-4">
                @la_access("Categories", "edit")
                {!! Form::model($category, ['route' => [config('laraadmin.adminRoute') . '.categories.update', $category->id ], 'method'=>'PUT', 'id' => 'category-edit-form']) !!}
                <div class="">
                    <div class="">
                        <h4 style="margin-top:0">Edit Category</h4>
                        @la_input($module, 'name')
                        @la_input($module, 'slug')
                        <div class="form-group">
                            <label for="parent">Parent name :</label>
                            <select class="form-control" data-placeholder="Enter Parent name" rel="select2" name="parent">

                                {!! $lsOptionByDefault; !!}
                            </select>
                        </div>
                        @la_input($module, 'description')
                        @la_input($module, 'status')

                        <div class="panel-group row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse1">SEO Meta: </a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        @la_input($module, 'seo_title')
                                        @la_input($module, 'seo_description')
                                        @la_input($module, 'seo_keywords')
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default"><a href="{{ url(config('laraadmin.adminRoute') . '/categories') }}">Cancel</a></button>
                    {!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!}

                </div>
                {!! Form::close() !!}
                @endla_access
            </div>
            <div class="col-md-8">


                <div class="tab-content">
                                <div class="panel infolist">
                                    <div class="row panel-body">

                                        <table id="example1" class="table table-bordered">
                                            <thead>
                                            <tr class="success">
                                                @foreach( $listing_cols as $col )
                                                    <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
                                                @endforeach
                                                @if($show_actions)
                                                    <th>Actions</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {!! $category->makeCatTable() !!}
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                </div>

            </div>
        </div>

        @endsection

        @push('styles')

        @endpush

        @push('scripts')
            <script>
                $(function () {
                    $("#category-edit-form").validate({

                    });
                });
            </script>
    @endpush