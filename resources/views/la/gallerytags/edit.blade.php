@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/gallerytags') }}">Gallerytag</a> :
@endsection
@section("contentheader_description", $gallerytag->$view_col)
@section("section", "Gallerytags")
@section("section_url", url(config('laraadmin.adminRoute') . '/gallerytags'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Gallerytags Edit : ".$gallerytag->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($gallerytag, ['route' => [config('laraadmin.adminRoute') . '.gallerytags.update', $gallerytag->id ], 'method'=>'PUT', 'id' => 'gallerytag-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'tags')
					@la_input($module, 'value_vi')
					@la_input($module, 'value_en')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/gallerytags') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#gallerytag-edit-form").validate({
		
	});
});
</script>
@endpush
