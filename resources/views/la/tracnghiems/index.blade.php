@extends("la.layouts.app")

@section("contentheader_title", "Tracnghiems")
@section("contentheader_description", "Tracnghiems listing")
@section("section", "Tracnghiems")
@section("sub_section", "Listing")
@section("htmlheader_title", "Tracnghiems Listing")

@section("headerElems")
@la_access("Tracnghiems", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Tracnghiem</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Tracnghiems", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Tracnghiem</h4>
			</div>
			{!! Form::open(['action' => 'LA\TracnghiemsController@store', 'id' => 'tracnghiem-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
						<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Basic Field</a></li>
					</ul>
                    <div class=" tab-content">
						<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
							<div class="tab-content">
								<div class="panel infolist">
									<div class="row panel-body">

										<div class="col-md-8">
											@la_form($module)
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="" style="display: block;">Câu trả lời: <i id="new-product-option" class="fa fa-plus btn btn-xs btn-success" style="float: right">New</i></label>
												<div class="container-fuid product-options">
													<div class="ioption">
														<div class="col-md-12">
															<input class="form-control" type="text" name="option[answer][]" placeholder="Đáp án" style="margin-bottom: 10px;">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/tracnghiem_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#tracnghiem-add-form").validate({
		
	});
	$("#new-product-option").click(function () {
		$('.product-options').append('<br><div class="ioption"><div class="col-md-12"><input class="form-control" name="option[answer][]" type="text" placeholder="Đáp án" style="margin-bottom: 10px;"></div></div>');
    });
});
</script>
@endpush
