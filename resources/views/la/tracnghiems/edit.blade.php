@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/tracnghiems') }}">Tracnghiem</a> :
@endsection
@section("contentheader_description", $tracnghiem->$view_col)
@section("section", "Tracnghiems")
@section("section_url", url(config('laraadmin.adminRoute') . '/tracnghiems'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Tracnghiems Edit : ".$tracnghiem->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($tracnghiem, ['route' => [config('laraadmin.adminRoute') . '.tracnghiems.update', $tracnghiem->id ], 'method'=>'PUT', 'id' => 'tracnghiem-edit-form']) !!}
				<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
					<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Basic Field</a></li>
				</ul>
				<div class=" tab-content">
					<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
						<div class="tab-content">
							<div class="panel infolist">
								<div class="row panel-body">
									<div class="col-md-8">
										@la_form($module)
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="" style="display: block;">Câu trả lời: <i id="new-product-option" class="fa fa-plus btn btn-xs btn-success" style="float: right">New</i></label>
											<div class="container-fuid product-options">
												@foreach( $options as $option)
												<div class="ioption">
													<div class="col-md-12">
														<input class="form-control" type="text" name="option[answer][]" placeholder="Đáp án" value="{{ $option->answer }}" style="margin-bottom: 10px;">
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/tracnghiems') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#tracnghiem-edit-form").validate({
		
	});
	$("#new-product-option").click(function () {
		$('.product-options').append('<br><div class="ioption"><div class="col-md-12"><input class="form-control" name="option[answer][]" type="text" placeholder="Đáp án" style="margin-bottom: 10px;"></div></div>');
    });
});
</script>
@endpush
