@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/product_labels') }}">Product label</a> :
@endsection
@section("contentheader_description", $product_label->$view_col)
@section("section", "Product labels")
@section("section_url", url(config('laraadmin.adminRoute') . '/product_labels'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Product labels Edit : ".$product_label->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($product_label, ['route' => [config('laraadmin.adminRoute') . '.product_labels.update', $product_label->id ], 'method'=>'PUT', 'id' => 'product_label-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'key')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/product_labels') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#product_label-edit-form").validate({
		
	});
});
</script>
@endpush
