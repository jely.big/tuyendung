@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/re_comments') }}">Re Comment</a> :
@endsection
@section("contentheader_description", $re_comment->$view_col)
@section("section", "Re Comments")
@section("section_url", url(config('laraadmin.adminRoute') . '/re_comments'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Re Comments Edit : ".$re_comment->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($re_comment, ['route' => [config('laraadmin.adminRoute') . '.re_comments.update', $re_comment->id ], 'method'=>'PUT', 'id' => 're_comment-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'cmt_id')
					@la_input($module, 'blog_id')
					@la_input($module, 'full_name')
					@la_input($module, 'phone_number')
					@la_input($module, 'content_re')
					@la_input($module, 'user_id')
					@la_input($module, 'status')
					@la_input($module, 'email_re')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/re_comments') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#re_comment-edit-form").validate({
		
	});
});
</script>
@endpush
