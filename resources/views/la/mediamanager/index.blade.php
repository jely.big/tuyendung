@extends('la.layouts.app')

@section('htmlheader_title') Media @endsection
@section('contentheader_title') Media Manager @endsection
@section('contentheader_description')  @endsection

@section('main-content')
<!-- Main content -->
<section class="content"><div class="row">
    <iframe src="/la-assets/plugins/ckfinder/ckfinder.html" width="100%" style="min-height:500px"></iframe>
</div></section><!-- /.content -->
@endsection
