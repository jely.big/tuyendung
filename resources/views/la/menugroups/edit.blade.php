@extends("la.layouts.app")

@section("contentheader_title", "Menu Group")
@section("contentheader_description", "Menu group editing")
@section("section", "Menugroups")
@section("sub_section", "Editing")
@section("htmlheader_title", "Menugroups editing")

@section("headerElems")

@endsection

@section("main-content")

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="box box-success">
		<!--<div class="box-header"></div>-->
		<div class="box-body">
			<div class=" col-md-4">

				@la_access("Menugroups", "create")

				{!! Form::model($menugroup, ['route' => [config('laraadmin.adminRoute') . '.menugroups.update', $menugroup->id ], 'method'=>'PUT', 'id' => 'menugroup-edit-form']) !!}
				<div class="">
					<div class="">
						<h4 style="margin-top:0">Add Menu Group</h4>
						@la_form($module)
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default pull-left"><a href="{{ url(config('laraadmin.adminRoute') . '/menugroups') }}">Cancel</a></button>
					{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}

				@endla_access

			</div>
			<div class=" col-md-8">
				<table id="example1" class="table table-bordered">
					<thead>
					<tr class="success">
						@foreach( $listing_cols as $col )
							<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
						@endforeach
						@if($show_actions)
							<th>Actions</th>
						@endif
					</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>


@endsection

@push('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
	<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
	<script>
        $(function () {
            $("#example1").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url(config('laraadmin.adminRoute') . '/menugroup_dt_ajax') }}",
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Search"
                },
				@if($show_actions)
                columnDefs: [ { orderable: false, targets: [-1] }],
				@endif
            });
            $("#menugroup-edit-form").validate({

            });
        });
	</script>
@endpush
