@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/sliderimages') }}">SliderImage</a> :
@endsection
@section("contentheader_description", $sliderimage->$view_col)
@section("section", "SliderImages")
@section("section_url", url(config('laraadmin.adminRoute') . '/sliderimages'))
@section("sub_section", "Edit")

@section("htmlheader_title", "SliderImages Edit : ".$sliderimage->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($sliderimage, ['route' => [config('laraadmin.adminRoute') . '.sliderimages.update', $sliderimage->id ], 'method'=>'PUT', 'id' => 'sliderimage-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'key')
					@la_input($module, 'image')
					@la_input($module, 'title')
					@la_input($module, 'content')
					@la_input($module, 'html')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/sliderimages') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#sliderimage-edit-form").validate({
		
	});
});
</script>
@endpush
