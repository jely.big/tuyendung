@extends("la.layouts.app")

@section("contentheader_title", "Thành viên")
{{-- @section("contentheader_description", "Users listing") --}}
@section("section", "Thành viên")
@section("sub_section", "Danh sách")
@section("htmlheader_title", "Danh sách thành viên")

@section("headerElems")
{{-- @la_access("Users", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add User</button>
@endla_access --}}
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>ID</th>
			<th>Thông tin User</th>
			<th>Chức năng</th>
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Users", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add User</h4>
			</div>
			{!! Form::open(['action' => 'LA\UsersController@store', 'id' => 'user-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'context_id')
					@la_input($module, 'email')
					@la_input($module, 'password')
					@la_input($module, 'type')
					@la_input($module, 'status')
					@la_input($module, 'active')
					@la_input($module, 'job')
					@la_input($module, 'fullname')
					@la_input($module, 'address')
					@la_input($module, 'phone')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

<div class="modal fade" id="DetailModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div id="DetailContent" class="modal-content">
			<!-- Ajax Details -->
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
$(document).ready(function(){
	$(document).on('click','.bnt-del',function(){
		var id = $(this).attr('data-id');
		$.get( "{{ url(config('laraadmin.adminRoute') . '/user_ajax_detail') }}/"+id, function( data ) {
			alert('Bạn chắc chắn muốn xóa User này?');
			$("#DetailContent").html(data);
		});

	});

	$(document).on('click','.DetailBtn',function(){
		var id = $(this).attr('data-id');
		$.get( "{{ url(config('laraadmin.adminRoute') . '/user_ajax_detail') }}/"+id, function( data ) {
			$("#DetailContent").html(data);
		  	$("#DetailModal").modal('show');
		});

	});

});
</script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/user_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#user-add-form").validate({
		
	});
});
</script>
@endpush
