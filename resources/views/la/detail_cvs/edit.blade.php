@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/detail_cvs') }}">Detail CV</a> :
@endsection
@section("contentheader_description", $detail_cv->$view_col)
@section("section", "Detail CVs")
@section("section_url", url(config('laraadmin.adminRoute') . '/detail_cvs'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Detail CVs Edit : ".$detail_cv->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($detail_cv, ['route' => [config('laraadmin.adminRoute') . '.detail_cvs.update', $detail_cv->id ], 'method'=>'PUT', 'id' => 'detail_cv-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'bangcap1')
					@la_input($module, 'bangcap2')
					@la_input($module, 'tentruong1')
					@la_input($module, 'tentruong2')
					@la_input($module, 'khoanganh1')
					@la_input($module, 'khoanganh2')
					@la_input($module, 'tgdt1')
					@la_input($module, 'tgdt2')
					@la_input($module, 'khoahoc1')
					@la_input($module, 'khoahoc2')
					@la_input($module, ' dvtc1')
					@la_input($module, 'dvtc2')
					@la_input($module, 'cc1')
					@la_input($module, 'cc2')
					@la_input($module, 'tgkh1')
					@la_input($module, 'tgkh2')
					@la_input($module, 'word')
					@la_input($module, 'excel')
					@la_input($module, 'powerpoint')
					@la_input($module, 'listening')
					@la_input($module, 'speaking')
					@la_input($module, 'writing')
					@la_input($module, 'skill_soft')
					@la_input($module, 'from_date1')
					@la_input($module, 'from_date2')
					@la_input($module, 'from_date3')
					@la_input($module, 'to_date1')
					@la_input($module, 'to_date2')
					@la_input($module, 'to_date3')
					@la_input($module, 'company_1')
					@la_input($module, 'company_2')
					@la_input($module, 'company_3')
					@la_input($module, 'title_1')
					@la_input($module, 'title_2')
					@la_input($module, 'title_3')
					@la_input($module, 'working_1')
					@la_input($module, 'working_2')
					@la_input($module, 'working_3')
					@la_input($module, 'salary_1')
					@la_input($module, 'salary_2')
					@la_input($module, 'salary_3')
					@la_input($module, 'fullname_1')
					@la_input($module, 'fullname_2')
					@la_input($module, 'fullname_3')
					@la_input($module, 'relation_1')
					@la_input($module, 'relation_2')
					@la_input($module, 'relation_3')
					@la_input($module, 'relation_title_1')
					@la_input($module, 'relation_title_2')
					@la_input($module, 'relation_title_3')
					@la_input($module, 'relation_company_1')
					@la_input($module, 'relation_company_2')
					@la_input($module, 'relation_company_3')
					@la_input($module, 'relation_phone_1')
					@la_input($module, 'relation_phone_2')
					@la_input($module, 'relation_phone_3')
					@la_input($module, 'new_city')
					@la_input($module, 'ditricst')
					@la_input($module, 'content')
					@la_input($module, 'candidate_position')
					@la_input($module, 'date_receive_job')
					@la_input($module, 'image')
					@la_input($module, 'fullname')
					@la_input($module, 'birthday')
					@la_input($module, 'address_birthday')
					@la_input($module, 'marital_status')
					@la_input($module, 'number_children')
					@la_input($module, 'mobile')
					@la_input($module, 'email')
					@la_input($module, 'address')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/detail_cvs') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#detail_cv-edit-form").validate({
		
	});
});
</script>
@endpush
