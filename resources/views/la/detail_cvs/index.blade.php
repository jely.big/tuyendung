@extends("la.layouts.app")

@section("contentheader_title", "Detail CVs")
@section("contentheader_description", "Detail CVs listing")
@section("section", "Detail CVs")
@section("sub_section", "Listing")
@section("htmlheader_title", "Detail CVs Listing")

@section("headerElems")
@la_access("Detail_CVs", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Detail CV</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<style type="text/css">
	.hidden{
		display: none;
	}
</style>
<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Detail_CVs", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Detail CV</h4>
			</div>
			{!! Form::open(['action' => 'LA\Detail_CVsController@store', 'id' => 'detail_cv-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'bangcap1')
					@la_input($module, 'bangcap2')
					@la_input($module, 'tentruong1')
					@la_input($module, 'tentruong2')
					@la_input($module, 'khoanganh1')
					@la_input($module, 'khoanganh2')
					@la_input($module, 'tgdt1')
					@la_input($module, 'tgdt2')
					@la_input($module, 'khoahoc1')
					@la_input($module, 'khoahoc2')
					@la_input($module, ' dvtc1')
					@la_input($module, 'dvtc2')
					@la_input($module, 'cc1')
					@la_input($module, 'cc2')
					@la_input($module, 'tgkh1')
					@la_input($module, 'tgkh2')
					@la_input($module, 'word')
					@la_input($module, 'excel')
					@la_input($module, 'powerpoint')
					@la_input($module, 'listening')
					@la_input($module, 'speaking')
					@la_input($module, 'writing')
					@la_input($module, 'skill_soft')
					@la_input($module, 'from_date1')
					@la_input($module, 'from_date2')
					@la_input($module, 'from_date3')
					@la_input($module, 'to_date1')
					@la_input($module, 'to_date2')
					@la_input($module, 'to_date3')
					@la_input($module, 'company_1')
					@la_input($module, 'company_2')
					@la_input($module, 'company_3')
					@la_input($module, 'title_1')
					@la_input($module, 'title_2')
					@la_input($module, 'title_3')
					@la_input($module, 'working_1')
					@la_input($module, 'working_2')
					@la_input($module, 'working_3')
					@la_input($module, 'salary_1')
					@la_input($module, 'salary_2')
					@la_input($module, 'salary_3')
					@la_input($module, 'fullname_1')
					@la_input($module, 'fullname_2')
					@la_input($module, 'fullname_3')
					@la_input($module, 'relation_1')
					@la_input($module, 'relation_2')
					@la_input($module, 'relation_3')
					@la_input($module, 'relation_title_1')
					@la_input($module, 'relation_title_2')
					@la_input($module, 'relation_title_3')
					@la_input($module, 'relation_company_1')
					@la_input($module, 'relation_company_2')
					@la_input($module, 'relation_company_3')
					@la_input($module, 'relation_phone_1')
					@la_input($module, 'relation_phone_2')
					@la_input($module, 'relation_phone_3')
					@la_input($module, 'new_city')
					@la_input($module, 'ditricst')
					@la_input($module, 'content')
					@la_input($module, 'candidate_position')
					@la_input($module, 'date_receive_job')
					@la_input($module, 'image')
					@la_input($module, 'fullname')
					@la_input($module, 'birthday')
					@la_input($module, 'address_birthday')
					@la_input($module, 'marital_status')
					@la_input($module, 'number_children')
					@la_input($module, 'mobile')
					@la_input($module, 'email')
					@la_input($module, 'address')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/detail_cv_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#detail_cv-add-form").validate({
		
	});
	$(document).on('click', '.down_cv', function() {
		var id = $(this).attr('data-id');
		var name = $(this).attr('data-name');
		$.ajax({
            url:"{{ url(config('laraadmin.adminRoute') . '/download_cv') }}",
            method:"POST",
            data:{
            	id:id,
            	name:name,
            	_token: '{{csrf_token()}}'
            },
            dataType:'JSON',
            success:function(data)
            {
            	if(data.Result == 200){
            		alert('Tải CV thành công');
            		window.open(data.Cv);
            	}else{
            		alert('Tải CV thất bại');
            	}
            }
        })
	});
});
</script>
@endpush
