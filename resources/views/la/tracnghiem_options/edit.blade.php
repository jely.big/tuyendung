@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/tracnghiem_options') }}">Tracnghiem Option</a> :
@endsection
@section("contentheader_description", $tracnghiem_option->$view_col)
@section("section", "Tracnghiem Options")
@section("section_url", url(config('laraadmin.adminRoute') . '/tracnghiem_options'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Tracnghiem Options Edit : ".$tracnghiem_option->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($tracnghiem_option, ['route' => [config('laraadmin.adminRoute') . '.tracnghiem_options.update', $tracnghiem_option->id ], 'method'=>'PUT', 'id' => 'tracnghiem_option-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'id_tracnghiem')
					@la_input($module, 'answer')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/tracnghiem_options') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#tracnghiem_option-edit-form").validate({
		
	});
});
</script>
@endpush
