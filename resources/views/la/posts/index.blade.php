@extends("la.layouts.app")

@section("contentheader_title", "Posts")
@section("contentheader_description", "Posts listing")
@section("section", "Posts")
@section("sub_section", "Listing")
@section("htmlheader_title", "Posts Listing")

@section("headerElems")
@la_access("Posts", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Post</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">

		<div class="row">
			<form action="" method="GET" class="form-inline" >
			<div class="col-sm-12">
				<div id="example1_filter" class="dataTables_filter text-right">
	
					<div class="form-group">
						<label>Categories:</label>
						<select id="filterCat" name="cat" class="form-control input-sm">
							<option value="0">All</option>
							{!!  $category->getCatOptionSelected(app('request')->input('cat')) !!}
						</select>
					</div>
					<div class="form-group">
						<input type="text"  name="keyword" class="form-control input-sm" placeholder="Search" aria-controls="example1" style="float:left">
						<input type="hidden" name="search" value="yes" />
						<button type="submit" style="float:left" value="search" ><i class="fa fa-search" aria-hidden="true"></i></button>
					</div>
				</div>
			</div>
			</form>
		</div>

		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
				<th>Created</th>
			@if($show_actions)

				<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			@foreach($posts as $post)
			<tr>
				<td>{{ $post->id }}</td>
				<td>@if($post->type_post == 'TT')
					<span class="label label-warning">Thông tin</span>
					@else
					<span class="label label-warning">Bài viết</span>
					@endif
				</td>
				<!-- <td><img src="{{ $post->thumbnail }}" width="100"/></td> -->
				<td>{{ $post->title }}</td>
				<td>
					@foreach($category->getCategoryByHomeAdmin($post->categories) as $value)
						<span class="label label-primary">{{ $value['name'] }}</span>
					@endforeach
				</td>
				<td><span class="label label-success">{!!  $user->getNameById($post->author)  !!}</span></td>
				<td>{!!  $htmlMaker->label($post->status, 'status')  !!}</td>
				<td>{!!  date('d-m-Y', strtotime($post->created_at)) !!}</td>
				<td>
					@if($moduelModel->hasAccess("Posts", "edit"))
					<a href="{{ url(config('laraadmin.adminRoute') . '/posts/'.$post->id.'/edit') }}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
					@endif

					@if($moduelModel->hasAccess("Posts", "delete"))
						{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.posts.destroy', $post->id], 'method' => 'delete', 'style'=>'display:inline']) !!}
                        <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
                        {!! Form::close() !!}
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
		</table>
		<div style="float:right">{{ $posts->appends($_GET)->links() }}</div>
	</div>
</div>

@la_access("Posts", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Post</h4>
			</div>
			{!! Form::open(['action' => 'LA\PostsController@store', 'id' => 'post-add-form']) !!}
			<div class="modal-body">
				<div class="row box-body">

					<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
						<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Basic Field</a></li>
						<li class=""><a role="tab" data-toggle="tab" href="#tab-seo" data-target="#tab-seo"><i class="fa fa-clock-o"></i> SEO Meta</a></li>
					</ul>

					<div class=" tab-content">
						<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
							<div class="tab-content">
								<div class="panel infolist">
									<div class="row panel-body">

										<div class="col-md-8">
											@la_input($module, 'title')
											@la_input($module, 'slug')
											@la_input($module, 'content')
										</div>
										<div class="col-md-4">
											@la_input($module, 'status')
                                            <div class="form-group">
                                                <label for="categories[]">Categories :</label>
                                                <select class="form-control select2-hidden-accessible" required="1" data-placeholder="Select multiple Categories" multiple="" rel="select2" name="categories[]" tabindex="-1" aria-hidden="true" aria-required="true">
                                                    {!! $lsCatOption !!}
                                                </select>
                                            </div>
											@la_input($module, 'type_post')
											@la_input($module, 'thumbnail')
											@la_input($module, 'tags')
										</div>

									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-seo">
							@la_input($module, 'seo_title')
							@la_input($module, 'seo_description')
							@la_input($module, 'seo_keywords')
						</div>

					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {

    $('input[name="title"]').on('input', function() {

        var slug = function(str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "ãàáảạäâấầẫẩậăặắẳẵằđẻẹẽèéëêếềểễệìíỉĩịïîọõỏõòóöôốồổỗộơớờởỡợụũủùúüûưứừửữựñç·/_,:;";
            var to   = "aaaaaaaaaaaaaaaaaadeeeeeeeeeeeeiiiiiiiooooooooooooooooooouuuuuuuuuuuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }
            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes
            return str;
        };

        var slug = slug( $(this).val() );
        $('input[name="slug"]').val(slug);

    });
	$("#example11").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/post_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});

    $('.dataTables_filter input').addClass('search-query');
    $('.dataTables_filter input').attr('placeholder', 'Search');

	$("#post-add-form").validate({
		
	});
});
</script>
@endpush