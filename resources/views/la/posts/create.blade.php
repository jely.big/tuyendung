@extends("la.layouts.app")

@section("contentheader_title")
    <a href="{{ url(config('laraadmin.adminRoute') . '/posts') }}">Post</a> :
@endsection
@section("contentheader_description", "Post Create")
@section("section", "Posts")
@section("section_url", url(config('laraadmin.adminRoute') . '/posts'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Posts Create : ")

@section("main-content")

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header">

        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 ">
                    {!! Form::open(['action' => 'LA\PostsController@store', 'id' => 'post-add-form']) !!}
                    <ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
                        <li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Basic Field</a></li>
                        <li class=""><a role="tab" data-toggle="tab" href="#tab-seo" data-target="#tab-seo"><i class="fa fa-clock-o"></i> SEO Meta</a></li>
                    </ul>

                    <div class=" tab-content">
                        <div role="tabpanel" class="tab-pane active fade in" id="tab-info">
                            <div class="tab-content">
                                <div class="panel infolist">
                                    <div class="row panel-body">

                                        <div class="col-md-8">
                                            @la_input($module, 'title')
                                            @la_input($module, 'slug')
                                            @la_input($module, 'content')
                                        </div>
                                        <div class="col-md-4">
                                            @la_input($module, 'status')
                                            @la_input($module, 'lang')
                                            <div class="form-group">
                                                <label for="categories[]">Categories :</label>
                                                <select class="form-control select2-hidden-accessible" required="1" data-placeholder="Select multiple Categories" multiple="" rel="select2" name="categories[]" tabindex="-1" aria-hidden="true" aria-required="true">
                                                    {!! $lsCatOption !!}
                                                </select>
                                            </div>
                                            @la_input($module, 'thumbnail')
                                            @la_input($module, 'tags')
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-seo">
                            @la_input($module, 'seo_title')
                            @la_input($module, 'seo_description')
                            @la_input($module, 'seo_keywords')
                        </div>

                    </div>
                    <div class="form-group">
                        {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
                        <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/posts') }}">Cancel</a></button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(function () {
            $(document).on('change', 'select[name="lang"]', function(){
                $.ajax({
                    method: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/post_add_changeLang_ajax') }}",
                    data: { lang: $(this).val(), _token: "{{ csrf_token() }}" }
                }).done(function( data ) {
                    $('select[name="categories[]"]').html(data);
                });
            });
            $("#post-edit-form").validate({

            });
        });
    </script>
@endpush
