@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/nganhnghes') }}">Nganhnghe</a> :
@endsection
@section("contentheader_description", $nganhnghe->$view_col)
@section("section", "Nganhnghes")
@section("section_url", url(config('laraadmin.adminRoute') . '/nganhnghes'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Nganhnghes Edit : ".$nganhnghe->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($nganhnghe, ['route' => [config('laraadmin.adminRoute') . '.nganhnghes.update', $nganhnghe->id ], 'method'=>'PUT', 'id' => 'nganhnghe-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'slug-name')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/nganhnghes') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#nganhnghe-edit-form").validate({
		
	});
});
</script>
@endpush
