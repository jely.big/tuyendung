@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/diachis') }}">Diachi</a> :
@endsection
@section("contentheader_description", $diachi->$view_col)
@section("section", "Diachis")
@section("section_url", url(config('laraadmin.adminRoute') . '/diachis'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Diachis Edit : ".$diachi->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($diachi, ['route' => [config('laraadmin.adminRoute') . '.diachis.update', $diachi->id ], 'method'=>'PUT', 'id' => 'diachi-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'parent')
					@la_input($module, 'name')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/diachis') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#diachi-edit-form").validate({
		
	});
});
</script>
@endpush
