@extends("la.layouts.app")

@section("contentheader_title", "Menu Editing")
@section("contentheader_description", "Menu Editing")
@section("section", "Menugroups")
@section("sub_section", "Editing")
@section("htmlheader_title", "Menug Editing")

@section("headerElems")

@endsection

@section("main-content")

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="box box-success">
		<!--<div class="box-header"></div>-->
		<div class="box-body">
			<div class=" col-md-4">

				@la_access("Menugroups", "create")

				{!! Form::model($menulist, ['route' => [config('laraadmin.adminRoute') . '.menulists.update', $menulist->id ], 'method'=>'PUT', 'id' => 'menulist-edit-form']) !!}
				<div class="">
					<div class="">
						<h4 style="margin-top:0">Edit Item Menu</h4>
						@la_input($module, 'name')
						@la_input($module, 'url')
						<div class="form-group">
							<label for="parent">Parent name :</label>
							<select class="form-control" data-placeholder="Enter Parent name" rel="select2" name="parent">
								<option value="0">No Parent</option>
								{!! $lsParent; !!}
							</select>
						</div>

						@la_input($module, 'icon')
						@la_input($module, 'status')
						@la_input($module, 'sort')
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="menugroup" value="{{ $menulist->menugroup }}" />
					<button class="btn btn-default pull-left"><a href="{{ url(config('laraadmin.adminRoute') . '/menulists') }}">Cancel</a></button>
					{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}

				@endla_access

			</div>
			<div class=" col-md-8">
				<div class="form-group">
					<label for="lang">Choose Menu Group :</label>
					<select class="form-control" data-placeholder="Choose Menu Group" rel="select2" name="menugroup" disabled="disabled" >
						{!! $lsGroup; !!}
					</select>
				</div>
				<table id="example1" class="table table-bordered">
					<thead>
					<tr class="success">
						@foreach( $listing_cols as $col )
							<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
						@endforeach
						@if($show_actions)
							<th>Actions</th>
						@endif
					</tr>
					</thead>
					<tbody>
					{!! $links !!}
					</tbody>
				</table>
			</div>
		</div>
	</div>


@endsection

@push('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
	<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
	<script>
        $(function () {

            $("#menulist-edit-form").validate({

            });
        });
	</script>
@endpush
