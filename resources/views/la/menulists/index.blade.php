@extends("la.layouts.app")

@section("contentheader_title", "Menu List")
@section("contentheader_description", "Menu listing")
@section("section", "Menugroups")
@section("sub_section", "Listing")
@section("htmlheader_title", "Menug Listing")

@section("headerElems")

@endsection

@section("main-content")

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="box box-success">
		<!--<div class="box-header"></div>-->
		<div class="box-body">
			<div class=" col-md-4">

				@la_access("Menugroups", "create")

				{!! Form::open(['action' => 'LA\MenulistsController@store', 'id' => 'menulist-add-form']) !!}
				<div class="">
					<div class="">
						<h4 style="margin-top:0">Add Item Menu</h4>
						@la_input($module, 'name')
						@la_input($module, 'url')
						<div class="form-group">
							<label for="parent">Parent name :</label>
							<select class="form-control" data-placeholder="Enter Parent name" rel="select2" name="parent">
								<option value="0">No Parent</option>
								{!! $lsOption; !!}
							</select>
						</div>

						@la_input($module, 'icon')
						@la_input($module, 'status')
						@la_input($module, 'sort')
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="menugroup" value="{{ app('request')->input('group') }}" />
					{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}

				@endla_access

			</div>
			<div class=" col-md-8">
				<div class="form-group">
					<label for="lang">Choose Menu Group :</label>
					<select class="form-control" data-placeholder="Choose Menu Group" rel="select2" name="menugroup" onchange="if (this.value) window.location.href='menulists?group='+this.value">
						{!! $lsGroup; !!}
					</select>
				</div>
				<table id="example1" class="table table-bordered">
					<thead>
					<tr class="success">
						@foreach( $listing_cols as $col )
							<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
						@endforeach
						@if($show_actions)
							<th>Actions</th>
						@endif
					</tr>
					</thead>
					<tbody>
						{!! $links !!}
					</tbody>
				</table>
			</div>
		</div>
	</div>


@endsection

@push('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
	<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
	<script>
        $(function () {

        	$('input[name="name"]').on('input', function() {

		        var slug = function(str) {
		            str = str.replace(/^\s+|\s+$/g, ''); // trim
		            str = str.toLowerCase();

		            // remove accents, swap ñ for n, etc
		            var from = "ãàáảạäâấầẫẩậăặắẳẵằđẻẹẽèéëêếềểễệìíỉĩịïîọõỏõòóöôốồổỗộơớờởỡợụũủùúüûưứừửữựñç·/_,:;";
		            var to   = "aaaaaaaaaaaaaaaaaadeeeeeeeeeeeeiiiiiiiooooooooooooooooooouuuuuuuuuuuuunc------";
		            for (var i=0, l=from.length ; i<l ; i++) {
		                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		            }
		            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		                .replace(/\s+/g, '-') // collapse whitespace and replace by -
		                .replace(/-+/g, '-'); // collapse dashes
		            return str;
		        };

		        var slug = slug( $(this).val() );
		        $('input[name="url"]').val(slug);

		    });

            $("#menulist-add-form").validate({

            });
        });
	</script>
@endpush
