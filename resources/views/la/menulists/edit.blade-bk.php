@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/menulists') }}">Menulist</a> :
@endsection
@section("contentheader_description", $menulist->$view_col)
@section("section", "Menulists")
@section("section_url", url(config('laraadmin.adminRoute') . '/menulists'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Menulists Edit : ".$menulist->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($menulist, ['route' => [config('laraadmin.adminRoute') . '.menulists.update', $menulist->id ], 'method'=>'PUT', 'id' => 'menulist-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'url')
					@la_input($module, 'menugroup')
					@la_input($module, 'parent')
					@la_input($module, 'icon')
					@la_input($module, 'status')
					@la_input($module, 'sort')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/menulists') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#menulist-edit-form").validate({
		
	});
});
</script>
@endpush
