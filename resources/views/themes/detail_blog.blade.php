@extends("themes.app")
@section('content')
	<!--NEWS-->
    <div class="col-md-12 search_ts news_content">
        <h3 class="text-center">{{ $DetailPost->title }}</h3>
        <table class="table">
            <tbody>
                <tr>
                    <td>Danh mục</td>
                    <td>{{ $DetailPost->title }}</td>
                </tr>
                <tr>
                    <td>Ngày</td>
                    <td>{{ date('d-m-Y', strtotime($DetailPost->created_at)) }}</td>
                </tr>
                <tr>
                    <td>Tác giả</td>
                    <td>Anzedo</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-12 search_ts news_content">
        <div class="static_content">
            {!! $DetailPost->content !!}
        </div> 
    </div>
    <script type="text/javascript">
        $(document).ready(function() { 
            $('#form-search').addClass('hidden');
        });
    </script>
@endsection