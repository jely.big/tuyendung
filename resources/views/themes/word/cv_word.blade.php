<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CV</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <style type="text/css">
        .css-cv{
            float: unset;
            width: 100%;
        }

        .select_jobDetail select {
            border: 1px dashed #000;
            padding: 5px;
            margin-left: 5px;
        }

        .table-bordered th,
        .table-bordered td {
            vertical-align: middle;
            text-align: center;
        }

        .table-bordered td h3 {
            text-transform: uppercase;
        }

        .table-bordered_ts td {
            text-align: left;
        }

        .tr_ts td {
            text-align: left;
            font-weight: bold;
            background: #2B2B2B;
        }

        .tr_ts_1 td {
            background: #2B2B2B;
            font-weight: bold;
            text-align: center;
        }

        form h3 {
            font-size: 20px;
            margin-top: 30px;
            margin-bottom: 20px;
        }

        .table-bordered_2 td {
            text-align: left;
        }

        form input {
            height: 25px;
            border: 0px;
            border-bottom: 1px dashed #000;
            border-left: 1px dashed #000;
            padding: 0 5px;
        }

        form input[type='radio'] {
            height: 10px;
        }

        form input[type='file'] {
            border: 0px;
        }

        form textarea {
            padding: 5px;
            width: 100%;
            height: 70px;
            border: 1px dashed #000;
        }

        form button {
            padding: 5px 15px;
            border: none;
            color: #fff;
            font-weight: bold;
        }

        form button[type='submit'] {
            background: #c29f75;
        }

        form button[type='reset'] {
            background: #c29f75;
        }

        form button:hover {
            cursor: pointer;
        }

        form p {
            margin-bottom: 5px !important;
        }        
    </style>
</head>
<body>
    <div>
        <form style="width: 100%; margin-top: 15px;" accept-charset="utf-8" id="formDemo" name="add" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <table class="table table-bordered">
                <tr>
                    <td style="width: 20%;"><img src="{{ $setting['logo'] ? $setting['logo'] : '' }}" width="150"></td>
                    <td colspan="3">
                        <h2>Phiếu phỏng vấn</h2>
                    </td>
                    <td>
                        <p><b>Ảnh:</b> (Kích thước ảnh <1MB)</p> <p><input type="file" name="uc_avatar"></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: left; width: 55%">Vị trí ứng tuyển: <input class="input-css" type="text" name="uc_chucdanh" placeholder="Bắt buộc điền" required></td>
                    <td colspan="2" style="text-align: left;">Ngày có thể nhận việc: <input class="input-css" type="date" name="uc_date_job" placeholder="Bắt buộc điền" required></td>
                </tr>
            </table>
            <h3>I. Thông tin cá nhân</h3>
            <table class="table table-bordered table-bordered_ts">
                <tr>
                    <td>Họ tên: <input class="input-css" type="text" style="margin-left: 73px;" name="uc_fullname" placeholder="Bắt buộc điền" required></td>
                    <td>Ngày sinh: <input class="input-css" type="date" name="uc_birthday" placeholder="Bắt buộc điền" required></td>
                    <td>Nơi sinh: <input class="input-css" type="text" name="uc_address" placeholder="Bắt buộc điền" required></td>
                </tr>
                <tr>
                    <td>Tình trạng hôn nhân: <input class="input-css" type="text" name="uc_marries" placeholder="Bắt buộc điền" required></td>
                    <td colspan="2">Số con: <input class="input-css" type="text" style="margin-left: 16px;" name="uc_totalsun" placeholder="Bắt buộc điền" required></td>
                </tr>
                <tr>
                    <td>Điện thoại: <input class="input-css" type="text" style="margin-left: 54px;" name="uc_mobile" placeholder="Bắt buộc điền" required></td>
                    <td colspan="2">Email: <input class="input-css" type="email" style="margin-left: 24px;" name="uc_email" placeholder="Bắt buộc điền" required></td>
                </tr>
                <tr>
                    <td colspan="3">Địa chỉ tạm trú: <input class="input-css" type="text" style="margin-left: 30px;" name="uc_address_tamtru" placeholder="Bắt buộc điền" required></td>
                </tr>
            </table>
            <h3 style="margin-top: 60px !important;">II. Quá trình học tập, đào tạo</h3>
            <table class="table table-bordered table-bordered_2">
                <tr class="tr_ts">
                    <td colspan="4">1. Trình độ học vấn:</td>
                </tr>
                <tr>
                    <td>Bằng cấp</td>
                    <td>Tên trường</td>
                    <td>Khoa/ngành</td>
                    <td>
                        Thời gian được đào tạo
                        <br />(Từ năm - Đến năm)
                    </td>
                </tr>
                <tr>
                    <td><input class="input-css" type="text" name="uc_bangcap_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tentruong_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_khoanganh_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_time_daotao_1" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr>
                    <td><input class="input-css" type="text" name="uc_bangcap_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tentruong_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_khoanganh_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_time_daotao_2" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr class="tr_ts">
                    <td colspan="4">2. Các khóa học đã tham dự:</td>
                </tr>
                <tr>
                    <td>Tên khóa học</td>
                    <td>Đơn vị tổ chức</td>
                    <td>Chứng chỉ (Xếp loại)</td>
                    <td>Thời gian được đào tạo</td>
                </tr>
                <tr>
                    <td><input class="input-css" type="text" name="uc_tenkhoahoc_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_donvitochuc_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_chungchi_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tgdt_1" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr>
                    <td><input class="input-css" type="text" name="uc_tenkhoahoc_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_donvitochuc_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_chungchi_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tgdt_2" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr class="tr_ts">
                    <td colspan="4">3. Kỹ năng:</td>
                </tr>
                <tr>
                    <td>Tin học</td>
                    <td>Word: <input class="input-css" type="text" name="uc_word" placeholder="Bắt buộc điền" required></td>
                    <td>Excel: <input class="input-css" type="text" name="uc_excel" placeholder="Bắt buộc điền" required></td>
                    <td>Powerpoint: <input class="input-css" type="text" name="uc_powerpoint" placeholder="Bắt buộc điền" required></td>
                </tr>
                <tr>
                    <td>Ngoại ngữ</td>
                    <td>Nghe: <input class="input-css" type="text" name="uc_listen" placeholder="Bắt buộc điền" required></td>
                    <td>Nói: <input class="input-css" type="text" style="margin-left: 10px;" name="uc_speak" placeholder="Bắt buộc điền" required></td>
                    <td>Viết: <input class="input-css" type="text" style="margin-left: 39px;" name="uc_read" placeholder="Bắt buộc điền" required></td>
                </tr>
                <tr>
                    <td>Kỹ năng mềm</td>
                    <td colspan="4"> <input class="input-css" type="text" style="width: 100%" name="uc_kynangmem" placeholder="Không bắt buộc điền"></td>
                </tr>
            </table>
            <h3 style="margin-top: 60px !important;">III. Quá trình công tác</h3>
            <table class="table table-bordered table-bordered_ts">
                <tr class="tr_ts_1">
                    <td>Thời gian</td>
                    <td>Tên công ty và<br>lĩnh vực hoạt động</td>
                    <td>Chức danh</td>
                    <td>Nhiệm vụ chính</td>
                    <td>Thu nhập/<br>Tháng</td>
                </tr>
                <tr>
                    <td>
                        Từ: <input class="input-css" type="text" style="margin-left: 8px;" name="uc_qtct_timestart_1" placeholder="Không bắt buộc">
                        <p style="margin-top: 15px; margin-bottom: 0px;">Đến: <input class="input-css" type="text" name="uc_qtct_timesend_1" placeholder="Không bắt buộc"></p>
                    </td>
                    <td><input class="input-css" type="text" name="uc_qtct_namecity_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_qtct_chucdanh_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_qtct_nhiemvuchinh_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_thunhapthang_1" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr>
                    <td>
                        Từ: <input class="input-css" type="text" style="margin-left: 8px;" name="uc_qtct_timestart_2" placeholder="Không bắt buộc">
                        <p style="margin-top: 15px; margin-bottom: 0px;">Đến: <input class="input-css" type="text" name="uc_qtct_timesend_2" placeholder="Không bắt buộc"></p>
                    </td>
                    <td><input class="input-css" type="text" name="uc_qtct_namecity_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_qtct_chucdanh_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_qtct_nhiemvuchinh_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_thunhapthang_2" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr>
                    <td>
                        Từ: <input class="input-css" type="text" style="margin-left: 8px;" name="uc_qtct_timestart_3" placeholder="Không bắt buộc">
                        <p style="margin-top: 15px; margin-bottom: 0px;">Đến: <input class="input-css" type="text" name="uc_qtct_timesend_3" placeholder="Không bắt buộc"></p>
                    </td>
                    <td><input class="input-css" type="text" name="uc_qtct_namecity_3" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_qtct_chucdanh_3" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_qtct_nhiemvuchinh_3" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_thunhapthang_3" placeholder="Không bắt buộc điền"></td>
                </tr>
            </table>
            <h3>IV. Nguồn cung cấp thông tin tham khảo</h3>
            <table class="table table-bordered table-bordered_ts">
                <tr class="tr_ts_1">
                    <td>Họ và tên</td>
                    <td>Quan hệ</td>
                    <td>Chức danh</td>
                    <td>Tên công ty</td>
                    <td>Số ĐT liên hệ</td>
                </tr>
                <tr>
                    <td><input class="input-css" type="text" name="uc_tttk_name_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_quanhe_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_chucdanh_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_namect_1" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_mobile_1" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr>
                    <td><input class="input-css" type="text" name="uc_tttk_name_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_quanhe_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_chucdanh_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_namect_2" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_mobile_2" placeholder="Không bắt buộc điền"></td>
                </tr>
                <tr>
                    <td><input class="input-css" type="text" name="uc_tttk_name_3" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_quanhe_3" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_chucdanh_3" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_namect_3" placeholder="Không bắt buộc điền"></td>
                    <td><input class="input-css" type="text" name="uc_tttk_mobile_3" placeholder="Không bắt buộc điền"></td>
                </tr>
            </table>
            <h3>V. Trắc nghiệm</h3>
            <table class="table table-bordered table-bordered_ts">
                <tr class="tr_ts">
                    <td>STT</td>
                    <td colspan="2"><b>Câu hỏi </b> <i style="font-weight: normal;">[ Ứng viên lưu ý chỉ được chọn một phương án trả lời cho mỗi câu hỏi ]</i></td>
                </tr>
                @php 
                    $stt = 1;
                @endphp
                @foreach( $Tracnghiem as $value )
                    @if( $value->type_tn == 1 )
                        <tr>
                            <td class="text-center">{{ $stt++ }}</td>
                            <td colspan="2">
                                <p>{{ $value->question }}</p>
                                @foreach( $Tracnghiem_Option->getAllOptByTracNghiem($value->id) as $key => $option)
                                    <p>
                                        <label>
                                            <input type="hidden" name="id_question_{{ $value->id }}" value="{{ $value->id }}">
                                            <input class="input-css" type="radio" name="uc_{{ $value->id }}" value="{{ $option->answer }}" required> {{ $option->answer }}
                                        </label>
                                    </p>
                                @endforeach
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td class="text-center">{{ $stt++ }}</td>
                            <td colspan="2" style="vertical-align: top;">
                                <p>{{ $value->question }}</p>
                                <input type="hidden" name="id_question_{{ $value->id }}" value="{{ $value->id }}">
                                <textarea class="input-css" name="uc_{{ $value->id }}" placeholder="Không bắt buộc điền"></textarea>
                            </td>
                        </tr>
                    @endif
                @endforeach
                <tr>
                    <td class="text-center">{{ $stt++ }}</td>
                    <td colspan="2">
                        Nếu được nhận, bạn có mong muốn làm việc tại:
                        <p style="margin-top: 15px;">Tỉnh/TP: <input class="input-css" type="text" name="uc_tinhtp_job" required placeholder="Bắt buộc điền"></p>
                        <p style="margin-top: 15px;">Quận/Huyện: <input class="input-css" type="text" name="uc_quanhuyen_job" required placeholder="Bắt buộc điền"></p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <button type="reset">Xóa thông tin</button> 
                        <button type="submit">Gửi CV</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>