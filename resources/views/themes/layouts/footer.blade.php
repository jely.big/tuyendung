<footer>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <ul class="list-inline col-md-12">
                    <li class="col-md-3 col-sm-3 col-xs-12"> 
                        {!! $setting['fanpage_facebook'] ? $setting['fanpage_facebook'] : '' !!}
                    </li>
                    <li class="col-md-9 col-sm-9 col-xs-12">
                        <ul class="list-unstyled">
                            <li><b>Hotline:</b> {{ $setting['company_hotline'] ? $setting['company_hotline'] : '' }} || <b>Mail:</b> {{ $setting['company_email'] ? $setting['company_email'] : '' }} </li>
                            <li style="padding-top: 10px;"><b>Địa chỉ:</b> {{ $setting['company_address'] ? $setting['company_address'] : '' }}</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>