<div class="container-fluid nav_ts">
    <nav class="navbar navbar-expand-lg container"> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <!--span class="navbar-toggler-icon"></span-->Menu </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
			    {!! $menus->getListMenuByGroup('main_menu')  !!}
			</ul>
        </div>
    </nav>
</div>