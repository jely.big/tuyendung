@extends("themes.app")
@section('content')
<style type="text/css">
    .preview {
        display: grid;
        grid-template-columns: 200px 1fr;
        grid-gap: 1rem;
        transition: transform 200ms;
    }

    .thumb {
        position: relative;
        width: 100%;
        min-height: 120px;
        border-radius: 1.5rem;
        background-size: cover;
        background-position: center;
        border: 0.5rem solid white;
        box-sizing: border-box;
        box-shadow: 0.5rem 0.75rem 1rem -0.25rem rgba(0, 0, 0, 0.2);
    }

    .thumb::before {
        content: '';
        display: block;
        border-radius: 1rem;
        position: absolute;
        left: -1rem;
        top: -1rem;
        width: 100%;
        height: 100%;
        background: inherit;
        z-index: -2;
        transition: left 200ms, top 200ms;
    }

    .thumb::after {
        content: '';
        display: block;
        border-radius: calc(1rem - 1px);
        position: absolute;
        left: calc(-1rem + 1px);
        top: calc(-1rem + 1px);
        width: calc(100% - 2px);
        height: calc(100% - 2px);
        background: white;
        z-index: -1;
        transition: left 200ms, top 200ms;
    }

    .content-text h3 {
        font-family: sans-serif;
        margin: 0.5rem 0 0.5rem;
        font-weight: bold;
        font-size: 24px;
    }

    .content-text .tag {
        padding: 0.25rem 0.75rem;
        margin: 0 0.25rem;
        background: gray;
        color: white;
        font-family: sans-serif;
        font-weight: lighter;
        font-size: 0.75rem;
        text-transform: uppercase;
        letter-spacing: 1px;
        border-radius: 1rem;
    }

    .content-text .tag:first-child {
        margin-left: 0;
    }

    .content-text p {
        font-family: sans-serif;
        font-weight: lighter;
        margin: 0.75rem 0 0.25rem;
        letter-spacing: 0.0375rem;
        line-height: 1.5rem;
    }

    .content-text a {
        display: block;
        text-decoration: none;
        text-transform: uppercase;
        color: darkorange;
        font-family: sans-serif;
        font-weight: bold;
        font-size: 0.75rem;
        margin: 0.5rem 0 0;
        padding: 0.25rem 0.5rem;
        border: 1px solid darkorange;
        border-radius: 1rem;
        width: 10ch;
        line-height: 1rem;
        transition: background 200ms, color 200ms;
    }

    .text-content:hover {
        cursor: pointer;
    }

    .text-content:hover .preview {
        transform: translateX(0.25rem);
        transition: transform 200ms;
    }

    .text-content:hover .thumb::before {
        left: -1.25rem;
        top: -0.65rem;
        transition: left 200ms, top 200ms;
    }

    .text-content:hover .thumb::after {
        left: calc(-1.25rem + 1px);
        top: calc(-.65rem + 1px);
        transition: left 200ms, top 200ms;
    }

    .text-content:hover .content-text > a {
        background: darkorange;
        color: white;
        width: 10ch;
    }

    @media screen and (max-width: 540px) {
        .preview {
            grid-template-columns: 1fr;
            grid-template-rows: 2fr;
            grid-gap: 0;
        }
    }
</style>
	<!--NEWS-->
    <div class="col-md-12 search_ts news_content">
        <div class="static_content">
            @foreach($GetPost as $Post)
            <div class="text-content" style="margin-bottom: 20px;">
                <div class="preview">
                    <div style="background-image: url('{{ $Post->thumbnail }}')" class="thumb"></div>
                    <div class="content-text">
                        <h3>{{ $Post->title }}</h3>
                        <div>
                            <span class="tag">
                                @foreach($category->getCategoryByHomeAdmin($Post->categories) as $value)
                                    {{ $value['name'] }}
                                @endforeach
                            </span>
                            <span class="tag">{!!  $user->getNameById($Post->author)  !!}</span>
                        </div>
                        <p>{!! mb_substr(strip_tags($Post->content),1,200) !!}...</p>
                        <a href="/bai-viet/{{ $Post->id }}-{{ $Post->slug }}-{{ $Post->id }}">Xem thêm</a>
                    </div>
                </div>
            </div>
            @endforeach
            <div style="float: right;cursor: pointer;">
                {{ $GetPost->links() }}
            </div>
        </div> 
    </div>
    <script type="text/javascript">
        $(document).ready(function() { 
            $('#form-search').addClass('hidden');
        });
    </script>
@endsection