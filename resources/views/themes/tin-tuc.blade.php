@extends("themes.app")
@section('content')
	<!--NEWS-->
    <div class="col-md-12 search_ts news_content">
        <h3 class="text-center">{{ $Post->title }}</h3>
        <div class="static_content">
            {!! $Post->content !!}
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() { 
            $('#form-search').addClass('hidden');
        });
    </script>
@endsection