@extends("themes.app")
@section('banner')
    <div id="owl-demo" class="owl-carousel owl-theme">
    	@foreach($Slider->getItemByKey('HOME_SLIDE') as $img)
        <div class="item"><a href="/"><img src="{{ $img->image }}" /></a></div>
        @endforeach
    </div>
@endsection