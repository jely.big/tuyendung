<div class="col-md-12 search_ts" id="form-search">
    <h4 style="color: #fff;">Tìm việc làm</h4>
    <form method="POST" id="frm_search" class="frm_search">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-12"> <input type="text" class="search_text input-css" id="search_text" name="search_text" placeholder="Tìm việc...">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12"> 
                        <select class="search_job" id="search_job">
                            <option selected disabled>Ngành nghề</option>
                            @foreach($Nganhnghe->get_All() as $nganh)
                                <option value="{{ $nganh->id }}">{{ $nganh->name }}</option>
                            @endforeach                            
                        </select> </div>
                    <div class="col-md-6 col-sm-6 col-xs-12"> 
                        <select class="search_city" id="search_city">
                            <option selected disabled>Nơi làm việc</option>
                            @foreach($Diachi->get_All() as $noi)
                                <option value="{{ $noi->diachi_id }}">{{ $noi->diachi }}</option>
                            @endforeach  
                        </select> </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12"> <button type="submit">Tìm kiếm</button> </div>
        </div>
    </form>
</div>
<!-- <script type="text/javascript">
    $(function() { 
        $('#frm_search').on('submit', function(event)
        { 
            var job_id = $(".search_job").val(); 
            var city_id = $(".search_city").val(); 
            var search_text = $(".search_text").val();
            $.ajax({ 
                url: '{{ route('search_ajax') }}', 
                type: 'GET', 
                data: { 
                    'job_id': job_id, 'city_id': city_id, 'search_text': search_text 
                }, 
                success: function(data) 
                { 
                    if (data != "") { 
                        $("#show_data").html(data); 
                    } else { 
                        $("#show_data").html("<div>Không có kết quả tìm kiếm</div>"); 
                    } 
                } 
            });
        event.preventDefault(); }); 
    })
</script> -->