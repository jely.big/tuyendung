@extends("themes.app")
@section('content')
    <div class="col-md-12 table_content">
	    <table class="table table-striped">
	        <thead>
	            <tr>
	                <th>STT</th>
	                <th>Chức danh</th>
	                <th>Mức lương</th>
	                <th>Nơi làm việc</th>
	                <th>Ngày hết hạn</th>
	            </tr>
	        </thead>
	        <tbody id="show_data">
	        	@php
					$key = 1;
	        	@endphp
	        	@foreach( $Tuyendung->get_All() as $value )
	        		@php
						$diachi = $Diachi->getListDiaChiHome($value->td_address);
	        		@endphp
		            <tr>
		                <td>{{ $key++ }}</td>
		                <td><a href="/tin-tuc/{{ $value->td_slug }}/{{ $value->td_id }}">
		                	{{ $value->td_name }}
		                </a></td>
		                <td>{{ $value->td_salary }} triệu</td>
		                <td>{!! $diachi !!}</td>
		                <td>{{ $value->td_deadline }}</td>
		            </tr>
		        @endforeach
	        </tbody>
	    </table>
	</div>
	{{ $Tuyendung->get_All()->links() }}
@endsection