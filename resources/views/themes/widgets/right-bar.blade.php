<div class="news_ind col-md-12">
    <h4>Việc làm theo ngành</h4>
    <ul class="list-unstyled data_nganh">
        @foreach($Nganhnghe->get_All() as $nganh)
            <li><a href="/nganh-nghe/{{ $nganh->slug_nganh_name }}/{{ $nganh->id }}">{{ $nganh->name }}</a></li>
        @endforeach
    </ul>
</div>
<div class="news_ind col-md-12">
    <h4>Việc làm theo địa điểm</h4>
    <ul class="list-unstyled data_diachi">
        @foreach($Diachi->get_All() as $noi)
            <li><a href="/dia-diem/{{ $noi->diachi_slug_url }}/{{ $noi->diachi_id }}">{{ $noi->diachi }}</a></li>
        @endforeach
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(function() 
    {
        $('ul.data_nganh li').click(function(e) 
        { 
            var job_id = $(this).find("a").attr('data-href');
            $.ajax({ 
                url: '{{ route('search_ajax') }}', 
                type: 'GET', 
                data: { 
                    'job_id': job_id
                }, 
                success: function(data) 
                { 
                    if (data != "") { 
                        $("#show_data").html(data); 
                    } else { 
                        $("#show_data").html("<div>Không có kết quả tìm kiếm</div>"); 
                    } 
                } 
            });
        });
        $('ul.data_diachi li').click(function(e) 
        { 
            var city_id = $(this).find("a").attr('data-href');
            $.ajax({ 
                url: '{{ route('search_ajax') }}', 
                type: 'GET', 
                data: { 
                    'city_id': city_id
                }, 
                success: function(data) 
                { 
                    if (data != "") { 
                        $("#show_data").html(data); 
                    } else { 
                        $("#show_data").html("<div>Không có kết quả tìm kiếm</div>"); 
                    } 
                } 
            });
        });
    });
</script>