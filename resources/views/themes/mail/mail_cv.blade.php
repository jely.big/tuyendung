<div>
	<h3 style="color: #c29f75;">Chúc mừng bạn đã đăng ký thành công!</h3>
	<p style="display: block;white-space: pre-line;">Cảm ơn bạn đã quan tâm tới thông tin tuyển dụng của ANZEDO chúng tôi và gửi hồ sơ ứng tuyển.
	ANZEDO sẽ xem xét kĩ lưỡng hồ sơ và mời phỏng vấn những ứng viên phù hợp thông qua mail: <a href="mailto:tuyendung@anzedo.net">tuyendung@anzedo.net</a> hoặc <a href="mailto:job@anzedo.net">job@anzedo.net</a>. 
	Hy vọng chúng ta sẽ có dịp cộng tác làm việc cùng nhau và tạo dựng những giá trị tốt đẹp.</p>
</div>
<div style="float: right;">
	<h4>Trân trọng,</h4>
	<h4>BAN QUẢN TRỊ</h4>
</div>