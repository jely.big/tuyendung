@extends("themes.app")
@section('banner')
    <div id="owl-demo" class="owl-carousel owl-theme">
    	@foreach($Slider->getItemByKey('HOME_SLIDE') as $img)
        <div class="item"><a href="/"><img src="{{ $img->image }}" /></a></div>
        @endforeach
    </div>
@endsection
@section('content')
    <div class="col-md-12 table_content">
	    <table class="table table-striped">
	        <thead>
	            <tr>
	                <th>STT</th>
	                <th>Chức danh</th>
	                <th>Mức lương</th>
	                <th>Ngành/Địa điểm làm việc</th>
	                <th>Ngày hết hạn</th>
	            </tr>
	        </thead>
	        <tbody id="show_data">
	        	@if( $total_Tuyendung > 1 )
		        	@php
						$key = 1;
		        	@endphp
		        	@foreach( $get_Tuyendung as $value )
			            <tr>
			                <td>{{ $key++ }}</td>
			                <td><a href="/tin-tuc/{{ $value['td_slug'] }}/{{ $value['td_id'] }}">{{ $value['td_name'] }}</a></td>
			                <td>{{ $value['td_salary'] }} triệu</td>
			                <td>{{ $value['td_get'] }}</td>
			                <td>{{ $value['td_deadline'] }}</td>
			            </tr>
			        @endforeach
			    @else
		    		<tr><td colspan="5"><div>Không có kết quả tìm kiếm</div></td></tr>
			    @endif
	        </tbody>
	    </table>
	</div>
@endsection