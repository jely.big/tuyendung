<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CV</title>
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    <style type="text/css">
        * {
            font-family: DejaVu Sans;
        } 
        .css-cv{
            float: unset;
            width: 100%;
        }

        .select_jobDetail select {
            border: 1px dashed #000;
            padding: 5px;
            margin-left: 5px;
        }

        .table-bordered th,
        .table-bordered td {
            vertical-align: middle;
            text-align: center;
        }

        .table-bordered td h3 {
            text-transform: uppercase;
        }

        .table-bordered_ts td {
            text-align: left;
        }

        .tr_ts td {
            text-align: left;
            font-weight: bold;
            background: #2B2B2B;
        }

        .tr_ts_1 td {
            background: #2B2B2B;
            font-weight: bold;
            text-align: center;
        }

        .table-bordered_2 td {
            text-align: left;
        }

        .text-css{
            color: #fff;
        }        
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            {{ csrf_field() }}
            <table class="table table-bordered">
                <tr>
                    <td style="width: 20%;"><img src="{{ public_path('assets/logo/anzedo.jpg') }}" width="150"></td>
                    <td colspan="3">
                        <p style="font-size: 24px;font-weight: bold;">Phiếu phỏng vấn</p>
                    </td>
                    <td style="float: right;">
                       <img src="{{ public_path('file').'/'.$data['image'] }}" width="200" height="200">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: left; width: 55%">Vị trí ứng tuyển: {{ $data['candidate_position'] }}</td>
                    <td colspan="2" style="text-align: left;">Ngày có thể nhận việc: {{ $data['date_receive_job'] }}</td>
                </tr>
            </table>
            <h3>I. <span>Thông tin cá nhân</span></h3>
            <table class="table table-bordered table-bordered_ts">
                <tr>
                    <td colspan="2">Họ tên: {{ $data['fullname'] }}</td>
                    <td>Ngày sinh: {{ $data['birthday'] }}</td>
                    
                </tr>
                <tr>
                    <td>Nơi sinh: {{ $data['address_birthday'] }}</td>
                </tr>
                <tr>
                    <td colspan="2">Tình trạng hôn nhân: {{ $data['marital_status'] }}</td>
                    <td>Số con: {{ $data['number_children'] }}</td>
                </tr>
                <tr>
                    <td colspan="2">Điện thoại: {{ $data['mobile'] }}</td>
                    <td>Email: {{ $data['email'] }}</td>
                </tr>
                <tr>
                    <td colspan="3">Địa chỉ tạm trú: {{ $data['address'] }}</td>
                </tr>
            </table>
            <h3 style="margin-top: 60px !important;">II. <span>Quá trình học tập, đào tạo</span></h3>
            <table class="table table-bordered table-bordered_2">
                <tr class="tr_ts">
                    <td colspan="4" class="text-css">1. Trình độ học vấn:</td>
                </tr>
                <tr>
                    <td>Bằng cấp</td>
                    <td>Tên trường</td>
                    <td>Khoa/ngành</td>
                    <td>
                        Thời gian được đào tạo
                        <br />(Từ năm - Đến năm)
                    </td>
                </tr>
                <tr>
                    <td>{{ $data['bangcap1'] }}</td>
                    <td>{{ $data['tentruong1'] }}</td>
                    <td>{{ $data['khoanganh1'] }}</td>
                    <td>{{ $data['tgdt1'] }}</td>
                </tr>
                <tr>
                    <td>{{ $data['bangcap2'] }}</td>
                    <td>{{ $data['tentruong2'] }}</td>
                    <td>{{ $data['khoanganh2'] }}</td>
                    <td>{{ $data['tgdt2'] }}</td>
                </tr>
                <tr class="tr_ts">
                    <td colspan="4" class="text-css">2. Các khóa học đã tham dự:</td>
                </tr>
                <tr>
                    <td>Tên khóa học</td>
                    <td>Đơn vị tổ chức</td>
                    <td>Chứng chỉ (Xếp loại)</td>
                    <td>Thời gian được đào tạo</td>
                </tr>
                <tr>
                    <td>{{ $data['khoahoc1'] }}</td>
                    <td>{{ $data['dvtc1'] }}</td>
                    <td>{{ $data['cc1'] }}</td>
                    <td>{{ $data['tgkh1'] }}</td>
                </tr>
                <tr>
                    <td>{{ $data['khoahoc2'] }}</td>
                    <td>{{ $data['dvtc2'] }}</td>
                    <td>{{ $data['cc2'] }}</td>
                    <td>{{ $data['tgkh2'] }}</td>
                </tr>
                <tr class="tr_ts">
                    <td colspan="4" class="text-css">3. Kỹ năng:</td>
                </tr>
                <tr>
                    <td>Tin học</td>
                    <td>Word: {{ $data['word'] }}</td>
                    <td>Excel: {{ $data['excel'] }}</td>
                    <td>Powerpoint: {{ $data['powerpoint'] }}</td>
                </tr>
                <tr>
                    <td>Ngoại ngữ</td>
                    <td>Nghe: {{ $data['listening'] }}</td>
                    <td>Nói: {{ $data['speaking'] }}</td>
                    <td>Viết: {{ $data['writing'] }}</td>
                </tr>
                <tr>
                    <td>Kỹ năng mềm</td>
                    <td colspan="4">{{ $data['skill_soft'] }}</td>
                </tr>
            </table>
            <h3 style="margin-top: 60px !important;">III. <span>Quá trình công tác</span></h3>
            <table class="table table-bordered table-bordered_ts">
                <tr class="tr_ts_1">
                    <td class="text-css">Thời gian</td>
                    <td class="text-css">Tên công ty và<br>lĩnh vực hoạt động</td>
                    <td class="text-css">Chức danh</td>
                    <td class="text-css">Nhiệm vụ chính</td>
                    <td class="text-css">Thu nhập/<br>Tháng</td>
                </tr>
                <tr>
                    <td>
                        Từ: {{ $data['from_date1'] }}
                        <p style="margin-top: 15px; margin-bottom: 0px;">Đến: {{ $data['to_date1'] }}</p>
                    </td>
                    <td>{{ $data['company_1'] }}</td>
                    <td>{{ $data['title_1'] }}</td>
                    <td>{{ $data['working_1'] }}</td>
                    <td>{{ $data['salary_1'] }}</td>
                </tr>
                <tr>
                    <td>
                        Từ: {{ $data['from_date2'] }}
                        <p style="margin-top: 15px; margin-bottom: 0px;">Đến: {{ $data['to_date2'] }}</p>
                    </td>
                    <td>{{ $data['company_2'] }}</td>
                    <td>{{ $data['title_2'] }}</td>
                    <td>{{ $data['working_2'] }}</td>
                    <td>{{ $data['salary_2'] }}</td>
                </tr>
                <tr>
                    <td>
                        Từ: {{ $data['from_date3'] }}
                        <p style="margin-top: 15px; margin-bottom: 0px;">Đến: {{ $data['to_date3'] }}</p>
                    </td>
                    <td>{{ $data['company_3'] }}</td>
                    <td>{{ $data['title_3'] }}</td>
                    <td>{{ $data['working_3'] }}</td>
                    <td>{{ $data['salary_3'] }}</td>
                </tr>
            </table>
            <h3>IV. <span>Nguồn cung cấp thông tin tham <span>khảo</span></h3>
            <table class="table table-bordered table-bordered_ts">
                <tr class="tr_ts_1">
                    <td class="text-css">Họ và tên</td>
                    <td class="text-css">Quan hệ</td>
                    <td class="text-css">Chức danh</td>
                    <td class="text-css">Tên công ty</td>
                    <td class="text-css">Số ĐT liên hệ</td>
                </tr>
                <tr>
                    <td>{{ $data['fullname_1'] }}</td>
                    <td>{{ $data['relation_1'] }}</td>
                    <td>{{ $data['relation_title_1'] }}</td>
                    <td>{{ $data['relation_company_1'] }}</td>
                    <td>{{ $data['relation_phone_1'] }}</td>
                </tr>
                <tr>
                    <td>{{ $data['fullname_2'] }}</td>
                    <td>{{ $data['relation_2'] }}</td>
                    <td>{{ $data['relation_title_2'] }}</td>
                    <td>{{ $data['relation_company_2'] }}</td>
                    <td>{{ $data['relation_phone_2'] }}</td>
                </tr>
                <tr>
                    <td>{{ $data['fullname_3'] }}</td>
                    <td>{{ $data['relation_3'] }}</td>
                    <td>{{ $data['relation_title_3'] }}</td>
                    <td>{{ $data['relation_company_3'] }}</td>
                    <td>{{ $data['relation_phone_3'] }}</td>
                </tr>
            </table>
            <h3>V. <span>Trắc nghiệm</span></h3>
            <table class="table table-bordered table-bordered_ts">
                <tr class="tr_ts">
                    <td class="text-css">STT</td>
                    <td colspan="2" class="text-css" style="text-align: center;"><b style="font-weight: bold;">Câu hỏi </b></td>
                </tr>
                <tr>
                    <td class="text-center">1</td>
                    <td colspan="2">
                        <b>Nếu được nhận, bạn có mong muốn làm việc tại:</b>
                        <p style="margin-top: 15px;"><b>Tỉnh/TP:</b> {{ $data['new_city'] }}</p>
                        <p style="margin-top: 15px;"><b>Quận/Huyện:</b> {{ $data['ditricst'] }}</p>
                    </td>
                </tr>
                {!! $data['content'] !!}
            </table>
        </div>
    </div>
</body>
</html>