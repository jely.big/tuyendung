@extends("themes.app")
@section('content')
	<!--NEWS-->
	<div class="col-md-12 search_ts news_content">
	    <h3 class="text-center">LIÊN HỆ</h3>
	    <div class="static_content">
	        <p><strong>LIÊN HỆ PHÒNG NHÂN SỰ ANZEDO</strong></p>
	        <!-- <p><span style="font-size:14px;"><b>Văn phòng Hà Nội:&nbsp;</b></span></p>
	        <p><span style="font-size:14px;">Địa chỉ: Tầng 14, tòa nhà Hapulico 24T, 85&nbsp;Vũ Trọng Phụng, Thanh Xuân, Hà Nội</span></p>
	        <p><span style="font-size:14px;">Điện thoại: 043.204.2222 - Máy lẻ 103 / 107</span></p>
	        <p><span style="font-size:14px;">Email: tuyendung.ivymoda@gmail.com hoặc tuyendung@ivy.com.vn</span></p>
	        <p><span style="font-size:14px;"><b>Văn phòng Tp. HCM:&nbsp;</b></span></p>
	        <p><span style="font-size:14px;">Địa chỉ: Phòng 6, tầng 5, tòa nhà Crescent Plaza, 105 Tôn Dật Tiên, P.&nbsp;Tân Phú, Quận 7, TP. HCM</span></p> -->
	        <p><span style="font-size:14px;"><b>Trụ sở chính:&nbsp;</b></span></p>
	        <p><span style="font-size:14px;">Địa chỉ: {{ $setting['company_address'] ? $setting['company_address'] : '' }}</span></p>
	        <p><span style="font-size:14px;"><b>Văn phòng giao dịch:&nbsp;</b></span></p>
	        <p><span style="font-size:14px;">Địa chỉ: {{ $setting['company_address1'] ? $setting['company_address1'] : '' }}</span></p>
	        <p><span style="font-size:14px;">Điện thoại: {{ $setting['company_phone'] ? $setting['company_phone'] : '' }}</span></p>
	        <p><span style="font-size:14px;">Email: <a style="color: #0056b3;" href="mailto:{{ $setting['company_email'] ? $setting['company_email'] : '' }}">{{ $setting['company_email'] ? $setting['company_email'] : '' }}</a></span></p>
	        <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.873118049193!2d106.70424731428731!3d10.821020761329185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175291f910fb45b%3A0x4edeb80303553770!2sAnzedo%20Tower!5e0!3m2!1svi!2s!4v1572183052082!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe></p>
	        <style type="text/css">
	        iframe {
	            width: 100% !important;
	        }
	        </style>
	    </div>
	</div>
	<script type="text/javascript">
        $(document).ready(function() { 
            $('#form-search').addClass('hidden');
        });
    </script>
@endsection