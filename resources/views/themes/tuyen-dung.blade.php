@extends("themes.app")
@section('content')
	<!--NEWS-->
    <div class="col-md-12 search_ts news_content">
        <h3 class="text-center">{{ $Tuyendung->td_name }}</h3>
        <table class="table">
            <tbody>
                <tr>
                    <td>Nơi làm việc</td>
                    <td>
                        @foreach($GetDiaChi as $key => $value)
                            @if($key == 0)
                                {{ $value->name }}
                            @else
                                - {{ $value->name }}
                            @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Cấp bậc</td>
                    <td>{{ $Tuyendung->td_rank }}</td>
                </tr>
                <tr>
                    <td>Mức lương</td>
                    <td>{{ $Tuyendung->td_salary }}</td>
                </tr>
                <tr>
                    <td>Ngành nghề</td>
                    <td>
                        @foreach($GetNganhNghe as $key => $value)
                            @if($key == 0)
                                {{ $value->name }}
                            @else
                                - {{ $value->name }}
                            @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Phòng ban</td>
                    <td>{{ $Tuyendung->td_department }}</td>
                </tr>
                <tr>
                    <td>Thời gian thử việc</td>
                    <td>{{ $Tuyendung->td_test_day }} tháng</td>
                </tr>
                <tr>
                    <td>Hình thức làm việc</td>
                    <td>{{ $Tuyendung->td_form }}</td>
                </tr>
                <tr>
                    <td>Yêu cầu bằng cấp</td>
                    <td>{{ $Tuyendung->td_degree }}</td>
                </tr>
                <tr>
                    <td>Hạn nộp hồ sơ</td>
                    <td>{{ $Tuyendung->td_deadline }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-12 search_ts news_content">
        <div class="static_content">
            {!! $Tuyendung->td_content !!}
        </div> 
        <a href="{{ url('ung-tuyen/nop-cv.php') }}" class="bottonRegister">Đăng ký ngay</a>
        <div class="clearfix"></div>
        <style type="text/css">
        a.bottonRegister {
            background: #c29f75;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 5px 20px;
            font-weight: bold;
            float: right;
            color: #fff;
        }

        a.bottonRegister:hover {
            text-decoration: none;
            color: #b30000;
            -webkit-box-shadow: 2px 4px 10px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 2px 4px 10px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 2px 4px 10px 0px rgba(0, 0, 0, 0.75);
        }
        /* a {
            color: #0056b3 !important;
        } */
        </style>
    </div>
    <script type="text/javascript">
        $(document).ready(function() { 
            $('#form-search').addClass('hidden');
        });
    </script>
@endsection