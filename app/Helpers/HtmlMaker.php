<?php

namespace App\Helpers;

use App\Models\Category;
use App\User;

class HtmlMaker
{
    public static function labelFromJson($json, $type='')
    {
        $arr = json_decode($json);
        $html = '';
        foreach ($arr as $val)
        {
            switch ($type){
                case 'categories':
                    $val = Category::getCatNameFromId($val);
                    break;
                case 'author':
                    $val = User::getNameById($val);
                    break;
                default:
                    break;
            }
            $html .= '<span class="label label-success">'.$val.'</span> ';
        }
        return $html;
    }

    public static function label($content, $type='')
    {
        switch ($type){
            case 'status':
                $status  = ( $content == 1 ) ? 'success' : 'danger';
                $content = ( $content == 1 ) ? 'On' : 'Off';
                break;
            default:
                break;
        }
        return '<span class="label label-'.$status.'">'.$content.'</span> ';
    }
}