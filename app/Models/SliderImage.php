<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Slider;
class SliderImage extends Model
{
    use SoftDeletes;
	
	protected $table = 'sliderimages';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function getItemByKey($key)
	{
		$key_ID = Slider::where('key',$key)->first()->id;
		return self::where('key',$key_ID)->get();
	}

	public function getImgByKey($key)
	{
		$key_ID = Slider::where('key',$key)->first()->id;
		return self::where('key',$key_ID)->orderBy('id', 'DESC')->first();
	}
}
