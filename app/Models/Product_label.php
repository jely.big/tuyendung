<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_label extends Model
{
    use SoftDeletes;
	
	protected $table = 'product_labels';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function getTrademark()
    {
        return $this->whereNull('deleted_at')->get();
    }

    public function getProductTrademark($id)
    {
        $label = $this->where('id', $id)->whereNull('deleted_at')->first();
        return $label;
    }
}
