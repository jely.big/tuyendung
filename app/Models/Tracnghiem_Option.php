<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tracnghiem_Option extends Model
{
    use SoftDeletes;
	
	protected $table = 'tracnghiem_options';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	// protected $dates = ['deleted_at'];
	
	public function getOptByTracNghiem($id)
	{
		$options = $this->where('id', $id)->whereNull('deleted_at')->first();
		if( isset($options) ){
			return $options->answer;
		}else{
			return '';
		}
	}
	// Get all option by id_tracnghiem
	public function getAllOptByTracNghiem($id)
	{
		$options = $this->where('id_tracnghiem', $id)->whereNull('deleted_at')->get();
		if( isset($options) ){
			return $options;
		}else{
			return '';
		}
	}
}
