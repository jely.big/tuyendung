<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;
	
	protected $table = 'settings';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public static function getValue($key = '', $default){
        $set = self::where('key',$key)->first();
        if($set)
        {

            return $set->value;
        }
        if( is_array($default))
        {
            return reset($default);
        }
        return $default;
    }
}
