<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Re_Comment extends Model
{
    use SoftDeletes;
	
	protected $table = 're_comments';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function getReComment($id)
	{
		return $this->where('cmt_id', $id)->where('status', 1)->whereNull('deleted_at')->get();
	}
}
