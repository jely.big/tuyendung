<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nganhnghe extends Model
{
    use SoftDeletes;
	
	protected $table = 'nganhnghes';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function get_All()
	{
		$Nganhnghe = Nganhnghe::whereNull('deleted_at')->orderBy('id', 'ASC')->get();
		return $Nganhnghe;
	}

	public static function getListNganhNghe($catJson)
    {
        $catArr = json_decode($catJson);
        $catCurrent = Nganhnghe::findOrFail($catArr[0]);
        $cats       = Nganhnghe::get();
        $html       = '';
        if($cats)
        {
            foreach($cats as $cat)
            {
                $selected = ( in_array($cat->id,$catArr) ) ? 'selected= "selected"' : '';
                $html .= "<option value='".$cat->id."' ".$selected." >".$cat->name."</option>";
            }
        }
        return $html; 
    }

    public static function getListNganhNgheHome($catJson)
    {
        $catArr = json_decode($catJson);
        $html = '';
        foreach($catArr as $key => $value){
            $catCurrent = Nganhnghe::where('id', $value)->first();
            if($key == 0){
                $html .= $catCurrent->name;
            }else{
                $html .= ' - '.$catCurrent->name;
            }
        }
        return $html; 
    }

}
