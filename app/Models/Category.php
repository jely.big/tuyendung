<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

class Category extends Model
{
	protected $table = 'categories';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public static function getCatNameFromId($catId)
    {
        $cat = Category::find($catId);
        if($cat)
        {
            return $cat->name;
        }
        return [];
    }

    public function getCatNameByPosts($catId)
    {
        $cat = Category::where('id', 'like', '%"'.$catId.'"%')->first();
        if($cat)
        {
            return $cat;
        }
        return [];
    }

    public static function getCatNameByPost($catId)
    {
        $cat = Category::where('id', $catId)->first();
        return $cat;
    }

    public static function getCategorySubmenu()
    {
        $menus = Category::where('parent', 0)->where('status', 1)->whereNull('deleted_at')->orderBy('id','DESC')->get();
        return $menus;
    }

    /*public static function getListOption($parent = 0, $slas = '')
    {
        $html = '';
        $cats = Menulist::where('parent', $parent)->get();
        if($cats)
        {
            foreach($cats as $cat)
            {
                $html .= "<option value='".$cat->id."'>".$slas.' '.$cat->name."</option>";
                $html .= self::getListOption($cat->id, $slas.'---');
            }
        }

        return $html;
    }*/

    public static function getListOption($parent = 0, $slas = '')
    {
        $html = '';
        $cats = Category::where('parent', $parent)->get();
        if($cats)
        {
            foreach($cats as $cat)
            {
                $html .= "<option value='".$cat->id."'>".$slas.' '.$cat->name."</option>";
                $html .= Category::getListOption($cat->id, $slas.'---');
            }
        }

        return $html;
    }

    public static function getCatOptionSelected($catID, $parent=0, $slas='')
    {
        $catCurrent = Category::find($catID);
        $cats       = Category::where('parent', $parent)->get();
        $html       = '';
        if($cats)
        {
            foreach($cats as $cat)
            {
                $selected = ( $cat->id == $catID ) ? 'selected= "selected"' : '';
                $html .= "<option value='".$cat->id."' ".$selected." >".$slas.' '.$cat->name."</option>";
                $html .= Category::getCatOptionSelected($catID, $cat->id, $slas.'---');
            }
        }
        return $html;
    }
    public static function lsOptionByDefault($catID=0, $parent = 0, $slas = '')
    {

        $catCurrent = Category::findOrFail($catID);
        $cats       = Category::where('parent', $parent)->get();
        $html       = ( $parent == 0 ) ? '<option value="0">No Parent</option>':'';
        if($cats)
        {
            foreach($cats as $cat)
            {
                if( $catCurrent->id == $cat->id )
                {
                    continue;
                }
                $selected = ( $catCurrent->parent == $cat->id ) ? 'selected= "selected"' : '';
                $html .= "<option value='".$cat->id."' ".$selected." >".$slas.' '.$cat->name."</option>";
                $html .= Category::lsOptionByDefault($catID, $cat->id, $slas.'---');
            }
        }
        return $html;
    }

    public static function getListOptionByDefaultArr($catJson, $parent = 0, $slas = '')
    {
        $catArr = json_decode($catJson);
        $catCurrent = Category::findOrFail($catArr[0]);
        $cats       = Category::where('parent', $parent)->get();
        $html       = '';
        if($cats)
        {
            foreach($cats as $cat)
            {

                $selected = ( in_array($cat->id,$catArr) ) ? 'selected= "selected"' : '';
                $html .= "<option value='".$cat->id."' ".$selected." >".$slas.' '.$cat->name."</option>";
                $html .= Category::getListOptionByDefaultArr($catJson, $cat->id, $slas.'---');
            }
        }
        return $html;
    }

    public static function makeCatTable($parent = 0, $slas = '')
    {
        $html = '';
        $cats = Category::where('parent', $parent)->orderBy('name', 'ASC')->get();
        if($cats)
        {
            foreach($cats as $cat)
            {
                $html .= '<tr>';
                $html .= '<td>'.$cat->id.'</td>';
                $html .= '<td>'.$slas.' '.$cat->name.'</td>';
                $html .= '<td>'.$cat->slug.'</td>';

                if( $cat->status == 1)
                {
                    $html .= '<td><span class="label label-success">On</span></td>';
                }else{
                    $html .= '<td><span class="label label-danger">Off</span></td>';
                }

                $html .= '<td>';
                if(Module::hasAccess("Categories", "edit")) {
                    $html .= '<a href="' . url(config('laraadmin.adminRoute') . '/categories/' . $cat->id . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
                }
                if(Module::hasAccess("Categories", "delete")) {
                    $html .= Form::open(['route' => [config('laraadmin.adminRoute') . '.categories.destroy', $cat->id], 'method' => 'delete', 'style'=>'display:inline']);
                    $html .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
                    $html .= Form::close();
                }
                $html .= '</td>';
                $html .= '</tr>';
                $html .= Category::makeCatTable( $cat->id, $slas.'---');
            }
        }

        return $html;
    }
    // Lấy tin theo id của Category
    public static function getCategory($slug)
    {
        // $langId = Language::getIdFromKey($langKey);
        $posts = self::join('posts','posts.categories', '=', 'categories.id')->where('posts.categories',$id)->orderBy('posts.id', 'DESC')->first();
        // dd($posts);
        return $posts;
    }

    public static function getPostByCatSlug($slug)
    {
        // $langId = Language::getIdFromKey($langKey);
        $posts = self::join('posts','posts.categories', '=', 'categories.id')->where('posts.categories',$id)->orderBy('posts.id', 'DESC')->first();
        // dd($posts);
        return $posts;
    }

    public static function getCategoryByHomeAdmin($catJson)
    {
        $catArr = json_decode($catJson);
        $cats       = Category::where('parent', 0)->get();
        $html       = array();
        if($cats)
        {
            foreach($cats as $cat)
            {
                if( in_array($cat->id,$catArr) ){
                    $arr = array(
                        'name' => $cat->name, 
                    );
                    array_push($html, $arr);
                }
            }
        }
        return $html;
    }

}
