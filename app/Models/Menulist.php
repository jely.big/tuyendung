<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
// use App\Models\Language;

class Menulist extends Model
{
    use SoftDeletes;
	
	protected $table = 'menulists';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public static function getParents($menuGroupKey)
    {
        $group = Menugroup::where('key', $menuGroupKey)->where('status',1)->first();
        return self::where('menugroup', $group->id)->where('status',1)->orderBy('sort','ASC')->get();
    }
	public static function makeMenus($menuGroupKey)
    {
        $group = Menugroup::where('key', $menuGroupKey)->where('status',1)->first();
        return self::getListMenuByGroup($group->id);
    }
    public static function getLangMenuGroup($group)
    {
        $group = Menugroup::where('id', $group)->first();
        return Language::getKeyFromId($group->lang);
    }
    public static function getListMenuByGroup($groupKey, $parent=0, $slas='')
    {
        $menus = Menulist::where('menugroup', 1)->where('parent', $parent)->where('status', 1)->orderBy('sort', 'ASC')->get();
        $html  = '';
        if($menus)
        {
            foreach($menus as $menu)
            {
                $url = str_replace('{site_url}', url('/'), $menu->url);
                $url = url($url);
                $count = Menulist::where('parent', $menu->id)->count();
                if( $menu->parent == 0 ){
                    if( $menu->id == 1 ){
                        $dropdown = 'active';
                    }else{
                        $dropdown = 'dropdown';
                    }
                    if( $count > 0 ){
                        $href = '<a class="nav-link dropdown-toggle" href="'.$url.'" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$menu->name.'</a>';
                    }else{
                        $href = '<a class="nav-link" href="'.$url.'">'.$menu->name.'</a>';
                    }
                }else{
                    $dropdown = '';
                    $href = '<a class="nav-link" href="'.$url.'">'.$menu->name.'</a>';
                }
                $html .= '<li class="nav-item '.$dropdown.'" >'.$href.'</l>';
                if( $count > 0 )
                {
                    $html .= '<div class="dropdown-menu" aria-labelledby="navbarDropdown">';
                    $get_sub_menu = Menulist::where('parent', $menu->id)->get();
                    foreach( $get_sub_menu as $sub_menu ){
                        if( $sub_menu->id == 8 ){
                            $html .= '<a class="dropdown-item" href="'.$sub_menu->url.'">'.$sub_menu->name.'</a>';
                        }else{
                            $html .= '<a class="dropdown-item" href="/'.$sub_menu->url.'">'.$sub_menu->name.'</a>';
                        }
                    }
                    $html .= '</div>';                    
                }
            }
        }
        return $html;
    }

    public static function getProductSubmenu()
    {
        $menus = Menulist::where('parent', 15)->orderBy('sort','ASC')->get();
        return $menus;
    }

    public static function getProductmenu()
    {
        $menus = Menulist::where('parent',0)->orderBy('sort','ASC')->get();
        return $menus;
    }

    public static function getSubMenu($id)
    {
        $menus = Menulist::where('parent',$id)->orderBy('sort','ASC')->get();
        return $menus;
    }

    public static function contSubMenu($id)
    {
        return Menulist::where('parent',$id)->count();
    }
}
