<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Diachi extends Model
{
    use SoftDeletes;
	
	protected $table = 'diachis';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function get_All()
	{
		$Diachi = DB::table('diachis as t1')
				->join('khuvucs as t2', 't2.id', '=', 't1.parent')
				->select(
					't1.id as diachi_id',
					't1.slug_url as diachi_slug_url',
					't1.name as diachi'
				)
				->whereNull('t1.deleted_at')
				->orderBy('t2.id', 'ASC')
				->get();
		return $Diachi;
	}

	public static function getListDiaChi($catJson)
    {
        $catArr = json_decode($catJson);
        $catCurrent = Diachi::findOrFail($catArr[0]);
        $cats       = Diachi::get();
        $html       = '';
        if($cats)
        {
            foreach($cats as $cat)
            {
                $selected = ( in_array($cat->id,$catArr) ) ? 'selected= "selected"' : '';
                $html .= "<option value='".$cat->id."' ".$selected." >".$cat->name."</option>";
            }
        }
        return $html; 
    }

    public static function getListDiaChiHome($catJson)
    {
        $catArr = json_decode($catJson);
        $html = '';
        foreach($catArr as $key => $value){
        	$catCurrent = Diachi::where('id', $value)->first();
        	if($key == 0){
            	$html .= $catCurrent->name;
            }else{
            	$html .= ' - '.$catCurrent->name;
            }
        }
        return $html; 
    }

}
