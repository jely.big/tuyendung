<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\User;

class Post extends Model
{
    use SoftDeletes;
	
	protected $table = 'posts';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];
	// protected $dates = ['deleted_at'];

    // Lấy tin đầu tiên cho div to ở home
    public static function PostDauTien()
    {
        // $langId = Language::getIdFromKey($langKey);
        $posts = self::join('users','posts.author', '=', 'users.id')->select('*','posts.id as Pid','users.id as Uid')->where('status',1)->whereNull('posts.deleted_at')->orderBy('posts.id', 'DESC')->first();
        // dd($posts);
        return $posts;
    }

    // Lấy 3 tin tiếp theo cho div nhỏ ở home
	public static function PostKhacDauTien($limit)
    {        
        $postDauTien = self::PostDauTien()->Pid;               
        $posts = self::join('users','posts.author', '=', 'users.id')->select('*','posts.id as Pid','users.id as Uid')->where('status',1)->where('posts.id','<>',$postDauTien)->whereNull('posts.deleted_at')->orderBy('Pid', 'DESC')->limit($limit)->get();
        return $posts;
    }
    
    // Lấy full tin cho home->blogs
    public static function postBlog()
    {
        $posts = self::join('users','posts.author', '=', 'users.id')->where('posts.status',1)->whereNull('posts.deleted_at')->select('*', 'posts.created_at as postsCreate')->orderBy('posts.id', 'DESC')->paginate(4);
        return $posts;
    }

    // Lấy 6 tin mới
    public static function postBlogLimit($limit)
    {
        $posts = self::join('users','posts.author', '=', 'users.id')->where('posts.status',1)->whereNull('posts.deleted_at')->orderBy('posts.id', 'DESC')->limit($limit)->get();
        return $posts;
    }

    public static  function getCatName($catId)
    {
        $cat = Category::select('id','name')->where('id',$catId)->first();
        if($cat == ''){
            $cat = '';
        }else{
            return $cat->name;
        }
    }

    public static  function getUserName($userId)
    {
        $getUser = User::where('id',1)->first();
        return $getUser->name;
    }

    
}
