<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\Diachi;
use App\Models\Nganhnghe;

class Tuyendung extends Model
{
    use SoftDeletes;
	
	protected $table = 'tuyendungs';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function get_All()
	{
		$Tuyendung = DB::table('tuyendungs as t1')
					->select(
						't1.id as td_id',
						't1.slug as td_slug',
						't1.name as td_name',
						't1.salary as td_salary',
						't1.address as td_address',
						't1.career as td_career',
						DB::raw('DATE_FORMAT(t1.deadline, "%d-%m-%Y") as td_deadline')
					)
					->whereNull('t1.deleted_at')
					->orderBy('t1.id', 'DESC')
					->paginate(10);
		return $Tuyendung;
	}
}
