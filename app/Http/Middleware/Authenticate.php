<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\EntrustFacade as Entrust;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        if(Auth::check()) {
            if(!isset($_SESSION)) 
            { 
                session_start();
            }
            if( Auth::user()->can('ADMIN_PANEL') )
            {
                $_SESSION['ckfinder_enabled'] = 'enabled';
            }else{
                $_SESSION['ckfinder_enabled'] = 'disable';
            }
            //session_destroy();
       }
        return $next($request);
    }

}
