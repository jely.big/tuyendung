<?php
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}
Route::pattern('id','([0-9]*)');
Route::pattern('slug','(.*)');
	
Route::get('/', 'FrontendController@index');
// View
Route::get('search_ajax', 'FrontendController@Get_Data')->name('search_ajax');
// Search theo ngành 
Route::get('nganh-nghe/{slug}/{id}', 'FrontendController@Get_NganhNghe');
// Search theo địa điểm 
Route::get('dia-diem/{slug}/{id}', 'FrontendController@Get_DiaDiem');
// Thông tin menu
Route::get('thong-tin/{slug}', 'FrontendController@Links');
// Chi tiết tuyển dụng
Route::get('tin-tuc/{slug}/{id}', 'FrontendController@Get_Blog');
// CV
Route::get('ung-tuyen/nop-cv.php', 'FrontendController@Get_CV');
// Gửi CV
Route::post('ajax_submit', 'FrontendController@Post_CV')->name('ajax_submit');
// Tin tức
Route::get('tin-tuc/{slug}', 'FrontendController@Get_Blogs');
Route::get('bai-viet/{id}-{slug}-{id1}', 'FrontendController@Detail_Blog');
/* ================== Admin Routes ================== */
require __DIR__.'/admin_routes.php';