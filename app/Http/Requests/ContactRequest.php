<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'email' =>  'required|email',
            'phone' =>  'required|min:10|max:11',
            'content' =>  'required|min:10'
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Vui lòng nhập họ tên',
            'name.min' => 'Vui lòng nhập chính xác họ tên | họ tên phải lớn hơn 2 ký tự',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Vui lòng nhập email đúng định dạng',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.min' => 'Vui lòng nhập số điện thoại tối thiểu 10 số',
            'phone.max' => 'Vui lòng nhập số điện thoại tối đa 11 số',
            'content.min' => 'Vui lòng nhập nội dung ít nhất 10 ký tự',
            'content.required' => 'Vui lòng nhập nội dung'
        ];
    }
}
