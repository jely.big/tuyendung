<?php

Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	Route::get(config('laraadmin.adminRoute').'/media-management', 'LA\MediaManagerController@index');
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/users/{id}/delete', 'LA\UsersController@destroy');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');






	/* ================== Contacts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/contacts', 'LA\ContactsController');
	Route::get(config('laraadmin.adminRoute') . '/contact_dt_ajax', 'LA\ContactsController@dtajax');

	/* ================== Gallerytags ================== */
	Route::resource(config('laraadmin.adminRoute') . '/gallerytags', 'LA\GallerytagsController');
	Route::get(config('laraadmin.adminRoute') . '/gallerytag_dt_ajax', 'LA\GallerytagsController@dtajax');



	/* ================== Categories ================== */
	Route::resource(config('laraadmin.adminRoute') . '/categories', 'LA\CategoriesController');
	Route::get(config('laraadmin.adminRoute') . '/category_dt_ajax', 'LA\CategoriesController@dtajax');

	/* ================== Posts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/posts', 'LA\PostsController');
	Route::get(config('laraadmin.adminRoute') . '/post_dt_ajax', 'LA\PostsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/post_add_changeLang_ajax', 'LA\PostsController@createChangeLangAjax');

	/* ================== Settings ================== */
	Route::get(config('laraadmin.adminRoute') . '/settings', 'LA\SettingsController@index');
	Route::post(config('laraadmin.adminRoute') . '/settings', 'LA\SettingsController@update');
	Route::get(config('laraadmin.adminRoute') . '/setting_dt_ajax', 'LA\SettingsController@dtajax');


	/* ================== Menugroups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/menugroups', 'LA\MenugroupsController');
	Route::get(config('laraadmin.adminRoute') . '/menugroup_dt_ajax', 'LA\MenugroupsController@dtajax');



	/* ================== Menulists ================== */
	Route::resource(config('laraadmin.adminRoute') . '/menulists', 'LA\MenulistsController');
	Route::get(config('laraadmin.adminRoute') . '/menulist_dt_ajax', 'LA\MenulistsController@dtajax');

	/* ================== Pages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/pages', 'LA\PagesController');
	Route::get(config('laraadmin.adminRoute') . '/page_dt_ajax', 'LA\PagesController@dtajax');

	/* ================== Contacts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/contacts', 'LA\ContactsController');
	Route::get(config('laraadmin.adminRoute') . '/contact_dt_ajax', 'LA\ContactsController@dtajax');

	/* ================== Sliders ================== */
	Route::resource(config('laraadmin.adminRoute') . '/sliders', 'LA\SlidersController');
	Route::get(config('laraadmin.adminRoute') . '/slider_dt_ajax', 'LA\SlidersController@dtajax');

	/* ================== SliderImages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/sliderimages', 'LA\SliderImagesController');
	Route::get(config('laraadmin.adminRoute') . '/sliderimage_dt_ajax', 'LA\SliderImagesController@dtajax');












	/* ================== Mail_posts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/mail_posts', 'LA\Mail_postsController');
	Route::get(config('laraadmin.adminRoute') . '/mail_post_dt_ajax', 'LA\Mail_postsController@dtajax');




	/* ================== Partners ================== */
	Route::resource(config('laraadmin.adminRoute') . '/partners', 'LA\PartnersController');
	Route::get(config('laraadmin.adminRoute') . '/partner_dt_ajax', 'LA\PartnersController@dtajax');



	/* ================== Product_labels ================== */
	Route::resource(config('laraadmin.adminRoute') . '/product_labels', 'LA\Product_labelsController');
	Route::get(config('laraadmin.adminRoute') . '/product_label_dt_ajax', 'LA\Product_labelsController@dtajax');


	/* ================== Comments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/comments', 'LA\CommentsController');
	Route::get(config('laraadmin.adminRoute') . '/comment_dt_ajax', 'LA\CommentsController@dtajax');

	/* ================== Re_Comments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/re_comments', 'LA\Re_CommentsController');
	Route::get(config('laraadmin.adminRoute') . '/re_comment_dt_ajax', 'LA\Re_CommentsController@dtajax');

	/* ================== Diachis ================== */
	Route::resource(config('laraadmin.adminRoute') . '/diachis', 'LA\DiachisController');
	Route::get(config('laraadmin.adminRoute') . '/diachi_dt_ajax', 'LA\DiachisController@dtajax');

	/* ================== Nganhnghes ================== */
	Route::resource(config('laraadmin.adminRoute') . '/nganhnghes', 'LA\NganhnghesController');
	Route::get(config('laraadmin.adminRoute') . '/nganhnghe_dt_ajax', 'LA\NganhnghesController@dtajax');

	/* ================== Khuvucs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/khuvucs', 'LA\KhuvucsController');
	Route::get(config('laraadmin.adminRoute') . '/khuvuc_dt_ajax', 'LA\KhuvucsController@dtajax');

	/* ================== Tuyendungs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/tuyendungs', 'LA\TuyendungsController');
	Route::get(config('laraadmin.adminRoute') . '/tuyendung_dt_ajax', 'LA\TuyendungsController@dtajax');

	/* ================== Tracnghiem_Types ================== */
	Route::resource(config('laraadmin.adminRoute') . '/tracnghiem_types', 'LA\Tracnghiem_TypesController');
	Route::get(config('laraadmin.adminRoute') . '/tracnghiem_type_dt_ajax', 'LA\Tracnghiem_TypesController@dtajax');

	/* ================== Tracnghiems ================== */
	Route::resource(config('laraadmin.adminRoute') . '/tracnghiems', 'LA\TracnghiemsController');
	Route::get(config('laraadmin.adminRoute') . '/tracnghiem_dt_ajax', 'LA\TracnghiemsController@dtajax');

	/* ================== Tracnghiem_Options ================== */
	Route::resource(config('laraadmin.adminRoute') . '/tracnghiem_options', 'LA\Tracnghiem_OptionsController');
	Route::get(config('laraadmin.adminRoute') . '/tracnghiem_option_dt_ajax', 'LA\Tracnghiem_OptionsController@dtajax');

	/* ================== Detail_CVs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/detail_cvs', 'LA\Detail_CVsController');
	Route::get(config('laraadmin.adminRoute') . '/detail_cv_dt_ajax', 'LA\Detail_CVsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/download_cv', 'LA\Detail_CVsController@download_CV');
});
