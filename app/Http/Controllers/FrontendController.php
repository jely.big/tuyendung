<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Language;
use App\Models\Menulist;
use App\Models\Translation;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use View;
use DB;
use Lang;
use Session;
use Mail;
use Auth;
use Exception;
use Datatables;
use PDF;
use App\Models\Slider;
use App\Models\SliderImage;
use App\Models\Post;
use App\Models\Diachi;
use App\Models\Khuvuc;
use App\Models\Nganhnghe;
use App\Models\Tuyendung;
use App\Models\Tracnghiem;
use App\Models\Tracnghiem_Option;
use App\Models\Tracnghiem_Type;
use App\Models\Detail_CV;
use App\Models\Category;

class FrontendController extends Controller
{
    public function __construct(Request $request)
    {
        $this->makeSetting();
        $this->makeMenu();

        $Slider = new SliderImage;
        View::share('Slider', $Slider);
        $Diachi = new Diachi;
        View::share('Diachi', $Diachi);
        $Nganhnghe = new Nganhnghe;
        View::share('Nganhnghe', $Nganhnghe);
        $Tuyendung = new Tuyendung;
        View::share('Tuyendung', $Tuyendung);
        $Tracnghiem_Option = new Tracnghiem_Option;
        View::share('Tracnghiem_Option', $Tracnghiem_Option);
    }
    public function index()
    {
        return view('themes.home',[
            'pagetitle' => 'Anzedo Tuyển dụng'
        ]);
    }
    private function makeSetting()
    {
        $settings = DB::table('settings')->get();
        foreach($settings as $set)
        {
            $setting[$set->key] = $set->value;
        }
        View::share('setting', $setting);
    }
    private function makeMenu()
    {
        $menus = new Menulist;
        $menuProducts = Menulist::getProductSubmenu();
        $menudad = Menulist::getProductmenu();
        View::share('menus', $menus);
        View::share('menuProducts', $menuProducts);
        View::share('menudad', $menudad);
    }
    // Menu
    public function Links(Request $request, $slug)
    {
        $Post = Post::where('slug', $slug)->first();
        if( $Post ){
            if( $slug == 'lien-he'){
                return view('themes.lien-he', [
                    'pagetitle' =>  'Liên hệ - Anzedo Tuyển dụng'
                ]);
            }else{
                return view('themes.tin-tuc', [
                    'Post'      =>  $Post,
                    'pagetitle' =>  $Post->title
                ]);
            }
        }else{
            return view('themes.errors.404', [
                'pagetitle' =>  'Lỗi'
            ]);
        } 
    }
    // Chi tiết tuyển dụng
    public function Get_Blog(Request $request, $slug, $id)
    {
        $Tuyendung = DB::table('tuyendungs as t1')
                    ->where('t1.id', $id)
                    ->select(
                        't1.id as td_id',
                        't1.slug as td_slug',
                        't1.address as td_address',
                        't1.career as td_career',
                        't1.name as td_name',
                        't1.salary as td_salary',
                        't1.content as td_content',
                        't1.rank as td_rank',
                        't1.salary as td_salary',
                        't1.department as td_department',
                        't1.form as td_form',
                        't1.degree as td_degree',
                        't1.test_day as td_test_day',
                        DB::raw('DATE_FORMAT(t1.deadline, "%d-%m-%Y") as td_deadline')
                    )
                    ->first();
        $catArr_address = json_decode($Tuyendung->td_address);
        $catArr_career = json_decode($Tuyendung->td_career);
        $Diachi = Diachi::whereIn('id', $catArr_address)->get();
        $Nganhnghe = Nganhnghe::whereIn('id', $catArr_career)->get();
        return view('themes.tuyen-dung', [
            'pagetitle' =>  $Tuyendung->td_name,
            'Tuyendung' =>  $Tuyendung,
            'GetDiaChi'    =>  $Diachi,
            'GetNganhNghe' =>  $Nganhnghe
        ]);
    }
    // CV
    public function Get_CV(Request $request)
    {
        $Tracnghiem = DB::table('tracnghiems as t1')
                    ->orderBy('type_tn', 'ASC')
                    ->get();
        $arr_Option = array();
        foreach($Tracnghiem as $value){
            $options = Tracnghiem_Option::where('id_tracnghiem', $value->id)->get();
            foreach($options as $option){
                $arr = [
                    'id_tracnghiem' => $option->id_tracnghiem,
                    'answer'        => $option->answer
                ];
                array_push($arr_Option, $arr);
            }
        }
        return view('themes.cv-home', [
            'pagetitle'     =>  'Anzedo Tuyển dụng',
            'Tracnghiem'    =>  $Tracnghiem,
            'arr_Option'    =>  $arr_Option
        ]);
    }
    // Ajax search home
    public function Get_Data(Request $request)
    {        
        $request->search_text ? $search_text = "AND t1.name like '%".$request->search_text."%'" : $search_text = '';
        $cond = '';
        if( $request->search_text ) { $cond .= $search_text; }
        $query_Tuyendung = "SELECT t1.id as td_id, 
                                t1.slug as td_slug,
                                t1.name as td_name,
                                t1.salary as td_salary,
                                t1.address as td_address,
                                t1.career as td_career,
                                DATE_FORMAT(t1.deadline, '%d-%m-%Y') as td_deadline
                            FROM tuyendungs t1
                            WHERE t1.deleted_at is null $cond
                            ORDER BY t1.id DESC
                            LIMIT 30";
        $Tuyendung = DB::select($query_Tuyendung);
        $html = '';
        $key = 1;
        $diachi = '';
        foreach( $Tuyendung as $value ){
            $catArr_address = json_decode($value->td_address);
            $catArr_career = json_decode($value->td_career);
            if($request->search_text){
                $Diachi = Diachi::whereIn('id', $catArr_address)->first();
                $diachi = $Diachi->name;
                $html .= '<tr>
                            <td>'.$key++.'</td>
                            <td><a href="/tin-tuc/'.$value->td_slug.'/'.$value->td_id.'">'.$value->td_name.'</a></td>
                            <td>'.$value->td_salary.' triệu</td>
                            <td>'.$diachi.'</td>
                            <td>'.$value->td_deadline.'</td>
                        </tr>';
            }
            if( $request->city_id && in_array($request->city_id, $catArr_address, true) ){
                $diachi = Diachi::where('id', $request->city_id)->first();
                $html .= '<tr>
                            <td>'.$key++.'</td>
                            <td><a href="/tin-tuc/'.$value->td_slug.'/'.$value->td_id.'">'.$value->td_name.'</a></td>
                            <td>'.$value->td_salary.' triệu</td>
                            <td>'.$diachi->name.'</td>
                            <td>'.$value->td_deadline.'</td>
                        </tr>';
            }
            if( $request->job_id && in_array($request->job_id, $catArr_career, true) ){
                $nganhnghe = Nganhnghe::where('id', $request->job_id)->first();
                $html .= '<tr>
                            <td>'.$key++.'</td>
                            <td><a href="/tin-tuc/'.$value->td_slug.'/'.$value->td_id.'">'.$value->td_name.'</a></td>
                            <td>'.$value->td_salary.' triệu</td>
                            <td>'.$nganhnghe->name.'</td>
                            <td>'.$value->td_deadline.'</td>
                        </tr>';
            }
        }
        return $html;
    }
    // Ajax search home ngành
    public function Get_NganhNghe(Request $request, $slug, $id)
    {        
        $get_Tuyendung = DB::table('tuyendungs as t1')
                    ->select(
                        't1.id as td_id',
                        't1.slug as td_slug',
                        't1.name as td_name',
                        't1.salary as td_salary',
                        't1.address as td_address',
                        't1.career as td_career',
                        DB::raw('DATE_FORMAT(t1.deadline, "%d-%m-%Y") as td_deadline')
                    )
                    ->orderBy('t1.id', 'DESC')
                    ->paginate(30);
        $arr_tuyendung = array();
        foreach($get_Tuyendung as $key => $value){
            $catArr_career = json_decode($value->td_career);
            if( in_array($id, $catArr_career, true) ){
                $Nganhnghe = Nganhnghe::where('id', $id)->first();
                $arr = array(
                    'td_id'         => $value->td_id,
                    'td_slug'       => $value->td_slug,
                    'td_name'       => $value->td_name,
                    'td_salary'     => $value->td_salary,
                    'td_address'    => $value->td_address,
                    'td_get'        => $Nganhnghe->name,
                    'td_deadline'   => $value->td_deadline
                );
                array_push($arr_tuyendung, $arr);
            }
        }
        $total_Tuyendung = count($arr_tuyendung);
        return view('themes.search-right', [
            'pagetitle'         =>  'Anzedo Tuyển dụng',
            'get_Tuyendung'     =>  $arr_tuyendung,
            'total_Tuyendung'   =>  $total_Tuyendung
        ]);
    }
    // Ajax search home địa điểm
    public function Get_DiaDiem(Request $request, $slug, $id)
    {        
        $get_Tuyendung = DB::table('tuyendungs as t1')
                    ->select(
                        't1.id as td_id',
                        't1.slug as td_slug',
                        't1.name as td_name',
                        't1.salary as td_salary',
                        't1.address as td_address',
                        't1.career as td_career',
                        DB::raw('DATE_FORMAT(t1.deadline, "%d-%m-%Y") as td_deadline')
                    )
                    ->orderBy('t1.id', 'DESC')
                    ->paginate(30);
        $arr_tuyendung = array();
        foreach($get_Tuyendung as $key => $value){
            $catArr_address = json_decode($value->td_address);
            if( in_array($id, $catArr_address, true) ){
                $Diachi = Diachi::where('id', $id)->first();
                $arr = array(
                    'td_id'         => $value->td_id,
                    'td_slug'       => $value->td_slug,
                    'td_name'       => $value->td_name,
                    'td_salary'     => $value->td_salary,
                    'td_address'    => $value->td_address,
                    'td_get'        => $Diachi->name,
                    'td_deadline'   => $value->td_deadline
                );
                array_push($arr_tuyendung, $arr);
            }
        }
        $total_Tuyendung = count($arr_tuyendung);
        return view('themes.search-right', [
            'pagetitle'         =>  'Anzedo Tuyển dụng',
            'get_Tuyendung'     =>  $arr_tuyendung,
            'total_Tuyendung'   =>  $total_Tuyendung
        ]);
    }
    private function slug_url( string $str )
    {
        //Kiểm tra xem dữ liệu truyền vào có phải là một chuỗi hay không
        if( is_string( $str ) ){
            // Chuyển đổi toàn bộ chuỗi sang chữ thường
            $str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8"); 
            //Tạo mảng chứa key và chuỗi regex cần so sánh
            $unicode = [
                'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
                'd' => 'đ',
                'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
                'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
                'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
                'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
                'i' => 'í|ì|ỉ|ĩ|ị',
                '-' => '\+|\*|\/|\&|\!| |\^|\%|\$|\#|\@',
            ];
            foreach ( $unicode as $key => $value )
            {
                //So sánh và thay thế bằng hàm preg_replace
                
                $str = preg_replace("/($value)/", $key, $str);
            }
            //Trả về kết quả
            return $str;
        } 
        //Nếu Dữ liệu không phải kiểu string thì trả về null
        return null;
    }
    // Gửi CV
    public function Post_CV(Request $request)
    {
        $data = $this->validate(request(), [
            'uc_chucdanh'           => 'required',
            'uc_date_job'           => 'required',
            'uc_avatar'             => 'required',
            'uc_fullname'           => 'required',
            'uc_birthday'           => 'required',
            'uc_address'            => 'required',
            'uc_marries'            => 'required',
            'uc_totalsun'           => 'required',
            'uc_mobile'             => 'required',
            'uc_email'              => 'required',
            'uc_address_tamtru'     => 'required',
            'uc_word'               => 'required',
            'uc_excel'              => 'required',
            'uc_powerpoint'         => 'required',
            'uc_listen'             => 'required',
            'uc_speak'              => 'required',
            'uc_read'               => 'required',
            'uc_tinhtp_job'         => 'required',
            'uc_quanhuyen_job'      => 'required'
        ]);
        $Tracnghiem = DB::table('tracnghiems as t1')
                    ->orderBy('type_tn', 'ASC')
                    ->get();
        $arr_Question = array();
        $content_answer = '';
        $content = '';
        $key = 2;
        foreach($Tracnghiem as $value){
            $uc_answer = 'uc_'.$value->id;
            $content_answer = $request->$uc_answer;
            $content .= '<tr>
                            <td class="text-center">'.$key++.'</td>
                            <td colspan="2">
                                <p><b>'.$value->question.'</b></p>
                                <p>
                                    <span>
                                        '.$content_answer.'
                                    </span>
                                </p>
                            </td>
                        </tr>';
        }
        if($request->hasfile('uc_avatar'))
        {
            $image = $request->file('uc_avatar');
            $filename = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('file/'), $filename);
        }else{
            $filename = 0;
        }

        $candidate_position     = strip_tags($request->uc_chucdanh);
        $date_receive_job       = strip_tags($request->uc_date_job);
        $fullname               = strip_tags($request->uc_fullname);
        $birthday               = strip_tags($request->uc_birthday);
        $address_birthday       = strip_tags($request->uc_address);
        $marital_status         = strip_tags($request->uc_marries);
        $number_children        = strip_tags($request->uc_totalsun);
        $mobile                 = strip_tags($request->uc_mobile);
        $email                  = strip_tags($request->uc_email);
        $address                = strip_tags($request->uc_address_tamtru);
        $bangcap1               = strip_tags($request->uc_bangcap_1);
        $tentruong1             = strip_tags($request->uc_tentruong_1);
        $khoanganh1             = strip_tags($request->uc_khoanganh_1);
        $tgdt1                  = strip_tags($request->uc_time_daotao_1);
        $bangcap2               = strip_tags($request->uc_bangcap_2);
        $tentruong2             = strip_tags($request->uc_tentruong_2);
        $khoanganh2             = strip_tags($request->uc_khoanganh_2);
        $tgdt2                  = strip_tags($request->uc_time_daotao_2);
        $khoahoc1               = strip_tags($request->uc_tenkhoahoc_1);
        $dvtc1                  = strip_tags($request->uc_donvitochuc_1);
        $cc1                    = strip_tags($request->uc_chungchi_1);
        $tgkh1                  = strip_tags($request->uc_tgdt_1);
        $khoahoc2               = strip_tags($request->uc_tenkhoahoc_2);
        $dvtc2                  = strip_tags($request->uc_donvitochuc_2);
        $cc2                    = strip_tags($request->uc_chungchi_2);
        $tgkh2                  = strip_tags($request->uc_tgdt_2);
        $word                   = strip_tags($request->uc_word);
        $excel                  = strip_tags($request->uc_excel);
        $powerpoint             = strip_tags($request->uc_powerpoint);
        $listening              = strip_tags($request->uc_listen);
        $speaking               = strip_tags($request->uc_speak);
        $writing                = strip_tags($request->uc_read);
        $skill_soft             = strip_tags($request->uc_kynangmem);
        $from_date1             = strip_tags($request->uc_qtct_timestart_1);
        $to_date1               = strip_tags($request->uc_qtct_timesend_1);
        $company_1              = strip_tags($request->uc_qtct_namecity_1);
        $title_1                = strip_tags($request->uc_qtct_chucdanh_1);
        $working_1              = strip_tags($request->uc_qtct_nhiemvuchinh_1);
        $salary_1               = strip_tags($request->uc_thunhapthang_1);
        $from_date2             = strip_tags($request->uc_qtct_timestart_2);
        $to_date2               = strip_tags($request->uc_qtct_timesend_2);
        $company_2              = strip_tags($request->uc_qtct_namecity_2);
        $title_2                = strip_tags($request->uc_qtct_chucdanh_2);
        $working_2              = strip_tags($request->uc_qtct_nhiemvuchinh_2);
        $salary_2               = strip_tags($request->uc_thunhapthang_2);
        $from_date3             = strip_tags($request->uc_qtct_timestart_3);
        $to_date3               = strip_tags($request->uc_qtct_timesend_3);
        $company_3              = strip_tags($request->uc_qtct_namecity_3);
        $title_3                = strip_tags($request->uc_qtct_chucdanh_3);
        $working_3              = strip_tags($request->uc_qtct_nhiemvuchinh_3);
        $salary_3               = strip_tags($request->uc_thunhapthang_3);
        $fullname_1             = strip_tags($request->uc_tttk_name_1);
        $relation_1             = strip_tags($request->uc_tttk_quanhe_1);
        $relation_title_1       = strip_tags($request->uc_tttk_chucdanh_1);
        $relation_company_1     = strip_tags($request->uc_tttk_namect_1);
        $relation_phone_1       = strip_tags($request->uc_tttk_mobile_1);
        $fullname_2             = strip_tags($request->uc_tttk_name_2);
        $relation_2             = strip_tags($request->uc_tttk_quanhe_2);
        $relation_title_2       = strip_tags($request->uc_tttk_chucdanh_2);
        $relation_company_2     = strip_tags($request->uc_tttk_namect_2);
        $relation_phone_2       = strip_tags($request->uc_tttk_mobile_2);
        $fullname_3             = strip_tags($request->uc_tttk_name_3);
        $relation_3             = strip_tags($request->uc_tttk_quanhe_3);
        $relation_title_3       = strip_tags($request->uc_tttk_chucdanh_3);
        $relation_company_3     = strip_tags($request->uc_tttk_namect_3);
        $relation_phone_3       = strip_tags($request->uc_tttk_mobile_3);
        $new_city               = strip_tags($request->uc_tinhtp_job);
        $ditricst               = strip_tags($request->uc_quanhuyen_job);
        $arItem      = [
            'candidate_position'    =>      $candidate_position,
            'date_receive_job'      =>      $date_receive_job,
            'fullname'              =>      $fullname,              
            'birthday'              =>      $birthday,
            'address_birthday'      =>      $address_birthday,
            'marital_status'        =>      $marital_status,
            'number_children'       =>      $number_children,
            'mobile'                =>      $mobile,
            'email'                 =>      $email,
            'address'               =>      $address,
            'image'                 =>      $filename,
            'bangcap1'              =>      $bangcap1,
            'bangcap2'              =>      $bangcap2,
            'tentruong1'            =>      $tentruong1,
            'tentruong2'            =>      $tentruong2,
            'khoanganh1'            =>      $khoanganh1,
            'khoanganh2'            =>      $khoanganh2,
            'tgdt1'                 =>      $tgdt1,
            'tgdt2'                 =>      $tgdt2,
            'khoahoc1'              =>      $khoahoc1,
            'khoahoc2'              =>      $khoahoc2,
            'dvtc1'                 =>      $dvtc1,
            'dvtc2'                 =>      $dvtc2,
            'cc1'                   =>      $cc1,
            'cc2'                   =>      $cc2,
            'tgkh1'                 =>      $tgkh1,
            'tgkh2'                 =>      $tgkh2,
            'word'                  =>      $word,
            'excel'                 =>      $excel,
            'powerpoint'            =>      $powerpoint,
            'listening'             =>      $listening,
            'speaking'              =>      $speaking,
            'writing'               =>      $writing,
            'skill_soft'            =>      $skill_soft,
            'from_date1'            =>      $from_date1,
            'from_date2'            =>      $from_date2,
            'from_date3'            =>      $from_date3,
            'to_date1'              =>      $to_date1,
            'to_date2'              =>      $to_date2,
            'to_date3'              =>      $to_date3,
            'company_1'             =>      $company_1,
            'company_2'             =>      $company_2,
            'company_3'             =>      $company_3,
            'title_1'               =>      $title_1,
            'title_2'               =>      $title_2,
            'title_3'               =>      $title_3,
            'working_1'             =>      $working_1,
            'working_2'             =>      $working_2,
            'working_3'             =>      $working_3,
            'salary_1'              =>      $salary_1,
            'salary_2'              =>      $salary_2,
            'salary_3'              =>      $salary_3,
            'fullname_1'            =>      $fullname_1,
            'fullname_2'            =>      $fullname_2,
            'fullname_3'            =>      $fullname_3,
            'relation_1'            =>      $relation_1,
            'relation_2'            =>      $relation_2,
            'relation_3'            =>      $relation_3,
            'relation_title_1'      =>      $relation_title_1,
            'relation_title_2'      =>      $relation_title_2,
            'relation_title_3'      =>      $relation_title_3,
            'relation_company_1'    =>      $relation_company_1,
            'relation_company_2'    =>      $relation_company_2,
            'relation_company_3'    =>      $relation_company_3,
            'relation_phone_1'      =>      $relation_phone_1,
            'relation_phone_2'      =>      $relation_phone_2,
            'relation_phone_3'      =>      $relation_phone_3,
            'new_city'              =>      $new_city,
            'ditricst'              =>      $ditricst,
            'content'               =>      $content
        ];
        if($registered = Detail_CV::create($arItem)){
            $new_id = DB::getPdo()->lastInsertId();
            $token = str_random(30);
            // Excel
            $data = $arItem; 
            $pdf = PDF::loadView('themes.pdf.cv_pdf', compact('data'));
            $pdf->save(public_path().'/file/'.$new_id.'_'.$this->slug_url($fullname).'.pdf');
            // Word
            /*$phpWord = new \PhpOffice\PhpWord\PhpWord();
            $section = $phpWord->addSection();
            $section->addText('ahihi');
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save('file\Appdividend_'.$new_id.'_'.$this->slug_url($fullname).'.docx');*/
            $email = $data['email'];
            View::share('data', $data);
            View::share('new_id', $new_id);
            View::share('fullname', $fullname);
            try{
                Mail::send('themes.mail.mail_hr', $data, function($message)use($data, $pdf, $new_id, $fullname) {
                    $message->to('tuyendung.anzedo@gmail.com', 'TO')->from('tuyendung.anzedo@gmail.com', 'Anzedo - Tuyển Dụng')
                    ->subject('Thông báo!')
                    ->attachData($pdf->output(), $new_id.'_'.$this->slug_url($fullname).'.pdf');
                });
                Mail::send('themes.mail.mail_cv', $data, function($message)use($data, $pdf, $email, $new_id, $fullname) {
                    $message->to($email, 'TO')->from('tuyendung.anzedo@gmail.com', 'Anzedo - Tuyển Dụng')
                    ->subject('Thư cảm ơn!');
                    // ->attachData($pdf->output(), $new_id.'_'.$this->slug_url($fullname).'.pdf');
                });
            }catch(JWTException $exception){
                $this->serverstatuscode = "0";
                $this->serverstatusdes = $exception->getMessage();
            }
            if (Mail::failures()) {
                 $this->statusdesc  =   "Error sending mail";
                 $this->statuscode  =   "0";
            }else{
               $this->statusdesc  =   "Message sent Succesfully";
               $this->statuscode  =   "1";
            }
            return response()->json(['Result' => 200]);
        }else{
            return response()->json(['Result' => 400]);
        }
    }
    // Tin tức
    public function Get_Blogs(Request $request, $slug)
    {
        $GetPost = Post::where('type_post', 'BV')->orderBy('id', 'DESC')->paginate(5);
        $user = new User;
        $category = new Category;
        $pagetitle = 'Tin tuyển dụng';
        return view('themes.blog', compact('GetPost', 'user', 'category', 'pagetitle'));
    }
    // Chi tiết tin tức
    public function Detail_Blog(Request $request, $id, $slug, $id1)
    {
        $DetailPost = Post::where('id', $id)->where('slug', $slug)->first();
        $user = new User;
        $category = new Category;
        $pagetitle = $DetailPost->title;
        return view('themes.detail_blog', compact('DetailPost', 'user', 'category', 'pagetitle'));
    }
}