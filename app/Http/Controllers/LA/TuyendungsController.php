<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use View;
use App\Models\Tuyendung;
use App\Models\Nganhnghe;
use App\Models\Diachi;

class TuyendungsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'rank', 'salary', 'deadline'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Tuyendungs', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Tuyendungs', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Tuyendungs.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Tuyendungs');
		if(Module::hasAccess($module->id)) {
			return View('la.tuyendungs.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new tuyendung.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created tuyendung in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Tuyendungs", "create")) {
			$rules = Module::validateRules("Tuyendungs", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Tuyendungs", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.tuyendungs.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified tuyendung.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Tuyendungs", "view")) {
			$tuyendung = Tuyendung::find($id);
			if(isset($tuyendung->id)) {
				$module = Module::get('Tuyendungs');
				$module->row = $tuyendung;
				
				return view('la.tuyendungs.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('tuyendung', $tuyendung);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tuyendung"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified tuyendung.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Tuyendungs", "edit")) {			
			$tuyendung = Tuyendung::find($id);
			if(isset($tuyendung->id)) {	
				$module = Module::get('Tuyendungs');
				$module->row = $tuyendung;
				return view('la.tuyendungs.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
					'getListDiaChi' => Diachi::getListDiaChi($tuyendung->address),
					'getListNganhNghe' => Nganhnghe::getListNganhNghe($tuyendung->career),
				])->with('tuyendung', $tuyendung);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tuyendung"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified tuyendung in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Tuyendungs", "edit")) {
			
			$rules = Module::validateRules("Tuyendungs", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Tuyendungs", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.tuyendungs.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified tuyendung from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Tuyendungs", "delete")) {
			Tuyendung::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.tuyendungs.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('tuyendungs')->select($this->listing_cols)->whereNull('deleted_at')->orderBy('id', 'DESC');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Tuyendungs');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/tuyendungs/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Tuyendungs", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/tuyendungs/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Tuyendungs", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.tuyendungs.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
