<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Menugroup;

class MenugroupsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'key', 'status'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Menugroups', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Menugroups', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Menugroups.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Menugroups');
		
		if(Module::hasAccess($module->id)) {
			return View('la.menugroups.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new menugroup.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created menugroup in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Menugroups", "create")) {
		
			$rules = Module::validateRules("Menugroups", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Menugroups", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.menugroups.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified menugroup.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Menugroups", "view")) {
			
			$menugroup = Menugroup::find($id);
			if(isset($menugroup->id)) {
				$module = Module::get('Menugroups');
				$module->row = $menugroup;
				
				return view('la.menugroups.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('menugroup', $menugroup);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("menugroup"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified menugroup.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Menugroups", "edit")) {			
			$menugroup = Menugroup::find($id);
			if(isset($menugroup->id)) {	
				$module = Module::get('Menugroups');
				
				$module->row = $menugroup;
				
				return view('la.menugroups.edit', [
                    'show_actions' => $this->show_action,
                    'listing_cols' => $this->listing_cols,
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('menugroup', $menugroup);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("menugroup"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified menugroup in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Menugroups", "edit")) {
			
			$rules = Module::validateRules("Menugroups", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Menugroups", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.menugroups.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified menugroup from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Menugroups", "delete")) {
			Menugroup::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.menugroups.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('menugroups')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Menugroups');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
                if($col == 'id')
                {
                    $menugroup = Menugroup::find($data->data[$i][$j]);
                }
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/menugroups/'.$data->data[$i][0]).'" title="'.$menugroup->description.'">'.$data->data[$i][$j].'</a>';
				}
                if($col == "status") {
                    if( $data->data[$i][$j] == 1)
                    {
                        $data->data[$i][$j] = '<span class="label label-success">On</span>';
                    }else{
                        $data->data[$i][$j] = '<span class="label label-danger">Off</span>';
                    }

                }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Menugroups", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/menugroups/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Menugroups", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.menugroups.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
