<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Tracnghiem_Option;

class Tracnghiem_OptionsController extends Controller
{
	public $show_action = true;
	public $view_col = 'id_tracnghiem';
	public $listing_cols = ['id', 'id_tracnghiem', 'answer'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Tracnghiem_Options', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Tracnghiem_Options', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Tracnghiem_Options.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Tracnghiem_Options');
		
		if(Module::hasAccess($module->id)) {
			return View('la.tracnghiem_options.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new tracnghiem_option.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created tracnghiem_option in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Tracnghiem_Options", "create")) {
		
			$rules = Module::validateRules("Tracnghiem_Options", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Tracnghiem_Options", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.tracnghiem_options.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified tracnghiem_option.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Tracnghiem_Options", "view")) {
			
			$tracnghiem_option = Tracnghiem_Option::find($id);
			if(isset($tracnghiem_option->id)) {
				$module = Module::get('Tracnghiem_Options');
				$module->row = $tracnghiem_option;
				
				return view('la.tracnghiem_options.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('tracnghiem_option', $tracnghiem_option);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tracnghiem_option"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified tracnghiem_option.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Tracnghiem_Options", "edit")) {			
			$tracnghiem_option = Tracnghiem_Option::find($id);
			if(isset($tracnghiem_option->id)) {	
				$module = Module::get('Tracnghiem_Options');
				
				$module->row = $tracnghiem_option;
				
				return view('la.tracnghiem_options.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('tracnghiem_option', $tracnghiem_option);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tracnghiem_option"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified tracnghiem_option in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Tracnghiem_Options", "edit")) {
			
			$rules = Module::validateRules("Tracnghiem_Options", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Tracnghiem_Options", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.tracnghiem_options.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified tracnghiem_option from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Tracnghiem_Options", "delete")) {
			Tracnghiem_Option::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.tracnghiem_options.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('tracnghiem_options')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Tracnghiem_Options');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/tracnghiem_options/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Tracnghiem_Options", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/tracnghiem_options/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Tracnghiem_Options", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.tracnghiem_options.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
