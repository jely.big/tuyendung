<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Tracnghiem;
use App\Models\Tracnghiem_Option;
use App\Models\Tracnghiem_Type;

class TracnghiemsController extends Controller
{
	public $show_action = true;
	public $view_col = 'question';
	public $listing_cols = ['id', 'question', 'type_tn'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Tracnghiems', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Tracnghiems', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Tracnghiems.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Tracnghiems');
		if(Module::hasAccess($module->id)) {
			return View('la.tracnghiems.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new tracnghiem.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created tracnghiem in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		date_default_timezone_set("Asia/Bangkok");
        $date = date('Y-m-d H:i:s');
		if(Module::hasAccess("Tracnghiems", "create")) {
			$rules = Module::validateRules("Tracnghiems", $request);
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$insert_id = Module::insert("Tracnghiems", $request);
            $options = $request->input('option');
            $answer  = $options['answer'];
			foreach($answer as $key => $value)
            {
                if($value != '' )
                {
                    Tracnghiem_Option::insert([
                        'id_tracnghiem'	=>	$insert_id,
                        'answer'		=>	$answer[$key],
                        'created_at'	=>	$date
                    ]);
                }
            }
			return redirect()->route(config('laraadmin.adminRoute') . '.tracnghiems.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified tracnghiem.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Tracnghiems", "view")) {
			
			$tracnghiem = Tracnghiem::find($id);
			if(isset($tracnghiem->id)) {
				$module = Module::get('Tracnghiems');
				$module->row = $tracnghiem;
				return view('la.tracnghiems.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('tracnghiem', $tracnghiem);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tracnghiem"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified tracnghiem.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{		
		if(Module::hasAccess("Tracnghiems", "edit")) {			
			$tracnghiem = Tracnghiem::find($id);
			if(isset($tracnghiem->id)) {	
				$module = Module::get('Tracnghiems');
				$module->row = $tracnghiem;
				$options     = Tracnghiem_Option::where('id_tracnghiem', $id)->get();
				return view('la.tracnghiems.edit', [
					'module' 	=> $module,
					'options'  	=> $options,
					'view_col' 	=> $this->view_col,
				])->with('tracnghiem', $tracnghiem);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tracnghiem"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified tracnghiem in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		date_default_timezone_set("Asia/Bangkok");
        $date = date('Y-m-d H:i:s');
		if(Module::hasAccess("Tracnghiems", "edit")) {
			$rules = Module::validateRules("Tracnghiems", $request, true);
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			$insert_id = Module::updateRow("Tracnghiems", $request, $id);
			//Del all optoin, images
			Tracnghiem_Option::where('id_tracnghiem', $id)->delete();
            $options = $request->input('option');
            $answer  = $options['answer'];
			foreach($answer as $key => $value)
            {
                if($value != '' )
                {
                    Tracnghiem_Option::insert([
                        'id_tracnghiem'	=>	$insert_id,
                        'answer'		=>	$answer[$key],
                        'updated_at'	=>	$date
                    ]);
                }
            }
			return redirect()->route(config('laraadmin.adminRoute') . '.tracnghiems.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified tracnghiem from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Tracnghiems", "delete")) {
			Tracnghiem::find($id)->delete();
			Tracnghiem_Option::where('id_tracnghiem', $id)->delete();
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.tracnghiems.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('tracnghiems')->select($this->listing_cols)->whereNull('deleted_at')->orderBy('id', 'DESC');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Tracnghiems');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/tracnghiems/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Tracnghiems", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/tracnghiems/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Tracnghiems", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.tracnghiems.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
