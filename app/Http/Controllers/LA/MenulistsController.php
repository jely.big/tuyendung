<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Menugroup;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Menulist;

class MenulistsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'url', 'status'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Menulists', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Menulists', $this->listing_cols);
		}
	}

    public function getListLangOption($request)
    {
        $langs = Language::orderBy('name', 'ASC')->get();
        $html  = '';
        $default = ( $request->input('lang') ) ? $request->input('lang') : Language::getLangDefault();
        foreach ($langs as $lang)
        {
            $seleccted = ( $lang->id == $default ) ? 'selected="selected"': '';
            $html .= '<option value="'.$lang->id.'" '.$seleccted.' >'.$lang->name.'</option>';
        }
        return $html;
    }

	/**
	 * Display a listing of the Menulists.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$module = Module::get('Menulists');
        $group = ($request->input('group'))  ? $request->input('group') : Menugroup::first()->id;
		if( $request->input('group') == '')
        {
            return redirect( config('laraadmin.adminRoute')."/".'menulists?group='.$group );
        }
		if(Module::hasAccess($module->id)) {

			return View('la.menulists.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
                'lsOption'  => $this->getListOption($group),
                'lsGroup'   => $this->getListGroup($group),
                'links'     => $this->getLinks($group)
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	public function getLinks($group, $parent = 0, $slas = '')
    {
        $html = '';
        $links = Menulist::where('parent', $parent)->where('menugroup', $group)->orderBy('sort', 'ASC')->get();
        if($links)
        {
            foreach($links as $link)
            {
                $html .= '<tr>';
                $html .= '<td>'.$link->id.'</td>';
                $html .= '<td>'.$slas.' '.$link->name.'</td>';
                $html .= '<td>'.$link->url.'</td>';

                if( $link->status == 1)
                {
                    $html .= '<td><span class="label label-success">On</span></td>';
                }else{
                    $html .= '<td><span class="label label-danger">Off</span></td>';
                }

                $html .= '<td>';
                if(Module::hasAccess("Menulists", "edit")) {
                    $html .= '<a href="' . url(config('laraadmin.adminRoute') . '/menulists/' . $link->id . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
                }
                if(Module::hasAccess("Menulists", "delete")) {
                    $html .= Form::open(['route' => [config('laraadmin.adminRoute') . '.menulists.destroy', $link->id], 'method' => 'delete', 'style'=>'display:inline']);
                    $html .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
                    $html .= Form::close();
                }
                $html .= '</td>';
                $html .= '</tr>';
                $html .= $this->getLinks($group, $link->id, $slas.'---');
            }
        }

        return $html;
    }

	public function getListGroup($group = 0)
    {

        $html = '';
        $lists = Menugroup::where('status',1)->get();
        if($lists)
        {
            foreach($lists as $list)
            {
                $selected = ( $group == $list->id ) ? 'selected="selected"': '';
                $html .= "<option value='".$list->id."' ".$selected." >".$list->name."</option>";
            }
        }

        return $html;

    }

    public function getListOption($group = 0, $parent = 0, $slas = '')
    {
        $html = '';
        if( $slas == '' )
        {
            //$group = ($group == '' || $group == 0 ) ? 1 : 0;
        }

        $lists = Menulist::where('parent', $parent)->where('menugroup', $group)->orderBy('sort', 'ASC')->get();
        if($lists)
        {
            foreach($lists as $list)
            {
                $html .= "<option value='".$list->id."'>".$slas.' '.$list->name."</option>";
                $html .= $this->getListOption($group, $list->id, $slas.'---');
            }
        }

        return $html;
    }

    public function listParentDefault($default=0, $group = 0, $parent = 0, $slas = '')
    {
        $html = '';
        if( $slas == '' )
        {
            //$group = ($group == '' || $group == 0 ) ? 1 : 0;
        }

        $lists = Menulist::where('parent', $parent)->where('menugroup', $group)->orderBy('sort', 'ASC')->get();
        if($lists)
        {
            foreach($lists as $list)
            {
                $selected = ( $default == $list->id) ? 'selected="selected"':'';
                $html .= "<option value='".$list->id."' ".$selected.">".$slas.' '.$list->name."</option>";
                if( $default == $list->id )
                {
                    continue;
                }
                $html .= $this->listParentDefault($default, $group, $list->id, $slas.'---');
            }
        }

        return $html;
    }

	/**
	 * Show the form for creating a new menulist.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created menulist in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Menulists", "create")) {
		
			$rules = Module::validateRules("Menulists", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Menulists", $request);

			return redirect( config('laraadmin.adminRoute').'/menulists?group='.$request->input('menugroup') );

			//return redirect()->route(config('laraadmin.adminRoute') . '.menulists.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified menulist.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Menulists", "view")) {
			
			$menulist = Menulist::find($id);
			if(isset($menulist->id)) {
				$module = Module::get('Menulists');
				$module->row = $menulist;
				
				return view('la.menulists.show', [

					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('menulist', $menulist);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("menulist"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified menulist.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Menulists", "edit")) {			
			$menulist = Menulist::find($id);
			if(isset($menulist->id)) {	
				$module = Module::get('Menulists');
				$module->row = $menulist;
				$group = $menulist->menugroup;
				return view('la.menulists.edit', [
                    'show_actions' => $this->show_action,
                    'listing_cols' => $this->listing_cols,
					'module' => $module,
					'view_col' => $this->view_col,
                    'lsParent'  => $this->listParentDefault($menulist->parent, $group),
                    'lsGroup'   => $this->getListGroup($group),
                    'links'     => $this->getLinks($group)
				])->with('menulist', $menulist);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("menulist"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified menulist in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Menulists", "edit")) {
			
			$rules = Module::validateRules("Menulists", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Menulists", $request, $id);
			$group = Menulist::find($id);
			return redirect( config('laraadmin.adminRoute').'/menulists?group='.$group->menugroup );
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified menulist from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Menulists", "delete")) {
			$m = Menulist::find($id);
			$m->delete();
			
			// Redirecting to index() method
            return redirect( config('laraadmin.adminRoute') . '/menulists?group='.$m->menugroup );
			//return redirect()->route(config('laraadmin.adminRoute') . '.menulists.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('menulists')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Menulists');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/menulists/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Menulists", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/menulists/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Menulists", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.menulists.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
