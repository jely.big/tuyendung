<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use App\Models\Detail_CV;
use PDF;

class Detail_CVsController extends Controller
{
	public $show_action = true;
	public $view_col = 'fullname';
	public $listing_cols = ['id', 'image', 'fullname', 'candidate_position', 'birthday', 'mobile', 'email'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Detail_CVs', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Detail_CVs', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Detail_CVs.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Detail_CVs');
		if(Module::hasAccess($module->id)) {
			return View('la.detail_cvs.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new detail_cv.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created detail_cv in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Detail_CVs", "create")) {
		
			$rules = Module::validateRules("Detail_CVs", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Detail_CVs", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.detail_cvs.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified detail_cv.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Detail_CVs", "view")) {
			
			$detail_cv = Detail_CV::find($id);
			if(isset($detail_cv->id)) {
				$module = Module::get('Detail_CVs');
				$module->row = $detail_cv;
				
				return view('la.detail_cvs.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('detail_cv', $detail_cv);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("detail_cv"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified detail_cv.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Detail_CVs", "edit")) {			
			$detail_cv = Detail_CV::find($id);
			if(isset($detail_cv->id)) {	
				$module = Module::get('Detail_CVs');
				
				$module->row = $detail_cv;
				
				return view('la.detail_cvs.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('detail_cv', $detail_cv);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("detail_cv"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified detail_cv in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Detail_CVs", "edit")) {
			
			$rules = Module::validateRules("Detail_CVs", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Detail_CVs", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.detail_cvs.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified detail_cv from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Detail_CVs", "delete")) {
			Detail_CV::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.detail_cvs.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	private function slug_url( string $str )
	{
		//Kiểm tra xem dữ liệu truyền vào có phải là một chuỗi hay không
		if( is_string( $str ) ){
			// Chuyển đổi toàn bộ chuỗi sang chữ thường
			$str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8"); 
			//Tạo mảng chứa key và chuỗi regex cần so sánh
			$unicode = [
				'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
				'd' => 'đ',
				'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
				'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
				'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
				'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
				'i' => 'í|ì|ỉ|ĩ|ị',
				'-'	=> '\+|\*|\/|\&|\!| |\^|\%|\$|\#|\@',
			];
			foreach ( $unicode as $key => $value )
			{
				//So sánh và thay thế bằng hàm preg_replace
				
				$str = preg_replace("/($value)/", $key, $str);
			}
			//Trả về kết quả
			return $str;
		} 
		//Nếu Dữ liệu không phải kiểu string thì trả về null
		return null;
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('detail_cvs')->select($this->listing_cols)->whereNull('deleted_at')->orderBy('id', 'DESC');
		$out = Datatables::of($values)->make();
		$data = $out->getData();
		$fields_popup = ModuleFields::getModuleFields('Detail_CVs');
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				$id = $data->data[$i][$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$name = $this->slug_url($data->data[$i][2]);
					$data->data[$i][$j] = '<div><a href="'.url(config('laraadmin.adminRoute') . '/detail_cvs/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a></div>';
					$data->data[$i][$j] .= '<img src="'.asset('public/file/'.$data->data[$i][1]).'" width="150px" style="border-radius:8px" class="down_cv" id="down_cv" data-id="'.$data->data[$i][0].'" data-name="'.$name.'" />';
				}
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Detail_CVs", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/detail_cvs/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Detail_CVs", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.detail_cvs.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

	// Download CV
	public function download_CV(Request $request)
	{
// 		$cv = public_path().'/file/'.$request->id.'_'.$request->name.'.pdf';
		$cv = asset('public/file/'.$request->id.'_'.$request->name.'.pdf');
// 		if( file_exists($cv) ){
        if($cv){
// 			$cv = $request->id.'_'.$request->name.'.pdf';
// 			PDF::loadFile(public_path().'/file/'.$cv)->download($cv);
			return response()->json(['Result' => 200, 'Cv' => $cv]);
		}else{
			return response()->json(['Result' => 400]);
		}
	}
	
}
