<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\User;

class UsersController extends Controller
{
	public $show_action = false;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'email'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Users', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Users', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Users.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Users');
		
		if(Module::hasAccess($module->id)) {
			return View('la.users.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Display the specified user.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Users", "view")) {
			$user = User::findOrFail($id);
			if(isset($user->id)) {
				if($user['type'] == "Employee") {
					return redirect(config('laraadmin.adminRoute') . '/employees/'.$user->id);
				} else if($user['type'] == "Client") {
					return redirect(config('laraadmin.adminRoute') . '/clients/'.$user->id);
				}
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("user"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	public function destroy(Request $request, $id)
	{;
		if(Module::hasAccess("Users", "delete")) {
			// Lấy id user cần xóa
			$user = User::find($id);
			if($user && $user->id != 1)
			{
				$deleted_at = date('Y-m-d');
				// Xóa user
				User::find($id)->delete();

				// Redirecting to index() method
				return redirect()->route(config('laraadmin.adminRoute') . '.users.index');
			}else{
				return redirect()->back();
			}
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax(Request $request)
	{
		
		$search = $request->input('search');
		$search = $search['value'];
		$list = ['id','name', 'context_id', 'email', 'password', 'type', 'remember_token'];
		if( $search !== '')
		{
			$values = DB::table('users')->select($list)->whereNull('deleted_at');
		}else{
			$values = DB::table('users')->select($list)->whereNull('deleted_at');
		}
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Users');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}

				if($col == 'id') {
					$id = $data->data[$i][$j];
					$userName = User::find($id);
				}

				if($col == 'email') {
                    $html  = '<label class="label label-default">Username:</label><span style="margin-left: 34px;">'.$userName->name.'</span><br/>';
                    $html .= '<label class="label label-default">Email:</label><span style="margin-left: 59px;">'.$userName->email.'</span><br/>';
					if($userName->type == 'Client'){
						$html .= '<label class="label label-default">Chức danh:</label><span style="margin-left: 30px;"> <button class="btn btn-xs btn-warning">Khách hàng</button></span><br/>';
					}else if($userName->type == 'Employee'){
						$html .= '<label class="label label-default">Chức danh:</label><span style="margin-left: 30px;"> <button class="btn btn-xs btn-primary">Quản lý</button></span><br/>';
					}else{
						$html .= '<label class="label label-default">Chức danh:</label><span style="margin-left: 30px;"> <button class="btn btn-xs btn-success">'.$userName->type.'</button></span><br/>';
					}
					$html .= '<a  class="bnt-del btn btn-xs btn-danger" href="'.url(config('laraadmin.adminRoute') . '/users/'.$userName->id).'/delete" onclick="return confirm(\'bạn có chắc chắn muốn chọn chức năng này?\')">Xóa</a>';

                    $data->data[$i][$j] = $html;
				}

                if($col == 'type') {
					if($userName->id != 1){
						$html  = '<label class="label label-default">Ngày tạo :</label><span style="margin-left: 34px;">'.date('H:i:s d-m-Y', strtotime($userName->created_at)).'</span><br/>';
						if($userName->status == 'DA_DUYET'){
							$html .= '<label class="label label-default">Trạng thái:</label><span style="margin-left: 30px;"> <button class="btn btn-xs btn-success">Đang hoạt động</button></span><br/>';
						}else if($userName->status == 'CHO_DUYET'){
							$html .= '<label class="label label-default">Trạng thái:</label><span style="margin-left: 30px;"> <button class="btn btn-xs btn-warning">Chờ duyệt</button></span><br/>';
						}else{
							$html .= '<label class="label label-default">Trạng thái:</label><span style="margin-left: 30px;"> <button class="btn btn-xs btn-success">'.$userName->status.'</button></span><br/>';
						}
						
					}

                    $data->data[$i][$j] = $html;
                }
			}
		}
		$out->setData($data);
		return $out;
	}
}
