<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Setting;
use App\Helpers\InputRender;

class SettingsController extends Controller
{
	public $show_action = true;
	public $view_col = 'key';
	public $listing_cols = ['id', 'key', 'value'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Settings', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Settings', $this->listing_cols);
		}
	}


	public function listOptions()
    {
        $options = array();
        $options['tabs'] = [
            [
                'id' => 'picture',
                'title' => 'Picture',
                'icon' => 'fa-image'
            ],[
                'id' => 'company',
                'title' => 'Company',
                'icon' => 'fa-building'
            ],[
                'id' => 'social',
                'title' => 'Social Network',
                'icon' => 'fa-facebook'
            ],[
                'id' => 'copyright',
                'title' => 'Copyright',
                'icon' => 'fa-copyright'
            ],[
                'id' => 'status',
                'title' => 'Web Status',
                'icon' => 'fa-cog'
            ],
        ];
        $options['content']['company'] = [
            [
                'id'=>'company_name',
                'type'=>'text',
                'title'=>'Company Name',
                'value'=> ''

            ],[
                'id'=>'company_address',
                'type'=>'text',
                'title'=>'Company Address',
                'value'=> ''
            ],[
                'id'=>'company_address1',
                'type'=>'text',
                'title'=>'Company Address 1',
                'value'=> ''
            ],[
                'id'=>'company_address2',
                'type'=>'text',
                'title'=>'Company Address 2',
                'value'=> ''
            ],[
                'id'=>'company_about',
                'type'=>'textarea',
                'title'=>'Giới thiệu',
                'value'=> ''
            ],[
                'id'=>'company_about_short',
                'type'=>'textarea',
                'title'=>'Giới thiệu ngắn',
                'value'=> ''
            ],[
                'id'=>'company_content',
                'type'=>'textarea',
                'title'=>'Lời nhắn',
                'value'=> ''
            ],[
                'id'=>'google_map',
                'type'=>'textarea',
                'title'=>'Bản đồ',
                'value'=> ''
            ],[
                'id'=>'company_email',
                'type'=>'text',
                'title'=>'Company Email',
                'value'=> ''
            ],[
                'id'=>'company_phone',
                'type'=>'text',
                'title'=>'Company Phone',
                'value'=> ''
            ],[
                'id'=>'company_hotline',
                'type'=>'text',
                'title'=>'Company hotline',
                'value'=> ''
            ],[
                'id'=>'company_fax',
                'type'=>'text',
                'title'=>'Company fax',
                'value'=> ''
            ]
        ];

        $options['content']['picture'] = [
            [
                'id'=>'logo',
                'type'=>'file',
                'title'=>'Company Logo',
                'value'=> ''
            ],[
                'id'=>'logo_50',
                'type'=>'file',
                'title'=>'Company Logo with 50px',
                'value'=> ''
            ],[
                'id'=>'favicon',
                'type'=>'file',
                'title'=>'Company Favicon',
                'value'=> ''
            ],[
                'id'=>'ads_1',
                'type'=>'file',
                'title'=>'Ads 300x250',
                'value'=> ''
            ],[
                'id'=>'ads_2',
                'type'=>'file',
                'title'=>'Ads 728x90',
                'value'=> ''
            ],[
                'id'=>'ads_3',
                'type'=>'file',
                'title'=>'Ads 300x450',
                'value'=> ''
            ]
        ];

        $options['content']['social']=[
            [
                'id' => 'facebook',
                'title' => 'Facebook',
                'type' => 'text',
                'value'=> ''
            ],[
                'id' => 'fanpage_facebook',
                'title' => 'Fanpage Facebook',
                'type' => 'textarea',
                'value'=> ''
            ],[
                'id' => 'instagram',
                'title' => 'Instagram',
                'type' => 'text',
                'value'=> ''
            ],[
                'id' => 'youtube',
                'title' => 'Youtube',
                'type' => 'text',
                'value'=> ''
            ],[
                'id' => 'googleplus',
                'title' => 'Google +',
                'type' => 'text',
                'value'=> ''
            ],[
                'id' => 'linkedin',
                'title' => 'Linkedin network',
                'type' => 'text',
                'value'=> 'linkedin'
            ],[
                'id' => 'twitter',
                'title' => 'Twitter network',
                'type' => 'text',
                'value'=> 'twitter'
            ],[
                'id' => 'pinterest',
                'title' => 'Pinterest network',
                'type' => 'text',
                'value'=> 'pinterest'
            ]
        ];

        $options['content']['copyright'] = [
            [
                'id'=>'copyright',
                'type'=>'text',
                'title'=>'Copyright text',
                'value'=> ''

            ]
        ];

        $options['content']['status']=[
            [
                'id' => 'web_status',
                'title' => 'Website Status',
                'type' => 'select',
                'value' => ['off'=>'OFFLINE', 'on' => 'ONLINE']
            ]
        ];

        return $options;
    }
	/**
	 * Display a listing of the Settings.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Settings');
		
		if(Module::hasAccess($module->id)) {
			return View('la.settings.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
                'module' => $module,
                'options'=>$this->listOptions(),
                'inputRender' => new InputRender,
                'setting' => new Setting
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Update the specified setting in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		if(Module::hasAccess("Settings", "edit")) {
            //$lsField = $this->listOptions();
            DB::table('settings')->delete();
			foreach ($request->all() as $key => $value )
            {
                //$input = $field['id'];
                $first = DB::table('settings')->where('key', $key)->first();
                if (!$first) {
                    DB::table('settings')->insert(['key' => $key, 'value' => $value ] );
                } else {
                    DB::table('settings')->where('key', $key)->update(['value' => $value ]);
                }
            }
			//print_r($request->all() );
			return redirect(config('laraadmin.adminRoute') . '/settings');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}


}
